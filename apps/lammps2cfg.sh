#!/bin/bash

# A shortcut for formatTransformer.jar

exe=~/bin/formatTransformer.jar

if [ $# -ne 3 ]; then
  echo "Need three arguments: elements, input, output: $@"
exit 1
fi

java -jar $exe -i LAMMPS -o CFG -e $1 $2 $3
