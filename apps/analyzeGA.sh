#!/bin/bash

# This is a script that analyzes GA active learning run. It collects information
# from .dat files and all_candidates: maxvol grade of as-generated, MTP-relaxed,
# VASP re-relaxed, MTP relaxed energy, VASP energy of first and last ionic step.

# It also determines whether a VASP calculation is performed since it's in pool
# or it's extrapolating. Note: unrealistic structure can also be added for
# retraining.

# Usage:
#   cd default/Al/34
#   analyze.sh Au,S,C,H <BREAKTHRESHOLD> <SELECTTHRESHOLD>
# It create an analyze folder and put all results in it.
#
# TODO: expand it on pure VASP and MTP calculations

# Sanity checks
if [ $# -lt 3 ]; then
  echo "Need element list, BREAKTHRESHOLD and SELECTTHRESHOLD. Present: $@"
  exit 1
fi

if [ ! -e ./all_candidates ]; then
  echo "all_candidates folder doesn't exist. Nothing to collect."
  exit 1
elif [ ! -e ./candidates.dat ] || [ ! -e pool.dat ]; then
  echo ".dat files aren't complete. Recover them first."
  exit 1
elif [ ! -e ./train ]; then
  echo "./train folder doesn't exist. This is not a GA_active_learning run."
  exit 1
fi

# Initialize
root=$( pwd )
elements=$1
bt=$2 # breakthreshold
st=$3 # selectthreshold
RETRAINCOUNT=$( find ./train -maxdepth 1 -type d -regex "./train/[0-9]+" | wc -l )

if [ ! -e ./analyze ]; then
  mkdir analyze;
fi
output=$( pwd )/analyze/results.txt
echo "" > $output

# output format:
#   index maxvol_for_as-gerenated maxvol_relaxed MTP_relaxed_energy
#   unrealistic extrapolating in_pool whether_vasp_calculated training_count
#   maxvol_last_step vasp_energy_first_step vasp_energy_last_step vasp_elapsed

cd all_candidates
lastIndex=$( ls -dv [0-9]* 2> /dev/null | tail -n 1 )
if [ -z $lastIndex ]; then
  echo "No clusters in all_candidates."
  exit 0
else
  echo "Cluster count: $lastIndex"
fi

index=1
while [ $index -le $lastIndex ]; do
  cd $index
  
  # Initialization
  maxvol_initial=N/A 
  maxvol_relaxed=N/A  
  mtp_energy=N/A 
  unrealistic=false 
  extrapolating=false 
  in_pool=false
  vasp_calced=false
  retrain_count=N/A 
  maxvol_vasp_last=N/A
  vasp_energy_first=N/A
  vasp_energy_last=N/A
  vasp_elapsed=N/A

  vasp_dir="./"
  lammps_dir="./"

  if [ -e OUTCAR ]; then
    vasp_calced="true"
    lammps_dir=$( ls -dv [0-9]* 2> /dev/null | tail -n 1 )
    retrain_count=$( expr $lammps_dir + 1 )
  elif grep "Elapsed" ${root}/train/*/${index}/OUTCAR &> /dev/null; then
    # GA was stopped at retraining. This structure has been relaxed, but not copied back.
    vasp_calced="true"
    retrain_count=$( ls -ld ${root}/train/*/${index} | awk -F "/" '{ print $(NF-1) }' )
    vasp_dir=${root}/train/${retrain_count}/${index}
  else
    vasp_calced="false"
    lammps_dir="./"
  fi
    

  if [ -e ${lammps_dir}/graded_struct.cfg ]; then 
    maxvol_initial=$( grep MV_grade ${lammps_dir}/graded_struct.cfg | awk '{ print $3 }' )
    # deals with scientific notation.
    cond=$( perl -e "{ if ($maxvol_initial > $bt) { print 1 } else { print 0 } }" )
    if [ $cond -eq 1 ]; then
      extrapolating="true"
    fi
  fi
  
  # Not necessarily retrained even if initial strucutre is extrapolating.
  if [ -e ${lammps_dir}/end.lmp ]; then 
    mtp_energy=$( grep -A 1 next-to-last ${lammps_dir}/log.lammps | grep -E [0-9]+ | awk '{ print $3 }' )
    # realistic is "true".
    realistic=$( java -jar ~/bin/StructureAnalyzer.jar checkRealistic -e $elements -i LAMMPS ${lammps_dir}/end.lmp )
    if [ $realistic == "true" ]; then
      unrealistic="false"
    else 
      unrealistic="true"
    fi
    if [ -e ${lammps_dir}/graded_end.cfg ]; then
      maxvol_relaxed=$( grep MV_grade ${lammps_dir}/graded_end.cfg | awk '{ print $3 }' )
      cond=$( perl -e "{ if ($maxvol_relaxed > $st) { print 1 } else { print 0 } }" )
      if [ $cond -eq 1 ]; then
        extrapolating="true"
      else 
        extrapolating="false"
      fi
    fi
  fi
  
  # VASP
  if [ -e ${vasp_dir}/OUTCAR ] && grep "Elapsed" ${vasp_dir}/OUTCAR &> /dev/null; then
    if [ $extrapolating == "false" ] && [ $unrealistic == "false" ]; then
      in_pool="true"
    else
      in_pool="false"
    fi
    maxvol_vasp_last=$( grep MV_grade ${vasp_dir}/outcar.cfg | tail -n 1 | awk '{ print $3 }' )
    vasp_energy_first=$( grep E0 ${vasp_dir}/OSZICAR | head -n 1 | awk '{ print $5 }' )
    vasp_energy_last=$( grep E0 ${vasp_dir}/OSZICAR | tail -n 1 | awk '{ print $5 }' )
    vasp_elapsed=$( grep Elapsed ${vasp_dir}/OUTCAR | awk '{ print $4 }' )
  else 
    in_pool="false"
  fi
  # output
  echo "$index $maxvol_initial $maxvol_relaxed $mtp_energy $unrealistic " \
       "$extrapolating $in_pool $vasp_calced $retrain_count $maxvol_vasp_last " \
       "$vasp_energy_first $vasp_energy_last $vasp_elapsed" >> $output
  cd ..
  let index++
done
cd .. # get out of all_candidates
cat $output | column -t > temp.txt
cp temp.txt $output
rm temp.txt
