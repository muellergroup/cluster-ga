#!/bin/bash

# Extract CFG file from VASP calculations in each subdirectory.
# Usage: ./convert_cfg.sh <element> <mag> ./folder/of/calculations

############################### Helper function ###############################
# Get number of maximum electronic scf steps from INCAR.
# Usage: 
#   getNELM ./vasp_calc_dir
# Output: NELM value to stdout
getNELM() {
  dir=$1
  if [ -e ${dir}/INCAR ] && grep -i NELM ${dir}/INCAR &> /dev/null; then
    grep -i "NELM" ${dir}/INCAR | sed -r -n "s/.*NELM[[:space:]]*=[[:space:]]*([0-9]+).*/\1/p"
  else
    echo "60" # default
  fi
}

# Usage: within the folder of VASP calculation, call:
#   convert_one_calculation( )
convert_one_calculation() {
  # Full ionic steps. Only remove unconverged ionic steps and steps with wromg mag.  
  mpirun -np 1 $MLP convert-cfg --input-format=vasp-outcar ./OUTCAR ./full_relax.cfg > /dev/null 
  nelm=$( getNELM ./ )
  java -jar $CFGRegulator -m $mag -n $nelm -c RELAX ./OSZICAR ./full_relax.cfg ./full_relax.cfg

  # Ground state only
  mpirun -np 1 $MLP convert-cfg --input-format=vasp-outcar --last ./OUTCAR ./ground_state.cfg > /dev/null

  # Ionic steps excluding similar ones
  java -jar $CFGRegulator removeSimilar -e $elements $SimilarityThreshold ./full_relax.cfg ./compact.cfg
  
  # To add extra weight on ground state image, uncomment the below for-loop.
  #  for n in 3 5; do
  #    if [ ! -e compact_multiple_${n}.cfg ]; then
  #      java -jar ~/bin/CFGRegulator.jar groundMultiple ${n} ./compact.cfg ./compact_multiple_${n}.cfg
  #    fi
  #  done
    
}

# Count the number of folders with only numbers as name under path given in
# arguemnt.
# Usage: count_numeric_folder <path>
count_numeric_folder() {
  cd $1
  count=$( find ./ -maxdepth 1 -type d -regex "./[0-9]+" | wc -l )
  if [ -z "$count" ]; then
    echo 0; # empty folder
  else 
    echo $count
  fi
  cd $OLDPWD
}

################################## main() #####################################
# CONSTANT
CFGRegulator=~/bin/CFGRegulator.jar
MLP=~/bin/mlp
SimilarityThreshold=0.15

# Sanity check:
if [ ! -e $CFGRegulator ]; then
  echo "Executable $CFGRegulator doesn't exist! Check its existence and modify"\
    "variable \"CFGRegulator\" in script. Exit."
  exit 1;
fi
if [ ! -e $MLP ]; then
  echo "$MLP dones't exist! Check its path! Exit."
  exit 1;
fi

# Command-line Argument:
if [ $# -lt 3 ]; then
  echo "Need three parameters: <elements> <mag> <path_to_calculations>. Exit."
  exit 1
fi
elements=$1
mag=$2
target=$3

cd $target;
if [ -e OUTCAR ] && grep "Elapsed" ./OUTCAR &> /dev/null; then
  convert_one_calculation
else 
  count=$( count_numeric_folder ./ )
  i=1;
  while [ $i -le $count ]; do
    echo "Current structure: $i";
    cd $i; 
    convert_one_calculation
    let i++
    cd ..;
  done
fi
