#!/bin/bash

# A shortcut for formatTransformer.jar

exe=~/bin/formatTransformer.jar

if [ $# -ne 2 ]; then
  echo "Need two arguments: input, output: $@"
  exit 1
fi

java -jar $exe -i POSCAR -o CFG $1 $2 
