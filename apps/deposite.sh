#!/bin/bash

# This script is used to deposite VASP calculations from different runs into
# a common folder. Duplications within the same source and between source and
# repo are checked to make sure no duplicate/similar structures are added.
# Subsequently, sort_repo.sh can be used to sort them in order of energy.
#
# Note: Only applied to VASP calculations.
#
# Usage:
#   ./deposite.sh <calc_folder> ./destination
# or
#   ./deposite.sh ./folder_of_multiple_calculations ./destination
#
# Demo:
# 1. Deposite trained calculations of GA_AL to a repo:
#      cd Al/34/train
#      for i in {1..13}; do
#        cd $i;
#        ~/bin/ga/deposite.sh ./ ./repo
#      done
# 2. Deposite vasp calculations of GA_DFT to a repo:
#      cd Al/34/all_candidates
#      ~/bin/ga/deposite.sh ./ ./repo
#

# Helper function

# Copy VASP files from source to target
# Usage: copy_files <target_dir> <dest_dir>
copy_files() {
  # must have
  for file in OUTCAR OSZICAR CONTCAR; do
    if [ ! -e $1/${file} ]; then
      echo "$( pwd )/${1}/${file} doesn't exist. Skip this calculation" 1>&2
      return 1
    fi
    cp $1/${file} $2/
  done

  # optional
  for file in INCAR POTCAR POSCAR KPOINTS; do
    if [ -e $1/${file} ]; then
      cp $1/${file} $2/
    fi
  done
}

# Check whether there are similar structrues already in the repo.
# Output 0 if there is none; output the index of first similar structure 
# encountered in the repo;
# Usage: check_duplicate ./target_CONTCAR ./repo current_count
check_duplicate() {
  upper_bound=$3
  i=1
  while [ $i -le $upper_bound ]; do
    score=$( java -jar $Analyzer calc-similarity $1 $2/${i}/CONTCAR | \
      awk '{ print $NF }' )
    # 0: not similar; 1: similar;
    cond=$( perl -e "{ if ($score > $similarityThreshold) { print 0 } else { print 1 }}" )
    # similar
    if [ $cond -eq 1 ]; then
      echo $i 
      return 1
    fi
    let i++
  done
  echo 0;
}

# Deposite one folder
# Usage: deposite_one <target_dir>
deposite_one() {
  cd $1
  if [ -e OUTCAR ] && grep Elapsed ./OUTCAR &> /dev/null && [ -e CONTCAR ]; then
    index=$( check_duplicate ./CONTCAR ${destination} $count )
    if [ $index -eq 0 ]; then
      let count++
      mkdir ${destination}/${count}
      copy_files ./ ${destination}/${count}

      # Check succeeded
      if [ $? -ne 0 ]; then
        rm -r ${destination}/${count}
        let count--;
      fi
    else
      echo "$( pwd | sed -r -n "s%.*/(.*)$%\1%p" )/CONTCAR" \
           "is similar to the repo structure of index: ${index}. Skipped."
    fi
  fi
  cd ${OLDPWD}
}

# Count the number of folders with only numbers as name under path given in
# arguemnt.
# Usage: count_numeric_folder <path>
count_numeric_folder() {
  cd $1
  count=$( find ./ -maxdepth 1 -type d -regex "./[0-9]+" | wc -l )
  if [ -z "$count" ]; then
    echo 0; # empty folder
  else 
    echo $count
  fi
  cd $OLDPWD
}

############################## main() ###################################
# CONSTANT
similarityThreshold=0.15 # very-similar visually.
Analyzer=~/bin/Analyzer.jar

# Sanity check
if [ ! -e $Analyzer ]; then
  echo "Executable $Analyzer doesn't exist! Check its existence and change" \
    "the varible \"Analyzer\" to its path. Exit."
  exit 1
fi

# Start
target=$( cd $1; pwd; cd $OLDPWD )
destination=$( cd $2; pwd; cd $OLDPWD )

# Count # of calculations already present in destination
old_count=0
old_count=$( count_numeric_folder $destination ) # 0 if empty
count=$old_count
echo "Current number of calcualtions: $old_count"

cd $target

# If it's a single calculation
if [ -e OUTCAR ]; then
  deposite_one ./
else 
  # If target contains multiple calculations
  while read -r line; do
    dir=$( echo $line | awk '{ print $NF }' )
    if [ -d $dir ]; then
      cd $dir
      deposite_one ./
      cd ..
    fi
  done < <( ls -lvd * 2> /dev/null ) # handle empty folder.
fi  

echo "Updated number of calculations: ${count}. Increased by: $( expr $count - $old_count )."
