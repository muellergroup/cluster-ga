#!/bin/bash

# This is a script that analyzes GA vasp run. It collects information from 
# all_candidates: ionic step, Elapsed time, VASP energy of first and last 
# ionic step, magetic moment.

# Usage:
#   cd default/Al/34
#   analyzeGA.vasp.sh 
# It create an analyze folder and put all results in it.

if [ ! -e ./all_candidates ]; then
  echo "all_candidates folder doesn't exist. Nothing to collect."
  exit 1
elif [ ! -e ./candidates.dat ] || [ ! -e pool.dat ]; then
  echo ".dat files aren't complete. Recover them first."
  exit 1
fi

if [ ! -e ./analyze ]; then
  mkdir analyze;
fi
output=$( pwd )/analyze/results.txt
echo -n "" > $output

# output format:
#   index ionic_steps vasp_energy_first_step vasp_energy_last_step mag vasp_elapsed
#   VASP_start_date VASP_start_time

cd all_candidates
lastIndex=$( ls -dv [0-9]* 2> /dev/null | tail -n 1 )
if [ -z $lastIndex ]; then
  echo "No clusters in all_candidates."
  exit 0
else
  echo "Cluster count: $lastIndex"
fi

index=1
while [ $index -le $lastIndex ]; do
  cd $index
  ionic_steps=N/A
  vasp_energy_first=N/A
  vasp_energy_last=N/A
  vasp_elapsed=N/A
  mag=N/A
  start_date=N/A
  start_time=N/A

  # VASP
  if [ -e OUTCAR ]; then
    ionic_steps=$( grep E0 ./OSZICAR | tail -n 1 | awk '{ print $1 }' )
    vasp_energy_first=$( grep E0 ./OSZICAR | head -n 1 | awk '{ print $5 }' )
    vasp_energy_last=$( grep E0 ./OSZICAR | tail -n 1 | awk '{ print $5 }' )
    mag=$( grep E0 ./OSZICAR | tail -n 1 | awk '{ print $NF }' )
    if grep Elapsed ./OUTCAR &> /dev/null; then
      vasp_elapsed=$( grep Elapsed ./OUTCAR | awk '{ print $4 }' )
    fi
    start_date=$( grep "executed on" ./OUTCAR | awk '{ print $5 }' )
    start_time=$( grep "executed on" ./OUTCAR | awk '{ print $6 }' )
  fi

  # output
  echo "$index $ionic_steps $vasp_energy_first $vasp_energy_last $mag $vasp_elapsed $start_date $start_time" >> $output
  cd ..
  let index++
done
cd .. # get out of all_candidates
cat $output | column -t > temp.txt
cp temp.txt $output
rm temp.txt
