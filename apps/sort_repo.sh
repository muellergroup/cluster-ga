#!/bin/bash

# This script is used to sort a repo of VASP calculations in ascending order
# of energies. It will rename calculation to their rank of energies. The folder
# of lowest energy structure will be renamed to 1.

# Usage:
#   sort_repo.sh [--rank-only] <repo_path> <mag>
# Example:
#   cd ./VASP_data/Al/24
#   sort_repo.sh [--rank-only] ./ 0

# Note: 
# 1. Calculations with different magnetization at ground state 
# would be stored in a separate folder ./mag_<value>. These calculations wouldn't
# be sorted though. To sort them, just run this script for that folder with
# desired magnetic moment.
# 2. The rank.txt is updated after the sorting finishes.
# 3. To only get a rank of calculations without renaming the folders, use
# the option "--rank-only"
# 4. <mag> should be integer withour decimal point, since this script uses
# bash string comparison. -1 and 1 are different.
# 5. This script can resume interrupted sorting. It will only sort remaining
# calculations in <repo_path> and append them to calculations already in 
# <repo_path>/temp


############################ HELPER FUNCTIONS ################################
# Usage: within repo of calculations:
#   write_rank_file $count_of_calculations
write_rank_file() {
  local count=$1
  echo "" > temp.txt
  i=1;
  while [ $i -le $count ]; do
    if [ -e $i ]; then
      echo -n "$i " >> temp.txt
      grep E0 ./$i/OSZICAR | tail -n 1 >> temp.txt
    fi
    let i++;
  done
  sort -n -k 6 temp.txt | column -t > rank.txt
  rm temp.txt
}

# Count the number of folders with only numbers as name under path given in
# arguemnt.
# Usage: count_numeric_folder <path>
count_numeric_folder() {
  cd $1
  count=$( find ./ -maxdepth 1 -type d -regex "./[0-9]+" | wc -l )
  if [ -z "$count" ]; then
    echo 0; # empty folder
  else 
    echo $count
  fi
  cd $OLDPWD
}

################################ main() #######################################

if [ $# -lt 2 ]; then
  echo "Need at least two arguments: <repo_path> <mag>. Exit."
  exit 1
fi

RANK_ONLY=FALSE

if [[ "$1" == "--rank-only" ]]; then
  RANK_ONLY=TRUE
  repo_dir=$2
  target_mag=$3
else 
  repo_dir=$1
  target_mag=$2
fi

cd $repo_dir
count=$( count_numeric_folder ./ )

write_rank_file $count

if [ "$RANK_ONLY" == "TRUE" ]; then
  exit 0;
fi

if [ ! -e temp ]; then
  mkdir temp;
fi
total_lines=$( wc -l rank.txt | awk '{ print $1 }' ) # number of lines in rank.txt
count=$( count_numeric_folder ./temp ) # in case restart from incomplete sort
if [ -z $new_count]; then
  count=0
fi

line_num=1
while [ $line_num -le $total_lines ]; do
  index=$( head -n $line_num ./rank.txt | tail -n 1 | awk '{ print $1 }' ) 

  # Check mag.
  # This calculation supposes to exist. Otherwise, it won't be in rank.txt
  mag=$( grep E0 $index/OSZICAR | tail -n 1 | awk '{ print $NF }' )
  mag=$( printf "%.0f" $mag )
  if [ $mag -ne $target_mag ]; then
    if [ -e mag_${mag} ]; then
      # get new count every time
      mag_count=$( count_numeric_folder ./mag_${mag} ) 
      let mag_count++;
      mv $index ./mag_${mag}/${mag_count}
    else
      mkdir mag_${mag};
      mv $index ./mag_${mag}/1
    fi
  else
    # Have the desired magnetization
    let count++;
    mv $index ./temp/${count};
  fi
  let line_num++;
done
mv temp/* ./

rmdir temp # Will report error in case it's not empty.

# Update the rank.txt
count=$( count_numeric_folder ./ )
write_rank_file $count
