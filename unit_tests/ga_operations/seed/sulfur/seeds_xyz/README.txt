The coordinates are obtained from SI of this article:

1.	Jin, Y.;  Maroulis, G.;  Kuang, X.;  Ding, L.;  Lu, C.;  Wang, J.;  
    Lv, J.;  Zhang, C.; Ju, M., Geometries, stabilities and fragmental channels 
    of neutral and charged sulfur clusters: SnQ (n = 3–20, Q = 0, ±1). 
    Phys Chem Chem Phys 2015, 17 (20), 13590-13597.
    doi: https://doi.org/10.1039/C5CP00728C
