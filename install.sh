#!/bin/bash

# Compile source code using build.xml
if [ -e ./bin ]; then
  ant clean
fi
ant build

# Extract the matsci library
cd lib

jar xf ejml-0.20.jar
rm -r META-INF
mv org ../bin

jar xf Jama*.jar
rm -r META-INF
mv Jama ../bin

jar xf matsci.jar
rm -r META-INF lib matsci.zip build.xml .classpath .project .gitignore
mv matsci old ../bin

cd ..

# Write the Manifest
mkdir ./bin/META-INF
cat <<EOF > ./bin/META-INF/MANIFEST.MF
Manifest-Version: 1.0
Class-Path: .
Main-Class: ga.Start

EOF

# Generate the executable jar
cd ./bin
jar cfm cluster-ga.jar ./META-INF/MANIFEST.MF ./*
mv cluster-ga.jar ..
cd ..

