package ga.potential;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ga.HPC;
import ga.Input;
import ga.Start;
import ga.io.GAIOUtils;
import ga.io.Logger;
import ga.io.LAMMPS.LAMMPSDataFile_MTP;
import ga.io.VASP.INCAR;
import ga.io.VASP.OSZICAR;
import ga.io.mtp.CFG;
import ga.io.mtp.CFGImage;
import ga.minimizer.VASP;
import ga.structure.Cluster;
import ga.utils.app.CFGRegulator;
import matsci.Element;
import matsci.io.app.log.Status;
import matsci.structure.Structure;
import matsci.util.MSMath;

/**
 * This is a wrapper of Moment Tensor Potential Application from Dr.Alexander V. Shapeev.
 * @author ywang393
 *
 */
public class MTP {
	static private final String ConvertCommand = "convert-cfg";
	static private final String TrainCommand = "train";
	static private final String CalcGradeCommand = "calc-grade";
	static private final String CalcGradeALSCommand = "calc-grade-from-als";
	static private final String ActiveSelectCommand = "select-add";
	static private final String CFGOption = "--input-format=";
	static private final String IterOption = "--max-iter=";
	static private final String EWOption = "--energy-weight=";
	static private final String FWOption = "--force-weight=";
	static private final String STOption = "--stress-weight=";
	static private final String ScaleByForceOption = "--scale-by-force=";
	static private final String SITEEWOption = "--nbh-weight=";
	static private final String SelectThresholdOption = "--select-threshold=";
	
	// Customizable variables.
	static public String MTPEXE = "mlp";
	static public String MTPSERIALEXE = MTPEXE;
	static private List<String> PARALLELEXE;
	static public String InputFormat = "vasp-outcar";
	static public String CFGOutput = "outcar.cfg";
	static public String GRADEDCFGOutput = "graded_outcar.cfg";
	static public String ALSFile = "state.als"; // Storing potential, matrix A for active selection and selected cfgs.
	public static String SELECTED = "selected.cfg";
	//Reconstructed dataset by active learning and manual weighting.
	static public String WEIGHTEDDATASET = "dataset.cfg";
	
	// Training hyperparameters. The defaults are overridden by defaults in the Input class. 
	static private int MaxTrainIteration = 5000;
	static private double EnergyWeight = 1.0;
	static private double ForceWeight = 0.001;
	static private double StressWeight = 0.0;
	static private double ScaleByForce = 0;
	/*
	static private double SelectThreshold = 1.01;
	static private double SelectEnergyWeight = EnergyWeight;
	static private double SelectForceWeight = ForceWeight;
	static private double SelectStressWeight = StressWeight;
	static private double SelectSiteEnergyWeight = 0.0;
	*/
	static private String Stdout = "stdout";
	
	/**
	 * Remove given number of lines from the .cfg file.
	 * @param cfg The .cfg file to be modified.
	 * @param removeIndex Number of data to be deleted.
	 * @param stride
	 * @return The final content of the .cfg file.
	 */
	private static List<String> removeDataFromCFG(String cfg, int removeIndex, int stride) {
		List<String> lines = HPC.read(cfg);
		Logger.basic("Remove " + removeIndex + " images. "
				   + "Add " + (lines.size() / stride - removeIndex) + " images to " + CFGOutput);
		if (lines.size() < removeIndex * stride) {
			Logger.error("Number of data to be removed is larger than data in " + cfg);
			System.exit(4);
		}
		lines = lines.subList(removeIndex * stride, lines.size()); // toIndex is exclusive
		HPC.write(lines, cfg, false);
		return lines;
	}
	
	/**
	 * We might need to train MTP potential on systems with different sizes. Therefore, this
	 * function should have not depend on Input.getStride().
	 * 
	 * @return The length of each block of training data in .cfg
	 */
	public static int getCFGStride(int numAtoms) {
		// [BEGIN] [SIZE] [CELL] [FORCES] [Energy] [Stress] [Type] [END] [Blank line]
		//    1       2     1+3     1+n       2         2       1     1         1
		// Total: 12 + 2*n
		return 15 + numAtoms;
	}
	
	public static int getCFGEnergyRow(int numAtoms) {
		//The row listing the energy in CFG. Indexed to 1, not 0.
		return 10 + numAtoms;
	}
	
	/**
	 * Used primarily as GA internal routine. Check the backbone prepareCFG() for parameter meaning.
	 */
	public static List<String> prepareCFG(String calcDir, String type) {
		Element[] elements = Input.getElementTypes();
		boolean calcGrade = (Input.getRetrainMode() == Input.RetrainMode.DOptimality);
		boolean includeInterpolation = calcGrade && Input.includeInterpolating();
		boolean removeSimilar = Input.removeSimilar();
		double HET = Input.getHighEnergyThreshold();
		double LET = Input.getLowEnergyThreshold();
		double SELT = Input.getSelectThreshold();
		double DST = Input.getDataSimilarityThreshold();
		int groundMultiple = Input.getGroundMultiple();
		if (Input.getRetrainMode() == Input.RetrainMode.DOptimality && type.equalsIgnoreCase("MD")) {
			calcGrade = false;
			includeInterpolation = false; // Doesn't matter when calcGrade = false.
			removeSimilar = false;
			HET = Double.MAX_VALUE;
			LET = Double.MAX_VALUE;
			DST = 0; // Irrelevant if removeSimilar = false.
		}
		// Relative path to calcDir is enough, since sub-process sets wkdir to calcDir.
		String potential = ".." + HPC.FILESPTR + "mlip.mtp";
		String cfg = ".." + HPC.FILESPTR + CFGOutput;
		String als = ".." + HPC.FILESPTR + ALSFile;
		return prepareCFG(calcDir, elements, potential, cfg, als, calcGrade, includeInterpolation, 
				          removeSimilar, HET, LET, SELT, DST, groundMultiple, type);
	}
	
	/**
	 * Create a .cfg file from the specified calculation folder. The folder should contain an 
	 * OUTCAR and an OSZICAR.
	 * 
	 * In a DFT relaxation calculation, not all relaxation steps are suitable for training, since
	 * their spin configurations might not right. Therefore, the first several steps in .cfg
	 * that have different magnetization values (decided by "mag" values from OSZICAR) will 
	 * be discarded.
	 * 
	 * For efficiency in active learning, only configurations with an energy residing within a 
	 * threshold (ENERGYTHRESHOLD) of the ground-state energy and those with maxvol grade larger
	 * than SELECTTHRESHOLD are kept.
	 * 
	 * @param calcDir folder containing a VASP calculation (must have INCAR, OSZICAR, and OUTCAR)
	 * @param elements
	 * @param pot relative path against calcDir to potential file mlip.mtp
	 * @param cfg relative path against calcDir to .cfg files used to train potential file
	 * @param als relative path against calcDir to .als files from pot and cfg. 
	 *            One of als and cfg being valid is enough.
	 * @param calcGrade whether calculate Maxvol grades.
	 * @param includeInterpolating whether include configurations with Maxvol grade lower than SELT
	 * @param removeSimilar whether remove similar ionic steps from a VASP calcluation.
	 * @param HET high energy threshold
	 * @param LET low energy threshold
	 * @param SELT Maxvol grade threshold to include in training data
	 * @param DST CFG image similarity threshold for filtering training data.
	 * @param groundMultiple repeat last image by this number of times.
	 * @param type "MD" or "RELAX". Used to retrieve magnetic moment from OSZICAR.
	 * @return
	 */
	public static List<String> prepareCFG(String calcDir, Element[] elements, String pot, 
			String cfg, String als, boolean calcGrade, boolean includeInterpolating, 
			boolean removeSimilar, double HET, double LET, double SELT, double DST, int groundMultiple, 
			String type) {
		if (!new File(calcDir + HPC.FILESPTR + VASP.getOutputFileName()).exists()) {
			Logger.error("OUTCAR doesn't exist! VASP evaluation might have failed. No CFGs are added.");
			return new ArrayList<String>();
		} else if (convertCFG(calcDir, "OUTCAR", CFGOutput) != 0) {
			Logger.error("Converting OUTCAR in " + calcDir + " failed! No CFGs are added.");
			return new ArrayList<String>();
		}
		
		INCAR incar = new INCAR(calcDir + HPC.FILESPTR + VASP.getInputFileName());
		OSZICAR oszicar = new OSZICAR(calcDir + HPC.FILESPTR + "OSZICAR", type);
		CFG cfgs = new CFG(calcDir + HPC.FILESPTR + CFGOutput);
		
		// Remove ionic steps that reaches NELM. SCF calculation doesn't converge for these steps.
		cfgs.removeUnconvergedImages(oszicar.getSCFSteps(), incar.getNELM());
		// Use last magnetization value as the correct magnetization.
		cfgs.removeWrongMagImages(oszicar.getMagetization(), oszicar.getMagnetizationList());
		// remove configurations with energies exceeding the ground-state energy more than ENERGYTHRESHOLD
		cfgs.removeHighEnergyImages(HET);
		cfgs.writeFile(calcDir + HPC.FILESPTR + CFGOutput);
		
		if (calcGrade) {
			if (Files.exists(Paths.get(calcDir + HPC.FILESPTR + als))) { // als is relative.
				MTP.calcMaxvolGradeALS(calcDir, pot, als, CFGOutput, GRADEDCFGOutput);
			} else {
				MTP.calcMaxvolGrade(calcDir, pot, cfg, CFGOutput, GRADEDCFGOutput);
				// In case multiple clusters are in the retraining pool. Reuse state.als.
				HPC.copy(calcDir + HPC.FILESPTR + ALSFile, calcDir + HPC.FILESPTR + als, false); 
			}
			cfgs = new CFG(calcDir + HPC.FILESPTR + GRADEDCFGOutput);
			// remove interpolating configurations but keep images close to ground state configuration.
			if (!includeInterpolating) {
				cfgs.removeIntrapolatingImages(SELT);
				cfgs.includeLowEnergyImages(LET);
			}
			// Skip configurations with include being false.
			cfgs.writeFile(calcDir + HPC.FILESPTR + CFGOutput);
			HPC.delete(calcDir + HPC.FILESPTR + GRADEDCFGOutput, true); // Reduce number of files.
		}
		
		if (removeSimilar) {
			cfgs.setElements(elements);
			cfgs.removeSimilarImages(DST);
		}
		
		CFGImage groundState = cfgs.getCFGImage(cfgs.getLastValidImageIndex());
		ArrayList<CFGImage> groundDuplicates = new ArrayList<>();
		for (int i = 0; i < groundMultiple - 1; i++) {
			groundDuplicates.add(groundState);
		}
		cfgs.add(groundDuplicates);
		cfgs.writeFile(calcDir + HPC.FILESPTR + CFGOutput);
		
		// cfgs sets includeImage[i] to false, but didn't remove them. Thus, a reload is needed.
		return HPC.read(calcDir + HPC.FILESPTR + CFGOutput);
	}
	
	/**
	 * Simply convert all images in OUTCAR to CFGs and return the file content as a list.
	 * @param OUTCAR
	 * @return
	 */
	public static List<String> prepareCFG(String calcDir) {
		String OUTCARPath = calcDir + HPC.FILESPTR + "OUTCAR";
		File OUTCAR = new File(OUTCARPath);
		if (!OUTCAR.exists()) {
			Logger.error("OUTCAR doesn't exist! No CFGs are added.");
			return new ArrayList<String>();
		}
		
		int exitCode = MTP.convertCFG(calcDir, OUTCARPath, calcDir + HPC.FILESPTR + CFGOutput);
		if (exitCode != 0) {
			Logger.error("Converting OUTCAR in " + calcDir + " failed! No CFGs are added.");
			return new ArrayList<String>();
		}
		
		return HPC.read(calcDir + HPC.FILESPTR + CFGOutput);
	}
	
	public static List<String[]> sortCFG(String calcDir) {
		List<String> dataLines = HPC.read(calcDir);
		List<String[]> sortedConfigurations = buildCFGArray(dataLines);
				
		Comparator<String[]> sortByEnergy = new Comparator<String[]>() {
			@Override
			public int compare(String[] thisConfig, String[] thatConfig) {
				double thisEnergy = getCFGEnergy(thisConfig);
				double thatEnergy = getCFGEnergy(thatConfig);
				return Double.compare(thisEnergy, thatEnergy);
			}
		};
			
		Collections.sort(sortedConfigurations,sortByEnergy);
		return sortedConfigurations;
	}
	
	public static double getCFGEnergy(String[] cfg) {
		double energy = 0;
		for (int l = 0; l < cfg.length; l++) {
			String line = cfg[l];
			if (line.contains("Energy")) {
				energy = Double.parseDouble(cfg[l+1].trim());
			}
		}
		return energy;
	}
	
	private static List<String[]> buildCFGArray(List<String> CFGLines){
		boolean reading = false;
		List<String[]> CFGArray = new ArrayList<>();
		ArrayList<String> eachConfiguration = new ArrayList<String>();
		
		for (int i = 0; i < CFGLines.size(); i++) {
			String line = CFGLines.get(i);
			if (line.contains("ID") || line.contains("MV")) {
				continue;
			} else if (line.contains("BEGIN")){
				eachConfiguration.add(line);
				reading = true;
			} else if (line.contains("END")) {
				eachConfiguration.add(line);
				if ((i+1) < CFGLines.size()) { 
					eachConfiguration.add(CFGLines.get(i+1)); 
					}
				String[] configArray = Arrays.stream(eachConfiguration.toArray()).toArray(String[]::new);;
				CFGArray.add(configArray);
				eachConfiguration.clear();
				reading = false;
			} else if (reading) {
				eachConfiguration.add(line);
			}
		}
		return CFGArray;
	}
	
	/**
	 * Wrapper of the command "mlp convert-cfg --input-format=vasp-outcar <outcar> <output>".
	 * @param outcar OUTCAR file from VASP relaxation or MD.
	 * @param output Path of output .cfg file.
	 * @return exit code of the "mlp convert-cfg" command.
	 */
	public static int convertCFG(String workDir, String outcar, String output) {
		List<String> commands = new ArrayList<>();
		commands.addAll(Arrays.asList(MTPSERIALEXE, ConvertCommand, CFGOption + InputFormat, 
				                      outcar, output));
		ProcessBuilder pb = new ProcessBuilder(commands);
		pb.directory(new File(workDir));
		pb.redirectErrorStream(true).redirectOutput(ProcessBuilder.Redirect.PIPE);
		return HPC.execute(pb, false);
	}
	
	/**
	 * Train the given potential by the given data set at the specified working directory.
	 * @param workDir
	 * @param potential
	 * @param cfg
	 * @return exit code of "mlp train"
	 */
	public static int train(String workDir, String potential, String cfg) {
		ArrayList<String> commands = new ArrayList<>();
		commands.addAll(PARALLELEXE);
		commands.addAll(Arrays.asList(MTPEXE, TrainCommand, potential, cfg, 
				                      IterOption + MaxTrainIteration,
				                      EWOption + EnergyWeight,
				                      FWOption + ForceWeight,
				                      STOption + StressWeight,
				                      ScaleByForceOption + ScaleByForce,
				                      "--trained-pot-name=mlip.mtp"));
		ProcessBuilder pb = new ProcessBuilder(commands);
		pb = pb.directory(new File(workDir))
			   .redirectErrorStream(true)
		       .redirectOutput(new File(workDir + HPC.FILESPTR + Stdout)); // Relative to workDir.
		return HPC.execute(pb, false);
	}
	
	public static void setMTPEXE(String exe) {
		MTPEXE = exe;
	}
	
	public static void setMTPSERIALEXE(String exe) {
	    MTPSERIALEXE = exe;
	}
	
	public static void setPARALLELEXE(List<String> exe) {
		PARALLELEXE = exe;
	}
	
	public static void setInputFormat(String format) {
		InputFormat = format;
	}
	
	public static void setEenergyWeight(double w) {
		EnergyWeight = w;
	}
	
	public static void setForceWeight(double w) {
		ForceWeight = w;
	}
	
	public static void setStressWeight(double w) {
		StressWeight = w;
	}
	
	public static void setScaleByForce(double s) {
		ScaleByForce = s;
	}
	
	public static void setMaxTrainIteration(int n) {
		MaxTrainIteration = n;
	}
	
	public static String getCFGOutput() {
		return CFGOutput;
	}
	
	public static String getGradedCFGOutput() {
		return GRADEDCFGOutput;
	}
	
	/**
	 * Calculate Maxvol grade for given new data set. See "mlp help calc-grade".
	 * 
	 * @param wkdir working directory.
	 * @param potential path to potential.
	 * @param trainData path to the training data of the given potential.
	 * @param newData path to the cfg file containing new data.
	 * @param output path to output for graded cfgs.
	 * @return
	 */
	public static int calcMaxvolGrade(String wkdir, String potential, String trainData,
			String newData, String output) {
		ArrayList<String> commands = new ArrayList<>();
		/*
		if (PARALLELEXE != null) {
			commands.add(PARALLELEXE.get(0)); // should be "mpirun"
			commands.addAll(Arrays.asList("-n", "1")); // Prevent serial MTP to hang on MARCC.
		}
		*/
		commands.addAll(Arrays.asList(MTPSERIALEXE, CalcGradeCommand, potential, trainData, newData, 
		        output, SelectThresholdOption + Input.getSelectThreshold(),
		        EWOption + Input.getSelectEnergyWeight(),
		        FWOption + Input.getSelectForceWeight(),
		        STOption + Input.getSelectStressWeight(),
		        SITEEWOption + Input.getSelectSiteEnergyWeight()));
		ProcessBuilder pb = new ProcessBuilder(commands);
		pb = pb.directory(new File(wkdir)).redirectErrorStream(true).redirectOutput(new File("/dev/null"));
		return HPC.execute(pb, false);
	}
	
	/**
	 * Load old .als file to calculate maxvol grade, instead of initializing selection matrix from scratch.
	 * 
	 * @param wkdir
	 * @param potential
	 * @param alsFile
	 * @param newData
	 * @param output
	 * @return
	 */
	public static int calcMaxvolGradeALS(String wkdir, String potential, String alsFile,
			String newData, String output) {
		ArrayList<String> commands = new ArrayList<>();
		/*
		if (PARALLELEXE != null) {
			commands.add(PARALLELEXE.get(0)); // should be "mpirun"
			commands.addAll(Arrays.asList("-n", "1")); // Prevent serial MTP to hang on MARCC.
		}
		*/
		commands.addAll(Arrays.asList(MTPSERIALEXE, CalcGradeALSCommand, potential, alsFile, newData, 
		        output, SelectThresholdOption + Input.getSelectThreshold(),
		        EWOption + Input.getSelectEnergyWeight(),
		        FWOption + Input.getSelectForceWeight(),
		        STOption + Input.getSelectStressWeight(),
		        SITEEWOption + Input.getSelectSiteEnergyWeight()));
		ProcessBuilder pb = new ProcessBuilder(commands);
		pb = pb.directory(new File(wkdir)).redirectErrorStream(true).redirectOutput(new File("/dev/null"));
		return HPC.execute(pb, false);
	}
	
	public static boolean isExtrapolating (Cluster cluster) {
		long st = System.currentTimeMillis();
		double maxvolGrade = Double.MAX_VALUE;
		String structFile, gradedFile;
		if (cluster.relaxed()) {
			structFile = cluster.getCalcDir() + HPC.FILESPTR + "end.cfg";
			gradedFile = cluster.getCalcDir() + HPC.FILESPTR + "graded_end.cfg";
		} else {
			structFile = cluster.getCalcDir() + HPC.FILESPTR + "struct.cfg";
			gradedFile = cluster.getCalcDir() + HPC.FILESPTR + "graded_struct.cfg";
		}
		// The cluster might not be at the center of cell. Read struct.lmp for now and write out.
		LAMMPSDataFile_MTP lammpsStruct;
		if (cluster.relaxed()) {
			lammpsStruct = new LAMMPSDataFile_MTP(cluster.getCalcDir() + HPC.FILESPTR + "end.lmp", 
                                                  Input.getElementTypes(), new boolean[]{true, true, true});
		} else {
			lammpsStruct = new LAMMPSDataFile_MTP(cluster.getCalcDir() + HPC.FILESPTR + "struct.lmp", 
					                              Input.getElementTypes(), new boolean[]{true, true, true});
		}
		CFG cfgs = new CFG(new Structure(lammpsStruct));
	   	cfgs.writeFile(structFile);
	   	
		// TODO: use cluster.getStructure() later.
		//CFG cfgs = new CFG(cluster.getStructure());
		//cfgs.writeFile(structFile);
	   	
	   	String alsFile = Input.getRoot() + HPC.FILESPTR + ALSFile;
	   	int returnCode = 1;
	   	long start = System.currentTimeMillis();
	   	if (Files.exists(Paths.get(alsFile))) {
	   		returnCode = MTP.calcMaxvolGradeALS(cluster.getCalcDir(), 
	   											Input.getRoot() + HPC.FILESPTR + "mlip.mtp",
						 						Input.getRoot() + HPC.FILESPTR + MTP.ALSFile,
						 						structFile, gradedFile);
	   	} else {
	   		if (Start.useWeightedDataset) {
	   			String latestTrainDir = Start.WorkDir + HPC.FILESPTR + "train" + HPC.FILESPTR + Start.TrainedTimes; 
	   			returnCode = MTP.calcMaxvolGrade(cluster.getCalcDir(), 
							 					Input.getRoot() + HPC.FILESPTR + "mlip.mtp",
							 					latestTrainDir + HPC.FILESPTR + MTP.WEIGHTEDDATASET,
							 					structFile, gradedFile);
	   		} else {
	   			returnCode = MTP.calcMaxvolGrade(cluster.getCalcDir(), 
	   										 	Input.getRoot() + HPC.FILESPTR + "mlip.mtp",
	   										 	Input.getRoot() + HPC.FILESPTR + "outcar.cfg",
	   										 	structFile, gradedFile);
	   		}
	   		if (returnCode == 0) {
	   			HPC.copy(cluster.getCalcDir() + HPC.FILESPTR + MTP.ALSFile,
	   					 Input.getRoot() + HPC.FILESPTR + MTP.ALSFile, false);
	   		}
	   	}
	   	long end = System.currentTimeMillis();
	   	Logger.debug("Executing calc-grade command takes " + GAIOUtils.formatElapsedTime(end - start));
		if (returnCode != 0) {
			if (cluster.relaxed()) {
				Logger.error("Calculate Maxvol grade for relaxed structure of cluster-" 
							+ cluster.getCandidateIndex() + " failed! "
							+ "Set the grade to the largest float value.");
			} else {
				Logger.error("Calculate Maxvol grade for initial structure of cluster-" 
						+ cluster.getCandidateIndex() + " failed! "
						+ "Set the grade to the largest float value.");
			}
			maxvolGrade = Double.MAX_VALUE;
		} else {
			// There should be only one cfg in newData;
			String[] gradeLines = HPC.grep("MV_grade", gradedFile);
			// The leading space is the first element after splitting.
			String grade = gradeLines[0].split("\\s+")[3];
			if (grade.equals("nan")) {
				Logger.error("Maxvol grade is \"nan\" Set grade to maximum float number.");
				maxvolGrade = Double.MAX_VALUE;
			} else {
				maxvolGrade = Double.parseDouble(grade);
				if (cluster.relaxed()) {
					Logger.detail("Maxvol grade of relaxed structure is " + maxvolGrade);
				} else {
					Logger.detail("Maxvol grade of initial structure is " + maxvolGrade);
				}
			}
		}
		
		boolean isExtrapolating = (!cluster.relaxed()) 
					            ? (maxvolGrade >= Input.getBreakThreshold()) 
				                : (maxvolGrade >= Input.getSelectThreshold());
		cluster.setMaxvolGrade(maxvolGrade);
		cluster.setExtrapolating(isExtrapolating);
		long et = System.currentTimeMillis();
		Logger.detail("Calculating maxvol grade takes " + GAIOUtils.formatElapsedTime(et - st));
		return isExtrapolating;
	}
	
	/**
	 * Extracting maxvol grades from CFG file for configurations having "MV_grade" tag.
	 * @param file
	 * @return A list of maxvol grades. Empty if the file doesn't exist or doesn't contain "MV_grade".
	 */
	public static List<Double> readMaxvolGrade(String file) {
		List<Double> grades = new ArrayList<>();
		if (Files.exists(Paths.get(file))) {
			String[] gradeLines = HPC.grep("MV_grade", file);
			for (String line: gradeLines) {
				try {
					String gradeString = line.split("\\s+")[3]; // The 4-th field.
					grades.add(Double.parseDouble(gradeString));
				} catch (NumberFormatException e) {
					Logger.error("Maxvol grade is not parsable: " + e.getLocalizedMessage() + ". "
							   + "Add to the list Double.MAX_VALUE.");
					grades.add(Double.MAX_VALUE);
				}
			}
		}
		return grades;
	}
	
	/**
	 * Select from newData a subset of CFGs to be added to trainingSet. Newly selected CFGs are saved
	 * in "workDir/diff.cfg" and all selected CFGs from both training data and new data are saved in
	 * "workDir/selected.cfg" (SELECTEDFile).
	 * 
	 * @param workDir
	 * @param potential
	 * @param trainingSet
	 * @param newData
	 * @return
	 */
	public static int selectAdd(String workDir, String potential, String trainingSet, String newData) {
		List<String> commands = new ArrayList<>();
		/*
		if (PARALLELEXE != null) {
			commands.add(PARALLELEXE.get(0)); // should be "mpirun"
			commands.addAll(Arrays.asList("-n", "1")); // Prevent serial MTP to hang on MARCC.
		}
		*/
		commands.addAll(Arrays.asList(MTPSERIALEXE, ActiveSelectCommand, potential, trainingSet, newData, 
		                              workDir + HPC.FILESPTR + "diff.cfg",
		                              SelectThresholdOption + Input.getSelectThreshold(),
		                              EWOption + Input.getSelectEnergyWeight(),
		                              FWOption + Input.getSelectForceWeight(),
		                              STOption + Input.getSelectStressWeight(),
		                              SITEEWOption + Input.getSelectSiteEnergyWeight(),
		                              "--als-filename=" + ALSFile,
		                              "--selected-filename=" + SELECTED));
		ProcessBuilder pb = new ProcessBuilder(commands);
		pb = pb.directory(new File(workDir)).redirectErrorStream(true).redirectOutput(new File(workDir + HPC.FILESPTR + "stdout"));
		return HPC.execute(pb, false);
	}
}
