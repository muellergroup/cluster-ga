package ga;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import ga.io.FileComparator;
import ga.io.Logger;
import ga.multithreaded.Candidates;
import ga.utils.FileLock;

/**
 * A interface between Java and the native platform. Many functions are wrappers of I/O operations 
 * and commands on the native platform.
 */
public class HPC {

	//private static String m_WorkDirectory = null;  // The folder contains candidates.dat. e.g <Root>/Al/8
	
	// Platform dependent constants
	public static final String FILESPTR = System.getProperty("file.separator");
	public static final String LINESPTR = System.getProperty("line.separator");
	
	public static Map<String, FileLock> fileLocks = new HashMap<>();
	static {
		fileLocks.put("candidates.dat", new FileLock("candidates.dat"));
		fileLocks.put("pool.dat", new FileLock("pool.dat"));
		fileLocks.put("score_table.dat", new FileLock("score_table.dat"));
		fileLocks.put("similarity_table.dat", new FileLock("similarity_table.dat"));
		fileLocks.put("exitcodes.dat", new FileLock("exitcodes.dat"));
		fileLocks.put("exploded.dat", new FileLock("exploded.dat"));
		fileLocks.put("chosen.dat", new FileLock("chosen.dat"));
		fileLocks.put("chosen.dat", new FileLock("mutation.dat"));
		fileLocks.put("select.dat", new FileLock("select.dat"));
		fileLocks.put("seed.dat", new FileLock("seed.dat"));
		fileLocks.put("seed.dat", new FileLock("maxvolGrades.dat"));
	}
	
	private HPC() {}

	
	/**
	 * Wrapper of "mkdir -p".
	 * @param directory
	 * @return True if the file exists or has been created.
	 *         False if the file doesn't exist and cannot be created.
	 */
	public static boolean mkdir(String directory){
		File f = new File(directory);
		if (!f.exists()) {
			return f.mkdirs();
		}
		return true;
	}
	
	/**
	 * Evaluate energy of the structure in the folder "destination". A wrapper of 
	 * Cluster.calculate(). //TODO: use Cluster.calculate();
	 * 
	 * stdout is necessary for energy evaluation commands. Otherwise, thread will be blocked
	 * when the IO buffer is used up.
	 * 
	 * @param destination working directory of the command. i.e. the folder of the cluster.
	 * @param commands command and arguments
	 * @param stdout File to redirect output of stdout.
	 * @return Exit status code of the energy-evaluation executable. 0 success.
	 */
	/*
	public static int submit(EnergyMinimizer minimizer){
		ProcessBuilder pb;
		File output = new File(minimizer.getCalDir() + HPC.FILESPTR + minimizer.getStdOutput());
		pb = new ProcessBuilder(minimizer.getCommands()).directory(new File(minimizer.getCalDir()))
				                                        .redirectErrorStream(true)
				                                        .redirectOutput(output);
		return HPC.launch(pb, true);	// Execute the energy evaluation.
	}
	*/
	
	/**
	 * 
	 * @param p	Subprocess whose output is flushed
	 * @param verbose If true, output to stdout. Otherwise, keep in m_Output object.
	 */
	private static void flush(Process p) {
		String line = null;
		// p.getInputStream() return a stream connect to the output of the subprocess.
		try (BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
			while ((line = br.readLine()) != null) {
				Logger.detail(line);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * 1. Both are folders: copy files in source to new files with same name in destination;
	 * 2. source is file & destination is folder: copy source to a new file with same name 
	 *    in destination;
	 * 3. source is a folder & destination is a file: delete the destination file and create a new
	 *    folder with the same name as destination. Then repeat case 1.
	 * 4. Both are files: copy file to file.
	 * 
	 * In all cases, if the target file exists, replace them.
	 * 
	 * If source doesn't exist, issue a warning, but don't abort.
	 * 
	 * @param source Folder or file to be copied.
	 * @param destination Folder or file.
	 * @param recursive Whether copy sub-folder if source is a folder.
	 */
	public static void copy(String source, String destination, boolean recursive){
		File dest = new File(destination);
		File src = new File(source);
		if (!src.exists()) {
			Logger.warning("Failed to copy " + source + ". It doesn't exist!");
			return;
		}
		
		if (src.isDirectory()) {
			// "destination" must be a folder in this case.
			if (!dest.isDirectory()) {
				dest.delete();
			}
			if (!dest.exists()) {
				dest.mkdirs();
			}
			File[] files = src.listFiles();
			for (File i : files) {
				if (i.isDirectory()) {
					if (recursive) {
						copy(i.getAbsolutePath(), dest.toPath().resolve(i.getName()).toString(), recursive);
					} // Don't copy sub-folder if it's not recursive.
				} else {
					try {
						Files.copy(i.toPath(), dest.toPath().resolve(i.getName()), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						e.printStackTrace();
						Logger.error("Failed to copy " + i.getAbsolutePath() + " to " + destination);
					}
				}
			}
		} else {
			try {
				if (!dest.getParentFile().exists()) {
					dest.getParentFile().mkdirs();
				}
				if (dest.isDirectory()) {
					Files.copy(src.toPath(), dest.toPath().resolve(src.getName()), StandardCopyOption.REPLACE_EXISTING);
				} else {
					Files.copy(src.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
				}
			} catch (IOException e) {
				e.printStackTrace();
				Logger.error("Failed to copy " + src.getAbsolutePath() + " to " + destination);
			}
		}
	}

	/**
	 * If path is a file, remove that file;
	 * If path is folder and recursive = true, remove all files, folders and the folder itself.
	 * If path is folder but not recursive, remove only regular files.
	 * @param path
	 * @param isDir
	 * @param recursive
	 * @param quiet Whether report error when exceptions are thrown.
	 */
	public static void rm(String path, boolean isDir, boolean recursive, boolean quiet) {
		if(isDir) {
			File dir = new File(path);
			// Remove regular files first.
			File[] files = dir.listFiles((file) -> file.isFile());
			for (File f: files) {
				HPC.delete(f.getPath(), quiet);
			}
			if (recursive) {
				files = dir.listFiles((file) -> file.isDirectory());
				for (File d: files) {
					HPC.rm(d.getAbsolutePath(), true, recursive, quiet);
				}
				HPC.delete(dir.getPath(), quiet); // if recursive, also delete the folder.
			}
		} else {
			HPC.delete(path, quiet);
		}
	}
	
	/**
	 * Remove all files and directories given in list argument. 
	 * 
	 * Note that if an element is a directory, its files and subdirectories are also deleted. 
	 * It is possbile that a folder and files / sub-directories are both contained in the list.
	 * When it happens, parent folder could be deleted before its contents. Therefore, checking
	 * existence before deletion is necessary.
	 * @param files
	 * @param quiet
	 */
	public static void rm(List<String> files, boolean quiet) {
		for (String file: files) {
			Path p = Paths.get(file);
			if (Files.exists(p)) {
				if (Files.isDirectory(p)) {
					HPC.rm(file, true, true, quiet);
				} else {
					HPC.delete(file, quiet);
				}
			}
		}
	}
	
	/**
	 * Simpler function for lighter IO operation. Backbone of more sophisticated HPC.rm().
	 * @param file
	 * @param quiet
	 */
	public static void delete(String file, boolean quiet) {
		try {
			Files.deleteIfExists(Paths.get(file));
		} catch (IOException ioe) {
			if (!quiet) {
				Logger.error("Fail to delete file: " + file + ". Exception: " + ioe.getLocalizedMessage());
			}
		}
	}
	
	/**
	 * Mimic behavior of mv in Unix system. Overwrite destination if it exists.
	 * 1. When both are files, rename source to destination.
	 * 2. When source is file, destination is folder, move source to destination.
	 * 3. When source is folder, destination is also folder, rename folder to destination. Entries
	 * are also moved.
	 * 4. When source is folder, destination is file, report error and return false
	 * 
	 * @param source
	 * @param destination
	 * @return Whether operation has succeeded.
	 */
	public static boolean mv(String source, String destination) {
		File src = new File(source);
		File dest = new File(destination);
		if (src.isFile()) {
			copy(source, destination, false);
			rm(source, false, false, true);
		} else if (src.isDirectory()) {
			if (!dest.exists()) {
				dest.mkdir();
				copy(source, destination, true);
				rm(source, true, true, true); // source is also deleted.
			} else if (dest.isDirectory()) {
				copy(source, destination, true);
				rm(source, true, true, true); // source is also deleted.
			} else {
				Logger.error("Cannot copy a folder to a file!" + "Source: " + source 
						+ " Destination: " + destination );
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Append contents in a list as lines to a specified destination (file).
	 * @param content
	 * @param path
	 */
	public static void append(List<String> contents, String path) {
		File file = new File(path);
		// Try-with-resources automatically close the stream.
		try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(file, true)))) {
            for	(String i: contents) {
            	pw.println(i);
            }
		} catch (IOException e) {
			Logger.error("Cannot append to a folder: " + path);
			e.printStackTrace(System.err);
			System.exit(3);
		}
	}
	
	public static int execute(List<String> commands, String workDir, String output, boolean flushOutput) {
		ProcessBuilder pb = new ProcessBuilder(commands).directory(new File(workDir))
                                                        .redirectErrorStream(true)
                                                        .redirectOutput(new File(output));
		return execute(pb, flushOutput);
	}
	
	/**
	 * Wrapper of ProcessBuilder.start().
	 * @param pb The process builder to create a subprocess to run on native platform
	 * @return The exit status of the subprocess as integer. 
	 *         0 represents success, and 1 - 255 means failure.
	 */
	public static int execute(ProcessBuilder pb, boolean flushOutput){
		try{
			pb.redirectErrorStream(true);
			Process p = pb.start();
			
			// Only output to stdout for non-energy-evaluation commands. i.e. Don't dump vasp
			// or lammps output to java standard output.
			List<String> commands = pb.command();
			StringBuilder sb = new StringBuilder("Executing command: \"");
			for (int i = 0; i < commands.size(); i++) {
				sb.append(commands.get(i));
				if (i < commands.size() - 1) {
					sb.append(" ");
				}
			}
			sb.append("\"" + " in " + pb.directory().getPath());
			Logger.detail(sb.toString());
			
			if (!flushOutput) {
				HPC.flush(p);
			}

			// HPC.flush() handles the output from subprocess, we can safely use waitFor() here.
			return p.waitFor();
		} catch(IOException e) {
			Logger.error("Failed to launch the command: " + pb.command().get(0));
			e.printStackTrace();
			return 1;
		} catch (InterruptedException e) {
			Logger.error("Launching the command is interrupted: " + pb.command().get(0));
			return 1;
		}
	}
	
	/**
	 * Find the sub-folder with the largest numerial folder name.
	 * 
	 * @param dir Path of folder to be checked.
	 * @return largest folder name in all_candidates. 0 if all_candidates doesn't exist or it is
	 *         empty.
	 */
	public static int findLastDir(String dir){
		String filePath = dir + HPC.FILESPTR + "all_candidates";
		boolean exists = HPC.exists(filePath);
		if (!exists) {
			new File(filePath).mkdirs();
			return 0;
		}
		File[] files = new File(filePath).listFiles((file, name) -> name.matches("[0-9]+"));
		if (files.length == 0) {
			return 0;
		}
		files = sortFolderByNumber(files);
		return Integer.parseInt(files[files.length - 1].getName());
	}
	
	public static File[] sortFolderByNumber(File[] f){
		Arrays.sort(f, new FileComparator());
		return f;
	}
	
	/**
	 * A thread-safe method to read all the lines from a file and return a List of Strings.
	 * 
	 * @param path
	 * @return A thread-safe list of lines.
	 *         If the file not exists, return an empty list.
	 */
	public static List<String> read(String path){
		if (!HPC.exists(path)) {
			return new ArrayList<String>();
		}

		List<String> lines = null;
		FileLock lock = null;
		File file = new File(path);
		
		if (fileLocks.containsKey(file.getName())) {
			lock = fileLocks.get(file.getName());
			lock.readLock();
		}
		try {
			lines = Collections.synchronizedList(HPC.readAllLines(file.getPath()));
		} catch (NoSuchFileException e) {
			e.printStackTrace();
			Logger.error("Fail to read: " + file.getAbsolutePath());
			Logger.error(e.getReason());
			System.exit(3);
		} catch (IOException e) {
			e.printStackTrace();
			Logger.error("Fail to read: " + file.getAbsolutePath());
			System.exit(3);
		} finally {
			if (lock != null) {
				lock.readUnlock();
			}
		}
		return lines;
	}
	
	/**
	 * A thread-safe method to write a list of strings to a file, with each string on a separate line.
	 * 
	 * @param lines List of strings to be written to the file
	 * @param fileName Name of the output file
	 * @param append whether append to the end or rewrite entire file.
	 */
	public static void write(List<String> lines, String fileName, boolean append){
		File f = new File(fileName);
		
		FileLock lock = null;
		if (fileLocks.containsKey(f.getName())) {
			lock = fileLocks.get(f.getName());
			lock.writeLock();
		}
		
		try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(f, append)))) {
			for (int i = 0; i < lines.size(); i++) {
				writer.println(lines.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(3);
		} finally {
			if (lock != null) {
				lock.writeUnLock();
			}
		}
	}
	
	/**
	 * A copy the write function with List<String>. Simply change argument from list to a single
	 * String.
	 * 
	 * @param newline New line to be write to the given file.
	 * @param fileName
	 * @param append
	 */
	public static void write(String newline, String fileName, boolean append){
		File f = new File(fileName);
		
		FileLock lock = null;
		if (fileLocks.containsKey(f.getName())) {
			lock = fileLocks.get(f.getName());
			lock.writeLock();
		}
		
		try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(f, append)))) {
			writer.println(newline);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(3);
		} finally {
			if (lock != null) {
				lock.writeUnLock();
			}
		}
	}
	
	/**
	 * Replace a section of lines with provided list of strings. 
	 * @param start The line number of the first line to be replaced. (inclusive)
	 * @param lines The list of lines to replace.
	 * @param path The path to the target file.
	 */
	public static void replace(int start, List<String> lines, String fileName) {
		File f = new File(fileName);
		
		FileLock lock = null;
		if (fileLocks.containsKey(f.getName())) {
			lock = fileLocks.get(f.getName());
			lock.writeLock();
		}
		
		List<String> contents;
		try {
			contents = Collections.synchronizedList(HPC.readAllLines(f.getPath()));
			try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(f, false)))) {
				// Replace
				for (int i = 0; i < lines.size(); i++) {
					contents.set(start + i, lines.get(i));
				}
				for (int i = 0; i < contents.size(); i++) {
					writer.println(contents.get(i));
				}
			} catch (IOException e) {
				Logger.error("Cannot replace lines in " + f.getPath() + ".");
				e.printStackTrace();
				System.exit(3);
			}
		} catch (IOException e) {
			Logger.error("Cannot read " + f.getPath() + ".");
			e.printStackTrace();
			System.exit(3);
		} finally {
			if (lock != null) {
				lock.writeUnLock();
			}
		}
	}
	
	/**
	 * Check whether a file exists or not.
	 * @param fileName Path of the file to be checked for existence. 
	 *                 Can be either relative or absolute.
	 * @return True if the file exists.
	 */
	public static boolean exists(String fileName){
		return Files.exists(Paths.get(fileName));
	}
	
	/**
	 * Create a folder "endString.lock" to denote that different ".dat" files are locked.
	 * 
	 * @param dirPath Folder path where .lock file is put.
	 * @param endString	Folder name. Could be "db" "cand"
	 * @param print If true, print the instance index and "Locking" to stdout.
	 */
	public static void lock(String dirPath, String endString, boolean print){
		if (print) {
			System.out.println(Thread.currentThread().getName() + " Locking");
		}
		try {
			File f = new File(dirPath + HPC.FILESPTR + endString + ".lock");
			f.mkdir();
			boolean hasLock = f.exists();
			while (!hasLock) {
				TimeUnit.MILLISECONDS.sleep(500);
				f.mkdir();
				hasLock = f.exists();
			}
		} catch (InterruptedException e) {
		}
	}
	
	/*
	public static void wrapLock(String endString, String status){
		try {
			m_wrapFile = new File(m_WorkDirectory + HPC.FILESPTR + endString + ".lock");
			boolean hasLock = m_wrapFile.mkdir();
			while (!hasLock) {
				TimeUnit.MILLISECONDS.sleep(500);
				hasLock = m_wrapFile.mkdir();
			}
		} catch (InterruptedException e) {
		}
	}
	*/
	
	/**
	 * Remove the folder "endString.lock" to imply that the corresponding ".dat" files have been
	 * unlocked.
	 * 
	 * TODO: If the file dones't exist, it might stuck in infinite loop.
	 * 
	 * @param dirPath Folder path where .lock file is put.
	 * @param endString	Folder name. Could be "db" "cand"
	 * @param print If true, print the instance index and "Unlocking" to stdout.
	 */
	public static void unlock(String dirPath, String endString, boolean print){
		if (print) {
			System.out.println(Thread.currentThread().getName() + " Unlocking");
		}
		File f = new File(dirPath + HPC.FILESPTR + endString + ".lock");
		boolean isDeleted = f.delete();
		while (!isDeleted) {
			isDeleted = f.delete();
		}
	}
	
	
	
//	private static int getProcessorsPerNode(){
//		if(!m_ComputerName.equals("Windows")){
//			ProcessBuilder pb = new ProcessBuilder("lscpu");
//			pb.redirectErrorStream(true);
//			int cps = 0;
//			int sockets = 0;
//			try{
//				Process p = pb.start();
//				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
//				String line;
//				while((line = br.readLine()) != null){
//					if(line.contains("Socket(s): ")){
//						String[] outputs = line.replaceAll("\\s+","").split(":");
//						sockets = Integer.parseInt(outputs[outputs.length - 1]);
//					}
//					else if(line.contains("Core(s) per socket: ")){
//						String[] outputs = line.replaceAll("\\s+","").split(":");
//						cps = Integer.parseInt(outputs[outputs.length - 1]);
//					}
//					
//				}
//			}
//			catch(IOException e){
//				e.printStackTrace();
//				System.exit(1);
//			}
//			System.out.println("cps * sockets is : " + cps * sockets );
//			return cps * sockets;
//		}	
//		else {
//			return 4;
//		}
//	}
	
//	private static int getNumberOfNodes(int numProcessors){
//		int[] eleNums = Input.getEleNums();
//		double totalAtoms = (double) Tools.sum(eleNums);
//		System.out.println("TotalAtoms = " + totalAtoms);
//		System.out.println("ppn = " + numProcessors);
//		double ratio = totalAtoms/((double) numProcessors);
//		BigDecimal bd = new BigDecimal(ratio).setScale(0, RoundingMode.UP);
//		return (int) bd.doubleValue();
//	}
	
	public static String getLastFinishedCalcNum(){
	    // Read candidates.dat file in working dir, and return all the lines in a list.
		List<String> candidates = HPC.read(Candidates.CANDIDATES);
		String calcNum = null;
		for(int i = 0; i < candidates.size(); i++){
			String line = candidates.get(i);
			if(line.contains(" Finished")){
				calcNum = line.split("\\s+")[3];
			}
		}
		return calcNum;
	}
	
	/**
	 * Attempt to compose commands with proper parameters to evaluate energy of GA-generated
	 * structures. It guesses a command according to the input parameters: m_
	 * @return
	 */
	/*
	private static String findSubString(){
//		int numProcessors = HPC.getProcessorsPerNode();
//		int nodes = HPC.getNumberOfNodes(numProcessors);
//		int totalProcessors = (numProcessors * nodes); //Number of physical processors on node minus the 1 taken by Java;
//		System.out.println("totalProcessors is : " + totalProcessors);
//		String subString;
//		String host = Start.getHost();
//		if(m_ComputerName.equals("HHPC")){
//			if(Input.isSurfaceCalc()){
//				subString = "mpirun -np " + totalProcessors + " /home/fyuan6/Software/VASP/vasp.5.4.1-tst/vasp.5.4.1/bin/vasp_std > output.dat";
//			}
//			else{
//				subString = "mpirun -np " + totalProcessors + " /home/fyuan6/Software/VASP/vasp.5.4.1-tst/vasp.5.4.1/bin/vasp_gam > output.dat";
//			}
//		}
//		else if(m_ComputerName.equals("Topaz")){
//			if(Input.isSurfaceCalc()){
//				subString = "mpirun " + host + " " + totalProcessors + " /app/vasp/platforms/vasp-5.4.1/vasp";
//			}
//			else {
//				subString = "mpirun " + host + " -np " + totalProcessors + " /app/vasp/platforms/vasp-5.4.1/vasp_real";
//			}
//		}
//		else if(m_ComputerName.equals("Gordon")){
//			//subString = "aprun -b" + " /app/vaspapp/platforms/vasp-5.4.1/vasp_real > output.dat";
//			//subString = "/app/vaspapp/platforms/vasp-5.4.1/vasp_real > output.dat";
//			subString = "aprun -n " + (totalProcessors) + " /app/vaspapp/platforms/vasp-5.4.1/vasp_real";
//		}
//		
//		else if(m_ComputerName.equals("Copper")){
////			subString = "aprun -n " + (totalProcessors) + " /usr/local/applic/vasp/5.3.5/bin/vasp_real";
//			subString = "aprun -n 256 /usr/local/applic/vasp/5.3.5/bin/vasp_real";
//		}
//		else if(m_ComputerName.equals("Conrad")){
//			subString = "aprun -N " + (totalProcessors+1) + " -n " + (totalProcessors+1) + " /app/vaspapp/platforms/vasp-5.3.5/vasp_real";
//		}
//		else if(m_ComputerName.equals("MARCC")){
//			subString = "mpirun -np" + totalProcessors + " /scratch/groups/tmuelle5/Chenyang/vasp.5.4.1/vasp.5.4.1/bin/vasp_gam";
//		}
//		else if(m_ComputerName.equals("Stampede")){
//			subString = "ibrun -n " + totalProcessors + " vasp_gam";
//		}
//		else{
//			subString = null;
//		}
//		System.out.println("Subs-string is:  " + subString);
//		return subString;
	}
	*/
	
	/**
	 * A simple implementation of grep.
	 * 
	 * @param str The substring to be searched.
	 * @param File Source file.
	 * @return An array of strings representing lines containing str.
	 *         An empty array, [], if failed to read from the file or not a single line contains str.
	 */
	public static String[] grep (String str, String path) {
		ArrayList<String> targetLines = new ArrayList<>();
		List<String> lines = null;
		try {
			lines = HPC.readAllLines(path);
		} catch (IOException e) {
			e.printStackTrace();
			Logger.error("IO error when grepping from: " + path);
		}
		if (lines != null) {
			for (String l: lines) {
				if (l.contains(str)) {
					targetLines.add(l);
				}
			}
		}
		return targetLines.toArray(new String[0]);
	}
	
	/**
	 * Search the file for lines that contain substring(s) matching the regular expression. A list
	 * of line numbers starting from 1 is returned. A match of substring of a line is enough. Regex
	 * doesn't has to match the entire line.
	 * 
	 * Be careful to use calcDir. Better add whitespace around regex. Otherwise, "all_candidates/10"
	 * could be matched to "all_candidates/1".
	 * 
	 * @param regex
	 * @param path
	 * @return A list of line number containing substring(s) matching the regex. If no matches
	 *         are found, return an empty list (not null).
	 */
	public static List<Integer> grepLineNumbers (String regex, String path) {
		List<Integer> lineNumbers = new ArrayList<>();
		List<String> lines = HPC.read(path);
		Pattern p = Pattern.compile(regex);
		for (int i = 0; i < lines.size(); i++) {
			if (p.matcher(lines.get(i)).find()) { // substring matching, not entire line.
				lineNumbers.add(i + 1);
			}
		}
		return lineNumbers;
	}
	
	/**
	 * Grep the index-th field from give line. The index starts from 0.
	 * 
	 * Whitespaces are always removed before splitting by separator. If "" is given as separator,
	 * then only whitespaces are used.
	 * 
	 * @param line
	 * @param separator
	 * @param index -1 means the last term.
	 * @return
	 */
	public static String fields(String line, String separator, int index) {
		String[] terms;
		if (separator == "") {
			terms = line.split("\\s+");
		} else {
		    terms = line.replaceAll("\\s+", "").split(separator);
		}
		if (index >= terms.length) {
			Logger.error("Trying to get " + index + "-th fields from string \"" + line + "\"");
			return "";
		} else if (index == -1) {
			return terms[terms.length - 1];
		}
		return terms[index];
	}

	/**
	 * List path of both regular files and subdirectories under a path (recursively).
	 * 
	 * Note that the returned file name is relative if dir is a relative path, and is absolute 
	 * if dir is an absolute path. To exclude certain files, iterate over elements on the return
	 * object. The simpler a function is, the less buggy it would be.
	 * @param dir
	 * @param recursive
	 * @return
	 */
	public static List<String> listFiles(String dir, boolean recursive) {
		List<String> files = new ArrayList<>();
		int maxDepth = 1;
		if (recursive) {
			maxDepth = Integer.MAX_VALUE;
		}
		try {
			// List only files beneath the given folder. Exclude itself.
			Files.walk(Paths.get(dir), maxDepth)
				 .filter(path -> !path.equals(Paths.get(dir)))
				 .forEach(path -> files.add(path.toFile().getPath()));
		} catch (IOException e) {
			Logger.error("Failed to walk regular files under " + dir + ".");
			e.printStackTrace();
		}
		return files;
	}

	/**
	 * Add one file to the .zip file. If fileName is a directory, only the 
	 * directory, not its content, is added to the .zip file.
	 * 
	 * .zip file is created if absent.
	 * @param fileName
	 * @param zipFileName
	 * @throws IOException
	 */
	public static void zipFile(String fileName, String zipFileName) throws IOException {
		try(ZipOutputStream zos = new ZipOutputStream(
			                      new BufferedOutputStream(
								  new FileOutputStream(zipFileName)))) {
			Path path = Paths.get(fileName);
			if (Files.isRegularFile(path)) {
				zos.putNextEntry(new ZipEntry(fileName));
				Files.copy(path, zos);
			} else {
				zos.putNextEntry(new ZipEntry(fileName + "/")); // A directory.
			}
			zos.closeEntry();
		}
	}

	public static void zipFiles(List<String> fileNames, String zipFileName) throws IOException {
		try(ZipOutputStream zos = new ZipOutputStream(
			                      new BufferedOutputStream(
								  new FileOutputStream(zipFileName)))) {
			for (String fileName: fileNames) {
				Path path = Paths.get(fileName);
				if (Files.isRegularFile(path)) {
					zos.putNextEntry(new ZipEntry(fileName));
					Files.copy(path, zos);
				} else {
					zos.putNextEntry(new ZipEntry(fileName + "/")); // A directory.
				}
				zos.closeEntry();
			}
		}
	}

	/**
	 * Zip given list of files with entry name relativized to the parent parameter.
	 * 
	 * Both fileNames and parent can be either relative (to "user.dir") or absolute. They are 
	 * changed to absolute internally before relativization.
	 * @param fileNames
	 * @param zipFileName
	 * @param parent
	 * @throws IOException
	 */
	public static void zipFilesRelative(List<String> fileNames, String zipFileName, String parent) 
			throws IOException {
		// Prepare relative path as names of zip entries.
		List<String> relativeNames = new ArrayList<>(fileNames.size());
		Path parentDir = Paths.get(parent).toAbsolutePath();
		if (!Files.isDirectory(parentDir)) {
			throw new IOException("Failed to zip files. Parent is not a folder. " + parent);
		}
		for (String name: fileNames) {
			relativeNames.add(parentDir.relativize(Paths.get(name).toAbsolutePath()).toString());
		}

		// Zip files.
		try(ZipOutputStream zos = new ZipOutputStream(
			                      new BufferedOutputStream(
								  new FileOutputStream(zipFileName)))) {
			for (int i = 0; i < fileNames.size(); i++) {
				Path path = Paths.get(fileNames.get(i));
				if (Files.isRegularFile(path)) {
					// Copy content from absolute path and set relative path as entry name.
					zos.putNextEntry(new ZipEntry(relativeNames.get(i)));
					Files.copy(path, zos);
				} else {
					// Manually add "/" to imple a folder. Otherwise, it's treated as a file with 0 length.
					zos.putNextEntry(new ZipEntry(relativeNames.get(i) + "/"));
				}
				zos.closeEntry();
			}
		}
	}

	/**
	 * Zip all files under the path into the given .zip file. 
	 * 
	 * If path is a regular file, add it to .zip file. If path is a folder, 
	 * add itself and sub-files to .zip. Recusively zip sub-directorys as well 
	 * if recursive is true.
	 * 
	 * Add to .zip if exists, otherwise create one.
	 * 
	 * If relative is true, entry names in .zip will be relative to the 
	 * parameter path (i.e. path is stripped from the entry name.)
	 * @param path
	 * @param zipFileName
	 * @param recursive
	 * @param relative Zip files with relative name against path.
	 */
	public static void zipAllFiles(String path, String zipFileName, boolean recursive, boolean relativize) 
			throws IOException {
		if (Files.isRegularFile(Paths.get(path))) {
			zipFile(path, zipFileName);
			return;
		}
		
		try(ZipOutputStream zos = new ZipOutputStream(
								  new BufferedOutputStream(
								  new FileOutputStream(zipFileName)))) {
			int maxDepth = Integer.MAX_VALUE;
			if (!recursive) {
				maxDepth = 1;
			}
            
			Files.walk(Paths.get(path), maxDepth).forEach(filePath -> {
				try {
                    if (filePath.equals(Paths.get(path))) {
                        return;
					}
					// We can give whatever name for each zipped file. The
					// content of entry is through Files.copy(), and is not
					// affected by changes to entry name.
                    String entryName = filePath.toFile().getPath();
                    if (relativize) {
                        entryName = Paths.get(path).relativize(filePath).toFile().getPath();
                    }
					if (Files.isRegularFile(filePath)) {
						zos.putNextEntry(new ZipEntry(entryName));
						Files.copy(filePath, zos);
					} else {
						zos.putNextEntry(new ZipEntry(entryName + "/"));
					}
					zos.closeEntry();
				} catch (IOException e) {
					Logger.error("Failed to compress folder " + path + ".");
					e.printStackTrace(System.err);
				}
			});
		}
	}
	
	/**
	 * Unzip entries of .zip file into destDir.
	 * @param zipFileName
	 * @param destDir Destination folder to unzip .zip files into.
	 */
	public static void unzip(String zipFileName, String destDir) {
		Path destDirPath = Paths.get(destDir);
		try(ZipFile zipFile = new ZipFile(zipFileName)) {
			if (!Files.exists(destDirPath)) {
				Files.createDirectories(destDirPath);
			}
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();
				Path p = destDirPath.resolve(entry.getName()); // Entry name = path of the entry
				if (!Files.exists(p)) { // throws exception if recreate existing files.
					if (entry.isDirectory()) {
						Files.createDirectories(p);
					} else {
						// Mimic zip function: create parent folder if absent.
						if (!Files.exists(p.getParent())) {
							Files.createDirectories(p.getParent());
						}
						Files.copy(zipFile.getInputStream(entry), p);
					}
				}
			}
		} catch (IOException e) {
			Logger.error("Failed to unzip " + zipFileName + " into " + destDir);
			e.printStackTrace();
		}
	}

	/**
	 * A replacement for Files.readAllLines(path). IOException at closing often happens on
	 * supercomputers like MARCC. They could be safely ignored. This method suppresses IOException
	 * at closing but logs it in output. Other IOExceptions at reading are not ignored.
	 * @param file
	 * @throws IOException IO errors at places other than closing.
	 */
	public static List<String> readAllLines(String file) throws IOException {
		List<String> lines = new ArrayList<>();
		BufferedReader reader = null;
		// Similar to the implementation of Files.readAllLines();
		try {
			reader = new BufferedReader(new FileReader(file));
			for (;;) {
				String line = reader.readLine();
				if (line == null) {
					break;
				}
				lines.add(line);
			}
		} finally {
			// Propagate read errors upwards and only silently ignore exception at closing.
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ioe) {
					Logger.error("Silently ignore IO errors at closing after reading the file: " + file);
					ioe.printStackTrace();
				}
			}
		}
		return lines;
	}
}