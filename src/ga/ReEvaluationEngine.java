package ga;

import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

import ga.io.GAIOUtils;
import ga.io.Logger;
import ga.minimizer.LAMMPS;
import ga.multithreaded.Candidates;
import ga.multithreaded.NewPool;
import ga.structure.Cluster;
import matsci.structure.Structure;

public class ReEvaluationEngine implements Runnable {
	private static ReentrantLock lock = new ReentrantLock();
	private Queue<Cluster> clusters;
	private Candidates candidates;
	private NewPool pool;
	private int trainedTimes;
	
	public ReEvaluationEngine(Queue<Cluster> clusters, int trainedTimes, Candidates candidates, NewPool pool) {
		this.clusters = clusters;
		this.trainedTimes = trainedTimes;
		this.candidates = candidates;
		this.pool = pool;
	}
	
	/**
	 * Strategies of re-evaluation:
	 * 1. Changes are reflected in Start.m_Candidates through shared Cluster object.
	 * 2. Structures changes are reflected in-time on score_table and pool. MaxvolGrades.txt is also
	 * updated.
	 * 3. If re-evaluation failed or relaxed to unrealistic configuration, use old relaxed structure.
	 * 4. Retrain could be set to true if re-evaluation find more extrapolating clusters. But
	 * retraining only start after leaving re-evaluation Engine. (Engine.run() checks 
	 * Start.m_Retrain at the beginning, and skip execution if it's already true.)
	 * 5. When GA was stopped at re-evaluation and is restarted, all changes to candidates.dat will
	 * be viewed as done by Engine and new extrapolating clusters created at re-evaluation stage
	 * will be added to Start.m_NewData as they were added from Engine.run().
	 */
	public void run() {
		long st, et;
		while (!Thread.currentThread().isInterrupted()) {
			Cluster cluster;
			lock.lock();
			Logger.debug("Cluster list for re-evalution is locked.");
			try {
				if (clusters.isEmpty()) {
					break;
				}
				cluster = clusters.poll();
			} finally {
				Logger.debug("Cluster list for re-evalution is unlocked.");
				lock.unlock();
			}
			Logger.detail("Re-evaluating cluster-" + cluster.getCandidateIndex() + ".");
			int index = cluster.getCandidateIndex();
			String calcDir = cluster.getCalcDir();
			// Skip to next cluster if this cluster was relaxed by VASP.
			if (cluster.getEnergyMinimizer().getName().equals("VASP")) {
				Logger.detail("Cluster-" + index + " was relaxed by VASP. Skip evaluation by MTP.");
				continue;
			}
			
			// Back up old calculations.
			String oldCalcDir = calcDir + HPC.FILESPTR + (trainedTimes - 1);
			HPC.mkdir(oldCalcDir);
			HPC.copy(calcDir + HPC.FILESPTR + LAMMPS.getStructureFileName(false), oldCalcDir, false);
			HPC.mv(calcDir + HPC.FILESPTR + LAMMPS.getStructureFileName(true), oldCalcDir);
			HPC.mv(calcDir + HPC.FILESPTR + "graded_end.cfg", oldCalcDir);
			HPC.mv(calcDir + HPC.FILESPTR + LAMMPS.getOutputFileName(), oldCalcDir);
			HPC.rm(calcDir + HPC.FILESPTR + "end.cfg", false, false, true);
			HPC.rm(calcDir + HPC.FILESPTR + "stdout", false, false, true);

			// Create new Cluster object.
			Structure struct = LAMMPS.readStructure(calcDir, LAMMPS.getStructureFileName(false));
			Cluster newCluster = new Cluster(struct, calcDir); // calcDir is already updated.
			newCluster.setClusterType(cluster.getClusterType()); // Used in minimize() function.
			newCluster.setCandidateIndex(index);
			newCluster.updateStatusLineNumber();
			synchronized (candidates) {
				// Update reference to the new cluster.
				candidates.updateCandidatesList(index - 1, newCluster);
				newCluster.writeToCandidates();
				/*
				Logger.basic("Update tables for cluster-" + newCluster.getCandidateIndex());
				st = System.currentTimeMillis();
				Tables.newUpdate(newCluster);
				et = System.currentTimeMillis();
				Logger.detail("Updating tables for cluster-" + newCluster.getCandidateIndex() + " takes "
				            + GAIOUtils.formatElapsedTime(et - st));
				*/
			}

			// Perform re-evaluation.
			newCluster.setEnergyMinimizer("LAMMPS");
			newCluster.getEnergyMinimizer().createInputs(false); // Update potential.
			st = System.currentTimeMillis();
			boolean successful = newCluster.minimize();
			et = System.currentTimeMillis();
			Logger.detail("Reevaluating cluster-" + newCluster.getCandidateIndex() + " takes "
					    + GAIOUtils.formatElapsedTime(et - st));
			
			if (!newCluster.realistic() || !successful) {
				HPC.copy(oldCalcDir, calcDir, false);
				HPC.rm(oldCalcDir, true, true, true);
				newCluster = cluster;
			} else {
				newCluster.setRelaxed(true, false);
				Engine.checkExtrapolation(newCluster);
				// Don't stop re-evaluate if m_Retrain is true. Finish re-evaluate all clusters.
				if (newCluster.extrapolating()) {
					Engine.updateRetrainList(newCluster);
					continue;
				}
			}
			
			synchronized (candidates) {
				newCluster.writeToCandidates();
				
				/*
				Logger.basic("Update tables for cluster-" + newCluster.getCandidateIndex());
				st = System.currentTimeMillis();
				Tables.newUpdate(newCluster);
				et = System.currentTimeMillis();
				Logger.detail("Updating tables for cluster-" + newCluster.getCandidateIndex() + " takes "
				            + GAIOUtils.formatElapsedTime(et - st));
				*/
			}
			
			synchronized(pool) {
				Logger.basic("Update pool.dat.");
				st = System.currentTimeMillis();
				pool.update(newCluster, true);
				et = System.currentTimeMillis();
				Logger.detail("Updating pool for cluster-" + cluster.getCandidateIndex()
						+ " takes " + GAIOUtils.formatElapsedTime(et - st));
			}
		}
	}
}
