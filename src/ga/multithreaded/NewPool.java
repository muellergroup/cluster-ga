package ga.multithreaded;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import ga.HPC;
import ga.Input;
import ga.Start;
import ga.io.Logger;
import ga.structure.Cluster;
import ga.utils.GAMath;
import ga.utils.GAUtils;
import matsci.structure.Structure;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class NewPool {
	public static String POOL;
	private ArrayList<Cluster> pool = new ArrayList<>();
	private String m_PoolType = "Random";
	private double m_PoolNum = 1;
	private double m_Convergence = Double.MAX_VALUE;
	private ArrayList<String> m_SelectLines = new ArrayList<String>(pool.size());
	private static final String SELECTFORMAT = "Energy = %16.12f Index = %5d Fitness = %16.12f Selectability = %16.12f";
	private AtomicBoolean m_Converged = new AtomicBoolean(false);
	
	public NewPool() {
		POOL = Start.WorkDir + HPC.FILESPTR + "pool.dat";
		m_PoolType = Input.getPoolType();
		m_PoolNum = Input.getPopNum();
	}
	
	/**
	 * Recover pool from stopped calculations. Interconnection between m_Pool and m_Candidates 
	 * should be avoided to prevent dead lock. m_Candidates is used only as temporary variable.
	 */
	public NewPool(Candidates candidates) {
		POOL = Start.WorkDir + HPC.FILESPTR + "pool.dat";
		m_PoolType = Input.getPoolType();
		m_PoolNum = Input.getPopNum();
		
		List<String> lines = HPC.read(POOL);
		if (lines.size() > 0) {
			Logger.basic("Recovering pool list from pool.dat ...");
			pool = NewPool.recover(candidates, lines);
		} else { // pool.dat missing or empty;
			Logger.basic("pool.dat is missing. Recreating by comparing all candidates.");
			this.reset(candidates.getCandidates());
		}
	}
	
	/**
	 * Recover from stopped run by parsing candidates.dat.
	 */
	private static ArrayList<Cluster> recover(Candidates candidates, List<String> lines) {
		ArrayList<Cluster> clusters = new ArrayList<>(20);
		int stride = Input.getStride();
		// Remove incomplete blocks.
		if (lines.size() % stride != 0) {
			lines = lines.subList(0, (lines.size() / stride) * stride);
		}
		for (int i = 0; i < lines.size(); i += stride) {
			List<String> clusterLines = lines.subList(i, i + stride);
			String[] secondLine = clusterLines.get(1).trim().split("\\s+");
			if (secondLine.length != 6) {
				Logger.error("Incompelte pool.dat file: " + lines.get(1));
				System.exit(3);
			}
			String calcDir = secondLine[5];
			int candidateIndex = Cluster.calcDirToIndex(calcDir);
			Cluster cluster = candidates.getCandidate(candidateIndex - 1);
			cluster.setPoolIndex(i / stride);
			clusters.add(cluster);
		}
		return clusters;
	}
	
	/**
	 * @return the size of the underlying list
	 */
	public synchronized int getSize() {
		return pool.size();
	}
	
	public List<Cluster> getPool() {
		return pool;
	}

	/**
	 * Prepare a list per request. Avoid locking of pool when probing pool
	 * information.
	 */
	public List<Integer> getPoolIndices() {
		ArrayList<Integer> indices = new ArrayList<>(pool.size());
		for (Cluster c: pool) {
			indices.add(c.getCandidateIndex());
		}
		return indices;
	}
	
	/**
	 * Used to be compatible with legacy code. TODO: Should delete it some day.
	 * @return Content of pool.dat file as a list of lines.
	 */
	public List<String> getPoolDatLines() {
		return HPC.read(POOL);
	}
	
	/**
	 * Compare the given cluster with pool clusters, and decide whether update the pool.
	 * Situations that the pool will be updated:
	 * 	 1. c is unique. If pool.size() < m_PoolNum, add it to the pool.
	 *   2. c in unique & pool.size() >= m_PoolNum. If it has lower energy than that of the highest 
	 *      energy cluster in the pool, replace the most energetic one with the new cluster.
	 *   3. c is similar to several clusters (one or more)  in the pool. Replace the one either
	 *      very similar or has the highest replaceability among similar clusters.
	 *      
	 * Note: There are cases when Cluster C (E=-62) is similar to both A(ss=0.49, E=-61) and 
	 * B(ss=0.09, E=-63), but A and B are not similar (ss=0.52) (Assume SIMILARITYTHRESHOLD=0.5). 
	 * In such case, we shouldn't simply replace the simlar pool cluster A with highest energy, but
	 * replace the very similar B. Replaceability helps deal with such cases.
	 *      
	 * TODO: What is m_PoolIndex of Cluster? order of energies?
	 * 
	 * @param c
	 * @return
	 */
	public synchronized void update(Cluster cluster, boolean writeToDisk) {
		boolean inPool = false;
		if (GAUtils.contains(pool, cluster)) {
			inPool = true;
		}
		List<Cluster> similarClusters = this.getSimilarPoolClusters(cluster);
		// 3.
		if (similarClusters.size() > 0) {
			int targetIndex = -1;
			double[] ss = new double[similarClusters.size()]; // similarity score
			double[] dE = new double[similarClusters.size()];
			double[] replaceability = new double[similarClusters.size()];
			for (int i = 0; i < similarClusters.size(); i++) {
				//ss[i] = Tables.getScore(similarClusters.get(i).getCandidateIndex() - 1, 
				//		                cluster.getCandidateIndex() - 1);
				ss[i] = Start.SimCalctor.calculate(similarClusters.get(i).getStructure(),
						                           cluster.getStructure());
				dE[i] = similarClusters.get(i).getEnergy() - cluster.getEnergy();
			}
			
			if (GAMath.min(ss) < 0.1) {
				// We want to maximize geometric diversity. Therefore, always avoid duplicate
				// very similar clusters. After retraining, previously dissimilar clusters could
				// fall into same local minimum and become very similar.
				targetIndex = GAUtils.locateMin(ss);
				Cluster target = similarClusters.get(targetIndex);
				if (inPool) { // Keep one from the pair of very similar cluster.
					if (target.getEnergy() > cluster.getEnergy()) {
						pool.remove(target);
					} else if (target.getEnergy() == cluster.getEnergy()) {
						pool.remove((target.getCandidateIndex() < cluster.getCandidateIndex()) ?
								cluster : target);
					} else {
						pool.remove(cluster);
					}
				} else { // Replace
					if (target.getEnergy() > cluster.getEnergy()) {
						cluster.setPoolIndex(target.getPoolIndex());
						pool.set(pool.indexOf(target), cluster);
					}
				}
			} else if (!inPool) { 
				// Skip this part if current cluster is already in pool, since similar clusters
				// aren't the almost identical ones. Keep them wouldn't hurt.
				// Algorithm:
				// Calculate a replaceability score for each similar pool cluster. It's a weighted
				// score between similarity and energy. It aims to increase geometric diversity and
				// decrease energies of pool clusters. The more similar to current cluster, the 
				// higher replaceability. The higher the energy difference of similar pool cluster
				// to current cluster, the higher replaceability. Negative dE would decrease 
				// replaceability, since we favor lower energy ones.
				// Example: Current Cluster: E = -62.5; maxDiff = 2 eV.
				// Pool Cluster A: ss = 0.12 E = -61.0 -> rep = 1.260
				// Pool Cluster B: ss = 0.30 E = -62.0 -> rep = 0.475 
				// Pool Cluster C: ss = 0.49 E = -61.0 -> rep = 0.760
				// Pool Cluster D: ss = 0.49 E = -63.0 -> rep = -0.2399
				// Pool Cluster E: ss = 0.12 E = -63.0 -> rep = 0.260
				// Pool Cluster F: ss = 0.12 E = -62.0 -> rep = 1.010
				// This weighted score scheme is used to determine the most beneficial replacement
				// when there are multiple similar clusters, like B and C.
				double maxEDiff = GAMath.max(ArrayUtils.appendElement(this.getEnergies(), cluster.getEnergy())) 
				                - GAMath.min(ArrayUtils.appendElement(this.getEnergies(), cluster.getEnergy()));
				for (int i = 0; i < similarClusters.size(); i++) {
					// Both criteria are normalized. Term on ss should have a concave shape.
					// If ss = 0.49, then similarity isn't an important criterion for deciding 
					// whether to replace it.
					replaceability[i] = (1 - Math.sqrt(ss[i]/Input.getSimilarityThreshold()))
							          + dE[i] / maxEDiff; 
				}
				Cluster c = similarClusters.get(GAUtils.locateMax(replaceability));
				if (cluster.getEnergy() < c.getEnergy()) {
					cluster.setPoolIndex(c.getPoolIndex());
					pool.set(pool.indexOf(c), cluster);
				}
			}
		} else if (!inPool){ // unique new cluster
			if (pool.size() < m_PoolNum) {
				// 1.
				cluster.setPoolIndex(pool.size());
				pool.add(cluster);
			} else {
				// 2.
				int index = GAUtils.getHighestEnergyClusterIndex(pool); // list index
				Cluster c = pool.get(index);
				if (cluster.getEnergy() < c.getEnergy()) {
					cluster.setPoolIndex(c.getPoolIndex());
					pool.set(index, cluster);
				}
			}
		}
		if (writeToDisk) {
			this.write();
		}
	}
	
	/**
	 * Get the list of similar clusters from the pool, exlcluding the argument if it's already 
	 * in pool. Note, it returns the references to the clusters in pool.
	 * 
	 * @param cluster
	 * @return List of similar clusters.
	 */
	private List<Cluster> getSimilarPoolClusters(Cluster cluster) {
		ArrayList<Cluster> similarClusters = new ArrayList<>();
		for (Cluster c: pool) {
			/*
			String similarity = "false";
			// There are chances pool clusters were retrained on, and pool.dat is updated using
			// the VASP results.
			if (cluster.getCandidateIndex() != c.getCandidateIndex()) {
				similarity = Tables.getSimilarity(cluster.getCandidateIndex() - 1, c.getCandidateIndex() - 1);
			}
			if (similarity.equals("true")) {
				similarClusters.add(c); // Shallow copy.
			}
			*/
			if (cluster.getCandidateIndex() != c.getCandidateIndex()) {
				double score = Start.SimCalctor.calculate(cluster.getStructure(), c.getStructure());
				if (score <= Input.getSimilarityThreshold()) {
					similarClusters.add(c); // Shallow copy.
				}
			}
		}
		return similarClusters;
	}
	
	/**
	 * Write to pool.dat
	 */
	public synchronized void write() {
		Logger.debug("Write pool.dat to disk.");
		ArrayList<String> lines = new ArrayList<String>();
		for (Cluster c: pool) {
			Structure struct = c.getStructure();
			lines.add(Integer.toString(struct.numDefiningSites()));
			lines.add("Energy = " + c.getEnergy() + " Dir = " + c.getCalcDir());
			for (int i = 0; i < struct.numDefiningSites(); i++) {
				String specie = struct.getSiteSpecies(i).getSymbol();
				double[] coord = struct.getSiteCoords(i).getCartesianArray();
				lines.add(String.format("%s " + Cluster.COORDINATEFORMAT, specie, coord[0], coord[1], coord[2]));
			}
		}
		HPC.write(lines, NewPool.POOL, false);
	}

	/**
	 * GA is considered converged (less likely to give new unique structures) when the difference
	 * of the highest and lowest selectability of pool clusters is under a given threshold. This
	 * indicate the all pool clusters have generated reasonable amount of offsprings without
	 * finding better clusters in the sense of having lower total energy.
	 * 
	 * @return the difference of highest and lowest 
	 */
	public boolean isConverged() {
		if (pool.size() == 1) {
			return false; // s has only one term. So dS = 0.
		}
		double[] s = this.selectabilities(true);
		Arrays.sort(s);
		double dS2 = Double.MAX_VALUE;
		if (s.length != 0) {
			//m_Convergence = s[s.length - 1] - s[0]; 
			dS2 = s[s.length - 1] - s[0]; 
			m_Convergence = MSMath.average(s);
		}
		Logger.detail("Check convergence: dS = " + m_Convergence);
		Logger.debug("Convergence as difference between max and min: " + dS2);
		if (m_SelectLines.size() > pool.size()) {
			m_SelectLines.set(m_SelectLines.size() - 1, "dS = " + m_Convergence);
		} else {
			m_SelectLines.add("dS = " + m_Convergence);
		}
		HPC.write(m_SelectLines, Start.WorkDir + HPC.FILESPTR + "select.dat", false);
		m_Converged.set(m_Convergence < Input.getConv());
		return (m_Converged.get());
	}
	
	public double getConvergence() {
		return m_Convergence;
	}
	
	public boolean getConverged() {
		return m_Converged.get();
	}
	
	/**
	 * Selectability of pool[i] = fitness characterizing energy * regulated frequencies of it as parent.
	 * @return
	 */
	public double[] selectabilities(boolean updateSelectLines) {
		double[] energies = this.getEnergies();
		double[] fitnesses = this.fitnesses(ArrayUtils.copyArray(energies));
		double[] freqs = this.regulatedFrequencies();
		// TODO: According to current formula, lowest energy should generate about 7500 off-springs
		// to read a value of s below 0.1 (= regulatedFrequencies(7500) * fitness(0) = 0.0114 * 0.88)
		// However, higher energy clusters have smaller s, e.g. regulatedFrequencies(1000) * fitness(1) = 0.0034.
		double[] s = MSMath.arrayMultiply(fitnesses, freqs);
		if (updateSelectLines) {
			for (int i = 0; i < pool.size(); i++) {
				String line = String.format(SELECTFORMAT, energies[i], pool.get(i).getCandidateIndex(), 
						                    fitnesses[i], s[i]);
				if (m_SelectLines.size() <= i) {
					m_SelectLines.add(i, line);
				} else {
					m_SelectLines.set(i, line);
				}
			}
		}
		return s;
	}
	
	/**
	 * fitness of the i-th cluster in the pool is defined as:
	 *     f = 1/2 * (1 - tanh(2*x - 1)), 0 =< x =< 1
	 *     x = (Energy(i) - lowest_Energy) / (highest_Energy - lowest_Energy);
	 * 
	 * f is a monotonically decrease function. f(0) = 0.880797; f(1) = 0.119203. In other words,
	 * the lower the i-th cluster is on the energy spectrum, the larger its "fitness" is. 
	 * 
	 * @return An array of values representing "fitness" of pool clusters.
	 */
	public double[] fitnesses(double[] energies) {
		if (energies.length == 0) {
			return new double[] {};
		}
		double[] copy = ArrayUtils.copyArray(energies);
		Arrays.sort(energies); // Ascending order. energies[i] < energies[i+1]
		double energyRange = energies[energies.length - 1] - energies[0];
		if (energyRange == 0) {
			energyRange = Math.random() * 1E-20; // Avoid dividing by 0.
		}
		double[] fitness = new double[energies.length];
		for (int i = 0; i < copy.length; i++) {
			double f = 0.5 * (1 - Math.tanh(2. * ((copy[i] - energies[0]) / energyRange) - 1.));
			fitness[i] = f;
		}
		return fitness;
	}
	
	/**
	 * y = 1 /(1 + sqrt(frequency))
	 * 
	 * y is a monotonically decreasing function. y(0) = 1; y(100) = 0.090; y(1000) = 0.0306. The 
	 * higher the frequency that a pool cluster is chosen as parent, the smaller y is. The more
	 * converged the GA results are.
	 * 
	 * @return An array of the size pool.size() representing some regulated value of frequencies of
	 *         pool clusters been chosen as parent to generate child clusters.
	 */
	private double[] regulatedFrequencies(){
		int[][] nums = this.getNums();
		if (nums.length == 0) {
			return new double[] {};
		}
		double[] frequencies = new double[nums.length];
		for (int i = 0; i < frequencies.length; i++) {
			// nums[i][1] is the times that the i-th cluster in the pool is chosen as 
			// a parent cluster to generate a child candidate.
			frequencies[i] = 1 / (1 + Math.sqrt(nums[i][1]));
			//frequencies[i] = 1 / (1 + Math.sqrt(nums[i][0]));
		}
		return frequencies;
	}
	
	/**
	 * @return Array of energies of pool clusters.
	 */
	public double[] getEnergies() {
		double[] energies = new double[pool.size()];
		for (int i = 0; i < pool.size(); i++) {
			energies[i] = pool.get(i).getEnergy();
		}
		return energies;
	}
	
	/**
	 * nums[i][]: For the i-th cluster in the pool, get a pair of numbers representing "the number 
	 * of clusters among all candidates that is similar to the i-th cluster in the pool" and "the
	 * number of times that the i-th cluster in the pool is chosen as a parent to generate a child
	 * cluster."
	 * 
	 * TODO: add a "num_of_children" flag to pool clusters, instead of reading from .dat files.
	 * This allow inheritance of children from same cluster group, and smoother convergence.
	 * 
	 * @return
	 */
	private int[][] getNums() {
		// Each line of chosen.dat is: "child_cluster_calcDir = parent_cluster_calcDir"
		List<String> chosenLines = HPC.read(Cluster.CHOSEN);
		List<String> chosen = new ArrayList<String>(chosenLines.size());
		for (int i = 0; i < chosenLines.size(); i++) {
			String[] calcDirs = chosenLines.get(i).replaceAll("\\s+", "").split("=");
			chosen.add(calcDirs[calcDirs.length - 1]);
		}
		
		List<String> mutationLines = HPC.read(Cluster.MUTATION);
		List<Integer> mutatedFrom = new ArrayList<Integer>(mutationLines.size());
		for (int i = 0; i < mutationLines.size(); i++) {
			mutatedFrom.add(Integer.parseInt(mutationLines.get(i).trim().split("\\s+")[0]));
		}

		int[][] nums = new int[pool.size()][];
		for (int i = 0; i < pool.size(); i++) {
			String calcDir = pool.get(i).getCalcDir();
			
			//int similars = Collections.frequency(Tables.getSimilarityRow(calcDir), "true") 
			//		       + Collections.frequency(Tables.getSimilarityCol(calcDir), "true");
			int similars = 0; // Not used in present convergence score.
			
			//Both cross-over and mutation.
			int offsprings = Collections.frequency(chosen, calcDir)
						  + Collections.frequency(mutatedFrom, pool.get(i).getCandidateIndex());
			
			nums[i] = new int[] { similars, offsprings };
		}
		return nums;
	}
	
	/**
	 * Find the list of pool clusters and rewrite pool.dat from scratch. This is
	 * necessary after if cluster is re-evaluated. For example, after re-evaluated
	 * pool clusters after re-training potential.
	 */
	public synchronized void reset(List<Cluster> candidates) {
		Logger.basic("Reseting pool ...");
		// Recreate a list object.
		pool = new ArrayList<Cluster>((int) Input.getPopNum());
		for (int i = 0; i < candidates.size(); i++) {
			if (candidates.get(i).relaxed()) {
				this.update(candidates.get(i), false);
			}
		}
		this.write();
	}
	
	/**
	 * Check whether a cluster is in the current pool by its candidate index. This function doesn't
	 * compare structure, but only index.
	 */
	public synchronized boolean containsByIndex(int index) {
		for (Cluster c: pool) {
			if (index == c.getCandidateIndex()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @return Ratio of clusters evaluated by LAMMPS only in the entire pool. If it's larger than
	 *         Input.m_SelectPoolRatio, retraining starts.
	 */
	public synchronized double getNonVASPClusterRatio() {
		int counter = 0;
		for (Cluster c: pool) {
			if (!c.relaxedByVASP()) { counter++; }
		}
		// Don't use pool.size(), it's not m_PoolNum before pool has been filled up.
		return counter / m_PoolNum;
	}
}
