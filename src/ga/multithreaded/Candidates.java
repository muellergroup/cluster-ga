package ga.multithreaded;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.TreeMap;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import ga.HPC;
import ga.Input;
import ga.Start;
import ga.io.FileComparator;
import ga.io.Logger;
import ga.minimizer.EnergyMinimizer;
import ga.minimizer.LAMMPS;
import ga.minimizer.VASP;
import ga.structure.Cluster;
import matsci.Species;
import matsci.structure.Structure;

/**
 * Thread-safe type representing all generated candidates.
 * 
 * @author ywang393
 */
public class Candidates {
	
	public static final String CANDIDATES = Start.WorkDir + HPC.FILESPTR + "candidates.dat";
	public static final String ALLCANDIDATES = Start.WorkDir + HPC.FILESPTR + "all_candidates";
	private List<Cluster> candidates = new ArrayList<>(100);
	// Store unfinished cluster after restart. The same objects as the ones in "this.candidates"
	private Queue<Cluster> unFinishedCandidates = new LinkedList<>();
	
	public Candidates(boolean restart) {
		if (restart) {
			Candidates.examCandidatesDat();
			this.recover();
		}
	}
	
	/**
	 * Recover from candidates.dat. It should follow a call of examCandidatesDat()
	 */
	private void recover() {
		Logger.basic("Recovering candidates list from candidates.dat ...");
		List<String> lines = HPC.read(CANDIDATES);
		int stride = Input.getStride();
		// Remove incomplete blocks.
		if (lines.size() % stride != 0) {
			lines = lines.subList(0, (lines.size() / stride) * stride);
		}
		for (int i = 0; i < lines.size(); i += stride) {
			List<String> clusterLines = lines.subList(i, i + stride);
			Cluster cluster = new Cluster(clusterLines, "candidates");
			candidates.add(cluster);
			//if (!cluster.getStatus().contains("Finished")) {
			//	this.unFinishedCandidates.add(cluster);
			//}
		}
	}
	
	/**
	 * Check existence and completeness of candidates.dat file. If it's incomplete, repair it.
	 * 
	 * Possible situations to be dealt with:
	 *     1. candidates.dat and all_candidates contains same number of clusters, do nothing.
	 *     1. all_candidates exists but "candidates.dat" doesn't. Rebuild from scratch.
	 *     2. all_candidates and candidates.dat have different number of clusters.
	 *     		i. if all_candidates > candidates.dat, add corresponding clusters to candidates.dat
	 *     		ii. if all_candidates < candidates.dat, remove the redundant lines since we don't
	 *              have calculation result anyway. Just treat them as new clusters.
	 */
	private static void examCandidatesDat() {
		File candidatesDir = new File(ALLCANDIDATES);
		File candidatesFile = new File(CANDIDATES);
		int stride = Input.getStride();
		if (candidatesFile.exists()) {
			List<String> lines = HPC.read(CANDIDATES);
			if (lines.size() % stride != 0) {
				lines = lines.subList(0, (lines.size() / stride) * stride);
			}
			int numInFile = lines.size() / stride;
			int numInDir = 0;
			File[] dirs = candidatesDir.listFiles((file, name) -> name.matches("[0-9]+"));
			if (dirs != null) {
				numInDir = dirs.length;
			}
			
			if (numInFile > numInDir) {
				// 2.ii
				lines = lines.subList(0, numInDir * stride);
				HPC.write(lines, CANDIDATES, false);
			} else if (numInFile < numInDir) {
				// 2.i
				repairCandidatesDat(numInFile * stride);
			}
		} else if (candidatesDir.exists()) { 
			// 1. candidates.dat doesn't exist but all_candidates does.
			repairCandidatesDat(0);
		}
	}
	
	/**
	 * Complete candidates.dat from folders in all_candidates. It's called at the beginning
	 * of GA to tidy up candidates.dat from previous stopped run and prepares a clean
	 * candidates.dat for recover() to rebuild the cluster list.
	 * 
	 * @param startIndex The line number to start writing to.
	 */
	private static void repairCandidatesDat(int startLine){
		Logger.detail("Build candidates.dat from line: " + startLine);
		File[] f = new File(Candidates.ALLCANDIDATES).listFiles((file, name) -> name.matches("[0-9]+"));
		Arrays.sort(f, new FileComparator());
		
		List<String> candidateLines = HPC.read(Candidates.CANDIDATES);
		
		for(int i = startLine / Input.getStride(); i < f.length; i++){
			// Assume folders have continuous numbering. No candidates are missing.
			String path = f[i].getPath();
			String calDir = path;
            Logger.detail("Adding " + calDir + " to candidates.dat");
			
            List <String> output = new ArrayList<>();
            StringBuilder status = new StringBuilder("Running"); // default
            Structure struct = null;
            String minimizer = null;
            if (Input.getMode().equalsIgnoreCase("VASP")) {
            	struct = collectInfoFromVASP(calDir, output, status);
            	minimizer = "VASP";
            } else if (Input.getMode().equalsIgnoreCase("LAMMPS")) {
    			struct = collectInfoFromLAMMPS(calDir, output, status);
    			minimizer = "LAMMPS";
            } else if (Input.getMode().equalsIgnoreCase("Active")) {
            	if (Files.exists(Paths.get(calDir + HPC.FILESPTR + LAMMPS.getStructureFileName(false)))) {
            		struct = collectInfoFromLAMMPS(calDir, output, status);
            		minimizer = "LAMMPS";
            	} else if (Files.exists(Paths.get(calDir + HPC.FILESPTR + VASP.getInputFileName()))) {
            		// Copied from VASP calculation in retraining stage.
            		struct = collectInfoFromVASP(calDir, output, status);
            		minimizer = "VASP";
            	}
            }
            
            if (struct != null) {
            	List<String> calculationDetails = extractClusterFromDir(calDir, minimizer, output, 
            			status.toString(), struct);
            	candidateLines.addAll(calculationDetails);
            } else {
            	Logger.error("Could not recover structure from " + calDir + ". Exit.");
            	System.exit(3);
            }
		}
		HPC.write(candidateLines, Candidates.CANDIDATES, false);
	}
	
	/**
	 * Collect in a helper function to reuse the code. "output" and "status" are changed in
	 * the function.
	 * 
	 * @param calDir
	 * @param output
	 * @param status
	 * @param struct
	 */
	private static Structure collectInfoFromVASP(String calDir, List<String> output, StringBuilder status) {
		Structure struct = null;
		File outputFile = new File(calDir + HPC.FILESPTR + VASP.getOutputFileName()); // OUTCAR
		if (outputFile.exists()) {
			output.addAll(HPC.read(calDir + HPC.FILESPTR + VASP.getOutputFileName()));
		}
		status.replace(0, status.length(), EnergyMinimizer.getStatus(output, "Elapsed time", "Error"));
		if (status.toString().equals("Finished")) {
			// Read relaxed structure
			struct = VASP.readStructure(calDir, VASP.getStructureFileName(true));
		} else {
			// Read unrelaxed structure
			struct = VASP.readStructure(calDir, VASP.getStructureFileName(false));
		}
		return struct;
	}
	
	private static Structure collectInfoFromLAMMPS(String calcDir, List<String> output, StringBuilder status) {
		Structure struct = null;
		File outputFile = new File(calcDir + HPC.FILESPTR + LAMMPS.getOutputFileName());
		if (outputFile.exists()) {
			output.addAll(HPC.read(calcDir + HPC.FILESPTR + LAMMPS.getOutputFileName()));
		}
		status.replace(0, status.length(), EnergyMinimizer.getStatus(output, "Stopping criterion", "ERROR"));
		if (status.toString().equals("Finished")) {
			struct = LAMMPS.readStructure(calcDir, LAMMPS.getStructureFileName(true));
		} else {
			struct = LAMMPS.readStructure(calcDir, LAMMPS.getStructureFileName(false));
		}
		return struct;
	}
	
	/**
	 * Construct the lines representing a cluster in candidates.dat format.
	 * 
	 * @param output Lines of output file of calculations. e.g OUTCAR
	 * @param status Calculation status. "Finished", "NotFinished" and etc.
	 * @param dir Path to the folder containing a candidate cluster.
	 * @param struct Initial structure if not finished yet.
	 *               Relaxed structure if finished.
	 * @return A list of strings representing a cluster in candidates.dat.
	 */
	private static List<String> extractClusterFromDir(String dir, String minimizer, List<String> output, String status, Structure struct) {
		List<String> clusterLines = new ArrayList<>();
		// First Line
		clusterLines.add(Double.toString(struct.getCellVectors()[0].length()));
		// Second Line
		String type = "RBG"; // Type of GA Op is later recovered by Start.recoverParents().
		String energy = "0.0";
		if (status.equals("Finished")) {
			if (minimizer.equalsIgnoreCase("VASP")) {
				energy = String.valueOf(VASP.readEnergy(output));
			} else if (minimizer.equalsIgnoreCase("LAMMPS")) {
				energy = String.valueOf(LAMMPS.readEnergy(output));
			}
		}
		String str = type + " Dir = " + dir + " " + status;
		if (status.equals("Finished")) {
			str += " Energy = " + energy + " eV";
		}
		clusterLines.add(str);
		
		// Species and Cartesian coordinates lines
		for (int i = 0; i < struct.numDefiningSites(); i++) {
			Species sp = struct.getSiteSpecies(i);
			double[] array = struct.getSiteCoords(i).getCartesianArray();
			String coordStr = sp + " " + String.format(Cluster.COORDINATEFORMAT, array[0], array[1], array[2]);
			clusterLines.add(coordStr);
		}
		return clusterLines;
	}
	
	public synchronized void addToCandidatesList(Cluster c) {
		// Dummy object as place holder. Make sure the list is in correct order in multi-threading
		// runs.
		for (int i = candidates.size(); i < c.getCandidateIndex() - 1; i++) {
			this.candidates.add(new Cluster());
		}
		this.candidates.add(c);
	}
	
	public synchronized void updateCandidatesList(int index, Cluster c) {
		this.candidates.set(index, c);
	}
	
	/**
	 * Add a cluster to the unfinished candidate queue. Engines will use the next available 
	 * unfinished cluster in the queue, instead of generate a new one.
	 * 
	 * Duplication is checked.
	 * 
	 * @param c
	 */
	public synchronized void addToUnfinishedCandidates(Cluster c) {
		boolean seen = false;
		for (Cluster cluster: unFinishedCandidates) {
			if (cluster.getCandidateIndex() == c.getCandidateIndex()) {
				seen = true;
				break;
			}
		}
		if (!seen) {
			this.unFinishedCandidates.add(c);
		}
	}
	
	public synchronized int getSize() {
		return this.candidates.size();
	}
	
	public synchronized List<Cluster> getCandidates() {
		return this.candidates;
	}
	
	public synchronized Queue<Cluster> getUnFinishedCandidates() {
		return this.unFinishedCandidates;
	}
	
	public synchronized Cluster getCandidate(int listIndex) {
		return candidates.get(listIndex);
	}
	
	/**
	 * Write the entire list of candidates to candidates.dat.
	 */
	public synchronized void writeToFile() {
		List<String> lines = Cluster.toStringList(candidates);
		HPC.write(lines, CANDIDATES, false);
	}
	
	public synchronized static void writeToFile(List<Cluster> clusters) {
		List<String> lines = Cluster.toStringList(clusters);
		HPC.write(lines, CANDIDATES, false);
	}
	
	/**
	 * Append a list of clusters to candidates.dat.
	 * @param clusters
	 */
	public synchronized void appendToFile(List<Cluster> clusters) {
		List<String> lines = Cluster.toStringList(clusters);
		HPC.write(lines, CANDIDATES, true);
	}
	
	// TODO: what if in multithreading case, previous cluster hasn't been recorded yet.
	public synchronized void appendToFile(Cluster cluster) {
		List<String> lines = cluster.toStringList();
		HPC.write(lines, CANDIDATES, true);
	}
	
	/**
	 * Sort relaxed candidates in the order of their energies. The smaller the energy, the higher the
	 * rank ("Rank": 1 is higher in rank than 2). 
	 * 
	 * @return List<Integer> candidate indices ranked reversely in terms of their energy.
	 */
	public synchronized List<Integer> getSortedClusterIndices() {
		TreeMap<Double, Integer> energies = new TreeMap<>(); // A sorted map.
		for (Cluster c: candidates) {
			if (c.relaxed() || !Start.TrainedList.contains(c.getCandidateIndex())) {
				energies.put(c.getEnergy(), c.getCandidateIndex());
			}
		}
		ArrayList<Integer> rank = new ArrayList<>(energies.values());
		return rank;
	}
}
