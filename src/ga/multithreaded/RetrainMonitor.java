package ga.multithreaded;

public class RetrainMonitor {
	private int counter = 0;
	private boolean retrain = false;
	
	public RetrainMonitor() {}
	
	public synchronized boolean isRetrain() {
		return retrain;
	}
	
	public synchronized void setRetrain(boolean b) {
		this.retrain = b;
	}
	
	public synchronized void increment() {
		counter++;
	}
	
	public synchronized int getCounter() {
		return counter;
	}
	
	public synchronized void reset() {
		counter = 0;
		retrain = false;
	}
}
