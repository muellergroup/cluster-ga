package ga.multithreaded;

import matsci.io.app.log.Status;

public class ParallelStopper implements Runnable{
	private Thread t;
	
	public ParallelStopper(Thread thread) {
		t = thread;
	}
	
	public void run() {
		try {
			Status.basic("Stopping thread-" + t.getName());
			t.interrupt();
			t.join();
		} catch (InterruptedException e) {
			Status.error("Thread-" + t.getName() + " is interrupted during stopping!");
		}
	}
}
