package ga;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import matsci.structure.Structure;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class SimilarityCalculator2 {
  
  private boolean m_IgnoreSpecies;
  private boolean m_IgnoreScale;
  private Map<String,Double> m_nearest_neighbor_distance;;
  
  public SimilarityCalculator2(boolean ignoreSpecies, boolean ignoreScale, Map<String,Double> nearest_neighbor_distance) {
    m_IgnoreSpecies = ignoreSpecies;
    m_IgnoreScale = ignoreScale;
    m_nearest_neighbor_distance = nearest_neighbor_distance;
  }
	
	public double getScore(Structure s1, Structure s2) {
		return this.calculate(s1, s2);
	}
	
	protected void scaleDistanceMatrix(Matrix base, Matrix matrixToScale) {
	  
	  double baseSum = 0;
	  double matrixToScaleSum = 0;
	  for (int rowNum = 0; rowNum < base.getRowDimension(); rowNum++) {
	    for (int colNum = 0; colNum < base.getColumnDimension(); colNum++) {
	      baseSum += base.get(rowNum, colNum);
	      matrixToScaleSum += matrixToScale.get(rowNum, colNum);
	    }
	  }

	  double scaleFactor = baseSum / matrixToScaleSum;
	  
	  for (int rowNum = 0; rowNum < base.getRowDimension(); rowNum++) {
	    for (int colNum = 0; colNum < base.getColumnDimension(); colNum++) {
	      double value = matrixToScale.get(rowNum, colNum);
	      matrixToScale.set(rowNum, colNum, value * scaleFactor);
	    }
	  }
	  
	}
	/**
	 * 
	 * @param a
	 * @param b
	 * @return similarity score, or NaN if structure a or b are not contiguous
	 */
	public double calculate(Structure a, Structure b){
		Matrix distanceMatrix_a = this.getdm(a);
		Matrix distanceMatrix_b = this.getdm(b);
		if(distanceMatrix_a == null || distanceMatrix_b == null) {
			return Double.NaN;
		}
		if (m_IgnoreScale) {
		  this.scaleDistanceMatrix(distanceMatrix_a, distanceMatrix_b);
		}
		
		EigenvalueDecomposition e_a = distanceMatrix_a.eig();
		EigenvalueDecomposition e_b = distanceMatrix_b.eig();
		double[] eigenValues_a = e_a.getRealEigenvalues();
		double[] eigenValues_b = e_b.getRealEigenvalues();

		double[][] V_a = e_a.getV().getArray();
		double[][] V_b = e_b.getV().getArray();
		
		double[][][] epf_a = this.getEpf(eigenValues_a, V_a);
		double[][][] epf_b = this.getEpf(eigenValues_b, V_b);
		
		return this.getDistance(epf_a, epf_b);
	}
	
	private double getDistance(double[][][] a, double[][][] b){
		double[][] differences = new double[a.length][b.length];
		for(int i = 0; i < a.length; i++){
			double[][] atom1 = a[i];
			for(int j = 0; j < b.length; j++){
				double[][] atom2 = b[j];
//				double diff = this.integrate(atom1, atom2);  
//                double diff = this.integrate2(atom1, atom2);  
                double diff = this.integrate3(atom1, atom2); //TODO 
				//differences[i][j] = new BigDecimal(diff).round(new MathContext(3)).doubleValue();
				differences[i][j] = diff;
			}
		}
		double[] match = this.match(differences);
		return MSMath.average(match);
	}
	
	private double[] match(double[][] m){
		HungarianAlgorithm ha = new HungarianAlgorithm(m);
		int[] order = ha.execute();
		double[] match = new double[order.length];
		for(int i = 0; i < order.length; i++){
			match[i]= m[i][order[i]];
		}
		return match;
	}
	
	private double[][] getDifference(double[][][] func){
		double[][] differences = new double[func.length][func.length];
		for(int i = 0; i < func.length; i++){
			double[][] atom1 = func[i];
			for(int j = 0; j <= i; j++){
				double[][] atom2 = func[j];
				double diff = this.integrate(atom1, atom2);
				//differences[j][i] = differences[i][j] = new BigDecimal(diff).round(new MathContext(3)).doubleValue();
				differences[j][i] = differences[i][j] = diff;
			}
		}
		return differences;
	}
	
	private static double integrate2(double[][] atom1, double[][] atom2) {
	  
	  double[] xCoords = new double[atom1.length + atom2.length];
	  double[][] yCoords = new double[atom1.length + atom2.length][2];
	  double lastX = 0;
	  for (int stepNum = 0; stepNum < atom1.length; stepNum++) {
	    lastX += atom1[stepNum][0];
	    xCoords[stepNum] = lastX;
	    yCoords[stepNum][0] = atom1[stepNum][1];
	    yCoords[stepNum][1] = Double.NaN;
	  }
	  
	  lastX = 0;
	  for (int stepNum = 0; stepNum < atom2.length; stepNum++) {
	    lastX += atom2[stepNum][0];
	    xCoords[atom1.length + stepNum] = lastX;
	    yCoords[atom1.length + stepNum][0] = Double.NaN;
	    yCoords[atom1.length + stepNum][1] = atom2[stepNum][1];
  	  }
	  
	  xCoords = ArrayUtils.appendElement(xCoords, 0);
	  yCoords = ArrayUtils.appendElement(yCoords, new double[] {0, 0});
	  
      xCoords = ArrayUtils.appendElement(xCoords, 1.1); // Add a buffer to protect against numerical noise
      yCoords = ArrayUtils.appendElement(yCoords, new double[] {0, 0});
	  
	  int[] map = ArrayUtils.getSortPermutation(xCoords);
	  double score = 0;
	  for (int stepNum = map.length - 2; stepNum >= 0; stepNum--) {
	    int prevXIndex = map[stepNum + 1];
	    int xIndex = map[stepNum];
	    if (Double.isNaN(yCoords[xIndex][0])) {yCoords[xIndex][0] = yCoords[prevXIndex][0];}
        if (Double.isNaN(yCoords[xIndex][1])) {yCoords[xIndex][1] = yCoords[prevXIndex][1];}
      }
	  
	  for (int stepNum = 1; stepNum < map.length; stepNum++) {
	    int prevXIndex = map[stepNum - 1];
	    int xIndex = map[stepNum];
	    double xIncrement = xCoords[xIndex] - xCoords[prevXIndex];
	    double yValue1 = yCoords[xIndex][0];
	    double yValue2 = yCoords[xIndex][1];
	    score += xIncrement * Math.abs(yValue2 - yValue1);
	    if (Double.isNaN(score)) {
	      System.currentTimeMillis();
	    }
	  }
	  
	  return score;
	}

    private static double integrate3(double[][] atom1, double[][] atom2) {
        
        int lastIndex1 = 0;
        int lastIndex2 = 0;
        double lastX = 0;
        double lastX1 = 0;
        double lastX2 = 0;
        double returnValue = 0;
        for (int stepNum = 0; stepNum < atom1.length + atom2.length - 1; stepNum++) {
          double yDelta = Math.abs(atom1[lastIndex1][1] - atom2[lastIndex2][1]);
          double nextX1 = lastX1 + atom1[lastIndex1][0];
          double nextX2 = lastX2 + atom2[lastIndex2][0];
          double nextX = Math.min(nextX1, nextX2);
          returnValue += (nextX - lastX) * yDelta;
          if ((nextX1 < nextX2) || lastIndex2 == atom2.length - 1) {
            lastIndex1++;
            lastX1 = nextX1;
          } else {
            lastIndex2++;
            lastX2 = nextX2;
          }
          lastX = nextX;
        }
       
        return returnValue;
      }
	
	private static double integrate(double[][] atom1, double[][] atom2){
		double start = 0;
		int getter1 = 0;
		int getter2 = 0;
		double path1 = 0;
		double path2 = 0;
		double sum = 0;

		while((1. - path1 > 1E-3) || (1. - path2 > 1E-3)){
			double dS;
			double l1;
			double l2;
			if(path1 - path2 > 1E-7){
				dS = path2 - start;
				l2 = atom2[getter2][1];
				l1 = atom1[getter1][1];
				getter2++;
				double x2 = atom2[getter2][0];
				if(Math.abs(x2) <= 1E-4){
					x2 = 0.;
				}
				path2 += x2;
			}
			else if(Math.abs(path1 - path2) <= 1E-12){
				double x1 = atom1[getter1][0];
				double x2 = atom2[getter2][0];
				if(Math.abs(x1) <= 1E-7){
					x1 = 0.;
				}
				if(Math.abs(x2) <= 1E-7){
					x2 = 0.;
				}
				path1 += x1;
				path2 += x2;
				dS = 0;
				l2 = atom2[getter2][1];
				l1 = atom1[getter1][1];
				if(Math.abs(path1 - path2) <= 1E-12){
					getter1++;
					getter2++;
				}
			}
			else{
				dS = path1 - start;
				l1 = atom1[getter1][1];
				l2 = atom2[getter2][1];
				getter1++;
				double x1 = atom1[getter1][0];
				if(Math.abs(x1) <= 1E-4){
					x1 = 0.;
				}
				path1 += x1 ;
			}
			double integral = Math.abs((l2 - l1)*dS);
			start += dS;
			sum += integral;
		}
		return sum;
	}	
	
	private double[][][] getEpf(double[] eigenValues, double[][] eigenVectors) {
	  
	  // Eigenvectors are passed to us in column vector format
	  //eigenVectors = MSMath.transpose(eigenVectors);
	  
	  double tolerance = 1E-4;
	  int[] map = ArrayUtils.getSortPermutation(eigenValues);
	  
	  double[][] sortedEigenvectorsSquare = new double[eigenVectors[0].length][eigenVectors.length];
	  double[] sortedEigenvalues = new double[eigenValues.length];
	  
	  for (int valNum = 0; valNum < map.length; valNum++) {
	    sortedEigenvalues[valNum] = eigenValues[map[valNum]];
	    for (int coordNum = 0; coordNum < eigenVectors.length; coordNum++) { // We take the transpose to the vectors are in rows at the same time.
	      double value = eigenVectors[coordNum][map[valNum]];
	      sortedEigenvectorsSquare[valNum][coordNum] = value * value;
	    }
	    //sortedEigenvectors[valNum] = MSMath.arrayPower(eigenVectors[map[valNum]], 2); //MSMath.arrayAbs(eigenVectors[map[valNum]]);
	  }
	  
	  double prevEigenvalue = Double.POSITIVE_INFINITY;
	  for (int valNum = sortedEigenvalues.length - 1; valNum >= 0; valNum--) {
	    double eigenValue = sortedEigenvalues[valNum];
	    if (prevEigenvalue - eigenValue < tolerance) {
	      for (int coordNum = 0; coordNum < sortedEigenvectorsSquare[valNum].length; coordNum++) {
	        double vecCoord = sortedEigenvectorsSquare[valNum][coordNum];
	        double prevVecCoord = sortedEigenvectorsSquare[valNum + 1][coordNum];
	        sortedEigenvectorsSquare[valNum][coordNum] = prevVecCoord + vecCoord;
	      }
	      sortedEigenvectorsSquare = ArrayUtils.removeElement(sortedEigenvectorsSquare, valNum + 1);
	      sortedEigenvalues = ArrayUtils.removeElement(sortedEigenvalues, valNum + 1);
	    }
	    prevEigenvalue = eigenValue;
	  }
	  
	  double[][][] epf = this.epf(sortedEigenvectorsSquare, sortedEigenvalues);
      
      return epf;
      
	}
	
	private double[][][] epf(double[][] epa, double[] eigenValue){
	  double[][][] test = new double[epa[0].length][eigenValue.length][2];
	  for(int i = 0; i < test.length; i++){
	    for(int j = 0; j < test[i].length; j++){
	      test[i][j] = new double[]{epa[j][i], eigenValue[j]};
	    }
	  };
	  return test;
	}
	
	private HashMap<Double, List<double[]>> getMap(double[] eigenValues, double[][] eigenVectors){
		Map<Double, List<double[]>> map = new HashMap<>();
		eigenVectors = MSMath.transpose(eigenVectors);
		List<double[]> multiple = new ArrayList<>();
		for(int i = 0; i < eigenVectors.length; i++){
			double eigenValue = eigenValues[i];
			double[] eigenVector = eigenVectors[i];
			if(!map.keySet().contains(eigenValue) && !(map.keySet().size() == 0)){
				multiple.clear();
			}
			multiple.add(eigenVector);
			ArrayList<double[]> clone = new ArrayList<>(multiple);
			map.put(eigenValue, clone);
		}
		return (HashMap<Double, List<double[]>>) map;
	}
	
	/**
	 * 
	 * @param s
	 * @return distance matrix, or null if structure is not contiguous
	 */
	public Matrix getdm(Structure s){
	    
	    /*s = centerStructure(s);
	    StructureBuilder builder = new StructureBuilder(s);
	    builder.setVectorPeriodicity(new boolean[3]);
	    s = new Structure(builder);*/
	    
	    // Alternative is to use countContiguous to populate seenSites array and then run below loop over that array.
	    Structure.Site[] contiguousSites = getContiguousSites(s, 1.5*m_nearest_neighbor_distance.get(s.getDefiningSite(0).getSpecies().toString()));
	    if(contiguousSites == null) {
			return null;
		}
		
		double[][] dm = new double[s.numDefiningSites()][s.numDefiningSites()];
		for(int i = 0; i < s.numDefiningSites(); i++){
//			dm[i][i] = m_IgnoreSpecies ? 0 : s.getSiteSpecies(i).getElement().getAtomicNumber(); // Don't need this line for single-element clusters.  For multi-element clusters, this still might not be the best approach as it makes the score depend on the atomic numbers
			dm[i][i] = m_IgnoreSpecies ? 0 : contiguousSites[i].getSpecies().getElement().getAtomicNumber();
			for(int j = 0; j < i; j++){
				//dm[i][j] = dm[j][i] = new BigDecimal(s.getSiteCoords(i).distanceFrom(s.getSiteCoords(j))).round(new MathContext(4)).doubleValue();
//				dm[i][j] = dm[j][i] = s.getSiteCoords(i).distanceFrom(s.getSiteCoords(j));
				dm[i][j] = dm[j][i] = contiguousSites[i].getCoords().distanceFrom(contiguousSites[j].getCoords());
			}
		}
		return new Matrix(dm);
	}
	
   public static void fillSeenSites(Structure structure, Structure.Site currSite, Structure.Site[] seenSites, double scaled_nnDist) {

	     if (seenSites[currSite.getIndex()] != null) {
	    	 return;
	    	 }

	     seenSites[currSite.getIndex()] = currSite;

	     Structure.Site[] nearbySites = structure.getNearbySites(currSite.getCoords(), scaled_nnDist, false);

	     for (int siteNum = 0; siteNum < nearbySites.length; siteNum++) {
	    	 fillSeenSites(structure, nearbySites[siteNum], seenSites, scaled_nnDist);
	     }
	     
	     return;

	}
   
   /**
    * 
    * @param structure
    * @param scaled_nnDist
    * @return contiguous sites, or null if structure is not contiguous
    */
	public static Structure.Site[] getContiguousSites(Structure structure, double scaled_nnDist) {
		Structure.Site site = structure.getDefiningSite(0);
	    Structure.Site[] seenSites = new Structure.Site[structure.numDefiningSites()];
	    fillSeenSites(structure, site, seenSites,scaled_nnDist);
	    
    	//Return null if structure is not contiguous
	    for (int i = 0; i < seenSites.length; i++) {
		    if(seenSites[i]==null) {
		    	return null;
		    }
		}
	    	    
	    return seenSites;
	}
	
}
