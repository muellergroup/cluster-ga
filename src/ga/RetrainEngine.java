package ga;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

import ga.io.GAIOUtils;
import ga.io.Logger;
import ga.minimizer.LAMMPS;
import ga.minimizer.VASP;
import ga.multithreaded.NewPool;
import ga.potential.MTP;
import ga.structure.Cluster;
import ga.utils.GAUtils;
import matsci.structure.Structure;

/**
 * A class for parallelizing the DFT calculation of new training structures, either from 
 * the pool clusters in batch mode or a single outlier cluster determined by D-optimality.
 * 
 * Note:
 *   1. VASP start from MTP relaxed structure, instead of un-relaxed ones.
 *   2. History is stored in separate folder for reference.
 * @author ywang393
 */
public class RetrainEngine implements Runnable {
	private static ReentrantLock lock = new ReentrantLock();
	private static String trainDir;
	private static int trainedTimes;
	private Queue<Cluster> clusters;
	private List<Integer> trainedList;
	private List<String> CFGLines;
	private NewPool m_Pool;
	
	RetrainEngine(Queue<Cluster> clusters, List<Integer> trainedList, List<String> CFGLines, NewPool pool) {
		this.clusters = clusters;
		this.trainedList = trainedList;
		this.CFGLines = CFGLines;
		this.m_Pool = pool;
	}
	
	/**
	 * Retraining all cluster in the "clusters" list passed into constructor. New cluster object 
	 * will be created and used to run VASP, not the object stored in "clusters".
	 * 
	 */
	public void run() {
		long st, et;
		while(!Thread.currentThread().isInterrupted()) {
			Cluster cluster;
			lock.lock();
			Logger.debug("Cluster list for retraining is locked.");
			try {
				if (clusters.peek() == null) { break; }
				cluster = clusters.poll(); // Make the peek() and poll() atomic.
			} finally {
				Logger.debug("Cluster list for retraining is unlocked.");
				lock.unlock();
			}
			int index = cluster.getCandidateIndex();
			boolean inPool = m_Pool.containsByIndex(index);
			if (inPool) {
				Logger.basic("Retrain pool cluster-" + index + ".");
			} else {
				Logger.basic("Retrain cluster-" + index + ".");
			}
			List<String> newCFGLines = new ArrayList<>();
			if (!trainedList.contains(index)) {
				String oldCalcDir = cluster.getCalcDir();
				// Use the relaxed structure for re-train. A new structure copy, instead of that
				// of cluster stored in candidates.
				Structure struct;
				if (cluster.relaxed() && cluster.realistic()) {
					struct = LAMMPS.readStructure(oldCalcDir, LAMMPS.getStructureFileName(true));
				} else {
					struct = LAMMPS.readStructure(oldCalcDir, LAMMPS.getStructureFileName(false));
				}
				String newCalcDir = trainDir + HPC.FILESPTR + index;
				HPC.mkdir(newCalcDir);
				Cluster dataCluster = new Cluster(struct, newCalcDir); // Make sure we get a new object
				dataCluster.setClusterType(cluster.getClusterType());
				dataCluster.setCalcDir(newCalcDir);
				dataCluster.setEnergyMinimizer("VASP");
				dataCluster.getEnergyMinimizer().createInputs(false); // "false": Don't add cellSize/2.
				if (Input.isLigatedCluster() && Input.useStaticINCAR() && !inPool) {
					// Don't fully relaxed ligated clusters when they are not in the pool. 
					Logger.detail("Cluster is not in the pool. Perform static calculation only.");
					HPC.copy(Input.getRoot() + HPC.FILESPTR + "INCAR_static", 
							 newCalcDir + HPC.FILESPTR + "INCAR", false);
				}
				Logger.basic("Add to training data: cluster-" + index + ".");

				// Relaxation + MD:
				st = System.currentTimeMillis();
				dataCluster.minimize(); // already attempt to edit INCAR to fix the problem.
				et = System.currentTimeMillis();
				Logger.detail("VASP evaluation of cluster-" + dataCluster.getCandidateIndex() 
						+ " takes " + GAIOUtils.formatElapsedTime(et - st));

				st = System.currentTimeMillis();
				newCFGLines = MTP.prepareCFG(dataCluster.getCalcDir(), "RELAX");
				et = System.currentTimeMillis();
				Logger.detail("Preparing CFG of cluster-" + dataCluster.getCandidateIndex() 
	            			+ " takes " + GAIOUtils.formatElapsedTime(et - st));
				GAUtils.cleanFolder(dataCluster.getEnergyMinimizer().getName(), dataCluster.getCalcDir());
				
				// Requirements to run MD after relaxation:
				// 1. RUNMD = true;
				// 2. VASP evaluation has succeed without error;
				// 3. For bare cluster, always run;
				//    For ligated cluster, in the current pool or "USESTATICINCAR = false";
				if (Input.runMD() && dataCluster.relaxed() && (!Input.isLigatedCluster() || inPool || !Input.useStaticINCAR())) {
					Logger.detail("Performing MD to relaxed structure ...");
					VASP vasp = (VASP) dataCluster.getEnergyMinimizer();
					String MDDir = dataCluster.getCalcDir() + HPC.FILESPTR + "MD";
					HPC.mkdir(MDDir);
					st = System.currentTimeMillis();
					vasp.MD(MDDir, newCalcDir + HPC.FILESPTR + VASP.getStructureFileName(true), 
							Input.getRoot() + HPC.FILESPTR + "INCAR_MD", 
							Input.getRoot() + HPC.FILESPTR + "KPOINTS", 
							Input.getRoot() + HPC.FILESPTR + "POTCAR");
					et = System.currentTimeMillis();
					Logger.detail("Performing MD of cluster-" + dataCluster.getCandidateIndex() 
							+ " takes " + GAIOUtils.formatElapsedTime(et - st));
					st = System.currentTimeMillis();
					// Include all converged images with correct magnetization, no matter maxvol grade.
					newCFGLines.addAll(MTP.prepareCFG(MDDir, "MD"));
					et = System.currentTimeMillis();
					Logger.detail("Preparing CFG of cluster-" + dataCluster.getCandidateIndex() 
        						+ " after MD takes " + GAIOUtils.formatElapsedTime(et - st));
					GAUtils.cleanFolder(dataCluster.getEnergyMinimizer().getName(), MDDir);
				}
			} else if (Files.exists(Paths.get(trainDir + HPC.FILESPTR + index))) {
				// Restarting from retraining. This cluster has been evaluated by VASP. Re-prepare 
				// .cfg file in case it was not done before abortion.
				// Since this cluster has been added to trainedList, both relaxation and MD should
				// have finished.
				st = System.currentTimeMillis();
				Logger.basic("Already computed cluster-" + index + ". Read cfg images.");
				
				newCFGLines = MTP.prepareCFG(trainDir + HPC.FILESPTR + index, "RELAX");
				if (Input.runMD()) {
					newCFGLines.addAll(MTP.prepareCFG(trainDir + HPC.FILESPTR + index + HPC.FILESPTR + "MD", "MD"));
				}
				et = System.currentTimeMillis();
				Logger.detail("Preparing CFG of cluster-" + index + " takes " 
				            + GAIOUtils.formatElapsedTime(et - st));
			}
			lock.lock();
			Logger.debug("CFGLines is locked.");
			try {
				CFGLines.addAll(newCFGLines);
				if (!trainedList.contains(index)) {
					trainedList.add(index);
				}
			} finally {
				Logger.debug("CFGLines is unlocked.");
				lock.unlock();
			}

			Logger.basic("Finish retraining cluster-" + cluster.getCandidateIndex() 
			           + ". Elapsed: " + GAIOUtils.elapsedTime());
		}
	}
	
	public static void updateTrainDir (String dir) {
		trainDir = dir;
	}
	
	public static void updateTrainedTime (int times) {
		trainedTimes = times;
	}
	
}
