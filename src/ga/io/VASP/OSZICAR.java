package ga.io.VASP;

import java.util.ArrayList;
import java.util.List;

import ga.HPC;

/**
 * 
 * @author Yunzhe Wang
 * @version 2020-08-15
 */
public class OSZICAR {

	private int ENERGYTERM = 4; // Index of energy if the line containing E0 is split by 
	                            // whitespaces. 4 for relaxation, 8 for MD.
	private int MAGTERM = 9;    // Index of mag value if E0 line if split by whitespaces.
	
	private String path = null;
	private String type = "RELAX";         // "MD", "RELAX" (relax or static)
	private List<String> contents = null;  // List of lines in OSZICAR.
	private List<Integer> SCFSteps = null; // SCF steps in each ionic step.
	private List<Double> energies = null;  // E0. For "MD", it's 0K energy.
	private List<Double> magnetization = null; // "mag" in E0 line. Absent if ISPIN=1.
	                                           // Doesn't consider Noncollinear calculation, 
	                                           // in which mag is an array.
	private int numIonicStep = 0;
	
	/**
	 * 
	 * @param path
	 * @param type "MD" "RELAX" "STATIC"
	 */
	public OSZICAR(String path, String type) {
		if (type.equalsIgnoreCase("MD")) {
			ENERGYTERM = 8;
			MAGTERM = 16;
		}
		this.type = type;
		this.path = path;
		contents = HPC.read(path);
		SCFSteps = this.getSCFSteps();
		energies = this.getEnergies();
		magnetization = this.getMagnetizationList();
		numIonicStep = this.getNumIonicSteps();
	}
	
	public List<Integer> getSCFSteps() {
		if (SCFSteps != null) {
			return SCFSteps;
		}
		SCFSteps = new ArrayList<Integer>();
		for (int i = 0; i < contents.size(); i++) {
			if (contents.get(i).contains("E0")) {
				// One line above E0 must exist and non-empty.
				SCFSteps.add(Integer.parseInt(contents.get(i-1).split("\\s+")[1]));
			}
		}
		return SCFSteps;
	}
	
	public List<Double> getEnergies() {
		if (energies != null) {
			return energies;
		}
		energies = new ArrayList<Double>();
		numIonicStep = 0;
		for (String line: contents) {
			if (line.contains("E0")) {
				// If removed leading whitespaces, energy is the 5-th term.
				energies.add(Double.parseDouble(line.trim().split("\\s+")[ENERGYTERM]));
				numIonicStep++;
			}
		}
		return energies;
	}
	
	public int getNumIonicSteps() {
		if (numIonicStep > 0) {
			return numIonicStep;
		}
		numIonicStep = 0;
		for (String line: contents) {
			if (line.contains("E0")) {
				numIonicStep++;
			}
		}
		return numIonicStep;
	}
	
	public List<Double> getMagnetizationList() {
		if (magnetization != null) {
			return magnetization;
		}
		magnetization = new ArrayList<Double>();
		for (String line: contents) {
			if (line.contains("mag")) {
				magnetization.add(Double.parseDouble(line.trim().split("\\s+")[MAGTERM]));
			}
		}
		return magnetization;
	}
	
	/**
	 * @return The magnetization of last step.
	 */
	public double getMagetization() {
		return magnetization.get(magnetization.size() - 1);
	}
	
	public String getPath() {
		return path;
	}
	
	/**
	 * Reset to a new OSZICAR file. This allows recycling of objects and save GC work.
	 * @param path
	 * @return
	 */
	public void reSetFile(String path, String type) {
		if (type.equalsIgnoreCase("MD")) {
			ENERGYTERM = 8;
			MAGTERM = 16;
		}
		this.path = path;
		contents = HPC.read(path);
		SCFSteps = this.getSCFSteps();
		energies = this.getEnergies();
		magnetization = this.getMagnetizationList();
		numIonicStep = this.getNumIonicSteps();
	}
}
