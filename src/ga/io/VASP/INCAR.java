package ga.io.VASP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ga.HPC;
import ga.io.Logger;

/**
 * Interface with VASP INCAR file.
 * 
 * To retrieve actual parameters used for calculation, use getXXX(), which returns value set
 * in INCAR if present, otherwise, return default values.
 * 
 * When write to disk, only set values are written. Default values that are not set in original
 * file are not written.
 * 
 * Default mode is relaxation.
 * 
 * @author ywang393
 * @version 2020-08-15
 */
public class INCAR {
	
	private String path;
	private Map<String, String> parameters = null;
	
	// Initialization and setup
	private String SYSTEM = "";
	private int ISTART = 0;
	
	// Type of calculation
	private int ISPIN = 1;
	private boolean LNONCOLLINEAR = false;
	private boolean LSORBIT = false;
	private double[] MAGMOM = null;
	
	// Functional
	private String GGA = null;
	
	// Basis
	private String PREC = "Normal";
	private Double ENCUT = null;
	
	// Electronic Optimization
	private String ALGO = "Fast";
	private double EDIFF = 1E-4;
	private int NELM = 60;
	private int ISMEAR = 0;
	private double SIGMA = 0.2;
	
	// Ionic Relaxation
	private int NSW = 60;
	private int ISIF = 2;
	private double EDIFFG = 1E-3;
	private int IBRION = 2;
	private Double POTIM = Double.NaN;
	
	// IO
	private boolean LWAVE = false;
	private boolean LCHARG = false;
	
	// Performance Optimization
	private int NPAR = 1;
	private boolean LPLANE = false;
	private boolean LREAL = false;
	
	public INCAR(String path) {
		this.path = path;
		parameters = this.getParameters();
		parseParameters();
	}
	
	private void parseParameters() {
		ISPIN = parameters.containsKey("ISPIN") ? Integer.parseInt(parameters.get("ISPIN")) : ISPIN;
		LNONCOLLINEAR = parameters.containsKey("LNONCOLLINEAR") ? parseFortranBoolean(parameters.get("LNONCOLLINEAR")) : LNONCOLLINEAR;
		NSW = parameters.containsKey("NSW") ? Integer.parseInt(parameters.get("NSW")) : NSW;
		NELM = parameters.containsKey("NELM") ? Integer.parseInt(parameters.get("NELM")) : NELM;
		IBRION = parameters.containsKey("IBRION") ? Integer.parseInt(parameters.get("IBRION")) : IBRION;
		POTIM = parameters.containsKey("POTIM") ? Double.parseDouble(parameters.get("POTIM")) : POTIM;
		
		// Clean up;
		if (!parameters.containsKey("IBRION")) { 
			if (NSW == 0 || NSW == -1) {
				IBRION = -1;
			} else {
				IBRION = 0;
			}
		}
		
		if (!parameters.containsKey("POTIM")) {
			if (IBRION == 0 && POTIM.isNaN()) {
				Logger.error("MD calculation has to set POTIM!");
			} else if (IBRION >=1 && IBRION <= 3) {
				POTIM = 0.5;
			} else if (IBRION == 4 || IBRION == 5) {
				POTIM = 0.015;
			}
		}
	}
	
	public Map<String, String> getParameters() {
		if (parameters != null) {
			return parameters;
		}
		parameters = new HashMap<>();
		List<String> lines = HPC.read(this.path);
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i).trim();
			if (line.length() == 0 || line.startsWith("#") || line.startsWith("!")) { 
				continue; // emtpy line;
			}
			String[] terms = line.split("[=!#]");  // Split by either one of them.
			if (terms.length >= 2) {
				parameters.put(terms[0].trim(), terms[1].trim()); // Get rid of whitespaces around "=".
			}
		}
		return parameters;
	}
	
	/**
	 * @param key
	 * @return Unparsed value of the given key.
	 */
	public String getValue(String key) {
		String value = parameters.get(key);
		if (value == null) {
			Logger.error("INCAR doesn't contain this key: " + key);
		}
		return value;
	}
	
	/**
	 * @param key
	 * @param value
	 */
	public void setValue(String key, String value) {
		parameters.put(key, value);
	}
	
	public void setValue(Map<String, String> params) {
		for (String key: params.keySet()) {
			parameters.put(key, params.get(key));
		}
	}
	
	public void setLNONCOLLINEAR(boolean value) {
		LNONCOLLINEAR = value;
		parameters.put("NONCOLLINEAR", toStringFortranBoolean(value));
	}
	
	public int getISPIN() { return ISPIN; }
	
	public int getNELM() { return NELM; }
	
	public int getNSW() { return NSW; }
	
	public int getIBRION() { return IBRION; }
	
	public double getPOTIM() { return POTIM; }
	
	public void setPOTIM(double potim) {
		this.POTIM = potim;
		parameters.put("POTIM", String.valueOf(POTIM));
	}
	
	public void write() {
		this.write(this.path);
	}
	
	public void write(String path) {
		INCAR.write(this, path);
	}
	
	public static void write(INCAR incar, String path) {
		Map<String, String> parameters = incar.getParameters();
		List<String> lines = new ArrayList<String>();
		for (String key : parameters.keySet()) {
			lines.add(key + "=" + parameters.get(key));
		}
		HPC.write(lines, path, false);
	}
	
	private boolean isBooleanType(String key) {
		Object obj = parameters.get(key);
		if (obj.getClass() == Boolean.class) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean parseFortranBoolean(String value) {
		value.trim().replaceAll("\\.", ""); // Literal dot should be escaped.
		return Boolean.parseBoolean(value);
	}
	
	public static String toStringFortranBoolean(boolean value) {
		return "." + String.valueOf(value).toUpperCase() + ".";
	}
}
