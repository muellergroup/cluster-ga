package ga.io;

import ga.Start;
import matsci.io.app.log.Status;

/**
 * Wrapper of matsci.io.app.log. This class is thread-safe and added thread name to each line.
 * 
 * @author ywang393
 */
public class Logger {
	private static int DEBUG_LEVEL = 9;
	private static int LEVEL = 6; // Default is 6.
	private static boolean includeHeader = false; // Set true for GA usage.
	private Logger() {}
	
	public static void setIncludeHeader(boolean header) {
		includeHeader = header;
	}
	
	public static synchronized void detail(String s) {
		if (includeHeader) {
			Status.detail("Thread-" + Thread.currentThread().getName() + ": " + s);
		} else {
			Status.detail(s);
		}
	}
	
	public static synchronized void basic(String s) {
		if (includeHeader) {
			Status.basic("Thread-" + Thread.currentThread().getName() + ": " + s);
		} else {
			Status.basic(s);
		}
	}
	
	public static synchronized void error(String s) {
		if (includeHeader) {
			Status.error("Thread-" + Thread.currentThread().getName() + ": " + s);
		} else {
			Status.error(s);
		}
	}
	
	public static synchronized void warning(String s) {
		if (includeHeader) {
			Status.warning("Thread-" + Thread.currentThread().getName() + ": " + s);
		} else {
			Status.warning(s);
		}
	}
	
	public static synchronized void debug(String s) {
		if (LEVEL >= DEBUG_LEVEL) {
			if (includeHeader) {
				System.out.println("Thread-" + Thread.currentThread().getName() + ": " + s);
			} else {
				System.out.println(s);
			}
		}
	}
	
	public static void setLogLevelDebug() {
		Status.setLogLevelDetail();
		LEVEL = 9;
	}
	
	public static void setLogLevelDetail() {
		Status.setLogLevelDetail();
		LEVEL = 8;
	}
	
	public static void setLogLevelBasic() {
		Status.setLogLevelBasic();
	}
	
	public static void setLogLevelQuiet() {
		Status.setLogLevelQuiet();
		LEVEL = 0;
	}
	
}
