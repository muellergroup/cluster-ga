package ga.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ga.HPC;
import ga.Start;
import ga.structure.Cluster;

public class GAIOUtils {
	
	private static final String FORMATDOUBLE = "%-18.16f"; // Same format POSCAR
	public static final String LINESPTR = System.getProperty("line.separator");
	
	/**
	 * Convert a cluster candidate folder name to its candidate index.
	 * 
	 * @param calcDir
	 * @return candidate index.
	 */
	public static int convertToIndex(String calcDir){
		String[] split = calcDir.split(HPC.FILESPTR);
		return Integer.parseInt(split[split.length - 1]);
	}
	
	public static String toString(double[][] matrix) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < matrix.length; i++) {
			sb.append("[");
			for (int j = 0; j < matrix[i].length; j++) {
				sb.append(String.format(FORMATDOUBLE, matrix[i][j]));
				if (j < matrix[i].length - 1) {
					sb.append(", ");
				}
			}
			sb.append("]" + LINESPTR);
		}
		return sb.toString();
	}
	
	public static String toString(double[] vector) {
		StringBuilder sb = new StringBuilder("[");
		for (int i = 0; i < vector.length; i++) {
			sb.append(String.format(FORMATDOUBLE, vector[i]));
			if (i < vector.length - 1) {
				sb.append(",");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
	public static String toString(Object[] clusList) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < clusList.length; i++) {
			Object[] atom = (Object[]) clusList[i];
			String species = (String) atom[0];
			String coords = String.format(Cluster.COORDINATEFORMAT, (double) atom[1], (double) atom[2], (double) atom[3]);
			sb.append(species + " " + coords + HPC.LINESPTR);
		}
		return sb.toString();
	}
	
	public static String toString(Object[][] ligList) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ligList.length; i++) {
			sb.append(toString(ligList[i]));
		}
		return sb.toString();
	}
	
	public static String formatElapsedTime(long elapsedMills) {
		long hours = elapsedMills / (3600 * 1000);
		long leftMills = elapsedMills % (3600 * 1000);
		long minutes = leftMills / (60 * 1000);
		leftMills = leftMills % (60 * 1000);
		double seconds = leftMills / 1000.;
		String time = String.format("%1dh %1dm %4.2fs", hours, minutes, seconds);
		return time;
	}
	
	public static String elapsedTime() {
		double elapsedInMinute = (System.currentTimeMillis() - Start.GlobalStartTime) / (60000.0);
		return String.format("%.4f minutes", elapsedInMinute);
	}
	
}
