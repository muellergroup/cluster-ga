package ga.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileReader {

	private List<String> list = new ArrayList<>();
	private long fileLength = 0L;

	public List<String> getList() {
		return new ArrayList<>(list);
	}

	public void read(String string, boolean accountForSpace) throws Exception {
		if (string.length() > 0) {
			File file = new File(string);
			if (file.exists() && file.canRead()) {
				long length = file.length();
				if (fileLength < length) {
					readFile(file, fileLength);
					if (!accountForSpace) {
						fileLength = length;
					}
				}
			}
		} else {
			System.out.println("No file to read");
		}
	}

	private void readFile(File file, Long fileLength) throws IOException {
		String line = null;
		BufferedReader in = new BufferedReader(new java.io.FileReader(file));
		in.skip(fileLength);
		while ((line = in.readLine()) != null) {
			list.add(line);
		}
		in.close();
	}
}
