package ga.io.mtp;

import java.io.LineNumberReader;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.structure.Structure;
import matsci.structure.Structure.Site;
import matsci.structure.StructureBuilder;
import matsci.Element;
import matsci.Species;
import matsci.location.Coordinates;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import ga.structure.Cluster;

import java.io.PrintWriter;

/**
 * Class represent an image in .cfg file transferred from OUTCAR. Other formats are not 
 * supported now. Only "fixed" format is supported. Site_energy and charge are not read.
 * 
 * @author ywang393
 */
public class CFGImage {

	// Formatting
	private static final String SIZEFORMAT = "%d";
	private static final String LATTICEFORMAT = "%13.6f %13.6f %13.6f";
	private static final String IDFORMAT = "%10d";
	private static final String TYPEFORMAT = "%4d";
	private static final String COORDINATEFORMAT = " %13.6f %13.6f %13.6f";
	private static final String FORCEFORMAT = " %11.6f %11.6f %11.6f";
	private static final String ENERGYFORMAT = "%20.12f";
	private static final String STRESSFORMAT = " %11.5f %11.5f %11.5f %11.5f %11.5f %11.5f";
	
	// Instance variable
	// Essential structural info:
	private Vector[] m_CellVectors;
	private Coordinates[] m_SiteCoordinates;
	private int[] m_SiteAtomicTypes;     // start from 0.
	
	// Additional info, might be empty:
	private double[][] m_SiteForces = null;
	private Double m_Energy = null;
	private double[] m_Stresses = null;
	private ArrayList<String> m_Features = new ArrayList<>(); // " Feature   EFS_by   VASP"
	private Double m_MaxvolGrade = null;   // Maxvol grade can never be zero. Otherwise, the matrix is singular.
	private Element[] m_Elements = null;
	private Structure m_Structure = null;
	
	// Redundant but useful.
	private int m_NumOfSites;
	
	/**
	 * Note: only .cfg transformed from OUTCAR are supported right now.
	 * @param lines The sections in a .cfg file representing one image.
	 */
    public CFGImage (LineNumberReader reader) {
    	try {
    		String line = reader.readLine();
    		while(line != null) {
    			if (line.trim().equals("Size")) {
    				readNumOfSites(reader);
    			} else if (line.trim().equals("Supercell")) {
    				readCellVectors(reader);
    			} else if (line.contains("AtomData")) {
    				readSiteCoordinates(reader);
    			} else if (line.trim().equals("Energy")) {
    				readEnergy(reader);
    			} else if (line.contains("Stress")) {
    				readStresses(reader);
    			} else if (line.contains("Feature")) {
    				m_Features.add(line);
    				if (line.contains("MV_grade")) {
    					// The beginning term is also counted as one.
    					m_MaxvolGrade = Double.parseDouble(line.split("\\s+")[3]);
    				}
    			} else if (line.trim().equals("END_CFG")) {
    				break;
    			}
    			line = reader.readLine();
    		}
    	} catch (IOException e) {
    		e.printStackTrace();
    		System.exit(1);
    	}
    }
    
    public CFGImage (LineNumberReader reader, Element[] elements) {
    	this(reader);
    	m_Elements = elements;
    }
    
    public CFGImage (LineNumberReader reader, String[] elements) {
    	this(reader);
    	m_Elements = new Element[elements.length];
    	for (int i = 0; i < elements.length; i++) {
    		m_Elements[i] = Element.getElement(elements[i]);
    	}
    }
    
    public CFGImage(Cluster cluster) {
    	this(cluster.getStructure());
    }
    
    public CFGImage(Structure struct) {
    	m_CellVectors = struct.getCellVectors();
    	m_NumOfSites = struct.numDefiningSites();
    	m_SiteCoordinates = new Coordinates[m_NumOfSites];
    	m_SiteAtomicTypes = new int[m_NumOfSites];
    	List<Species> distinctSpecies = Arrays.asList(struct.getDistinctSpecies());
    	Species currSpecies = null;
    	for (int i = 0; i < m_NumOfSites; i++) {
    		m_SiteCoordinates[i] = struct.getDefiningSite(i).getCoords().getCartesianCoords();
    		currSpecies = struct.getDefiningSite(i).getSpecies();
    		m_SiteAtomicTypes[i] = distinctSpecies.indexOf(currSpecies);
    	}
    }
    
    private void readNumOfSites(LineNumberReader reader) throws IOException {
    	String line = reader.readLine();
    	m_NumOfSites = Integer.parseInt(line.trim());
    }
    
    private void readCellVectors(LineNumberReader reader) throws IOException {
    	Vector[] vectors = new Vector[3];
    	StringTokenizer st;
    	double[] coords = new double[3];
    	for (int i = 0; i < 3; i++) {
    		String line = reader.readLine();
    		st = new StringTokenizer(line);
    		for (int col = 0; col < 3; col++) {
    			coords[col] = Double.parseDouble(st.nextToken());
    		}
    		vectors[i] = new Vector(coords, CartesianBasis.getInstance());
    	}
    	m_CellVectors = vectors;
    }
    
    /**
     * Read m_SiteAtomicTypes, m_SiteForces, m_SiteCoordinates all together.
     * @param reader
     * @throws IOException
     */
    private void readSiteCoordinates(LineNumberReader reader) throws IOException {
    	m_SiteCoordinates = new Coordinates[m_NumOfSites];
    	m_SiteAtomicTypes = new int[m_NumOfSites];
    	String line; 
    	StringTokenizer st; 
    	double[] coords = new double[3];
    	for (int i = 0; i < m_NumOfSites; i++) {
    		line = reader.readLine();
    		st = new StringTokenizer(line);
    		st.nextToken(); // id.
    		m_SiteAtomicTypes[i] = Integer.parseInt(st.nextToken());
    		for (int col = 0; col < 3; col++) {
    			coords[col] = Double.parseDouble(st.nextToken());
    		}
    		m_SiteCoordinates[i] = new Coordinates(coords, CartesianBasis.getInstance());
    		if (st.hasMoreTokens()) {
    			if (m_SiteForces == null) { // Initialize only once
    				m_SiteForces = new double[m_NumOfSites][3];
    			}
	    		for (int col = 0; col < 3; col++) {
	    			m_SiteForces[i][col] = Double.parseDouble(st.nextToken());
	    		}
    		}
    	}
    }
    
    private void readEnergy(LineNumberReader reader) throws IOException {
    	String line = reader.readLine().trim();
    	m_Energy = Double.parseDouble(line);
    }
    
    private void readStresses(LineNumberReader reader) throws IOException {
    	m_Stresses = new double[6];
    	String line = reader.readLine();
    	StringTokenizer st = new StringTokenizer(line);
    	for (int i = 0; i < 6; i++) { // xx, yy, zz, yz, xz, xy
    		m_Stresses[i] = Double.parseDouble(st.nextToken());
    	}
    }
    
    public Double getEnergy() {
    	return m_Energy;
    }
    
    public Double getMaxvolGrade() {
    	return m_MaxvolGrade;
    }
    
    public void setElements(Element[] elements) {
    	m_Elements = elements;
    }
    
    public Structure toStructure() {
    	if (m_Structure == null) {
    		StructureBuilder sb = new StructureBuilder();
    		sb.setCellVectors(m_CellVectors);
    		for (int i = 0; i < m_SiteCoordinates.length; i++) {
    			int atomType = m_SiteAtomicTypes[i];
    			sb.addSite(m_SiteCoordinates[i], Species.get(m_Elements[atomType]));
    		}
    		StringBuilder strBuilder = new StringBuilder("Transformed from cfg. ");
    		int[] numSitesPerElement = this.numSitesPerElement();
    		for (int i = 0; i < m_Elements.length; i++) {
    			strBuilder.append(m_Elements[i].getSymbol() + "_" + numSitesPerElement[i]);
    			if (i < m_Elements.length - 1) {
    				strBuilder.append("_");
    			}
    		}
    		sb.setDescription(strBuilder.toString());
    		m_Structure = new Structure(sb);
    	}
    	return m_Structure;
    }
    
    /**
     * Write .cfg file according to MTP's formatting. 
     * Check "mlip/src/configuration.cpp: Save(ofstream &, unsigned int) const"
     */
    public void write(PrintWriter writer) throws IOException {
    	writeHeader(writer); // BEGIN_CFG
    	writeSize(writer);
    	writeCellVectors(writer);
    	writeSites(writer);
    	writeEnergy(writer);
    	writeStresses(writer);
    	writeFeature(writer);
    	writeEnd(writer); // END_CFG
    	writer.println(); // The blank line after each configuration.
    }
    
    // TODO: Add a toString() function.
    
    private void writeHeader(PrintWriter writer) {
    	writer.println("BEGIN_CFG");
    }
    
    private void writeSize(PrintWriter writer) {
    	writer.println(" Size");
    	writer.printf("    " + SIZEFORMAT, m_NumOfSites);
    	writer.println();
    }
    
    private void writeCellVectors(PrintWriter writer) {
    	writer.println(" Supercell");
    	double[] coords;
    	for (int i = 0; i < 3; i++) {
    		coords = m_CellVectors[i].getCartesianDirection();
    		writer.printf("    " + LATTICEFORMAT, coords[0], coords[1], coords[2]);
    		writer.println();
    	}
    }
    
    /**
     * Output the site coordinates and forces. Don't support direct coordinates (fractional), 
     * site energies, and site charges for now.
     * @param writer
     */
    private void writeSites(PrintWriter writer) {
    	if (m_SiteForces != null ) {
    		writer.println(" AtomData:  id type       cartes_x      cartes_y      cartes_z           fx          fy          fz");
    	} else {
    		writer.println(" AtomData:  id type       cartes_x      cartes_y      cartes_z");
    	}
    	for (int i = 0; i < m_NumOfSites; i++) {
    		writer.printf("    " + IDFORMAT, i + 1);
    		writer.printf(" " + TYPEFORMAT, m_SiteAtomicTypes[i]);
    		double[] coords = m_SiteCoordinates[i].getCartesianArray();
    		writer.printf(" " + COORDINATEFORMAT, coords[0], coords[1], coords[2]);
    		if (m_SiteForces != null) {
    			writer.printf(" " + FORCEFORMAT, m_SiteForces[i][0], m_SiteForces[i][1], m_SiteForces[i][2]);
    		}
    		writer.println();
    	}
    }
    
    private void writeEnergy(PrintWriter writer) {
    	if (m_Energy != null) {
    		writer.println(" Energy");
    		writer.printf("    " + ENERGYFORMAT, m_Energy);
    		writer.println();
    	}
    }
    
    private void writeStresses(PrintWriter writer) {
    	if (m_Stresses != null) {
	    	writer.println(" PlusStress:  xx          yy          zz          yz          xz          xy");
	    	writer.printf("    " + STRESSFORMAT, m_Stresses[0], m_Stresses[1], m_Stresses[2],
	    			                             m_Stresses[3], m_Stresses[4], m_Stresses[5]);
	    	writer.println();
    	}
    }
    
    private void writeFeature(PrintWriter writer) {
    	boolean wroteGrade = false;
    	for (String i : m_Features) {
    		writer.println(i);
    		if (i.contains("MV_grade")) {
    			wroteGrade = true;
    		}
    	}
    	if (!wroteGrade && m_MaxvolGrade != null) {
    		writer.println(" Feature   MV_grade	" + m_MaxvolGrade);
    	}
    }
    
    private void writeEnd(PrintWriter writer) {
    	writer.println("END_CFG");
    }
    
    private int[] numSitesPerElement() {
    	int[] numSitesPerElement = new int[m_Elements.length];
    	for (int i = 0; i < m_SiteAtomicTypes.length; i++) {
    		numSitesPerElement[m_SiteAtomicTypes[i]]++;
    	}
    	return numSitesPerElement;
    }
}
