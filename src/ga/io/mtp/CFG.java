package ga.io.mtp;

import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import ga.HPC;
import ga.SimilarityCalculator2;
import ga.io.Logger;
import ga.utils.GAUtils;
import ga.utils.app.CFGRegulator;

import java.io.FileWriter;
import java.io.PrintWriter;

import matsci.Element;
import matsci.io.app.log.Status;
import matsci.structure.Structure;

/**
 * A class represent the .cfg file. Internally, it decomposes the .cfg file into an array of 
 * CFGImages.
 * 
 * @author ywang393
 */
public class CFG {
	
	private String sourceFile;
	private ArrayList<CFGImage> images = new ArrayList<>(100);
	private boolean[] include; // TODO: change to List<Boolean>
	private double lowestEnergy = Double.POSITIVE_INFINITY;
	private int validImageSize = 0;
	private List<Structure> structures; // Don't contain energy, forces, and stresses info. Use CFGImage.
	private Element[] elements;
	// For local use only. In GA, use Start.simCalctor.
	private static SimilarityCalculator2 simCalctor = new SimilarityCalculator2(false, false, GAUtils.getNNDistanceMap());
	
	// Status flags.
	private boolean sortedByEnergy = false;
	private boolean sortedByMaxvolGrade = false;
	
	// Dummy constructor
	public CFG() {
		include = new boolean[0];
	}
	
	public CFG(String path) {
		sourceFile = path;
		try (LineNumberReader reader = new LineNumberReader(new FileReader(path))) {
			String line = reader.readLine();
			while (line != null) {
				if (line.trim().equals("BEGIN_CFG")) {
					CFGImage image = new CFGImage(reader);
					images.add(image);
					if (image.getEnergy() != null && image.getEnergy() < lowestEnergy) {
						lowestEnergy = image.getEnergy();
					}
				}
				line = reader.readLine(); // null if reach end of file.
			}
			validImageSize = images.size();
			include = new boolean[images.size()];
			Arrays.fill(include, true);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public CFG(String path, Element[] elements) {
		this(path);
		this.elements = elements;
		for (CFGImage i: images) {
			i.setElements(this.elements);
		}
	}
	
	// For format transformation. LAMMPS/POSCAR -> CFG (without force, stress and energy info).
	public CFG(Structure[] structures) {
		for (Structure s: structures) {
			images.add(new CFGImage(s));
		}
		validImageSize = images.size();
		include = new boolean[images.size()];
		Arrays.fill(include, true);
	}
	
	public CFG(Structure structure) {
		images.add(new CFGImage(structure));
		validImageSize = images.size();
		include = new boolean[images.size()];
		Arrays.fill(include, true);
	}
	
	public void add(CFGImage image) {
		images.add(image);
		validImageSize++;
		boolean[] newInclude = new boolean[images.size()];
		for (int i = 0; i < include.length; i++) {
			newInclude [i] = include[i];
		}
		newInclude[images.size() - 1] = true; // newly added ones.
		include = newInclude;
		if (image.getEnergy() != null) {
		    lowestEnergy = Math.min(lowestEnergy, image.getEnergy());
		}
	}
	
	public void add(List<CFGImage> newImages) {
		this.images.addAll(newImages);
		validImageSize += newImages.size();
		boolean[] newInclude = new boolean[images.size()];
		for (int i = 0; i < include.length; i++) {
			newInclude [i] = include[i];
		}
		for (int i = include.length; i < newInclude.length; i++) { // newly added ones.
			newInclude[i] = true;
		}
		include = newInclude;
		for (CFGImage image: newImages) {
		    if (image.getEnergy() != null) {
	            lowestEnergy = Math.min(lowestEnergy, image.getEnergy());
	        }
		}
	}
	
	public CFGImage getCFGImage(int index) {
		return images.get(index);
	}
	
	/**
	 * Retrieve list of images from underlying list. Note that given indices are list indices, 
	 * starting from 0. Also note that validity is not considered, i.e. an image will be retrieved
	 * even if it has been removed by functions like CFG.removeImages().
	 * @param indices List index of CFG images to retrieve.
	 * @return
	 */
	public List<CFGImage> getCFGImages(List<Integer> indices) {
		List<CFGImage> selectedImages = new ArrayList<>(indices.size());
		for (int i: indices) {
			selectedImages.add(images.get(i));
		}
		return selectedImages;
	}
	
	/**
	 * Retrieve list of images that are not included in the given list. 
	 * 
	 * This function can be used to collect test/validation data that are not used in training.
	 * Note that invalid images will not be included.
	 * 
	 * @param indices Indices of CFG images selected
	 * @return
	 */
	public List<CFGImage> getRemainingCFGImages(List<Integer> indices) {
	    List<CFGImage> remainingImages = new ArrayList<>(validImageSize - indices.size());
	    for (int i = 0; i < images.size(); i++) {
	        if (include[i] && !indices.contains(i)) {
	            remainingImages.add(images.get(i));
	        }
	    }
	    return remainingImages;
	}
	
	
	public int getLastValidImageIndex() {
		for (int i = include.length - 1; i >= 0; i--) {
			if (include[i]) { return i; };
		}
		return 0;
	}
	
	/**
	 * Recalculate the lowest energy of CFGs and return this value.
	 * 
	 * @return Lowest energy of the CFGs.
	 */
	public double updateLowestEnergy() {
	    for (CFGImage i: images) {
            if (i.getEnergy() != null && lowestEnergy < i.getEnergy()) {
                lowestEnergy = i.getEnergy();
            }
        }
	    return lowestEnergy;
	}
	
	/**
	 * Remove unconverged self-consistency steps from training data.
	 * 
	 * @param elecSteps List of self-consistency steps of each ionic step
	 * @param NELM Maximum scf steps in each set in INCAR
	 */
	public void removeUnconvergedImages(List<Integer> SCFSteps, int NELM) {
		int counter = 0;
		if (SCFSteps.size() != images.size()) {
			Logger.error("Different number of ionic steps! Failed to remove unconverged steps. "
					+ "SCFSteps: " + SCFSteps.size() + " CFGs: " + images.size());
			return;
		}
		for (int i = 0; i < SCFSteps.size(); i++) {
			if (SCFSteps.get(i) == NELM) { 
				include[i] = false;
				validImageSize--;
				counter++;
				Logger.debug("Exclude unconverged ionic step-" + i + ". SCFStep: " + SCFSteps.get(i));
			}
		}
		if (counter > 0) {
			Logger.basic("Removed " + counter + " unconverged ionic steps.");
		}
	}
	/**
	 * 
	 * @param window If the energy of an image is above this value from the lowest energy, this
	 *               image will not be leave out when writing output.
	 */
	public void removeHighEnergyImages(double window) {
	    this.updateLowestEnergy();
		double threshold = lowestEnergy + window;
		for (int i = 0; i < images.size(); i++) {
			Double thisEnergy = images.get(i).getEnergy();
			if (thisEnergy != null && thisEnergy > threshold) {
				include[i] = false;
				validImageSize--;
				Logger.debug("Exclude the " + (i+1) + "-th image: Energy: " + images.get(i).getEnergy() + ".");
			}
		}
	}
	
	/**
	 * Use magnetization of the last line as the correct magnetization of this configuration.
	 * @param magLines
	 */
	public void removeWrongMagImages(String[] magLines) {
		if (magLines.length >= 1) {
			ArrayList<Double> mags = CFGRegulator.extractMag(magLines);
			double magnetization = mags.get(mags.size() - 1);
			this.removeWrongMagImages(magnetization, mags);
		}
		// return if magLines is empty.
	}
	
	/**
	 * Allow user to specify a different magnetization as the last line of a OSZICAR file.
	 * @param magnetization
	 * @param mags
	 */
	public void removeWrongMagImages(double magnetization, List<Double> mags) {
		// Sanity check. The mags and images should belong to same calculation.
		if (mags.size() != images.size()) {
			Logger.error("OSZICAR and cfg file don't have same number of ionic steps! Exclude all configurations.");
			Arrays.fill(include, false);
		}
		
		if (mags.get(mags.size() - 1) != magnetization) {
			Arrays.fill(include, false);
			validImageSize = 0;
		} else {
			for (int i = 0; i < images.size(); i++) {
				if (mags.get(i) != magnetization) {
					include[i] = false;
					validImageSize--;
					Logger.debug("Exclude the " + (i + 1) + "-th image: Magnetization: " + mags.get(i) + ".");
				}
			}
		}
	}
	
	/**
	 * Remove all configurations with maxvol grade smaller than selectThreshold. If maxvol grade
	 * is null, don't remove (safer to have more training data than less).
	 * 
	 * @param selectThreshold
	 */
	public void removeIntrapolatingImages(double selectThreshold) {
		for (int i = 0; i < images.size(); i++) {
			if (include[i]) {
				if (images.get(i).getMaxvolGrade() != null) {
					include[i] = (images.get(i).getMaxvolGrade() >= selectThreshold);
					if (!include[i]) { validImageSize--; }
				}
			}
		}
	}
	
	/**
	 * Use a backward iteration to progressively remove similar images from the list. 
	 * It firstly add the last included image (ground state image by default), and then iterates 
	 * images in descending order of index. If similar to images already included, set include[i] to 
	 * false, otherwise true. And then continue the iteration.
	 * 
	 * To accelerate to procedure and make it an O(n) operation, it only checks the last included
	 * image for similarity. We assume here that during an VASP relaxation, if 
	 * sim_score(i, j) > threshold, then sim_score(i, j+n) > threshold. This assumption is generally
	 * right and can reduce computing complexity.
	 * 
	 * @param similarityThreshold
	 */
	public void removeSimilarImages(double similarityThreshold) {
		ArrayList<Integer> includedIndex = new ArrayList<> (images.size());
		for (int i = images.size() - 1; i >= 0; i-- ) {
			if (!include[i]) { continue; }
			if (includedIndex.size() == 0) {
				includedIndex.add(i);
			} else {
				Structure struct1 = images.get(i).toStructure();
				Structure struct2 = images.get(includedIndex.get(includedIndex.size() - 1)).toStructure();
				double score = simCalctor.calculate(struct1, struct2);
				if (score > similarityThreshold) {
					includedIndex.add(i);
				} else {
					include[i] = false;
					validImageSize--;
				}
			}
		}
	}
	
	/**
	 * Exclude the given list of indices. Note indices should be array index, i.e. start from 0.
	 * @param indices
	 */
	public void removeImages(List<Integer> indices) {
		for (int i: indices) {
			if (i > images.size()) {
				Logger.error("Trying to exclude " + i + "-th image, while there are only " + 
			                 images.size() + " images! Ignore this index.");
			} else {
				include[i] = false;
				validImageSize--;
			}
		}
	}
	
	/**
	 * 
	 * @param LET LowEnergyThreshold
	 */
	public void includeLowEnergyImages(double LET) {
		if (lowestEnergy == Double.POSITIVE_INFINITY) {
			this.updateLowestEnergy();
		}
		
		// If all images don't have energy values, lowestEnergy is still infinity.
		if (lowestEnergy != Double.POSITIVE_INFINITY) {
			double threshold = lowestEnergy + LET;
			for (int i = 0; i < images.size(); i++) {
				if (images.get(i).getEnergy() < threshold) {
					include[i] = true;
					validImageSize ++;
				}
			}
		} else {
		    Logger.basic("CFGs don't have energy values! Cannot re-include low-energy images with "
		               + String.format("threshold of %.2f eV.", LET));
		}
	}
		
	/**
	 * This will create a new list of CFGs sorted in energy. Irreversible. Original reading order 
	 * will be lost. Include[] is also updated accordingly.
	 */
	public void sortByEnergy() {
		SortedMap<Double, Integer> sortedMap = new TreeMap<>();
		for (int i = 0; i < images.size(); i++) {
			sortedMap.put(images.get(i).getEnergy(), i);
		}
		
		ArrayList<Integer> sortedIndices = new ArrayList<>(sortedMap.values());
		ArrayList<CFGImage> sortedCFGs = new ArrayList<>(images.size());
		boolean[] includeCopy = new boolean[include.length];
		System.arraycopy(include, 0, includeCopy, 0, include.length);
		for (int i = 0; i < sortedIndices.size(); i++) {
			sortedCFGs.add(images.get(sortedIndices.get(i)));
			include[i] = includeCopy[sortedIndices.get(i)];
		}
		images = sortedCFGs;
		sortedByEnergy = true;
		this.updateLowestEnergy();
	}
	
	public void sortByMaxvolGrade() {
		SortedMap<Double, Integer> sortedMap = new TreeMap<>();
		for (int i = 0; i < images.size(); i++) {
			sortedMap.put(images.get(i).getMaxvolGrade(), i);
		}
		
		ArrayList<Integer> sortedIndices = new ArrayList<>(sortedMap.values());
		ArrayList<CFGImage> sortedCFGs = new ArrayList<>(images.size());
		boolean[] includeCopy = new boolean[include.length];
		System.arraycopy(include, 0, includeCopy, 0, include.length);
		for (int i = 0; i < sortedIndices.size(); i++) {
			sortedCFGs.add(images.get(sortedIndices.get(i)));
			include[i] = includeCopy[sortedIndices.get(i)];
		}
		images = sortedCFGs;
		sortedByMaxvolGrade = true;
	}
		
	/**
	 * Index of selected CFG images of the underlying list. Note it calls sortByEnergy() and could
	 * potentially changed index correspondence.
	 * @param count
	 * @return
	 */
	public List<Integer> selectLowEnergyImages(int targetCount) {
		if (!sortedByEnergy) {
			this.sortByEnergy();
		}
		List<Integer> selected = new ArrayList<>(targetCount);
		int count = 0;
		
		int curIndex = 0;
		while (count < targetCount && curIndex <= include.length) {
			if (include[curIndex]) {
				selected.add(curIndex);
				count++;
			}
			curIndex++;
		}
		if (curIndex > images.size()) {
			Logger.warning("Input file don't have enough CFG images to satify target count: " + targetCount);
		}
		
		return selected;
	}
	
	/**
	 * Index of selected CFG images of the underlying list. Note it calls sortByEnergy() and could
	 * potentially changed index correspondence.
	 * @param count
	 * @return
	 */
	public List<Integer> selectHighEnergyImages(int targetCount) {
		if (!sortedByEnergy) {
			this.sortByEnergy();
		}
		List<Integer> selected = new ArrayList<>(targetCount);
		int count = 0;
		int curIndex = images.size() - 1;
		while (count < targetCount && curIndex >=0) {
			if (include[curIndex]) {
				selected.add(curIndex);
				count++;
			}
			curIndex--;
		}
		if (curIndex < 0) {
			Logger.warning("Input file don't have enough CFG images to satify target count: " + targetCount);
		}
		
		return selected;
	}
	
	/**
	 * Randomly select targetCount number of valid images (include[i] = true). Exclude the ones
	 * given in the list "exclude"
	 * @param targetCount
	 * @param exclude List of image indices to exclude from selection
	 * @return
	 */
	public List<Integer> selectRandomImages(int targetCount, List<Integer> exclude) {
		List<Integer> selected = new ArrayList<>(targetCount);
		Random random = new Random();
		int count = 0;
		while (count < targetCount && count < validImageSize) {
			int index = random.nextInt(images.size());
			// Forbid repeating.
			if (include[index] && !exclude.contains(index) && !selected.contains(index)) {
				selected.add(index);
				count++;
			}
		}
		if (count < targetCount) { // Exit loop because count == validImageSize
			Logger.warning("Not enough valid CFG image to choose from! Target count: " + targetCount
					     + ". Valid CFG Images: " + validImageSize + ".");
		}
		return selected;
	}
	
	/**
	 * For unit testing purposes. Reset all images to valid.
	 */
	public void reset() {
		Arrays.fill(include, true);
		validImageSize = images.size();
	}
	
	public int validImageNum() {
		return validImageSize;
	}
	
	/**
	 * Only export valid images.
	 * @return
	 */
	public List<Structure> getStructures () {
		if (structures == null) {
			structures = new ArrayList<Structure> (validImageSize);
			for (int i = 0; i < images.size(); i++) {
				if (include[i]) {
					structures.add(images.get(i).toStructure());
				}
			}
		}
		return structures;
	}
	
	public void setElements(Element[] elements) {
		this.elements = elements;
		for (CFGImage i: images) {
			i.setElements(this.elements);
		}
	}
	
	/**
	 * Write a subset of CFG images from the index start to end (exclusive) to output. Invalid images
	 * are excluded.
	 * 
	 * @param start Index in the list of images stored in this CFG object.
	 * @param end End index of the list of images to be written out. Exclusive.
	 * @param output
	 */
	public void writeFile(int start, int end, String output) {
		try (PrintWriter writer = new PrintWriter(new FileWriter(output))) {
			for (int i = start; i < end; i++) {
				if (include[i]) {
					images.get(i).write(writer);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * Write all valid CFG images to output.
	 * @param output
	 */
	public void writeFile(String output) {
		this.writeFile(0, images.size(), output);
	}
	
	public String log() {
		return "Have " + validImageSize + " images";
	}
}
