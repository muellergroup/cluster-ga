package ga.io.LAMMPS;
/*
 * Created on Jan 2, 2011
 *
 */

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Writer;
import java.util.StringTokenizer;

import matsci.Element;
import matsci.Species;
import matsci.io.app.log.Status;
import matsci.io.structure.IStructureFile;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.IStructureData;
import matsci.util.arrays.ArrayUtils;

public class LAMMPSDataFile_MTP implements IStructureFile {
  
  private String m_Description;
  private Vector[] m_CellVectors;
  
  private Species[] m_SiteSpecies;
  private Coordinates[] m_SiteCoordinates;
  private double[] m_AtomCharges;
  
  private boolean[] m_IsVectorPeriodic;
  
  private String m_AtomStyle = "charge";
  private Element[] m_AtomTypesInOrder;
  
  public LAMMPSDataFile_MTP(String fileName, Element[] types, boolean[] isVectorPeriodic) {
    
    m_AtomTypesInOrder = (Element[]) ArrayUtils.copyArray(types);
    m_IsVectorPeriodic = ArrayUtils.copyArray(isVectorPeriodic);
    
    try {
      LineNumberReader reader = new LineNumberReader(new FileReader(fileName));
      this.readHeader(reader);
      String line = this.nextMeaningfulLine(reader);
      while (line != null) {
        String canonicalLine = line.trim().toLowerCase();
        if (canonicalLine.equals("masses")) {
          this.readMasses(reader);
        } else if (canonicalLine.equals("atoms # atomic") || canonicalLine.equals("atoms")) {
          this.readSites(reader);
        } else if (canonicalLine.equals("velocities")) {
          // not implemented
          break;
        } else {
          throw new RuntimeException("Reading " + line + " from LAMMPS data file not yet implemented.");
        }
        line = this.nextMeaningfulLine(reader);
      }
      reader.close();
      
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
  }
  
  public void readSites(LineNumberReader reader) throws IOException {
    
    if (!m_AtomStyle.equals("charge")) {
      throw new RuntimeException("Reading atom style " + m_AtomStyle + " not implemented yet.");
    }
   
    String line = this.nextMeaningfulLine(reader);
    while (line != null && line.trim().length() > 0) {
      StringTokenizer st = new StringTokenizer(line);
      int index = Integer.parseInt(st.nextToken()) - 1;
      int elementIndex = Integer.parseInt(st.nextToken()) - 1;
      m_SiteSpecies[index] = Species.get(m_AtomTypesInOrder[elementIndex]);
      //MTP version of lammps don't output charge column.
      m_AtomCharges[index] = Double.parseDouble("0.0");
      double[] cartArray = new double[3];
      cartArray[0] = Double.parseDouble(st.nextToken());
      cartArray[1] = Double.parseDouble(st.nextToken());
      cartArray[2] = Double.parseDouble(st.nextToken());
      m_SiteCoordinates[index] = new Coordinates(cartArray, CartesianBasis.getInstance());
      line = reader.readLine();
    }
    
  }
  
  public void readMasses(LineNumberReader reader) throws IOException {
    
    // Ignore for now
    String line = this.nextMeaningfulLine(reader);
    while (line.trim().length() > 0) {
      line = reader.readLine();
    }
    
  }
  
  public void readHeader(LineNumberReader reader) throws IOException {
    
    m_Description = reader.readLine();
    reader.readLine();
    
    String line = reader.readLine();
    while (line.trim().length() > 0) {
      StringTokenizer st = new StringTokenizer(line);
      String value = st.nextToken();
      String key = st.nextToken("");
      String trimmedKey = key.trim().toLowerCase();
      if (trimmedKey.equals("atoms")) {
        int numSites = Integer.parseInt(value);
        m_SiteSpecies = new Species[numSites];
        m_SiteCoordinates = new Coordinates[numSites];
        m_AtomCharges = new double[numSites];
      } else if (trimmedKey.equals("atom types")) {
        break;
      } else {
        throw new RuntimeException("Reading " + key + " from LAMMPS data file header is not yet implemented.");
      }
      line = reader.readLine();
    }
    
    while (line.trim().length() == 0) {
      line = reader.readLine();
    }
    
    while (line.trim().length() > 0) {
      StringTokenizer st = new StringTokenizer(line);
      String value = st.nextToken();
      String key = st.nextToken("");
      if (key.trim().toLowerCase().equals("atom types")) {
        int numTypes = Integer.parseInt(value);
        if (numTypes != m_AtomTypesInOrder.length) {
          Status.warning(numTypes + " atom types in LAMMPS data file but " + m_AtomTypesInOrder.length + " types expected.");
        }
      } else {
        throw new RuntimeException("Reading " + key + " from LAMMPS data file header is not yet implemented.");
      }
      line = reader.readLine();
    }
    
    double[] originArray = new double[3];
    double[][] cartArray = new double[3][3];
    line = reader.readLine();
    while (line.trim().length() > 0) {
      StringTokenizer st = new StringTokenizer(line);
      if ((st.countTokens() %2 ) == 0) {
        String[] tokens = new String[st.countTokens()];
        for (int tokenNum = 0; tokenNum < tokens.length; tokenNum++) {
          tokens[tokenNum] = st.nextToken();
        }
        for (int tokenNum = tokens.length / 2; tokenNum < tokens.length; tokenNum++) {
          String key = tokens[tokenNum];
          double value = Double.parseDouble(tokens[tokenNum - tokens.length / 2]);
          if (key.equals("xlo")) {
            originArray[0] = value;
          } else if (key.equals("xhi")) {
            cartArray[0][0] += value;
          } else if (key.equals("ylo")) {
            originArray[1] = value;
          } else if (key.equals("yhi")) {
            cartArray[1][1] += value;
          } else if (key.equals("zlo")) {
            originArray[2] = value;
          } else if (key.equals("zhi")) {
            cartArray[2][2] += value;
          } else if (key.equals("xy")) {
            cartArray[1][0] += value;
          } else if (key.equals("xz")) {
            cartArray[2][0] += value;
          } else if (key.equals("yz")) {
            cartArray[2][1] += value;
          } 
        }
      } else {
        throw new RuntimeException("Can't interpret an odd number of tokens when defining lattice in LAMMPS data file.");
      }
      line = reader.readLine();
    }
    
    Coordinates origin = new Coordinates(originArray, CartesianBasis.getInstance());
    m_CellVectors = new Vector[3];
    for (int vecNum = 0; vecNum < m_CellVectors.length; vecNum++) {
      Coordinates head = new Coordinates(cartArray[vecNum], CartesianBasis.getInstance());
      m_CellVectors[vecNum] = new Vector(origin, head);
    }
    
  }
  
  public String nextMeaningfulLine(LineNumberReader reader) throws IOException {
    String line = reader.readLine();
    while (line != null && (line.trim().length() == 0)) {
      line = reader.readLine();
    }
    return line;
        
  }
  
  public LAMMPSDataFile_MTP(IStructureData structureData, boolean alphabetizeSpecies) {

    // Vectors must be rotated so that they are aligned with the Cartesian axis
    Vector[] oldCellVectors = structureData.getCellVectors();
    LinearBasis oldBasis = new LinearBasis(oldCellVectors);
    m_IsVectorPeriodic = structureData.getVectorPeriodicity();
    BravaisLattice givenLattice = new BravaisLattice(oldCellVectors, m_IsVectorPeriodic);
    BravaisLattice rotatedLattice = givenLattice.rotateToTriangularLattice();
    m_CellVectors = rotatedLattice.getCellVectors();
    LinearBasis newBasis = new LinearBasis(m_CellVectors);
    
    m_Description = structureData.getDescription();
    
    m_SiteSpecies = new Species[structureData.numDefiningSites()];
    m_SiteCoordinates = new Coordinates[structureData.numDefiningSites()];
    m_AtomTypesInOrder = new Element[0];
    int outSiteNum =0;
    for (int siteNum = 0; siteNum < m_SiteSpecies.length; siteNum++) {
      if (structureData.getSiteSpecies(siteNum) == Species.vacancy) {
        continue;
      }
      m_SiteSpecies[outSiteNum] = structureData.getSiteSpecies(siteNum);
      double[] directArray = structureData.getSiteCoords(siteNum).getCoordArray(oldBasis);
      m_SiteCoordinates[outSiteNum] = new Coordinates(directArray, newBasis);
      Element element = m_SiteSpecies[outSiteNum].getElement();
      if (!ArrayUtils.arrayContains(m_AtomTypesInOrder, element)) {
        m_AtomTypesInOrder = (Element[]) ArrayUtils.appendElement(m_AtomTypesInOrder, element);
      }
      outSiteNum++;
    }

    m_SiteCoordinates = (Coordinates[]) ArrayUtils.truncateArray(m_SiteCoordinates, outSiteNum);
    m_SiteSpecies = (Species[]) ArrayUtils.truncateArray(m_SiteSpecies, outSiteNum);
    
    if (alphabetizeSpecies) {
      alphabetizeElementOrder();
    }
    
  }
  
  protected void alphabetizeElementOrder() {
    
    String[] elementSymbols = new String[m_AtomTypesInOrder.length];
    for (int specNum = 0; specNum < elementSymbols.length; specNum++) {
      elementSymbols[specNum] = m_AtomTypesInOrder[specNum].getSymbol();
    }
    
    int[] map = ArrayUtils.getSortPermutation(elementSymbols);
    
    Element[] sortedElements = new Element[m_AtomTypesInOrder.length];
    
    for (int specNum= 0; specNum < map.length; specNum++) {
      sortedElements[specNum] = m_AtomTypesInOrder[map[specNum]];
    }
    
    m_AtomTypesInOrder = sortedElements;
    
  }

  
  public LAMMPSDataFile_MTP(IStructureData structureData) {
    this(structureData, false);
  }

  public String getDescription() {
    return m_Description;
  }

  public Vector[] getCellVectors() {
    return (Vector[]) ArrayUtils.copyArray(m_CellVectors);
  }

  public boolean[] getVectorPeriodicity() {
    return ArrayUtils.copyArray(m_IsVectorPeriodic);
  }

  public int numDefiningSites() {
    return m_SiteSpecies.length;
  }

  public Coordinates getSiteCoords(int index) {
    return m_SiteCoordinates[index];
  }

  public Species getSiteSpecies(int index) {
    return m_SiteSpecies[index];
  }
  
  public void writeFile(String fileName) {
    
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
      this.write(writer);
      writer.flush();
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
  }
  
  public void write(Writer writer) {
    
    try {
      this.writeDescription(writer);
      this.newLine(writer);
      this.writeAtomCounts(writer);
      this.newLine(writer);
      this.writeLattice(writer);
      this.newLine(writer);
      this.writeMasses(writer);
      this.newLine(writer);
      this.writeSites(writer);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
  }
  
  protected void writeDescription(Writer writer) throws IOException {
    
    writer.write(m_Description);
    this.newLine(writer);
    
  }
  
  protected void writeAtomCounts(Writer writer) throws IOException {
    
    writer.write(this.numDefiningSites() + " atoms");
    this.newLine(writer);
    this.newLine(writer);

    writer.write(m_AtomTypesInOrder.length + " atom types");
    this.newLine(writer);
    
  }
  
  protected void writeLattice(Writer writer) throws IOException {
    
    double xHi = m_CellVectors[0].getCartesianDirection()[0];
    double yHi = m_CellVectors[1].getCartesianDirection()[1];
    double zHi = m_CellVectors[2].getCartesianDirection()[2];
    double xy = m_CellVectors[1].getCartesianDirection()[0];
    double xz = m_CellVectors[2].getCartesianDirection()[0];
    double yz = m_CellVectors[2].getCartesianDirection()[1];
    
    if (xHi < 0) {
      xHi *= -1;
    }
    
    if (yHi < 0) {
      yHi *= -1;
      xy *= -1;
    }
    
    if (zHi < 0) {
      zHi *= -1;
      xz *= -1;
      yz *= -1;
    }
    
    
    // LAMMPS requires the vectors meet these compactness criteria
    xz -= Math.round(yz / yHi) * xy;
    yz -= Math.round(yz / yHi) * yHi;
    
    xz -= Math.round(xz / xHi) * xHi;
    xy -= Math.round(xy / xHi) * xHi;
    
    writer.write("0 " + xHi + " xlo xhi");
    this.newLine(writer);
    writer.write("0 " + yHi + " ylo yhi");
    this.newLine(writer);
    writer.write("0 " + zHi + " zlo zhi");
    this.newLine(writer);
    writer.write(xy + " " + xz + " " + yz + " xy xz yz");
    this.newLine(writer);
    
  }
  
  protected void writeMasses(Writer writer) throws IOException {
    
    writer.write("Masses");
    this.newLine(writer);
    this.newLine(writer);
    
    for (int typeNum = 0; typeNum < m_AtomTypesInOrder.length; typeNum++) {
      writer.write((typeNum + 1) + " " + m_AtomTypesInOrder[typeNum].getAtomicWeight());
      this.newLine(writer);
    }
    
  }
  
  protected void writeSites(Writer writer) throws IOException {
    
    writer.write("Atoms");
    this.newLine(writer);
    this.newLine(writer);
    
    if (m_AtomStyle.equals("charge")) {
      this.writeSitesChargeStyle(writer);
    } else {
      throw new RuntimeException("Writing atom with style " + m_AtomStyle + " is not implemented yet");
    }
    
  }
  
  protected void writeSitesChargeStyle(Writer writer) throws IOException {
    
    for (int siteNum = 0; siteNum < m_SiteSpecies.length; siteNum++) {
      Element element = m_SiteSpecies[siteNum].getElement();
      int atomTypeNum = ArrayUtils.findIndex(m_AtomTypesInOrder, element);
      //MTP lammps doesn't allow charge column
      //double charge = (m_AtomCharges == null) ? 0 : m_AtomCharges[siteNum];
      double[] cartesianArray = m_SiteCoordinates[siteNum].getCartesianArray();
      writer.write((siteNum + 1) + " ");
      writer.write((atomTypeNum + 1) + " ");
      //writer.write(charge + " ");
      writer.write(cartesianArray[0] + " " + cartesianArray[1] + " " + cartesianArray[2]);
      this.newLine(writer);
    }
    
  }
  
  protected void newLine(Writer writer) throws IOException {
    writer.write("\n");
  }

  public IStructureFile create(IStructureData structureData) {
    return new LAMMPSDataFile_MTP(structureData);
  }

}

