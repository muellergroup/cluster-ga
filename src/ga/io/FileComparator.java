package ga.io;

import java.io.File;
import java.util.Comparator;

public class FileComparator implements Comparator<File> {

	public FileComparator() {} // Dummy constructor.
	
	/**
	 * Folders in "all_candidates" are named by the candidate index of containing cluster. Rank
	 * folders by the candidate indices.
	 */
	public int compare(File f1, File f2) {
		try {
			int i1 = Integer.parseInt(f1.getName());
			int i2 = Integer.parseInt(f2.getName());
			return i1 - i2;
		} catch (NumberFormatException e) {
			throw new AssertionError(e);
		}
	}

	public int compare(String file1, String file2) {
		File f1 = new File(file1);
		File f2 = new File(file2);
		return compare(f1, f2);
	}
}
