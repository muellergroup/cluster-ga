package ga;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ejml.simple.SimpleEVD;
import org.ejml.simple.SimpleMatrix;

import ga.io.Logger;
import ga.structure.Cluster;
import matsci.Species;
import matsci.io.vasp.POSCAR;
import matsci.structure.Structure;
import matsci.structure.Structure.Site;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class SimilarityCalculator {
	private boolean m_isLigatedCluster = Input.isLigatedCluster();
	private static final double SIMILARITYTHRESHOLD = Input.getSimilarityThreshold();
	
	public double getScore(Cluster a, Cluster b){
		Structure s1, s2;
		if (m_isLigatedCluster) {
			s1 = a.getStructureWithoutSideChains();
			s2 = b.getStructureWithoutSideChains();
		} else {
			s1 = a.getStructure();
			s2 = b.getStructure();
		}
		return new BigDecimal(this.calculate(s1, s2)).round(new MathContext(4)).doubleValue();
	}
	
	/**
	 * "none" if score = -1; "true" if in [0, SIMILARITYTHRESHOLD]; "false" if in 
	 * (SIMILARITYTHRESHOLD, infinity)
	 * 
	 * @param score
	 * @return
	 */
	public String getSimilarity(double score) {
		if (0 <= score && score <= SIMILARITYTHRESHOLD) {
			return "true";
		} else if (score > SIMILARITYTHRESHOLD) {
			return "false";
		}
		return "none";
	}
	
	public double calculate(Structure a, Structure b){
		SimpleMatrix distanceMatrix_a = this.getdm(a);
		SimpleMatrix distanceMatrix_b = this.getdm(b);
		
		//Peter's code:
		//EigenvalueDecomposition e_a = distanceMatrix_a.eig();
        //EigenvalueDecomposition e_b = distanceMatrix_b.eig();
		// Get the real part of the eigenvalues
		//double[] eigenValues_a = this.round(e_a.getRealEigenvalues(), 2); 
        //double[] eigenValues_b = this.round(e_b.getRealEigenvalues(), 2);
		
		SimpleEVD<SimpleMatrix> e_a = distanceMatrix_a.eig();
		SimpleEVD<SimpleMatrix> e_b = distanceMatrix_b.eig();
		double[] eigenValues_a = new double[e_a.getNumberOfEigenvalues()];
		for (int i = 0; i < e_a.getNumberOfEigenvalues(); i++) {
		    eigenValues_a[i] = e_a.getEigenvalue(i).getReal();
		}
		double[] eigenValues_b = new double[e_b.getNumberOfEigenvalues()];
		for (int i = 0; i < e_b.getNumberOfEigenvalues(); i++) {
		    eigenValues_b[i] = e_b.getEigenvalue(i).getReal();
		}

		//Matrix test = this.test(e_a.getD(), e_a.getV());
		
		//double[][] V_a = e_a.getV().getArray();
		//double[][] V_b = e_b.getV().getArray();
		
		// Although use rows and cols here, they should be the same.
		double[][] V_a = new double[distanceMatrix_a.numRows()][distanceMatrix_a.numCols()];
		double[][] V_b = new double[distanceMatrix_b.numRows()][distanceMatrix_b.numCols()];
		for (int j = 0; j < e_a.getNumberOfEigenvalues(); j++) {
		    SimpleMatrix e_a_j = e_a.getEigenVector(j);
		    for (int i = 0; i < distanceMatrix_a.numRows(); i++) {
		        V_a[i][j] = e_a_j.get(i); // The eigenvectors are 1D array. Here, get the i-th value.
		    }
		}
		
		for (int j = 0; j < e_b.getNumberOfEigenvalues(); j++) {
            SimpleMatrix e_b_j = e_b.getEigenVector(j);
            for (int i = 0; i < distanceMatrix_b.numRows(); i++) {
                V_b[i][j] = e_b_j.get(i); 
            }
        } 
		
		
		double[][][] epf_a = this.getEpf(eigenValues_a, V_a);
		double[][][] epf_b = this.getEpf(eigenValues_b, V_b);

		return this.getDistance(epf_a, epf_b);
	}
	
	private SimpleMatrix test(SimpleMatrix d, SimpleMatrix v){
		SimpleMatrix a = v.mult(d.mult(v.transpose()));
		return null;
	}
	
	private double getDistance(double[][][] a, double[][][] b){
		double[][] differences = new double[a.length][b.length];
		for(int i = 0; i < a.length; i++){
			double[][] atom1 = a[i];
			for(int j = 0; j < b.length; j++){
				double[][] atom2 = b[j];
				double diff = integrate(atom1, atom2);
				differences[i][j] = new BigDecimal(diff).round(new MathContext(3)).doubleValue();
			}
		}
		double[] match = this.match(differences);
		return MSMath.average(match);
	}
	
//	private static double getDistance(double[][] a, double[][] b){
//		a = this.sort(a);
//		b = this.sort(b);
//		new Matrix(a).print(6, 6);
//		new Matrix(b).print(6, 6);
//		double sum = 0;
//		for(int i = 0; i < a.length; i++){
//			
//		}
//		return 0;
//	}
	
	private double[] match(double[][] m){
		HungarianAlgorithm ha = new HungarianAlgorithm(m);
		int[] order = ha.execute();
		double[] match = new double[order.length];
		for(int i = 0; i < order.length; i++){
			match[i]= m[i][order[i]];
		}
		return match;
	}
	
	private double[][] getDifference(double[][][] func){
		double[][] differences = new double[func.length][func.length];
		for(int i = 0; i < func.length; i++){
			double[][] atom1 = func[i];
			for(int j = 0; j <= i; j++){
				double[][] atom2 = func[j];
				double diff = this.integrate(atom1, atom2);
				differences[j][i] = differences[i][j] = new BigDecimal(diff).round(new MathContext(3)).doubleValue();
			}
		}
		return differences;
	}
	
	private static double integrate(double[][] atom1, double[][] atom2){
		double start = 0;
		int getter1 = 0;
		int getter2 = 0;
		double path1 = 0;
		double path2 = 0;
		double sum = 0;

		while((1. - path1 > 1E-2) || (1. - path2 > 1E-2)){
			double dS;
			double l1;
			double l2;
			if(path1 - path2 > 1E-7){
				dS = path2 - start;
				l2 = atom2[getter2][1];
				l1 = atom1[getter1][1];
				getter2++;
				double x2 = new BigDecimal(atom2[getter2][0]).round(new MathContext(8)).doubleValue();
				if(Math.abs(x2) <= 1E-4){
					x2 = 0.;
				}
				path2 += x2;
			}
			else if(Math.abs(path1 - path2) <= 1E-12){
				double x1 = new BigDecimal(atom1[getter1][0]).round(new MathContext(8)).doubleValue();
				double x2 = new BigDecimal(atom2[getter2][0]).round(new MathContext(8)).doubleValue();
				if(Math.abs(x1) <= 1E-7){
					x1 = 0.;
				}
				if(Math.abs(x2) <= 1E-7){
					x2 = 0.;
				}
				path1 += x1;
				path2 += x2;
				dS = 0;
				l2 = atom2[getter2][1];
				l1 = atom1[getter1][1];
				if(Math.abs(path1 - path2) <= 1E-12){
					getter1++;
					getter2++;
				}
			}
			else{
				dS = path1 - start;
				l1 = atom1[getter1][1];
				l2 = atom2[getter2][1];
				getter1++;
				double x1 = new BigDecimal(atom1[getter1][0]).round(new MathContext(8)).doubleValue();
				if(Math.abs(x1) <= 1E-4){
					x1 = 0.;
				}
				path1 += x1 ;
			}
			double integral = Math.abs((l2 - l1)*dS);
			start += dS;
			sum += integral;
		}
		return sum;
	}	
	
	private double[][] getDifferences(double[] areas_a, double[] areas_b){
				
		double[][] diffs = new double[areas_a.length][areas_b.length];
		for(int i = 0; i < areas_a.length; i++){
			for(int j = 0; j <= i; j++){
				diffs[i][j] = diffs[j][i] = Math.abs(areas_a[i] - areas_b[j]);
			}
		}
		return diffs;
	}
	
	private double[][][] getEpf(double[] eigenValues, double[][] eigenVectors){
		Map<Double, List<double[]>> map = this.getMap(eigenValues, eigenVectors);
		
		Object[] keySet = map.keySet().toArray();
		Arrays.sort(keySet);
		double[][] epa = new double[keySet.length][eigenVectors.length];
		
		for(int i = 0; i < keySet.length; i++){
			List<double[]> l = map.get(keySet[i]);
			double[][] coordArray = new double[l.size()][eigenVectors.length];
			for(int j = 0; j < l.size(); j++){
				for(int k = 0; k < eigenVectors.length; k++){
					coordArray[j][k] = l.get(j)[k];
				}
			}
			coordArray = MSMath.transpose(coordArray);
			for(int j = 0; j < coordArray.length; j++){
				double[] sq = MSMath.arrayPower(coordArray[j], 2);
				double sum = MSMath.arraySum(sq);
				epa[i][j] = new BigDecimal(sum).setScale(10, RoundingMode.HALF_UP).doubleValue();
			}
		}
				
		epa = MSMath.transpose(epa);

		double[][][] epf = this.Epf(epa, keySet);
		
		return epf;
	}
	
	private double[][][] Epf(double[][] epa, Object[] eigenValue){
		double[][][] test = new double[epa.length][eigenValue.length][2];
		for(int i = 0; i < test.length; i++){
			for(int j = 0; j < test[i].length; j++){
				test[i][j] = new double[]{epa[i][j], (double) eigenValue[j]};
			}
		};
		return test;
	}
	
	private HashMap<Double, List<double[]>> getMap(double[] eigenValues, double[][] eigenVectors){
		Map<Double, List<double[]>> map = new HashMap<>();
		eigenVectors = MSMath.transpose(eigenVectors);
		List<double[]> multiple = new ArrayList<>();
		for(int i = 0; i < eigenVectors.length; i++){
			double eigenValue = eigenValues[i];
			double[] eigenVector = eigenVectors[i];
			if(!map.keySet().contains(eigenValue) && !(map.keySet().size() == 0)){
				multiple.clear();
			}
			multiple.add(eigenVector);
			ArrayList<double[]> clone = new ArrayList<>(multiple);
			map.put(eigenValue, clone);
		}
		return (HashMap<Double, List<double[]>>) map;
	}
	
	private double[] round(double[] a, int places){
		for(int i = 0; i < a.length; i++){
			a[i] = new BigDecimal(a[i]).round(new MathContext(places, RoundingMode.HALF_UP)).doubleValue();
		}
		return a;
	}
	
	public SimpleMatrix getdm(Structure s){
		double[][] dm = new double[s.numDefiningSites()][s.numDefiningSites()];
		for(int i = 0; i < s.numDefiningSites(); i++){
			dm[i][i] = s.getSiteSpecies(i).getElement().getAtomicNumber();
			for(int j = 0; j < i; j++){
				dm[i][j] = dm[j][i] = new BigDecimal(s.getSiteCoords(i).distanceFrom(s.getSiteCoords(j))).round(new MathContext(4)).doubleValue();
			}
		}
		return new SimpleMatrix(dm);
	}
	
//	private double max(double[] l){
//		double m = Double.NEGATIVE_INFINITY;
//		for(int i = 0; i < l.length; i++){
//			if(l[i]> m){
//				m = l[i];
//			}
//		}
//		return m;
//	}
//	
//	private double calculate(Structure a, Structure b){
//		Species[] distinctSpecies = a.getDistinctSpecies();
//		double[][] aList = this.getSortedDistList(a);
//		double[][] bList = this.getSortedDistList(b);
//		double maxDifference = 0;
//		double totalCumulativeDifference = 0;
//		for(int n = 0; n < distinctSpecies.length; n++){
//			assert(aList[n].length == bList[n].length);
//			double distSize = this.sum(aList[n]);
//			double[] d = this.getAbsDiff(aList[n], bList[n]);
//			double cum_diff = this.sum(d);
//			if(this.sum(d) > maxDifference){
//				maxDifference = this.max(d);
//			}
//			int nType = 0;
//			for(int i = 0; i < distinctSpecies.length; i++){
//				if(distinctSpecies[i].getSymbol().equals(distinctSpecies[n].getSymbol())){
//					nType++;
//				}
//			}
//			totalCumulativeDifference += cum_diff/distSize*nType/distinctSpecies.length;
//		}
//		return totalCumulativeDifference + maxDifference;
//	}
//	
//	private double[] getAbsDiff(double[] a, double[] b){
//		double[] d = new double[a.length];
//		for(int i = 0; i < a.length; i++){
//			d[i] = Math.abs(a[i] - b[i]);
//		}
//		return d;
//	}
//	
//	private double sum(double[] list){
//		double sum = 0;
//		for(int i = 0; i < list.length; i++){
//			sum+=list[i];
//		}
//		return sum;
//	}
//	
//	private double[][] getSortedDistList(Structure structure){
//		Species[] species = structure.getDistinctSpecies();
//		double[][] pair_cor = new double[species.length][];;
//		for(int n = 0; n < species.length; n++){
//			ArrayList<Double> dist = new ArrayList<>();
//			ArrayList<Site> num = new ArrayList<Site>();
//			for(int i = 0; i < structure.numDefiningSites(); i++){
//				if(structure.getSiteSpecies(i).getSymbol().equals(species[n].getSymbol())){
//					num.add(structure.getDefiningSite(i));
//				}
//			}
//			for(int i = 0; i < num.size(); i++){
//				for(int j = 0; j < i + 1; j++){
//					dist.add(num.get(i).getCoords().distanceFrom(num.get(j).getCoords()));
//				}
//			}
//			Collections.sort(dist);
//			double[] sortedDistances = new double[dist.size()];
//			for(int i = 0; i < dist.size(); i++){
//				sortedDistances[i] = dist.get(i);
//			}
//			pair_cor[n] = sortedDistances;
//		}
//		return pair_cor;
//	}

}
