package ga;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import ga.io.Logger;
import ga.minimizer.LAMMPS;
import ga.minimizer.VASP;
import ga.potential.MTP;
import ga.structure.ClusterFactory;
import ga.structure.Seeder;
import ga.utils.GAUtils;
import matsci.Element;
import matsci.Species;
import matsci.io.vasp.POSCAR;
import matsci.structure.Structure;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class Input {

    // Default values of input parameters.
    private static double m_PopNum = 10; 		  // Pool size.
    private static String m_GaType = "Pool";  	  // Pool-based GA, instead of generation-based.
    private static String m_InitType = "Random";  // "Random", "Seed", Beta: "Ring"
    private static int m_InitSize = (int) m_PopNum;
    // TODO: implement the varying-pool-size method by structure weighting.
    private static String m_PoolType = "Fixed";	  // Pool size is fixed for now.
    private static double m_Conv = 0.01; 		  // Convergence criterion.
    private static String[] m_Mutation = new String[] { "Rotate", "Move" }; // Rotate, Move, Beta: Move_local
    private static String[] m_Cross = new String[] { "Even" }; // Even, Random, Weighted
    private static double m_MutatedAtomRatio = 0.2;
    private static double m_MutRate = 0.2;
    private static double m_SeedRate = 0.0;
    //private static double m_Rij = Double.NaN;
    private static double m_SimilarityThreshold = 0.3;
    // # of atoms per species(element) in bare clusters. These for ligands are automatically added later on.
    // "4" "20" "20" : # elements of specific types
    private static int[] m_EleNums; 
    // Names of each species(element) in base clusters. These for ligands are automatically added later on.
    // "Al" "C" "H"
    private static String[] m_EleNames; 
    private static double m_DistImage = 10; 	 // Distance between periodic image.
    private static double m_ElitistRatio = 0.1;  // TODO: ??
    private static String m_Comp = "Linux";
    private static String m_Verbosity = "Basic"; // "Debug" "Detail" "Basic" "Quiet"
    private static boolean m_Zip = false;        // Whether zip non-pool LAMMPS calculations.
    private static boolean m_StructureCheck = true; // Whether check realisticness after relxation.
    private static int m_NumOperations = 10000;  // Maximum number of clusters to be generated.
    private static String m_Start = "0";
    private static String m_SurfaceCalc = "False";
    private static String m_SurfaceType = "Graphene";
    private static String m_TestMode = "False";
    private static Boolean m_IsLigatedCluster = false;
    private static int m_NumLigands = 0;
    private static String m_ActiveSite = "-1";
    private static double m_LigBuffer = 0.5;
    // Command for running energy evaluation executable.
    private static String m_Root = null; // Don't have "/" at the end.
    private static double m_RLL = 1.0;
    private static double m_RLC = 0.5;
    private static int m_NUMCLS = 0; // Number of clusters in CLUSTER file for ligated complex.
    private static double m_PROBCLS = 0.5;
    
    // Newly added flags
    private static String m_Mode = "VASP";
    private static boolean m_Parallel = true;
    private static String m_VASPMPI = "";
    private static String m_LAMMPSMPI = "";
    private static String m_TrainingMPI = "";
    private static int m_EngineThreads = 1;
    private static int m_TrainingThreads = 1;
    private static String m_LAMMPSEXE = null;
    private static String m_VASPEXE = null;
    private static String m_MTPEXE = null;
    private static String m_MTPSERIALEXE = null;
    private static double m_EnergyLimit = 10000;
    private static RetrainMode m_RetrainMode = null;
    private static boolean m_InitPot = false; 		// Trained potential to start with or empty ones.
    private static int m_RetrainStep = 500;
    private static int m_PreProcessing = 0; 		// Number of lammps steps before AL kicks in.
    private static int m_InterpolatingAttempts = 0; // Attempts to relax a structure into interpolating
                                                    // region during lammps preprocessing.
    private static boolean m_IncludeInterpolating = true;
    private static boolean m_RemoveSimilar = true;  // Whether remove similar ionic steps from a VASP relaxation data set.
                                                    // Only for relaxation calculation, not MD.
    private static boolean m_RunMD = false;
    private static boolean m_UseStaticINCAR = false;
    private static double m_SelectThreshold = 1.01;
    private static double m_BreakThreshold = 10;
    private static int m_SelectPoolSize = 5;
    private static double m_MaxLAMMPSPoolRatio = 0.75;
    private static int m_LAMMPSRatioOffset = 500;
    private static double m_SelectEnergyWeight = 1;
    private static double m_SelectForceWeight = 0.01;
    private static double m_SelectStressWeight = 0;
    private static double m_SelectSiteEnergyWeight = 0;
    private static double m_HighEnergyThreshold = 10;
    private static double m_LowEnergyThreshold = 0.05; // 50 meV
    private static double m_DataSimilarityThreshold = 0.15;
    private static int m_GroundMultiple = 1;
    private static double m_EnergyWeight = 1;
    private static double m_ForceWeight = 0.001;
    private static double m_StressWeight = 0;
    private static double m_ScaleByForce = 0;
    private static int m_MaxTrainIteration = 5000;
    private static boolean m_Reevaluate = false;
    private static double m_ReCalcFraction = 1.0; //1.0: all; 0.0: only the re-trained ones (at least the pool clusters).
    private static boolean m_ScaleTopCandidates = false;
    private static int m_TopSelection = 50;
    private static int m_TopMultiple = 20;
    private static Structure m_BareClusterStructure = null;
    private static Structure m_LigandStructure = null;
    private static Structure[] m_BareClusterStructureArray = new Structure[] {};

    // Not flags in INGA
    private static Element[] m_EleTypes;
    public enum RetrainMode { Batch, DOptimality };
    
    // Fixed-size list, immutable.
    private static ArrayList<String> keys = new ArrayList<>(Arrays.asList( 
            "POPNUM", 
            "GATYPE", 
            "INITTYPE",
            "INITSIZE",
            "POPTYPE", 
            "CONV", 
            "MUTATION", 
            "CROSS",
            "MUTRATE",
            "MUTATEDATOMRATIO",
            "SEEDRATE",
            "RIJ", 
            "ELENUMS", 
            "ELENAMES", 
            "DISTIMAGE", 
            "ELITIST", 
            "START",
            "SIMILARITYTHRESHOLD",
            "COMP", 
            "VERBOSITY",
            "ZIP",
            "STRUCTURECHECK",
            "NUMOPS",
            "SURFACECALC", 
            "SURFACE", 
            "TEST", 
            "LIGAND", 
            "NUMLIGANDS", 
            "ACTIVESITE",
            "LIGBUFFER",
            "SUBSTRING", 
            "ROOT", 
            "RLL", 
            "RLC", 
            "LBOX",
            "NUMCLS",
            "PROBCLS",
            "MODE",
            "PARALLEL",
            "MPIVASP",
            "MPILAMMPS",
            "MPITRAINING",
            "THREADSENGINE",
            "THREADSTRAINING",
            "MTPEXE",
            "MTPSERIALEXE",
            "LAMMPSEXE",
            "VASPEXE",
            "RETRAINMODE",
            "INITPOT",
            "RETRAINSTEP",
            "PREPROCESSING",
            "INTERPOLATINGATTEMPTS",
            "INCLUDEINTERPOLATING",
            "REMOVESIMILAR",
            "RUNMD",
            "USESTATICINCAR",
            "ENERGYLIMIT",
            "SELECTTHRESHOLD",
            "BREAKTHRESHOLD",
            "SELECTPOOLSIZE",
            "MAXLAMMPSPOOLRATIO",
            "LAMMPSRATIOOFFSET",
            "SELECTENERGYWEIGHT",
            "SELECTFORCEWEIGHT",
            "SELECTSTRESSWEIGHT",
            "SELECTSITEENERGYWEIGHT",
            "HIGHENERGYTHRESHOLD",
            "LOWENERGYTHRESHOLD",
            "DATASIMILARITYTHRESHOLD",
            "GROUNDMULTIPLE",
            "ENERGYWEIGHT",
            "FORCEWEIGHT",
            "STRESSWEIGHT",
            "SCALEBYFORCE",
            "MAXTRAINITERATION",
            "REEVALUATE",
            "RECALCFRAC",
            "SCALETOPCANDIDATES",
            "TOPSELECTION",
            "TOPMULTIPLE"
    ));

    private static File INGA;
    private static LinkedHashMap<String, String> inputs = new LinkedHashMap<>();

    // Methods are all static. Don't need a constructor.
    private Input() {}
    
    public static void setRoot(String path) {
    	m_Root = path;
    }
    
    public static void initialize() {
    	if (m_Root == null) {
    		m_Root = System.getProperty("user.dir");
    	}
        INGA = new File(m_Root + HPC.FILESPTR + "INGA");
        Logger.basic("Root folder: " + m_Root);
        readInputs();
        setInputs();
        checkMode();
    }

    /**
     * Read settings from input file "INGA". For parameters with multiple values,
     * use comma to separate the values. E.g. "MUTATION = Move, Rotate".
     * 
     * Leading and trailing whitespaces are allowed and will be removed.
     * 
     */
    private static void readInputs() {
    	Logger.basic("Reading inputs from INGA ...");
        try {
            List<String> lines = HPC.readAllLines(INGA.getAbsolutePath());
            for (int i = 0; i < lines.size(); i++) {
                String str = lines.get(i);
                str = str.split("#")[0].trim();    // allow in-line comments by "#".
				String[] splits = str.split("=");  
				if (splits.length > 1) {           // Has a value and is not empty;
					if (keys.contains(splits[0].toUpperCase())) {
						String input = splits[splits.length - 1];
						inputs.put(splits[0], input);
					}
				}
            }
        } catch (IOException e) {
            Logger.error("Cannot read from INGA file. Please check its existence. Exiting.");
            System.exit(1);
        }

    }

    private static void setInputs() {
        m_PopNum = (!inputs.containsKey("POPNUM")) ? m_PopNum : Double.parseDouble(inputs.get("POPNUM"));
        m_GaType = (!inputs.containsKey("GATYPE")) ? m_GaType : inputs.get("GATYPE");
        m_InitType = (!inputs.containsKey("INITTYPE")) ? m_InitType : inputs.get("INITTYPE");
        m_InitSize = (!inputs.containsKey("INITSIZE")) ? (int) m_PopNum : Integer.parseInt(inputs.get("INITSIZE"));
        m_PoolType = (!inputs.containsKey("POPTYPE")) ? m_PoolType : inputs.get("POPTYPE");
        m_Conv = (!inputs.containsKey("CONV")) ? m_Conv : Double.parseDouble(inputs.get("CONV"));
        m_Mutation = (!inputs.containsKey("MUTATION")) ? m_Mutation
                : inputs.get("MUTATION").replaceAll("\\s+", "").split(",");
        m_Cross = (!inputs.containsKey("CROSS")) ? m_Cross : inputs.get("CROSS").replaceAll("\\s+", "").split(",");
        m_MutRate = (!inputs.containsKey("MUTRATE")) ? m_MutRate : Double.parseDouble(inputs.get("MUTRATE"));
        m_MutatedAtomRatio = (!inputs.containsKey("MUTATEDATOMRATIO")) ? m_MutatedAtomRatio : Double.parseDouble(inputs.get("MUTATEDATOMRATIO"));
        m_SeedRate = (!inputs.containsKey("SEEDRATE")) ? m_SeedRate : Double.parseDouble(inputs.get("SEEDRATE"));
        m_SimilarityThreshold = (!inputs.containsKey("SIMILARITYTHRESHOLD"))
        		? m_SimilarityThreshold : Double.parseDouble(inputs.get("SIMILARITYTHRESHOLD"));
        m_DistImage = (!inputs.containsKey("DISTIMAGE")) ? m_DistImage
                : Double.parseDouble(inputs.get("DISTIMAGE"));
        m_ElitistRatio = (!inputs.containsKey("ELITIST")) ? m_ElitistRatio : Double.parseDouble(inputs.get("ELITIST"));
        m_Start = (!inputs.containsKey("START")) ? m_Start : inputs.get("START");
        m_Comp = (!inputs.containsKey("COMP")) ? m_Comp : inputs.get("COMP");
        m_Verbosity = (!inputs.containsKey("VERBOSITY")) ? m_Verbosity : inputs.get("VERBOSITY");
        m_Zip = (!inputs.containsKey("ZIP")) ? m_Zip : Boolean.parseBoolean(inputs.get("ZIP"));
        m_StructureCheck = (!inputs.containsKey("STRUCTURECHECK")) ? m_StructureCheck : Boolean.parseBoolean(inputs.get("STRUCTURECHECK"));
        m_NumOperations = (!inputs.containsKey("NUMOPS")) ? m_NumOperations : Integer.parseInt(inputs.get("NUMOPS"));
        m_SurfaceCalc = (!inputs.containsKey("SURFACECALC")) ? m_SurfaceCalc : inputs.get("SURFACECALC");
        m_SurfaceType = (!inputs.containsKey("SURFACETYPE")) ? m_SurfaceType : inputs.get("SURFACETYPE");
        m_TestMode = (!inputs.containsKey("TEST")) ? m_TestMode : inputs.get("TEST");
        m_IsLigatedCluster = (!inputs.containsKey("LIGAND")) ? m_IsLigatedCluster : Boolean.parseBoolean(inputs.get("LIGAND"));
        m_NumLigands = (!inputs.containsKey("NUMLIGANDS")) ? m_NumLigands : Integer.parseInt(inputs.get("NUMLIGANDS"));
        m_ActiveSite = (!inputs.containsKey("ACTIVESITE")) ? m_ActiveSite : inputs.get("ACTIVESITE");
        m_LigBuffer = (!inputs.containsKey("LIGBUFFER")) ? m_LigBuffer : Double.parseDouble(inputs.get("LIGBUFFER"));
        m_Root = (!inputs.containsKey("ROOT")) ? m_Root : inputs.get("ROOT");
        if (m_Root.endsWith(HPC.FILESPTR)) { 
        	m_Root = m_Root.substring(0, m_Root.length() - 1);
        }
        m_RLL = (!inputs.containsKey("RLL")) ? m_RLL : Double.parseDouble(inputs.get("RLL"));
        m_RLC = (!inputs.containsKey("RLC")) ? m_RLC : Double.parseDouble(inputs.get("RLC"));
        m_EnergyLimit = (!inputs.containsKey("ENERGYLIMIT")) ? m_EnergyLimit : Double.parseDouble(inputs.get("ENERGYLIMIT"));
        m_PROBCLS = (!inputs.containsKey("PROBCLS")) ? m_PROBCLS : Double.parseDouble(inputs.get("PROBCLS"));
        m_NUMCLS = (!inputs.containsKey("NUMCLS")) ? m_NUMCLS : Integer.parseInt(inputs.get("NUMCLS"));
        m_Mode = (!inputs.containsKey("MODE")) ? m_Mode : inputs.get("MODE");
        m_Parallel = (!inputs.containsKey("PARALLEL")) ? m_Parallel : Boolean.parseBoolean(inputs.get("PARALLEL"));
        m_VASPMPI = (!inputs.containsKey("MPIVASP")) ? m_VASPMPI : inputs.get("MPIVASP");
        m_LAMMPSMPI = (!inputs.containsKey("MPILAMMPS")) ? m_LAMMPSMPI : inputs.get("MPILAMMPS");
        m_TrainingMPI = (!inputs.containsKey("MPITRAINING")) ? m_VASPMPI : inputs.get("MPITRAINING");
        m_TrainingThreads =  (!inputs.containsKey("THREADSTRAINING")) ? m_TrainingThreads : Integer.parseInt(inputs.get("THREADSTRAINING"));
        m_EngineThreads = (!inputs.containsKey("THREADSENGINE")) ? m_EngineThreads : Integer.parseInt(inputs.get("THREADSENGINE"));
        m_LAMMPSEXE = (!inputs.containsKey("LAMMPSEXE")) ? m_LAMMPSEXE : inputs.get("LAMMPSEXE");
        m_VASPEXE = (!inputs.containsKey("VASPEXE")) ? m_VASPEXE : inputs.get("VASPEXE");
        m_MTPEXE = (!inputs.containsKey("MTPEXE")) ? m_MTPEXE : inputs.get("MTPEXE");
        m_MTPSERIALEXE = (!inputs.containsKey("MTPSERIALEXE")) ? m_MTPEXE : inputs.get("MTPSERIALEXE");
        
        // Retraining
        if (m_Mode.equalsIgnoreCase("Active") && inputs.get("RETRAINMODE") != null) {
        	String mode = inputs.get("RETRAINMODE");
        	if (mode.equalsIgnoreCase("Batch")) {
        		m_RetrainMode = RetrainMode.Batch;
        	} else if (mode.equalsIgnoreCase("DOptimality")) {
        		m_RetrainMode = RetrainMode.DOptimality;
        	} else {
        		Logger.error("Unrecognized retrain mode: " + mode + ". Switch off retraining.");
        	}
        }
        
        m_InitPot = (!inputs.containsKey("INITPOT")) ? m_InitPot : Boolean.parseBoolean(inputs.get("INTIPOT"));
        m_RetrainStep = (!inputs.containsKey("RETRAINSTEP")) ? m_RetrainStep : Integer.parseInt(inputs.get("RETRAINSTEP"));
        m_PreProcessing = (!inputs.containsKey("PREPROCESSING")) ? m_PreProcessing : Integer.parseInt(inputs.get("PREPROCESSING"));
        m_InterpolatingAttempts = (!inputs.containsKey("INTERPOLATINGATTEMPTS")) ? m_InterpolatingAttempts : Integer.parseInt(inputs.get("INTERPOLATINGATTEMPTS"));
        m_IncludeInterpolating = (!inputs.containsKey("INCLUDEINTERPOLATING")) ? m_IncludeInterpolating : Boolean.parseBoolean(inputs.get("INCLUDEINTERPOLATING"));
        m_RemoveSimilar = (!inputs.containsKey("REMOVESIMILAR")) ? m_RemoveSimilar : Boolean.parseBoolean(inputs.get("REMOVESIMILAR"));
        m_RunMD = (!inputs.containsKey("RUNMD")) ? m_RunMD : Boolean.parseBoolean(inputs.get("RUNMD"));
        m_UseStaticINCAR = (!inputs.containsKey("USESTATICINCAR")) ? m_UseStaticINCAR : Boolean.parseBoolean(inputs.get("USESTATICINCAR"));
        m_SelectThreshold = (!inputs.containsKey("SELECTTHRESHOLD")) ? m_SelectThreshold : Double.parseDouble(inputs.get("SELECTTHRESHOLD"));
        m_BreakThreshold = (!inputs.containsKey("BREAKTHRESHOLD")) ? m_SelectThreshold : Double.parseDouble(inputs.get("BREAKTHRESHOLD"));
        m_SelectPoolSize = (!inputs.containsKey("SELECTPOOLSIZE")) ? m_SelectPoolSize : Integer.parseInt(inputs.get("SELECTPOOLSIZE"));
        m_MaxLAMMPSPoolRatio = (!inputs.containsKey("MAXLAMMPSPOOLRATIO")) ? m_MaxLAMMPSPoolRatio : Double.parseDouble(inputs.get("MAXLAMMPSPOOLRATIO"));
        m_LAMMPSRatioOffset = (!inputs.containsKey("LAMMPSRATIOOFFSET")) ? m_LAMMPSRatioOffset : Integer.parseInt(inputs.get("LAMMPSRATIOOFFSET"));
        m_SelectEnergyWeight = (!inputs.containsKey("SELECTENERGYWEIGHT")) ? m_SelectEnergyWeight : Double.parseDouble(inputs.get("SELECTENERGYWEIGHT"));
        m_SelectForceWeight = (!inputs.containsKey("SELECTFORCEWEIGHT")) ? m_SelectForceWeight : Double.parseDouble(inputs.get("SELECTFORCEWEIGHT"));
        m_SelectStressWeight = (!inputs.containsKey("SELECTSTRESSWEIGHT")) ? m_SelectStressWeight : Double.parseDouble(inputs.get("SELECTSTRESSWEIGHT"));
        m_SelectSiteEnergyWeight = (!inputs.containsKey("SELECTSITEENERGYWEIGHT")) ? m_EnergyWeight : Double.parseDouble(inputs.get("SELECTSITEENERGYWEIGHT"));
        m_HighEnergyThreshold = (!inputs.containsKey("HIGHENERGYTHRESHOLD")) ? m_HighEnergyThreshold : Double.parseDouble(inputs.get("HIGHENERGYTHRESHOLD"));
        m_LowEnergyThreshold = (!inputs.containsKey("LOWENERGYTHRESHOLD")) ? m_LowEnergyThreshold : Double.parseDouble(inputs.get("LOWENERGYTHRESHOLD"));
        m_DataSimilarityThreshold = (!inputs.containsKey("DATASIMILARITYTHRESHOLD")) ? m_DataSimilarityThreshold : Double.parseDouble(inputs.get("DATASIMILARITYTHRESHOLD"));
        m_GroundMultiple = (!inputs.containsKey("GROUNDMULTIPLE")) ? m_GroundMultiple : Integer.parseInt(inputs.get("GROUNDMULTIPLE"));
        
        // Fitting
        m_EnergyWeight = (!inputs.containsKey("ENERGYWEIGHT")) ? m_EnergyWeight : Double.parseDouble(inputs.get("ENERGYWEIGHT"));
        m_ForceWeight = (!inputs.containsKey("FORCEWEIGHT")) ? m_ForceWeight : Double.parseDouble(inputs.get("FORCEWEIGHT"));
        m_StressWeight = (!inputs.containsKey("STRESSWEIGHT")) ? m_StressWeight : Double.parseDouble(inputs.get("STRESSWEIGHT"));
        m_ScaleByForce = (!inputs.containsKey("SCALEBYFORCE")) ? m_ScaleByForce : Double.parseDouble(inputs.get("SCALEBYFORCE"));
        m_MaxTrainIteration = (!inputs.containsKey("MAXTRAINITERATION")) ? m_MaxTrainIteration : Integer.parseInt(inputs.get("MAXTRAINITERATION"));
        m_Reevaluate = (!inputs.containsKey("REEVALUATE")) ? m_Reevaluate : Boolean.parseBoolean(inputs.get("REEVALUATE"));
        m_ReCalcFraction = (!inputs.containsKey("RECALCFRAC")) ? m_ReCalcFraction : Double.parseDouble(inputs.get("RECALCFRAC"));
        m_ScaleTopCandidates = (!inputs.containsKey("SCALETOPCANDIDATES")) ? m_ScaleTopCandidates : Boolean.parseBoolean(inputs.get("SCALETOPCANDIDATES"));
        m_TopSelection = (!inputs.containsKey("TOPSELECTION")) ? m_TopSelection : Integer.parseInt(inputs.get("TOPSELECTION"));
        m_TopMultiple = (!inputs.containsKey("TOPMULTIPLE")) ? m_TopMultiple : Integer.parseInt(inputs.get("TOPMULTIPLE"));
        
        // Set logging level
		if (m_Verbosity.equalsIgnoreCase("Debug")) {
			Logger.setLogLevelDebug();
		} else if(m_Verbosity.equalsIgnoreCase("Detail")) {
			Logger.setLogLevelDetail(); // Logger is also set since it's wrapper of Status.
		} else if (m_Verbosity.equalsIgnoreCase("Basic")) {
			Logger.setLogLevelBasic();
		} else if (m_Verbosity.equalsIgnoreCase("Quiet")) {
			Logger.setLogLevelQuiet();
		}
		Logger.basic("Verbosity: " + m_Verbosity);

        // read LIGAND and CLUSTER File
		if (isLigatedCluster()) {
			try {
				setLigandStructure(m_Root + HPC.FILESPTR + "LIGAND");
				setBareClusterStructure(m_Root + HPC.FILESPTR + "CLUSTER");

				String[] clusterArray = new String[m_NUMCLS];
				for (int i = 0; i < clusterArray.length; i++) {
					clusterArray[i] = m_Root + HPC.FILESPTR + "clusterFiles" + HPC.FILESPTR 
							        + (i + 1) + HPC.FILESPTR + "POSCAR";
				}
				m_BareClusterStructureArray = setBareClusterStructureArray(clusterArray);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
        
		// ELENUMS : "4" "20" "20" : # elements of specific types
        try {
            String[] eleNums = inputs.get("ELENUMS").replaceAll("\\s+", "").split(",");
            m_EleNums = new int[eleNums.length];
            for (int i = 0; i < eleNums.length; i++) {
                m_EleNums[i] = Integer.parseInt(eleNums[i]);
            }
            
            // Append number of atoms per species in ligands in m_EleNums.
            if (isLigatedCluster()) {
                Structure ligandStructure = getLigandStructure();
                Species[] distinctSpecies = ligandStructure.getDistinctSpecies();
                for (int speciesNum = 0; speciesNum < distinctSpecies.length; speciesNum++) {
                    Species species = distinctSpecies[speciesNum];
                    int numSitesPerLigand = ligandStructure.numDefiningSitesWithSpecies(species);
                    m_EleNums = ArrayUtils.appendElement(m_EleNums, numSitesPerLigand * getNumLigands());
                }
            }
        } catch (NullPointerException n) {
            Logger.error("No element numbers supplied !! Please set ELENUMS tag in INGA. Exiting.");
            System.exit(1);
        }

        // ELENAMES : Types "Al" "C" "H" : type of distinct elements of specific types
        try {
            String[] eleNames = inputs.get("ELENAMES").replaceAll("\\s+", "").split(",");
            m_EleNames = new String[eleNames.length];
            for (int i = 0; i < eleNames.length; i++) {
                m_EleNames[i] = eleNames[i];
            }

            if (isLigatedCluster()) {
                Structure ligandStructure = getLigandStructure();
                Species[] distinctSpecies = ligandStructure.getDistinctSpecies();
                for (int speciesNum = 0; speciesNum < distinctSpecies.length; speciesNum++) {
                    m_EleNames = (String[]) ArrayUtils.appendElement(m_EleNames,
                            distinctSpecies[speciesNum].getElementSymbol());
                }
            }
        } catch (NullPointerException n) {
            Logger.error("No element names supplied !! Please set ELENAMES tag in INGA. Exiting.");
            System.exit(1);
        }
        
        // Set GA Operation related variables:
        if (inputs.containsKey("RIJ")) {
        	// Update nndistance list if provided. Otherwise, use the internal list.
        	GAUtils.m_NearestNeighborDistance.put(m_EleNames[0], Double.parseDouble(inputs.get("RIJ")));
        }
        ClusterFactory.MUTATEDATOMRATIO = m_MutatedAtomRatio;
        
        // Configure Seeder
        if (m_SeedRate > 0) {
            String seedsDir = m_Root + HPC.FILESPTR + "seeds";
            if (!new File(seedsDir).exists()) {
                Logger.error(seedsDir + " missing! Exit.");
                System.exit(1);
            }
            Seeder.configSeeder(seedsDir, m_EleNames, m_EleNums);
        }

        // Initiate settings of interfaces to external executables.
        // Separate them from Input for easier unit testing.
        
        // MTP
        if (Input.getMode().equals("Active")) {
            if (Input.isParallel()) {
                MTP.setPARALLELEXE(Input.getTrainingMPI());
            } else if (m_MTPEXE == null && m_MTPSERIALEXE != null) {
                // Safe guard in case user forget to set MTPEXE in serial GA.
                m_MTPEXE = m_MTPSERIALEXE;
            }
	        MTP.setMTPEXE(m_MTPEXE);
	        MTP.setMTPSERIALEXE(m_MTPSERIALEXE);
	        MTP.setEenergyWeight(m_EnergyWeight);
	        MTP.setForceWeight(m_ForceWeight);
	        MTP.setStressWeight(m_StressWeight);
	        MTP.setMaxTrainIteration(m_MaxTrainIteration);
	        MTP.setScaleByForce(m_ScaleByForce);
        }
        
        // Set redundant files to be removed:
        if (Input.getMode().equals("Active")) {
        	LAMMPS.REDUNDANTFILELIST = (String[]) ArrayUtils.appendArray(LAMMPS.REDUNDANTFILELIST, 
        			new String[]{ "struct.cfg", "end.cfg", "state.als" });
        	VASP.REDUNDANTFILELIST = (String[]) ArrayUtils.appendArray(VASP.REDUNDANTFILELIST, 
        			new String[] { "state.als" });
        	if (Input.runMD()) {
        		VASP.REDUNDANTFILELIST = (String[]) ArrayUtils.appendArray(VASP.REDUNDANTFILELIST, 
        				new String[] {"REPORT", "PENALTYPOT", "ICONST", "HILLSPOT"});
        	}
        }
        
        // Final adjustment of parameters based on all inputs:
        if (m_PreProcessing != 0 && !inputs.containsKey("INTERPOLATINGATTEMPTS")) {
            m_InterpolatingAttempts = 5;
        }
    }
    
    /**
     * Sanity checks and default setup. If there are illegal values which are fatal, 
     * report and abort the calculation.
     */
    private static void checkMode() {
    	if (!m_Mode.equalsIgnoreCase("VASP") && !m_Mode.equalsIgnoreCase("LAMMPS")
    			&& !m_Mode.equalsIgnoreCase("Active")) {
    		Logger.error("Illegal MODE value: " + m_Mode);
    		Logger.error("Acceptable values are: VASP LAMMPS Active");
    		System.exit(2);
    	}
    	
    	if (m_Mode.equalsIgnoreCase("Active") && m_RetrainMode == null) {
    		Logger.error("Retrain mode is not set. Use default DOptimality.");
    		m_RetrainMode = RetrainMode.DOptimality;
    	}
    	
    	Logger.basic("Calculation mode: " + m_Mode);
    }

    public static void setBareClusterStructure(String fileName) throws FileNotFoundException {
        m_BareClusterStructure = new Structure(new POSCAR(fileName));
    }

    public static Structure[] setBareClusterStructureArray(String[] fileName) throws FileNotFoundException {
        Structure[] m_BareClusterStructure = new Structure[fileName.length];
        for (int i = 0; i < fileName.length; i++) {
            m_BareClusterStructure[i] = new Structure(new POSCAR(fileName[i])); 
        }
        return m_BareClusterStructure;
    }

    public static Structure[] getBareClusterStructureArray() {
        return m_BareClusterStructureArray;
    }

    public static void setLigandStructure(String fileName) throws FileNotFoundException {
        m_LigandStructure = new Structure(new POSCAR(fileName));
    }

    public static Structure getBareClusterStructure() {
        return m_BareClusterStructure;
    }

    public static Structure getLigandStructure() {
        return m_LigandStructure;
    }

    public static double getPopNum() {
        return m_PopNum;
    }
    /**
     * @return The number of lines of each structure block in candidates.dat file.
     */
    public static int getStride() {
        int add = 2;	// Two header lines for each structure.
        int s = MSMath.arraySum(m_EleNums);
        return s + add;
    }

    public static int getNumOperations() {
        return m_NumOperations;
    }
    
    public static String getGaType() {
        return m_GaType;
    }

    public static String getInitType() {
        return m_InitType;
    }
    
    public static int getInitSize() {
    	return m_InitSize;
    }

    public static String getPoolType() {
        return m_PoolType;
    }

    public static double getConv() {
        return m_Conv;
    }

    public static String[] getMutationType() {
        return m_Mutation;
    }

    public static String[] getCrossType() {
        return m_Cross;
    }

    public static double getMutRate() {
        return m_MutRate;
    }
    
    public static double getSeedRate() {
    	return m_SeedRate;
    }

    /*
    public static double getRij() {
    	if (m_Rij == Double.NaN) {
    		m_Rij = GAUtils.getNNDistance(m_EleNames[0]);
    	}
        return m_Rij;
    }
    */
    
    public static double getSimilarityThreshold() {
    	return m_SimilarityThreshold;
    }

    public static int[] getEleNums() {
        return m_EleNums;
    }

    public static String[] getEleNames() {
        return m_EleNames;
    }
    
    public static Element[] getElementTypes() {
    	if (m_EleTypes == null) {
    		m_EleTypes = new Element[m_EleNames.length];
    		for (int i = 0; i < m_EleNames.length; i++) {
    			m_EleTypes[i] = Element.getElement(m_EleNames[i]);
    		}
    	}
    	return m_EleTypes;
    }

    public static double getDistImage() {
        return m_DistImage;
    }

    public static double getElitistRatio() {
        return m_ElitistRatio;
    }
    
    public static double getLigBuffer() {
        return m_LigBuffer;
    }

    public static String getStartStatus() {
        return m_Start;
    }

    public static String getComp() {
        return m_Comp;
    }

    public static String getVerbosity() {
        return m_Verbosity;
    }

    public static boolean zip() {
        return m_Zip;
    }

    public static boolean checkStructure() {
        return m_StructureCheck;
    }

    public static int getNumLigands() {
        return m_NumLigands;
    }
    
    public static int getActiveSite() {
        return Integer.parseInt(m_ActiveSite);
    }

    public static boolean getTestMode() {
        return Boolean.parseBoolean(m_TestMode);
    }

    public static boolean isSurfaceCalc() {
        return Boolean.parseBoolean(m_SurfaceCalc);
    }

    public static boolean isLigatedCluster() {
        return m_IsLigatedCluster;
    }

    public static String getSurfaceType() {
        return m_SurfaceType;
    }

    public static String getRoot() {
        return m_Root;
    }

    public static double getRLL() {
        return m_RLL;
    }

    public static double getRLC() {
        return m_RLC;
    }

    public static int getNumCls() {
        return m_NUMCLS;
    }
    
    public static double getProbCls() {
    	return m_PROBCLS;
    }
    
    public static String getMode() {
        return m_Mode;
    }
    
    public static boolean isParallel() {
    	return m_Parallel;
    }
    
    public static int getEngineThreads() {
    	return m_EngineThreads;
    }
    
    public static int getTrainingThreads() {
    	return m_TrainingThreads;
    }
    
    public static String getVASPMPI() {
    	return m_VASPMPI;
    }
    
    public static String getLAMMPSMPI() {
    	return m_LAMMPSMPI;
    }
    
    /**
     * @return List of MPI commands split by space.
     */
    public static List<String> getTrainingMPI() {
    	ArrayList<String> mpiCommands = new ArrayList<>();
    	String[] list = m_TrainingMPI.split("\\s+");
		for (String i : list) {
			mpiCommands.add(i);
		}
		return mpiCommands;
    }
    
    public static String getMTPEXE() {
        return m_MTPEXE;
    }
    
    public static String getMTPSERIALEXE() {
        return m_MTPSERIALEXE;
    }
    
    public static String getLAMMPSEXE() {
        return m_LAMMPSEXE;
    }
    
    public static String getVASPEXE() {
        return m_VASPEXE;
    }
    
    public static RetrainMode getRetrainMode() {
    	return m_RetrainMode;
    }
    
    public static boolean initPot() {
    	return m_InitPot;
    }
    
    public static int getRetrainStep() {
    	return m_RetrainStep;
    }
    
    public static int getPreprocessing() {
    	return m_PreProcessing;
    }
    
    public static int getInterpolatingAttempts() {
    	return m_InterpolatingAttempts;
    }
    
    public static boolean includeInterpolating() {
    	return m_IncludeInterpolating;
    }
    
    public static boolean removeSimilar() {
    	return m_RemoveSimilar;
    }
    
    public static int getGroundMultiple() {
    	return m_GroundMultiple;
    }
    
    public static boolean runMD() {
    	return m_RunMD;
    }
    
    public static boolean useStaticINCAR() {
    	return m_UseStaticINCAR;
    }
    
    public static double getSelectThreshold() {
    	return m_SelectThreshold;
    }
    
    public static double getBreakThreshold() {
    	return m_BreakThreshold;
    }
    
    public static int getSelectPoolSize() {
    	return m_SelectPoolSize;
    }
    
    public static double getMaxLAMMPSPoolRatio() {
    	return m_MaxLAMMPSPoolRatio;
    }
    
    public static double getLAMMPSRatioOffset() {
    	return m_LAMMPSRatioOffset;
    }
    
    public static double getSelectEnergyWeight() {
    	return m_SelectEnergyWeight;
    }
    
    public static double getSelectForceWeight() {
    	return m_SelectForceWeight;
    }
    
    public static double getSelectStressWeight() {
    	return m_SelectStressWeight;
    }
    
    public static double getSelectSiteEnergyWeight() {
    	return m_SelectSiteEnergyWeight;
    }
    
    public static double getHighEnergyThreshold() {
    	return m_HighEnergyThreshold;
    }
    
    public static double getLowEnergyThreshold() {
    	return m_LowEnergyThreshold;
    }
    
    public static double getDataSimilarityThreshold() {
    	return m_DataSimilarityThreshold;
    }
    
    public static double getEnergyLimit() {
    	return Math.abs(m_EnergyLimit);
    }
    
    
    public static double getEnergyWeight() {
    	return m_EnergyWeight;
    }
    
    public static double getForceWeight() {
    	return m_ForceWeight;
    }
    
    public static double getStressWeight() {
    	return m_StressWeight;
    }
    
    public static double getScaleByForce() {
    	return m_ScaleByForce;
    }
    
    public static int getMaxTrainIteration() {
    	return m_MaxTrainIteration;
    }

    public static boolean reevaluate() {
    	return m_Reevaluate;
    }
    
    public static double getReCalcFraction() {
    	return m_ReCalcFraction;
    }

    public static boolean scaleTopCandidates() {
    	return m_ScaleTopCandidates;
    }
    
    public static int getTopMultiple() {
    	return m_TopMultiple;
    }
    
    public static int getTopSelection() {
    	return m_TopSelection;
    }

    /**
	 * Get the working directory that contains *.dat files and all_candidates
	 * @return A folder path without "/" at the end.
	 */
	public static String getWorkDirectory(){
		String constituentsFolder = Input.getConstituentsFolder();	// "Al", "CuZn"
		String compositionFolder = Input.getCompositionFolder();		// "21", "Cu10Zn10"
		return Input.getRoot() + HPC.FILESPTR + constituentsFolder + compositionFolder;
	}
	
	/**
	 * Compose a folder name by concatenate names of elements in the cluster, in the sequence of
	 * Input.m_EleNames. E.g. "Al/" and "CuZn/".
	 * 
	 * @return Folder name composed by element names. 
	 */
	private static String getConstituentsFolder(){
		StringBuilder sb = new StringBuilder();
		String[] eleNames = Input.getEleNames();
		for(int i = 0; i < eleNames.length; i++){
			sb.append(eleNames[i]);
		}
		return sb.toString() + HPC.FILESPTR;
	}
	
	/**
	 * Compose a folder name by concatenate name and number of atoms for each element, in the
	 * sequence of Input.m_EleNames and Input.m_EleNums.
	 * 
	 * E.g. "Cu10Zn10/" for bimetallic cluster; "21/" for single-constituent cluster.
	 * 
	 * @return
	 */
	private static String getCompositionFolder(){
		StringBuilder sb = new StringBuilder();
		String[] eleNames = Input.getEleNames();
		int[] eleNums = Input.getEleNums();
		for(int i = 0; i < eleNums.length; i++){
			if (eleNames.length > 1) {
				sb.append(eleNames[i] + eleNums[i]);
			} else {
				sb.append(eleNums[i]);
			}
		}
		return sb.toString();
	}
}
