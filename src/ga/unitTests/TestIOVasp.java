package ga.unitTests;

import java.util.List;

import ga.HPC;
import ga.io.Logger;
import ga.io.VASP.INCAR;
import ga.io.VASP.OSZICAR;
import ga.utils.GAUtils;

public class TestIOVasp {
	public static void main(String[] args) {
		System.setProperty("user.dir", "/home/ywang393/eclipse-workspace/cluster-ga/unit_tests/IOVASP");
		TestIOVasp.testINCAR();
		TestIOVasp.testOSZICAR();
		TestIOVasp.testOSZICARMD();
	}
	
	private static void testINCAR() {
		INCAR incar = new INCAR(System.getProperty("user.dir") + HPC.FILESPTR + "Al_21_ISPIN_2/INCAR");
		GAUtils.assertion(incar.getIBRION() == 2);
		GAUtils.assertion(incar.getNELM() == 100);
		Logger.basic("Passed INCAR test.");
	}
	
	private static void testOSZICAR() {
		String path = "/home/ywang393/eclipse-workspace/cluster-ga/unit_tests/IOVASP/Al_21_ISPIN_2/OSZICAR";
		OSZICAR oszicar = new OSZICAR(path, "RELAX");
		GAUtils.assertion(oszicar.getNumIonicSteps() == 221);
		int[] scfSteps = {55, 28, 37, 29, 24, 26, 25, 21, 23, 19, 79, 24, 22, 21, 33, 21, 21, 27,
				          23, 26, 24, 29, 25, 21, 21, 10, 20, 19, 21, 21, 22, 21, 22, 21, 20, 21,
				          20, 21, 3, 20, 20, 21, 20, 21, 21, 20, 6, 20, 20, 20, 20, 19, 19, 20,
				          20, 19, 20, 20, 20, 18, 18, 19, 4, 17, 6, 6, 17, 5, 17, 5, 19, 20, 20, 16,
				          19, 20, 22, 14, 19, 20, 19, 6, 5, 18, 6, 18, 19, 10, 16, 6, 18, 4, 6, 8,
				          10, 17, 8, 17, 10, 18, 6, 6, 19, 21, 20, 6, 19, 21, 20, 16, 18, 10, 18, 6,
				          19, 18, 19, 17, 6, 19, 17, 20, 17, 18, 10, 16, 16, 10, 18, 6, 10, 19, 18,
				          17, 10, 10, 17, 10, 10, 6, 15, 10, 18, 10, 16, 18, 10, 18, 4, 16, 6, 20,
				          16, 16, 19, 6, 18, 6, 16, 6, 17, 10, 4, 6, 16, 6, 4, 18, 6, 16, 4, 6, 5,
				          17, 6, 13, 6, 5, 4, 4, 16, 3, 6, 16, 6, 6, 8, 6, 3, 16, 2, 2, 14, 2, 6,
				          3, 5, 4, 5, 14, 6, 16, 6, 17, 6, 16, 18, 6, 16, 17, 16, 14, 6, 10, 10, 3,
				          5, 3, 3, 2, 3};
		List<Integer> scfStepsResults = oszicar.getSCFSteps();
		for (int i = 0; i < scfStepsResults.size(); i++) {
			assert (scfStepsResults.get(i) == scfSteps[i]);
		}
		// Energy
		List<Double> energiesResults = oszicar.getEnergies();
		GAUtils.assertion(energiesResults.size() == 221);
		GAUtils.assertion(Math.abs(energiesResults.get(220) - (-62.112398)) < 0.000001);
		// Magnetization
		GAUtils.assertion(Math.abs(oszicar.getMagnetizationList().get(9) - 5.1074) < 0.0001);
		Logger.basic("Passed relaxation OSZICAR parsing test.");
	}
	
	private static void testOSZICARMD() {
		String path = "/home/ywang393/eclipse-workspace/cluster-ga/unit_tests/IOVASP/Al_21_ISPIN_2_MD/OSZICAR";
		OSZICAR oszicar = new OSZICAR(path, "MD");
		GAUtils.assertion(oszicar.getNumIonicSteps() == 20);
		int[] scfSteps = {30, 6, 11, 15, 7, 4, 3, 12, 11, 11, 3, 3, 5, 5, 8, 2, 3, 11, 11, 5};
		
		List<Integer> scfStepsResults = oszicar.getSCFSteps();
		for (int i = 0; i < scfStepsResults.size(); i++) {
			assert (scfStepsResults.get(i) == scfSteps[i]);
		}
		// Energy
		List<Double> energiesResults = oszicar.getEnergies();
		GAUtils.assertion(energiesResults.size() == 20);
		GAUtils.assertion(Math.abs(energiesResults.get(19) - (-61.600170)) < 0.000001);
		// Magnetization
		GAUtils.assertion(Math.abs(oszicar.getMagnetizationList().get(0) - 1.001) < 0.001);
		GAUtils.assertion(Math.abs(oszicar.getMagnetizationList().get(1) - 1.000) < 0.001);
		Logger.basic("Passed MD OSZICAR parsing test.");
	}
}
