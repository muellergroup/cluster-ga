package ga.unitTests;

import ga.HPC;
import ga.Input;
import ga.io.Logger;
import ga.io.VASP.INCAR;
import ga.io.VASP.OSZICAR;
import ga.io.mtp.CFG;
import ga.potential.MTP;
import ga.utils.GAUtils;
import matsci.Element;

public class TestMTP {
	public static void main(String[] args) {
		// Set a prototype INGA to use MTP.prepareCFG()
		Logger.setLogLevelQuiet();
		Input.setRoot("/home/ywang393/eclipse-workspace/cluster-ga/unit_tests/MTP");
		Input.initialize();
		Logger.setLogLevelBasic();
		testCFGFunctions();
		testPrepareCFG();
	}
	
	private static void testCFGFunctions() {
		String dir = "/home/ywang393/eclipse-workspace/cluster-ga/unit_tests/MTP/manipulate_cfg";
		INCAR incar = new INCAR(dir + HPC.FILESPTR + "INCAR");
		OSZICAR oszicar = new OSZICAR(dir + HPC.FILESPTR + "OSZICAR", "RELAX");
		
		CFG cfgs = new CFG(dir + HPC.FILESPTR + "outcar.cfg");
		cfgs.removeIntrapolatingImages(1.01);
		GAUtils.assertion(cfgs.validImageNum() == 76);
		
		cfgs.reset();
		cfgs.removeHighEnergyImages(2.0);
		GAUtils.assertion(cfgs.validImageNum() == 187);
		
		cfgs.reset();
		cfgs.removeUnconvergedImages(oszicar.getSCFSteps(), incar.getNELM());
		GAUtils.assertion(cfgs.validImageNum() == 276);
		
		cfgs.reset();
		cfgs.removeWrongMagImages(oszicar.getMagetization(), oszicar.getMagnetizationList());
		GAUtils.assertion(cfgs.validImageNum() == 226);
		Logger.basic("Passed CFG manipulating test.");
	}
	
	private static void testPrepareCFG() {
		Element[] elements = { Element.getElement("Al") };
		String dir = "/home/ywang393/eclipse-workspace/cluster-ga/unit_tests/MTP/convert_cfg";
		MTP.prepareCFG(dir, elements, "mlip.mtp", "dummy.cfg", "state.als", true, false, false, 2, 0.05, 1.01, 0.3, 1, "RELAX");
		CFG cfgs = new CFG(dir + HPC.FILESPTR + "outcar.cfg");
		GAUtils.assertion(cfgs.validImageNum() == 66, String.valueOf(cfgs.validImageNum()), String.valueOf(66));
		
		dir = "/home/ywang393/eclipse-workspace/cluster-ga/unit_tests/MTP/convert_cfg_MD";
		MTP.prepareCFG(dir, elements, "mlip.mtp", "dummy.cfg", "state.als", true, true, false, Double.MAX_VALUE, Double.MAX_VALUE, 1.01, 0.3, 1, "MD");
		cfgs = new CFG(dir + HPC.FILESPTR + "outcar.cfg");
		GAUtils.assertion(cfgs.validImageNum() == 19, String.valueOf(cfgs.validImageNum()), String.valueOf(19));
		
		Logger.basic("Passed prepare CFG from OUTCAR test.");
	}
}
