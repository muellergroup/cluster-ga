package ga.unitTests;

import java.io.File;

import ga.structure.ClusterFactory;
import ga.structure.Seeder;
import matsci.Species;
import matsci.io.vasp.POSCAR;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;

public class TestGeneticOperation {
    
    public static void main(String[] args) {
		//testSeedOperation();
        testRingGenerator();
        testLocalMutationOfRing();
	}

    public static void testSeedOperation() {
        // Testing ring clusters (Sulfur)
        String workDir = System.getProperty("user.dir") + "/unit_tests/ga_operations/seed/sulfur";
        String seedsDir = workDir + "/seeds";
        String outputDir = workDir + "/seededStructures";
        File oDir = new File(outputDir);
        if (!oDir.exists()) {
            oDir.mkdir();
        }
        Seeder.configSeeder(seedsDir, new String[] {"S"}, new int[] {21});
        for (int i = 0; i < Seeder.seeds.size(); i++) {
            Structure s = Seeder.buildStructure(Seeder.seeds.get(i));
            StructureBuilder sb = new StructureBuilder(s);
            Object[] clusList = ClusterFactory.structureToObjectArray(s);
            double cellSize = ClusterFactory.getCellSize(clusList, 10);
            Vector[] cellVectors = new Vector[] {
                new Vector(new double[] {cellSize, 0, 0}),
                new Vector(new double[] {0, cellSize, 0}),
                new Vector(new double[] {0, 0, cellSize})
            };
            sb.setCellVectors(cellVectors);
            POSCAR poscar = new POSCAR(new Structure(sb));
            poscar.writeFile(outputDir + "/POSCAR_" + i);
        }

        // Testing compact clusters (Aluminum)
        workDir = System.getProperty("user.dir") + "/unit_tests/ga_operations/seed/aluminum";
        seedsDir = workDir + "/seeds";
        outputDir = workDir + "/seededStructures";
        oDir = new File(outputDir);
        if (!oDir.exists()) {
            oDir.mkdir();
        }
        Seeder.configSeeder(seedsDir, new String[] {"Al"}, new int[] {31});
        for (int i = 0; i < Seeder.seeds.size(); i++) {
            Structure s = Seeder.buildStructure(Seeder.seeds.get(i));
            StructureBuilder sb = new StructureBuilder(s);
            Object[] clusList = ClusterFactory.structureToObjectArray(s);
            double cellSize = ClusterFactory.getCellSize(clusList, 10);
            Vector[] cellVectors = new Vector[] {
                new Vector(new double[] {cellSize, 0, 0}),
                new Vector(new double[] {0, cellSize, 0}),
                new Vector(new double[] {0, 0, cellSize})
            };
            sb.setCellVectors(cellVectors);
            POSCAR poscar = new POSCAR(new Structure(sb));
            poscar.writeFile(outputDir + "/POSCAR_" + i);
        }
    }
    
    public static void testRingGenerator() {
        int[] numAtoms = new int[] {20, 35, 52, 53, 55};
        for (int i: numAtoms) {
            Structure struct = ringGenerator("S", i, 2.059, 107.9);
            POSCAR poscar = new POSCAR(struct);
            String outputDir = System.getProperty("user.dir") + "/unit_tests/initialize/ring/sulfur";
            poscar.writeFile(outputDir + "/POSCAR_" + i);
        }
    }
    
    public static void testLocalMutationOfRing() {
        int[] numAtoms = new int[] {20, 35, 52, 53, 55};
        for (int i: numAtoms) {
            Structure struct = ringGenerator("S", i, 2.059, 107.9);
            Object[] clusList = ClusterFactory.structureToObjectArray(struct);
            clusList = ClusterFactory.mutateBareClusterRandomMoveLocal(clusList);
            struct = ClusterFactory.objectToStructure(clusList, struct.getCellVectors()[0].length());
            POSCAR poscar = new POSCAR(struct);
            String outputDir = System.getProperty("user.dir") 
                             + "/unit_tests/ga_operations/local_mutation/ring/sulfur";
            poscar.writeFile(outputDir + "/POSCAR_" + i);
        }
    }
    
    private static Structure ringGenerator(String element, int numAtoms, double bondLength, double bondAngle) {
        // Generate the ring
        Object[] clusList = ClusterFactory.ringGenerator(element, numAtoms, bondLength, bondAngle);
        double cellSize = ClusterFactory.getCellSize(clusList, 15);
        ClusterFactory.moveCGToCenter(clusList, cellSize);
        ClusterFactory.fixOverlapCluster(clusList);
        return ClusterFactory.objectToStructure(clusList, cellSize);
    }
    
}
