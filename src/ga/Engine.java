package ga;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import ga.io.GAIOUtils;
import ga.io.Logger;
import ga.multithreaded.Candidates;
import ga.multithreaded.NewPool;
import ga.potential.MTP;
import ga.structure.Cluster;
import ga.utils.GAMath;
import ga.utils.GAUtils;

public class Engine implements Runnable {
	// Variables inherit from INGA
	private double m_MutRate = Input.getMutRate();
	private double m_SeedRate = Input.getSeedRate();
	private int m_NumAtoms = GAMath.arraySum(Input.getEleNums());
	private int[] m_EleNums = Input.getEleNums();
	private String[] m_EleNames = Input.getEleNames();
	private int m_InterpolatingAttemptLimit = Input.getInterpolatingAttempts();
	
	// Local binding for global variables.
	private Candidates m_Candidates;
	private NewPool m_Pool;
	private boolean m_ChargedSystem;

	public Engine(Candidates candidates, NewPool pool, boolean isCharged){
		this.m_Candidates = candidates;
		this.m_Pool = pool;
		this.m_ChargedSystem = isCharged;
	}
	
	public void run() {
		long st, et;
		while (!Thread.currentThread().isInterrupted() && !Start.Complete.get() && !Start.Retrain.get()) {
			Logger.detail("Start evaluating next cluster.");
			Cluster cluster = null;
			boolean successful = false;
			boolean realistic = false;     // Relaxed structure is either collapsed or exploded.
			boolean wasRetrained = false;  // Was retrained.
			
			// Only applicable to preprocessing in GA_AL with DOptimality.
			boolean extrapolating = true;  // Relaxed structure is extrapolating.
			int interpolatingAttempts = 0; // Attempts to only relax into interpolating region 
										   // during preprocessing of GA_AL
			
			while (!successful || !realistic || extrapolating) {
				
				//**************************** Get Cluster structure *****************************//
				// Use unfinished candidates when ever there is one.
				// Clusters are added to unfinished list only at restart or at Start.retrainMTP().
				boolean fromUnfinishedList = false;
				synchronized(this.m_Candidates) {
					if(!m_Candidates.getUnFinishedCandidates().isEmpty()) {
						cluster = m_Candidates.getUnFinishedCandidates().poll();
						fromUnfinishedList = true;
						Logger.basic("Relax unfinished candidates: " + "cluster-" + cluster.getCandidateIndex());
						if (Start.TrainedList.contains(cluster.getCandidateIndex())) {
							wasRetrained = true;
						} // If it wasn't retrained, it should be from restart recovery.
					}
				}
				
				// Generate an entirely new cluster;
				if (!fromUnfinishedList) {
					if (cluster == null) {
						int index = this.getNextIndex();
						String type = this.getNewType(index);
						Logger.basic("Generate a new structure of type \"" + type + "\" for cluster-" + index + ".");
						cluster = this.getNewCluster(type, index);
						Start.Zipped.add(false);
					} else if (!successful || !realistic || extrapolating) {
						// Last evaluation failed because: 1) code failed; 2) relaxed structure 
						// being exploded or overlapping; 3) Step into extrapolating region in
						// preprocessing stage of GA_AL mode.
						
						// Generate an entirely new cluster to replace the failed calculation.
						int oldIndex = cluster.getCandidateIndex();
						Logger.basic("Regenerate a new cluster of type \"" + cluster.getClusterType() 
						           + "\" for cluster-" + oldIndex + ".");
						cluster = this.getNewCluster(cluster.getClusterType(), oldIndex);
					}
				}
				
				// Reset flags to state will exit the loop, since a new cluster has been assigned.
				realistic = true; 
				extrapolating = false;
				
				//***************** Initialize calculation and check GA status *******************//
				if (!wasRetrained) {
					if (cluster.getCandidateIndex() >= Input.getNumOperations()) {
						Start.Complete.set(true); // Will exit loop after this cluster.
					}
					
					this.updateDatFiles(cluster); // Record initial structure.
					cluster.initializeCalculation();
					
					if (Input.getRetrainMode() == Input.RetrainMode.DOptimality) {
						Engine.checkExtrapolation(cluster);
						if (cluster.extrapolating()) {
    						if (cluster.getCandidateIndex() > Input.getPreprocessing()) {
    							Engine.updateRetrainList(cluster);
    							GAUtils.cleanFolder(cluster.getEnergyMinimizer().getName(), cluster.getCalcDir());
    							break; // Skip energy evaluation if extrapolating.
    						} else {
    						    extrapolating = true;
								Logger.detail("Preprocessing: Initial structure is extrapolating. "
								            + "Regenerate a structure.");
    						    continue;
    						}
						} 
					} else if (Input.getRetrainMode() == Input.RetrainMode.Batch
							&& cluster.getCandidateIndex() % Input.getRetrainStep() == 0) {
						// Because there is only one thread locks m_Candidates, there won't be a
						// cluster being assigned the index "Input.getRetrainStep + 1".
						Start.Retrain.set(true);
					}
				} else {
					// Backup past calculation and copy here VASP calculation from training folder;
					// Cluster.m_Structure is not updated yet. It's unsafe to use this structure.
					// VASP relaxed structure is read later by Cluster.minimize() call.
					Logger.detail("It has been retrained on. Copy VASP results from training folder.");
					cluster.setEnergyMinimizer("VASP");
					String backup = cluster.getCalcDir() + HPC.FILESPTR + (Start.TrainedTimes - 1);
					HPC.mkdir(backup);
					HPC.copy(cluster.getCalcDir(), backup, false);
					HPC.rm(cluster.getCalcDir(), true, false, true);
					String trainDir = Start.WorkDir + HPC.FILESPTR + "train" 
								    + HPC.FILESPTR + Start.TrainedTimes 
								    + HPC.FILESPTR + cluster.getCandidateIndex();
					HPC.copy(trainDir, cluster.getCalcDir(), false);
					
					// Use Cluster.minimize() below to update structure and read energy.
					cluster.setRelaxed(true, false);
				}
				
				//**************************** Energy Evaluation ********************************//
				// Skip expensive evaluation if extrapolating in DOptimality mode or interrupted.
				if (!Thread.currentThread().isInterrupted()) {
					st = System.currentTimeMillis();
					successful = cluster.minimize();
					et = System.currentTimeMillis();
					Logger.detail("Evaluating cluster-" + cluster.getCandidateIndex() + " takes "
							    + GAIOUtils.formatElapsedTime(et - st));
					
					// Executable stopped with non-zero exit code or has ErrorStr in output.
					if (!successful) {
						continue;
					}
					
					cluster.setRelaxed(true, false);
					//Accept the structure if they were retrained by VASP even if they are unrealistic.
					if (!wasRetrained && !cluster.realistic()) {
						realistic = false;
						if (cluster.getCandidateIndex() > Input.getPreprocessing() 
								&& Input.getRetrainMode() == Input.RetrainMode.DOptimality) {
							// GA_AL
							if (cluster.isCollapsed()) {
								// VASP will have memory issues relaxing POSCARs with completely overlapped atoms;
								Logger.detail("Relaxed structure collapses, use initial structure for retraining.");
							} else if (cluster.isExploded()) {
								Logger.detail("Relaxed structure explodes, use initial structure for retraining.");
							}
							if (!GAUtils.contains(Start.NewData, cluster)) {
								Start.NewData.add(cluster);
								if (Start.NewData.size() >= Input.getSelectPoolSize()) {
									Start.Retrain.set(true);
								}
							}
							GAUtils.cleanFolder(cluster.getEnergyMinimizer().getName(), cluster.getCalcDir());
							break; // Continue on next cluster. This one is added to training list.
						} else {
							// GA_DFT or GA_LAMMPS or Preprocessing of GA_AL
							if (Input.checkStructure()) {
								continue; // Regenerate a new structure for this cluster.
							} else {
								realistic = true;
								Logger.warning("Structure check has been shut down. " +
								               "Unrealistic structures are acceptable. Use with caution!");
							}
						}
					}

					// Active mode: check extrapolation for relaxed structure.
					if (Input.getRetrainMode() == Input.RetrainMode.DOptimality) {
						Engine.checkExtrapolation(cluster); // Score of VASP / MTP relaxed structure.
						if (!wasRetrained && cluster.extrapolating()) {
							if (cluster.getCandidateIndex() <= Input.getPreprocessing()) {
								// Attempts to relax within interpolating region.
								if (interpolatingAttempts < m_InterpolatingAttemptLimit) {
									Logger.detail("Preprocessing: step into extrapolating region! "
											    + "Attempted: " + interpolatingAttempts + " times.");
									extrapolating = true;
									interpolatingAttempts++;
									continue; // Regenerate a new cluster and re-relax by LAMMPS.
								} else {
									Logger.detail("Preprocessing: step into extrapolating region! "
												+ "Attempted: " + interpolatingAttempts + " times. "
											    + "Reached attempt limit. Set energy to 0 eV.");
									cluster.setEnergy(0);
								}
							} else {
								// Add to retraining.
								Engine.updateRetrainList(cluster);
								GAUtils.cleanFolder(cluster.getEnergyMinimizer().getName(), cluster.getCalcDir());
								break; // Continue on next cluster.
							}
						}
					}
					
					// Updating candidates.dat, similarity tables, and pool.dat.
					// 1. Not active mode: successful relaxation and realistic relaxed structure
					//    (could be unrealistic if STRUCTURECHECK=False).
					// 2. DOptimality mode: not been added to training list (unrealistic or extrapolating).
					// 3. DOptimality mode: Preprocessing stage, interpolating or exceeding attempt limit.
					this.updateDatFiles(cluster);
					GAUtils.cleanFolder(cluster.getEnergyMinimizer().getName(), cluster.getCalcDir());
				}
			}

			// Clean-up operations for this cluster. Cluster won't be null here.

			if (Input.zip()) {
				st = System.currentTimeMillis();
				Engine.archiveOneCalculation(cluster, m_Pool);
				et = System.currentTimeMillis();
				Logger.detail("Archiving cluster-" + cluster.getCandidateIndex() + " takes " 
				            + GAIOUtils.formatElapsedTime(et - st));
				
				if (m_Candidates.getSize() % 100 == 0) {
					// Periodically compress calculations. Since old calculations are constantly dropped from pool,
					// we should from-time-to-time scan calculations and zip unnecessary ones.
					st = System.currentTimeMillis();
					Engine.archiveCalculations(m_Candidates, m_Pool);
					et = System.currentTimeMillis();
					Logger.detail("Scanning and archiving calculations takes " + GAIOUtils.formatElapsedTime(et - st));
				}
			}
			Logger.basic("Finish evaluating cluster-" + cluster.getCandidateIndex() 
			           + ". Elapsed: " + GAIOUtils.elapsedTime());
		}
	}
	
	/**
	 * Generate an entirely new cluster or re-generate a new one because last evaluation failed to
	 * relaxed the structure to a realistic shape. Candidate index and other index-related fields
	 * are also updated.
	 * 
	 * @param type
	 * @param index Candidate index to be assigned.
	 * @return
	 */
	private Cluster getNewCluster(String type, int index) {
		Cluster cluster;
		
		//Logger.basic("Generating new cluster of type \"" + type + "\"");
		synchronized (m_Pool) {
			cluster = new Cluster(type, m_NumAtoms, Input.getDistImage(), m_EleNames, m_EleNums, m_ChargedSystem, m_Pool);
		}
		
		// Assign candidate index to the new cluster and update informations that are determined by
		// candidate index: m_CalcDir, m_StatusLine.
		synchronized (m_Candidates) {
			cluster.setCandidateIndex(index);
			cluster.setCalcDir(Candidates.ALLCANDIDATES + HPC.FILESPTR + index);
			cluster.updateStatusLineNumber();
		}
		
		return cluster;
	}
	
	private String getNewType(int index) {
		String type;
		synchronized (m_Pool) {
			double targetPoolSize = Input.getPopNum();
			double poolSize = m_Pool.getSize();
			double mutateRate = m_MutRate * targetPoolSize;
			double seedRate = m_SeedRate * targetPoolSize;
			double nextChoice = GAMath.random() * targetPoolSize; // [0, targetPoolSize)
			if (index <= Input.getInitSize()) {
				type = Input.getInitType();
			} else if (poolSize == 1) { // Cannot cross-over.
				if (m_MutRate + m_SeedRate != 0) {
					double choice = GAMath.random() * (m_MutRate + m_SeedRate); // Normalize.
					if (choice <= m_MutRate && m_MutRate != 0) {
						type = "Mut";
					} else {
						type = "Seed";
					}
				} else { // Use purely cross-over.
					type = Input.getInitType();
				}
			} else if (nextChoice <= mutateRate) {
				type = "Mut"; // Mutation: 0 <= nextChoice <= mutateRate
			} else if (nextChoice <= mutateRate + seedRate) {
				type = "Seed"; // Seeded: mutateRate < nextChoice <= seedRate + mutateRate
			} else {
				type = "Off"; // Off-spring: seedRate + mutateRate < nextChoice < poolSize
			}
		}
		return type;
	}
	
	/**
	 * Update candidates.dat, tables and chosen.dat
	 * @param cluster
	 */
	private void updateDatFiles(Cluster cluster) {
		long st, et;
		int index = cluster.getCandidateIndex();
		
		if (!cluster.relaxed()) {
			// 1. New cluster 
			// 2. regenerate a new cluster due to unrealistic relaxation result
			// 3. unfinished cluster recovered at restart.
			if (cluster.getClusterType().equals("Off")) {
				// We can only write chosen.dat after an index has been assigned.
				cluster.writeChosen();
			} else if (cluster.getClusterType().equals("Mut")) {
				HPC.write(String.format("%4d -> %4d type = %s %s", cluster.getParents()[0], index, 
						"Mut", cluster.getClusterSubType()), Cluster.MUTATION, true);
			}
			synchronized(m_Candidates) {
				if (index > m_Candidates.getSize()) {
					// Initialize lines for candidates.dat.
					Logger.detail("Initialize lines for cluster-" + index + " in candidates.dat.");
					m_Candidates.addToCandidatesList(cluster);
					m_Candidates.appendToFile(cluster); // write unrelaxed structure to candidates.dat.
				} else {
					// Update lines for candidates.dat and tables.
					Logger.detail("Update candidates.dat for cluster-" + index + ".");
					m_Candidates.updateCandidatesList(index - 1, cluster);
					cluster.writeToCandidates();
				}
				
				/*
				// To guarantee the tables are written in correct order, the tables have to be updated
				// when a cluster is assigned an index for the first time and added to candidates.
				// This implies that tables and candidates should be bind together to be thread-safe.
				Logger.basic("Update tables for cluster-" + cluster.getCandidateIndex());
				st = System.currentTimeMillis();
				Tables.newUpdate(cluster);
				et = System.currentTimeMillis();
				Logger.detail("Updating tables for cluster-" + cluster.getCandidateIndex() + " takes "
				            + GAIOUtils.formatElapsedTime(et - st));
				*/
			}
		} else {
			// Successfully relaxed
			synchronized(m_Candidates) {
				Logger.basic("Write ground-state energy.");
				st = System.currentTimeMillis();
				cluster.writeToCandidates();
				et = System.currentTimeMillis();
				Logger.detail("Writing status takes " + GAIOUtils.formatElapsedTime(et - st));
				
				/*
				Logger.basic("Update tables for cluster-" + cluster.getCandidateIndex());
				st = System.currentTimeMillis();
				Tables.newUpdate(cluster);
				et = System.currentTimeMillis();
				Logger.detail("Updating tables for cluster-" + cluster.getCandidateIndex()
				            + " takes " + GAIOUtils.formatElapsedTime(et - st));
				*/
			}
			
			synchronized(m_Pool) {
				// TODO: check whether require reads on Tables. If it does, it might cause deadlock.
				Logger.basic("Update pool.dat.");
				st = System.currentTimeMillis();
				// There are scenarios in which cluster energy could be manually set to 0:
				// 1) In GA_AL preprocessing, if a cluster is still extrapolating after attempted
				//    several times, it's deemed as a bad cluster and shouldn't be added to pool.
				//if (cluster.getEnergy() != 0.0) {
				//	m_Pool.update(cluster, true);
				//}
				m_Pool.update(cluster, true);
				et = System.currentTimeMillis();
				Logger.detail("Updating pool for cluster-" + cluster.getCandidateIndex()
						+ " takes " + GAIOUtils.formatElapsedTime(et - st));
				if (Input.getRetrainMode() == Input.RetrainMode.DOptimality
						&& !cluster.relaxedByVASP() // avoid retraining when copying VASP results from last retrain cycle.
						&& cluster.getCandidateIndex() > Input.getPreprocessing()
						&& cluster.getCandidateIndex() > Input.getLAMMPSRatioOffset() + Start.lastRetrainedIndex
						&& m_Pool.getNonVASPClusterRatio() >= Input.getMaxLAMMPSPoolRatio()) {
					Logger.basic("Non-VASP pool clusters exceed maximum allowed ratio. " + 
								 "Start retraining. Ratio: " + m_Pool.getNonVASPClusterRatio());
					Start.Retrain.set(true);
				}
				if (m_Pool.isConverged()) {
					// Only when it's false, set to true.
					Start.Complete.compareAndSet(false, true);
				}
			}
		}
	}
	
	private int getNextIndex() {
		int index;
		synchronized(m_Candidates) {
			// The candidateIndex doesn't represent the chronological order of the time at 
			// which relaxation finishes, but the time that the cluster is firstly generated.
			index = m_Candidates.getSize() + 1; // Starts from 1.
		}
		return index;
	}
	
	/**
	 * Check whether the current structure of given cluster is extrapolating or not. If it is,
	 * add it to the training pool. When the training pool exceed SELECTPOOLSIZE, set m_Retrain to
	 * true to signal the start of retrain.
	 * 
	 * @param cluster
	 * @return
	 */
	public static boolean checkExtrapolation(Cluster cluster) {
		if (cluster.getEnergyMinimizer().getName().equals("LAMMPS")) {
			MTP.isExtrapolating(cluster);
		} else if (cluster.getEnergyMinimizer().getName().equals("VASP")) {
			// Has been retrained on. Use maxvol grade of relaxed structure.
			List<Double> grades = MTP.readMaxvolGrade(cluster.getCalcDir() + HPC.FILESPTR + MTP.getCFGOutput());
			double grade = grades.get(grades.size() - 1);
			cluster.setMaxvolGrade(grade);
			boolean isExtrapolating = (!cluster.relaxed()) ? (grade > Input.getBreakThreshold()) 
	                                                         : (grade > Input.getSelectThreshold());
			cluster.setExtrapolating(isExtrapolating);
		}
		cluster.writeMaxvolGrade();
		return cluster.extrapolating();
	}
	
	/**
	 * Helper function to update the to-be-retrained list of clusters.
	 * @param cluster
	 */
	public static void updateRetrainList(Cluster cluster) {
		synchronized(Start.NewData) {
			if (!GAUtils.contains(Start.NewData, cluster)) {
				Logger.basic("Cluster-" + cluster.getCandidateIndex() + " extrapolates! Add it to the training pool.");
				Start.NewData.add(cluster);
				if (Start.NewData.size() >= Input.getSelectPoolSize()) {
					Logger.basic("Reached extrapolating pool size. Start retraining. "
							   + "Extrapolating clusters: " + Start.NewData.size());
					Start.Retrain.set(true);
				}
			}
		}
	}

	/**
	 * Archive a single calculation.
	 * @param c
	 * @param m_Pool
	 */
	public static void archiveOneCalculation(Cluster c, NewPool m_Pool) {
		List<Integer> poolIndices = new ArrayList<>();
		synchronized(m_Pool) {
			poolIndices = m_Pool.getPoolIndices();
		}

		int index = c.getCandidateIndex();
		if (!Start.Zipped.get(index - 1)
		 && c.relaxed()
		 && !poolIndices.contains(index)
		 && !Start.TrainedList.contains(c.getCandidateIndex())
		 && !GAUtils.contains(Start.NewData, c)) {
			Engine.archiveOneCalculation(c.getCalcDir());
		}
	}

	/**
	 * Periodically compress calculations periodically to release resources.
	 * We need to periodically check this since old calculations could be
	 * dropped from pool and they need to be compressed as well.
	 * 
	 * Make it static to use at restart.
	 * @param m_Candidates
	 * @param m_Pool
	 */
	public static void archiveCalculations(Candidates m_Candidates, NewPool m_Pool) {
		List<Integer> poolIndices = new ArrayList<>();
		synchronized(m_Pool) {
			poolIndices = m_Pool.getPoolIndices();
		}

		synchronized(m_Candidates) {
			List<Cluster> allCandidates = m_Candidates.getCandidates();
			for (Cluster c: allCandidates) {
				int index = c.getCandidateIndex();
				if (!Start.Zipped.get(index - 1)
				 && c.relaxed()
				 && !poolIndices.contains(index)
				 && !Start.TrainedList.contains(c.getCandidateIndex())
				 && !GAUtils.contains(Start.NewData, c)) {
					Engine.archiveOneCalculation(c.getCalcDir());
				}
			}
		}
	}

	private static void archiveOneCalculation(String calcDir) {
		int index = GAIOUtils.convertToIndex(calcDir);
		try {
			List<String> files = HPC.listFiles(calcDir, true);
			
			// Exclude index.zip from the files.
			List<String> filesCopy = files.stream().collect(Collectors.toList());
			for (String file: filesCopy) {
				if (file.endsWith(".zip")) {
					files.remove(file);
				}
			}
			// Save entry names relative to calcDir.
			HPC.zipFilesRelative(files, calcDir + HPC.FILESPTR + index + ".zip", calcDir);
			//HPC.zipAllFiles(calcDir, calcDir + HPC.FILESPTR + index + ".zip", true, true);
			HPC.rm(files, true);
			Start.Zipped.set(index - 1, true);
		} catch (IOException e) {
			Logger.error("Failed to archive cluster-" + index);
		}
	}
}