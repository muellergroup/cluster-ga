package ga.minimizer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ga.HPC;
import ga.Input;
import ga.io.Logger;
import ga.io.VASP.INCAR;
import ga.structure.Cluster;
import ga.structure.ClusterFactory;
import matsci.Species;
import matsci.io.vasp.POSCAR;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.util.arrays.ArrayUtils;

public class VASP implements EnergyMinimizer{
	
	private static String Name = "VASP";
	private static String[] InputFiles = {"POTCAR", "KPOINTS", "INCAR"};
	private static String InputFile = "INCAR";
	private static String OutputFile = "OUTCAR";
	private static String OutputStructureFile = "CONTCAR";
	private static String InputStructureFile = "POSCAR";
	private static String Stdout = "stdout"; // Redirect output to. Inspected when fixing failed runs.
	private static String ErrorStr = "Error";
	private static String EnergyStr = "energy  without entropy="; // Unique energy string at the end of each ioinc step.
	private static String FinishStr = "Elapsed";
	private String calcDir;	// e.g. <Root>/Al/8/all_candidates/3. "m_CalNum" in Cluster.java.
	private Cluster cluster; // The cluster structure to be calculated.
	private String exe = null;
	private boolean useParallel = true;
	private String parallelCommand = null; // "orterun", "mpiexec" is also possible.
	private List<String> output = null; // List of lines in OUTCAR.
	
	public static String[] REDUNDANTFILELIST = {"CHG", "CHGCAR", "REPORT", "WAVECAR"};
	
	public VASP(Cluster cluster, String exe, boolean useParallel, String parallelCommand) {
		this.cluster = cluster;
		this.calcDir = cluster.getCalcDir();
		this.exe = exe;
		this.useParallel = useParallel;
		this.parallelCommand = parallelCommand;
		// We don't initiate output because the calculation is not submitted yet.
	}
	
	/**
	 * Copy input files from the Input.Root folder to the m_CalNum folder of the target cluster.
	 */
	public void createInputs(boolean moveToCenter){
		Logger.detail("Generating " + this.getName() + " input files.");
		
		if(cluster.getIsSurfaceCalc()){
			this.writeSurfaceStructure(moveToCenter);
			// TODO: check whether break anything.
			//m_Structure = m_SupportedCluster.getSupportedClusterStructure();
		} else {
			this.writeStructure(moveToCenter);
		}
		
		// Assume all these are in the Root dir.
		for(int i = 0; i < InputFiles.length; i++){
			HPC.copy(Input.getRoot() + HPC.FILESPTR + InputFiles[i], calcDir, false); 
		}
	}
	
	/**
	 * Write GA-generated structure to file. The cluster's center of mass is moved from
	 * the origin to the center of the unit cell, compared with Cluster.convertToStructure() 
	 * function.
	 * 
	 * This function should be able to handle both bare clusters and ligated clusters.
	 */
	public void writeStructure(boolean moveToCenter) {
		double cellSize = cluster.getCellSize();
		int[] eleNums = cluster.getEleNums();
		// This method should not change the cluster.m_ClusList.
		Object[] clusList = cluster.getClusList();
		
		// Assume 3-dimensional cell for now.
		double[][] latticeVecsArray = {{cellSize, 0.0, 0.0},
		                               {0.0, cellSize, 0.0},
		                               {0.0, 0.0, cellSize}};
		Vector[] latticeVecs = new Vector[3];
		for (int i = 0; i < latticeVecs.length; i++) {
			latticeVecs[i] = new Vector(latticeVecsArray[i]);
		}
		BravaisLattice lattice = new BravaisLattice(latticeVecs);
		
		StructureBuilder sb = new StructureBuilder(lattice);
		sb.setDescription(calcDir);
		
		int totalNumAtoms = Arrays.stream(eleNums).sum();
		Coordinates[] siteCoords = new Coordinates[totalNumAtoms];
		Species[] siteSpecies = new Species[totalNumAtoms];
		int counter = 0;
		for (int i = 0; i < eleNums.length; i++) {
			for (int j = 0; j < eleNums[i]; j++) {
				Object[] atom = (Object[]) clusList[counter];
				siteSpecies[counter] = Species.get((String) atom[0]);
				// Move the COM of cluster to the center of unit cell. ClusList here is already
				// called with cluster.CoM() and therefore, its COM is at the origin.
				double[] coord = new double[3];
				if (moveToCenter) {
					coord[0] = ((double) atom[1]) + cellSize/2;
					coord[1] = ((double) atom[2]) + cellSize/2;
					coord[2] = ((double) atom[3]) + cellSize/2;
				} else {
					coord[0] = ((double) atom[1]);
					coord[1] = ((double) atom[2]);
					coord[2] = ((double) atom[3]);
				}
				siteCoords[counter] = new Coordinates(coord, CartesianBasis.getInstance());
				counter++;
			}
		}
		sb.addSites(siteCoords, siteSpecies);
		// Keep the order as LIGAND for now. In the future, we should alphabetize the order of elements.
		POSCAR poscar = new POSCAR(sb, false);
		poscar.writeFile(this.calcDir + HPC.FILESPTR + "POSCAR");
	}
	
	//TODO
	public void writeSurfaceStructure(boolean moveToCenter) {
		
	}
	
	/**
	 * Read structure from file.
	 * 
	 * @param relaxed Read initial structure or relaxed structure
	 * @return structure
	 */
	public Structure readStructure(boolean relaxed){
		if (relaxed) {
			return readStructure(calcDir, OutputStructureFile);
		} else {
			return readStructure(calcDir, InputStructureFile);
		}
	}
	
	/**
	 * A generic function to read a structure file (either initial or relaxed) for this executable.
	 * @return the Structure object.
	 */
	public static Structure readStructure(String calDir, String structureFile) {
		POSCAR poscar = new POSCAR(calDir + HPC.FILESPTR + structureFile);
		String[] elementsStr = Input.getEleNames();
		StructureBuilder sb = new StructureBuilder(poscar);
		for (int i = sb.numDefiningSites() - 1; i >= 0; i--) {
			String elementSymbol = sb.getSiteSpecies(i).getElementSymbol();
			if (!ArrayUtils.arrayContains(elementsStr, elementSymbol)) {
				sb.removeSite(i);
			}
		}
		return new Structure(sb);
	}
	
	/**
	 * Read only coordinates from the relaxed structure.
	 * @return coordinates of atoms of the relaxed structure in the format of m_ClusList.
	 */
	public Object[] readCoords(){
		POSCAR poscar = new POSCAR(calcDir + HPC.FILESPTR + OutputStructureFile);
		Object[] clusList = new Object[poscar.numDefiningSites()];
		for (int i = 0; i < poscar.numDefiningSites(); i++) {
			Object[] atom = new Object[] {poscar.getSiteSpecies(i).getElementSymbol(), 
					                      poscar.getSiteCoords(i).cartCoord(0),
					                      poscar.getSiteCoords(i).cartCoord(1),
					                      poscar.getSiteCoords(i).cartCoord(2)}; 
			clusList[i] = atom;
		}
		return clusList;
	}
	
	/**
	 * Convert the output file content to a list of strings.
	 * TODO: make it more efficient. For a large cluster and poor structure, the relaxation might
	 * give a huge amount of output and the List representation could be memory-intensive.
	 * 
	 * @return The output file content as a list of strings.
	 */
	public List<String> readOutput() {
		String file = calcDir + HPC.FILESPTR + OutputFile;
		if (Files.exists(Paths.get(file))) {
			try {
				output = HPC.readAllLines(file);
				return output;
			} catch (IOException e) {
				e.printStackTrace();
				Logger.error("Cannot read output: " + file + ". Don't exist! Attempting restart.");
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Read the energy from the minimizer's output file.
	 * @return Total energy of the relaxed cluster.
	 *         Double.MAX_VALUE if the output file doesn't exist.
	 */
	public double readEnergy() {
		if (output == null) {
			this.readOutput();
		}
		return VASP.readEnergy(output);
	}
	
	/**
	 * @param output
	 * @return The value after "energy(sigma->0)"
	 */
	public static double readEnergy(List<String> output) {
		double finalEnergy = 0.0;
		if (output != null) {
			for(int i = output.size() - 1; i >= 0; i--){
				if(output.get(i).contains(EnergyStr)){
					String[] split = output.get(i).split("=");
					// Use "energy(sigma->0)" value instead of "energy without entropy".
					String energyStr = split[split.length - 1].trim();
					try {
						finalEnergy = Double.parseDouble(energyStr);
					} catch (NumberFormatException e) {
						Logger.error("Failed to parse energy string: " + energyStr + ". Set 0 as Eenrgy.");
						finalEnergy = 0.0;
					}
					break;
				}
			}
			return finalEnergy;
		}
		return finalEnergy;
	}
	
	/**
	 * Check whether calculation complete successfully, by searchinf for "Elapsed" string in OUTCAR.
	 * @return True if there is no ErrorStr in the output file.
	 *         False if there is ErrorStr or the output file doesn't exist.
	 */
	public boolean isCalSuccessful() {
		List<String> output = this.readOutput();
		if (output == null) {
			Logger.detail("OUTCAR doesn't exist. Calculation failed.");
			return false;
		}
		
		for (String line: output) {
			if (line.contains(ErrorStr)) {
				Logger.detail("OUTCAR contains \"Error\". Calculation failed.");
				return false;
			} else if (line.contains(FinishStr)) {
				return true;
			}
		}
		Logger.debug("OUTCAR doesn't contain \"Elapsed\". Calculation failed.");
		return false;	
	}
	
	/**
	 * Determine whether a VASP calculation in the given folder finished successfully.
	 * @param dir The folder where VASP calculation happens.
	 * @return True if succeed.
	 */
	public static boolean isCalcSuccessful(String dir) {
		List<String> outputLines;
		boolean finished = false;
		try {
			outputLines = HPC.readAllLines(dir + HPC.FILESPTR + OutputFile);
		} catch (IOException e) {
			// Cannot read this file or it doesn't exist.
			return false;
		}
		if (outputLines == null || outputLines.isEmpty()) {
			return false;
		} else {
			for (String i: outputLines) {
				if (i.contains(ErrorStr)) {
					return false;
				} else if (i.contains(FinishStr)) {
					finished = true;
				}
			}
		}
		if (finished) {
			return true; // TODO: if a calculation failed, does it also contain "Elapsed time"?
		}
		return false; // Don't contain the string "Elapsed time"
	}
	
	/**
	 * Set the path of VASP executable on the native platform.
	 * @param path Path of VASP executable.
	 */
	public void setExe(String path) {
		exe = path;
	}
	
	/**
	 * @return A list of string for creating a ProcessBuilder object.
	 */
	public List<String> getCommands() {
		// e.g. mpirun -n 12 vasp_gam
		List<String> commands = new ArrayList<String>();
		if (this.useParallel && this.parallelCommand != null) {
			String[] mpi = this.parallelCommand.split("\\s+");
			for (String i : mpi) {
				commands.add(i);
			}
		}
		commands.add(this.exe);
		return commands;
	}
	
	public static String[] getInputFiles() {
		return InputFiles;
	}
	
	public String getName() {
		return Name;
	}
	
	public boolean existOutput() {
		if (Files.exists(Paths.get(calcDir + System.getProperty("file.separator") + OutputFile))) {
			return true;
		}
		return false;
	}
	
	public static String getOutputFileName() {
		return OutputFile;
	}
	
	public static String getInputFileName() {
		return InputFile;
	}
	
	public static String getStructureFileName(boolean output) {
		if (output) {
			return OutputStructureFile;
		} else {
			return InputStructureFile;
		}
	}
	
	public String getFinishStr() {
		return FinishStr;
	}
	
	public String getErrorStr() {
		return ErrorStr;
	}
	
	public String getStdOutput() {	
		return Stdout;
	}
	
	public String getCalDir() {
		return this.calcDir;
	}
	
	public int relax() {
		int exitCode = 1;
		if (!cluster.isCharged()) {
			exitCode = HPC.execute(this.getCommands(), calcDir, calcDir + HPC.FILESPTR + Stdout, true);
		} else {
			Logger.detail("Submitting preconverge calculation.");
			int preconvergeCode = this.preconverge();
			Logger.detail("Preconvergence finished. Checking for errors.");
			List<String> output = EnergyMinimizer.readOutput(calcDir + HPC.FILESPTR + "preconverge", "OUTCAR");
			boolean isCalSuccessful = EnergyMinimizer.isCalSuccessful(output, "Error");
			// Don't need to check successfulness of pre-convergence calculation.
			if (!isCalSuccessful || (preconvergeCode != 0)) {
				Logger.warning("Preconvergence failed! Better check what happened.");
			}
			Logger.detail("Continue relaxation by preconverged WAVECAR.");
			HPC.copy(calcDir + HPC.FILESPTR + "preconverge" + HPC.FILESPTR + "WAVECAR", calcDir, false);
			Map<String, String> param = new HashMap<>();
			param.put("ISTART", "1");
			VASP.editINCAR(calcDir + HPC.FILESPTR + "INCAR", param);
			exitCode = HPC.execute(this.getCommands(), calcDir, calcDir + HPC.FILESPTR + Stdout, true);
		}
		output = null; // refresh;
		return exitCode;
	}
	
	/**
	 * Perform pre-converge calculations for charged system.
	 * @return exitCode of the pre-converge calculation.
	 */
	public int preconverge() {
		// Create folder and copy files over
		String preconverge = calcDir + HPC.FILESPTR + "preconverge";
		File preconvergeDir = new File(preconverge);
		if (!preconvergeDir.exists()) {
			preconvergeDir.mkdirs();
		}
		HPC.copy(calcDir + HPC.FILESPTR + InputStructureFile, preconverge, false);
		for (int i = 0; i < InputFiles.length; i++) {
			HPC.copy(calcDir + HPC.FILESPTR + InputFiles[i], preconverge, false);
		}
		String incar = preconverge + HPC.FILESPTR + "INCAR";
		List<String> incarList = VASP.editIncarForPreconverge(incar);
		HPC.write(incarList, incar, false);
		
		return HPC.execute(this.getCommands(), preconverge, preconverge + HPC.FILESPTR + "stdout", true);
	}
	
	/**
	 * Perform MD calculation in workDir and redirect stdout and stderr to "workDir/stdout".
	 * 
	 * @param workDir
	 * @param Struct
	 * @param INCAR_MD
	 * @param KPOINTS
	 * @param POTCAR
	 * @return
	 */
	public int MD(String workDir, String Struct, String INCAR_MD, String KPOINTS, String POTCAR) {
		HPC.copy(Struct, workDir + HPC.FILESPTR + InputStructureFile, false);
		HPC.copy(INCAR_MD, workDir + HPC.FILESPTR + "INCAR", false);
		HPC.copy(KPOINTS, workDir, false);
		HPC.copy(POTCAR, workDir, false);
		return HPC.execute(this.getCommands(), workDir, workDir + HPC.FILESPTR + "stdout", true);
	}
	
	/**
	 * Attempt to modify INCAR to complete calculation.
	 * Scenarios:
	 *   1. ZBRENT in 
	 * 
	 * @return whether we have a strategy to edit INCAR and fix the calculation.
	 */
	public boolean attemptToFix(int attempt) {
		// backup calculation.
		HPC.mkdir(calcDir + HPC.FILESPTR + "attempt_" + String.valueOf(attempt - 1));
		HPC.copy(calcDir, calcDir + HPC.FILESPTR + "attempt_" + (attempt -1), false);
		
		if (!HPC.exists(calcDir + HPC.FILESPTR + OutputStructureFile)) {
			return false;
		}

		Structure restartStruct;
		try {
			restartStruct = this.readStructure(true);
		} catch (Exception e) {
			Logger.detail("Failed to read CONTCAR: either empty or absent. Use POSCAR to attempt a fix.");
			e.printStackTrace();
			restartStruct = this.readStructure(false);
		}
		// Use CONTCAR (if read successfully) to restart.
		POSCAR restartPOSCAR = new POSCAR(restartStruct);
		restartPOSCAR.writeFile(calcDir + HPC.FILESPTR + VASP.InputStructureFile);

		//Case 1: Structure exploded after relaxation.
		boolean exploded = ClusterFactory.checkExplosionByContiguous(restartStruct);
		boolean collapsed = ClusterFactory.checkCollapse(restartStruct);
		if (exploded) {
			VASP.decreasePOTIM(calcDir + HPC.FILESPTR + "INCAR");
			return true;
		}
		
		// Case 2: "ZBRENT: fatal error in bracketing"
		List<String> stdout = HPC.read(calcDir + HPC.FILESPTR + Stdout);
		for (int i = stdout.size() - 1; i >= 0; i--) {
			if (stdout.get(i).contains("ZBRENT: fatal error in bracketing")) {
				Logger.detail("Attempt to fix: Change IBRION from CG to RMM-DIIS.");
				Map<String, String> params = new HashMap<>();
				params.put("IBRION", "1"); // Use RMM-DIIS instead of CG;
				VASP.editINCAR(calcDir + HPC.FILESPTR + "INCAR", params);
				return true;
			}
		}
		
		// Case 3: Unknown reason. Simply restart from CONTCAR (if read successfully).
		return true;
	}
	
	/**
	 * @param incar Full path to INCAR file from System.getProperty("user.dir").
	 * @return Synchronized list of lines of the edited INCAR.
	 */
	public static List<String> editIncarForPreconverge(String incar) {
		List<String> incarList = HPC.read(incar);
		for(int i = 0; i < incarList.size(); i++){
			if (incarList.get(i).contains("DIPOL")) {
				incarList.set(i, "#" + incarList.get(i));
			} else if (incarList.get(i).contains("LWAVE")) {
				incarList.set(i, "LWAVE=.TRUE.");
			} else if (incarList.get(i).contains("LVTOT")) {
				incarList.set(i, "LVTOT=.TRUE.");
			} else if (incarList.get(i).contains("LCHARG")) {
				incarList.set(i, "LCHARG=.TRUE.");
			}
		}
		return incarList;
	}
	
	/**
	 * Decrease POTIM by 5 fold when cluster exploded during relaxation because of overlapping.
	 * @param cluster
	 */
	public static void decreasePOTIM(String INCAR) {
		List<String> lines = HPC.read(INCAR);
		boolean replaced = false;
		for (int i = 0; i < lines.size(); i++) {
			String str = lines.get(i);
			if (str.startsWith("POTIM") || str.startsWith("#POTIM")) {
				str.replace("\\s+", "");
				String valueStr = str.split("=")[1].split("!")[0];
				double value = Double.parseDouble(valueStr);
				str = "POTIM=" + (value / 5); // A new object is created.
				lines.set(i, str);
				replaced = true;
				break;
			}
		}
		if (!replaced) {
			lines.add("POTIM=0.1") ; // default is 0.5.
		}
		HPC.write(lines, INCAR, false);
	}
	
	public static void editINCAR(String INCAR, Map<String, String> params) {
		INCAR incar = new INCAR(INCAR);
		Map<String, String> incarParams = incar.getParameters();
		
		for (String key: params.keySet()) {
			incarParams.put(key, params.get(key));
		}
		List<String> newIncarLines = new ArrayList<>(incarParams.size());
		for (String key: incarParams.keySet()) {
			newIncarLines.add(key + "=" + incarParams.get(key));
		}
		HPC.write(newIncarLines, INCAR, false);
	}
}
