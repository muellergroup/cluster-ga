package ga.minimizer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ga.HPC;
import ga.Input;
import ga.io.Logger;
import ga.io.LAMMPS.LAMMPSDataFile_MTP;
import ga.structure.Cluster;
import matsci.Element;
import matsci.Species;
import matsci.io.vasp.POSCAR;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.util.arrays.ArrayUtils;

/**
 * A wrapper of LAMMPS.
 */
public class LAMMPS implements EnergyMinimizer {
	
	private static String Name = "LAMMPS";
	private static String[] InputFiles = {"mlip.mtp", "input.lmp", "mlip.ini"};
	private static String InputFile = "input.lmp";
	private static String OutputFile = "log.lammps";
	private static String OutputStructureFile = "end.lmp";
	private static String InputStructureFile = "struct.lmp";
	private static String Potential = "mlip.mtp";
	private static String Stdout = "stdout"; // File to redirect stdout.
	private static String ErrorStr = "ERROR:";
	private static String EnergyStr = "Energy initial";
	private static String FinishStr = "Stopping criterion";
	private String calcDir;	// e.g. <Root>/Al/8/all_candidates/3. "m_CalNum" in Cluster.java.
	private Cluster cluster; // The cluster structure to be calculated.
	private String exe = null;
	private boolean useParallel = true;
	private String parallelCommand = ""; // "orterun", "mpiexec" is also possible.
	private List<String> output = null; // List of lines in LAMMPS output file.
	
	public static String[] REDUNDANTFILELIST = { "input.lmp", "mlip.ini", "stdout", "POSCAR", "lotf.log" };
	
	public LAMMPS(Cluster cluster, String exe, boolean useParallel, String parallelCommand) {
		this.cluster = cluster;
		this.calcDir = cluster.getCalcDir();
		this.exe = exe;
		this.useParallel = useParallel;
		this.parallelCommand = parallelCommand;
		// We don't initiate output because the calculation is not submitted yet.
	}
	
	/**
	 * Copy input files from the Input.Root folder to the m_CalNum folder of the target cluster.
	 */
	public void createInputs(boolean moveToCenter){
		Logger.basic("Generating " + this.getName() + " input files.");
		
		if(cluster.getIsSurfaceCalc()){
			this.writeSurfaceStructure(moveToCenter);
			//m_Structure = m_SupportedCluster.getSupportedClusterStructure();
		} else {
			this.writeStructure(moveToCenter);
		}
		
		// Assume all these are in the Root dir.
		for(int i = 0; i < InputFiles.length; i++){
			HPC.copy(Input.getRoot() + HPC.FILESPTR + InputFiles[i], calcDir, false); 
		}
	}
	
	/**
	 * Write GA-generated structure to file. The cluster's center of mass is moved from
	 * the origin to the center of the unit cell. This function should be able to handle both
	 * bare clusters and ligated clusters.
	 */
	public void writeStructure(boolean moveToCenter) {
		double cellSize = cluster.getCellSize(); // Side effect: Move center of mass to origin.
		int[] eleNums = cluster.getEleNums();
		// This method should not change the cluster.m_ClusList.
		Object[] clusList = cluster.getClusList();
		
		// Assume 3-dimensional cell for now.
		double[][] latticeVecsArray = {{cellSize, 0.0, 0.0},
		                               {0.0, cellSize, 0.0},
		                               {0.0, 0.0, cellSize}};
		Vector[] latticeVecs = new Vector[3];
		for (int i = 0; i < latticeVecs.length; i++) {
			latticeVecs[i] = new Vector(latticeVecsArray[i]);
		}
		BravaisLattice lattice = new BravaisLattice(latticeVecs);
		
		StructureBuilder sb = new StructureBuilder(lattice);
		sb.setDescription(calcDir);
		
		int totalNumAtoms = Arrays.stream(eleNums).sum();
		Coordinates[] siteCoords = new Coordinates[totalNumAtoms];
		Species[] siteSpecies = new Species[totalNumAtoms];
		int counter = 0;
		for (int i = 0; i < eleNums.length; i++) {
			for (int j = 0; j < eleNums[i]; j++) {
				Object[] atom = (Object[]) clusList[counter];
				siteSpecies[counter] = Species.get((String) atom[0]);
				// Move the COM of cluster to the center of unit cell. ClusList here is already
				// called with cluster.CoM() and therefore, its COM is at the origin.
				double[] coord = new double[3];
				if (moveToCenter) {
					coord[0] = ((double) atom[1]) + cellSize/2;
					coord[1] = ((double) atom[2]) + cellSize/2;
					coord[2] = ((double) atom[3]) + cellSize/2;
				} else {
					coord[0] = ((double) atom[1]);
					coord[1] = ((double) atom[2]);
					coord[2] = ((double) atom[3]);
				}
				siteCoords[counter] = new Coordinates(coord, CartesianBasis.getInstance());
				counter++;
			}
		}
		sb.addSites(siteCoords, siteSpecies);
		
		// Keep the order as LIGAND for now. In the future, we should alphabetize the order of elements.
		LAMMPSDataFile_MTP lammps = new LAMMPSDataFile_MTP(sb, false);
		lammps.writeFile(this.calcDir + HPC.FILESPTR + InputStructureFile); // struct.lmp
		POSCAR poscar = new POSCAR(sb, false);
		poscar.writeFile(this.calcDir + HPC.FILESPTR + VASP.getStructureFileName(false)); // POSCAR.
	}
	
	//TODO
	public void writeSurfaceStructure(boolean moveToCenter) {
		
	}
	
	/**
	 * Read structure from file.
	 * 
	 * @param relaxed Read initial structure or relaxed structure
	 * @return structure
	 */
	public Structure readStructure(boolean relaxed){
		if (relaxed) {
			return readStructure(calcDir, OutputStructureFile);
		} else {
			return readStructure(calcDir, InputStructureFile);
		}
	}
	
	/**
	 * Generic function for reading a LAMMPS structure.
	 * @return the Structure object.
	 */
	public static Structure readStructure(String calDir, String structureFile) {
		String lammpsName = calDir + HPC.FILESPTR + structureFile;
		String[] elementsStr = Input.getEleNames();
		Element[] elements = new Element[elementsStr.length];
		for (int i = 0; i < elementsStr.length; i++) {
			elements[i] = Element.getElement(elementsStr[i]);
		}
		boolean[] isVectorPeriodic = { true, true, true };
		LAMMPSDataFile_MTP lammpsFile = new LAMMPSDataFile_MTP(lammpsName, elements, isVectorPeriodic);

		StructureBuilder sb = new StructureBuilder(lammpsFile);
		for (int i = sb.numDefiningSites() - 1; i >= 0; i--) {
			String elementSymbol = sb.getSiteSpecies(i).getElementSymbol();
			if (!ArrayUtils.arrayContains(elementsStr, elementSymbol)) {
				sb.removeSite(i);
			}
		}
		return new Structure(sb);
	}
	
	/**
	 * Read only coordinates from the relaxed structure.
	 * @return coordinates of atoms of the relaxed structure in the format of m_ClusList.
	 */
	public Object[] readCoords() {
		String lammpsName = calcDir + HPC.FILESPTR + OutputStructureFile;
		String[] elementsStr = cluster.getEleNames();
		Element[] elements = new Element[elementsStr.length];
		for (int i = 0; i < elementsStr.length; i++) {
			elements[i] = Element.getElement(elementsStr[i]);
		}
		boolean[] isVectorPeriodic = {true, true, true};
		LAMMPSDataFile_MTP lammpsFile = new LAMMPSDataFile_MTP(lammpsName, elements, isVectorPeriodic);
		
		Object[] clusList = new Object[lammpsFile.numDefiningSites()];
		for (int i = 0; i < lammpsFile.numDefiningSites(); i++) {
			Object[] atom = new Object[] {lammpsFile.getSiteSpecies(i).getElementSymbol(), 
					                      lammpsFile.getSiteCoords(i).cartCoord(0),
					                      lammpsFile.getSiteCoords(i).cartCoord(1),
					                      lammpsFile.getSiteCoords(i).cartCoord(2)}; 
			clusList[i] = atom;
		}
		
		return clusList;
	}
	
	/**
	 * Convert the output file content to a list of strings.
	 * TODO: make it more efficient. For a large cluster and poor structure, the relaxation might
	 * give a huge amount of output and the List representation could be memory-intensive.
	 * 
	 * @return The output file content as a list of strings.
	 */
	public List<String> readOutput() {
		String file = calcDir + HPC.FILESPTR + OutputFile;
		if (Files.exists(Paths.get(file))) {
			try {
				output = HPC.readAllLines(file);
				return output;
			} catch (IOException e) {
				e.printStackTrace();
				Logger.error("Cannot read output: " + file + ". Don't exist! Attempting restart.");
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Read the energy from the minimizer's output file.
	 * @return Total energy of the relaxed cluster.
	 *         Double.MAX_VALUE if the output file doesn't exist.
	 */
	public double readEnergy() {
		if (output == null) {
			this.readOutput();
		}
		return LAMMPS.readEnergy(output);
	}
	
	public static double readEnergy(List<String> output) {
		double finalEnergy = 0.0;
		if (output != null) {
			for(int i = output.size() - 1; i >= 0; i--){
				if(output.get(i).contains(EnergyStr)){
					String[] split = output.get(i+1).split("\\s+");
					finalEnergy = Double.parseDouble(split[split.length-1]);
					break;
				}
			}
			return finalEnergy;
		}
		return finalEnergy;
	}
	
	/**
	 * Check whether the energy evaluation complete successfully.
	 * @return True if there is no ErrorStr in the output file.
	 *         False if there is ErrorStr or the output file doesn't exist.
	 */
	public boolean isCalSuccessful() {
		if (this.readOutput() == null) {
			return false;
		}
		for (int i = 0; i < output.size(); i++) {
			if (output.get(i).contains(ErrorStr)) {
				return false;
			}
		}
		return true;	
	}
	
	/**
	 * Set the path of LAMMPS executable on the native platform.
	 * @param path Path of LAMMPS executable.
	 */
	public void setExe(String path) {
		this.exe = path;
	}
	
	/**
	 * @return A list of string for creating a ProcessBuilder object.
	 */
	public List<String> getCommands() {
		// e.g. mpirun -np 3 lmp_mpi -in input.lmp
		List<String> commands = new ArrayList<String>();
		if (this.useParallel) {
			String[] mpi = this.parallelCommand.split("\\s+");
			for (String i : mpi) {
				if (i != "")
					commands.add(i);
			}
		}
		commands.add(this.exe);
		commands.add("-in");
		commands.add("input.lmp");
		return commands;
	}
	
	public static String[] getInputFiles() {
		return InputFiles;
	}
	
	public String getName() {
		return Name;
	}
	
	public boolean existOutput() {
		if (Files.exists(Paths.get(calcDir + System.getProperty("file.separator") + OutputFile))) {
			return true;
		}
		return false;
	}
	
	public static String getOutputFileName() {
		return OutputFile;
	}
	
	public static String getInputFileName() {
		return InputFile;
	}
	
	public static String getStructureFileName(boolean output) {
		if (output) {
			return OutputStructureFile;
		} else {
			return InputStructureFile;
		}
	}
	
	public String getFinishStr() {
		return FinishStr;
	}
	
	public String getErrorStr() {
		return ErrorStr;
	}
	
	
	public static String getPotentialFileName() {
		return Potential;
	}
	
	public String getStdOutput() {	
		return Stdout;
	}
	
	public String getCalDir() {
		return this.calcDir;
	}
	
	public int relax() {
		return HPC.execute(this.getCommands(), calcDir, calcDir + HPC.FILESPTR + Stdout, true);
	}
	
	// LAMMPS don't need to be fixed for now, not like VASP. 
	public boolean attemptToFix(int attempt) { return true; }
}
