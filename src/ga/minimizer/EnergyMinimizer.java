package ga.minimizer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import ga.HPC;
import ga.io.Logger;
import matsci.structure.Structure;

/**
 * A wrapper of external executables. Provide functionalities of create commands, check calculation
 * status, read output results and structures, and write necessary input files.
 * 
 * @author ywang393
 */
public interface EnergyMinimizer {	
	
	/**
	 * Copy input files from the Input.Root folder to the m_CalNum folder of the target cluster.
	 */
	void createInputs(boolean moveToCenter);
	
	/**
	 * Perform relaxation by the underlying program.
	 * @return exit code of the wrapped command.
	 */
	int relax();
	
	boolean attemptToFix(int attempt);
	
	/**
	 * Write GA-generated structure to file.
	 */
	void writeStructure(boolean moveToCenter);
	
	void writeSurfaceStructure(boolean moveToCenter);
	
	/**
	 * Read the relaxed structure after finish running the executable.
	 * @return A list of strings representing the structure (in current GA's way).
	 */
	Structure readStructure(boolean relaxed);
	
	/**
	 * Read only coordinates from the relaxed structure.
	 * @return coordinates of atoms of the relaxed structure.
	 */
	Object[] readCoords();
	
	/**
	 * Convert the output file content to a list of strings.
	 * TODO: make it more efficient. For a large cluster and poor structure, the relaxation might
	 * give a huge amount of output and the List representation could be memory-intensive.
	 * 
	 * @return The output file content as a list of strings.
	 */
	List<String> readOutput();

	/**
	 * Read the energy from the minimizer's output file.
	 * @return Total energy of the relaxed cluster.
	 */
	double readEnergy();
	
	/**
	 * Check whether the energy evaluation complete successfully.
	 * @return True if there is no ErrorStr in the output file.
	 *         False if there is ErrorStr or the output file doesn't exist.
	 */
	boolean isCalSuccessful();
	
	/**
	 * Set the path of VASP executable on the native platform.
	 * @param path Path of VASP executable.
	 */
	void setExe(String path);
	
	/**
	 * @return A list of string for creating a ProcessBuilder object.
	 */
	List<String> getCommands();
	
	String getName();
	
	String getCalDir();
	
	/**
	 * @return Name of the file to which the stdout is redirected. e.g. ./stdout
	 */
	String getStdOutput();

	String getFinishStr();
	
	String getErrorStr();
	
	/**
	 * Check whether output file (OUTCAR for VASP, log.lammps for LAMMPS) exists.
	 * @return
	 */
	boolean existOutput();
	
	// The two methods below are temporarily put here to satisfy the preconverge calculation for
	// m_IsCharged = true calculation. TODO: figure out a better OOP way.
	public static List<String> readOutput(String calcDir, String OutputFile) {
		List<String> output = new ArrayList<String>();
		String file = calcDir + HPC.FILESPTR + OutputFile;
		if (Files.exists(Paths.get(file))) {
			try {
				output = HPC.readAllLines(file);
				return output;
			} catch (IOException e) {
				e.printStackTrace();
				Logger.error("Cannot read output: " + file + ". Don't exist! Attempting restart.");
				return null;
			}
		} else {
			return null;
		}
	}
	
	public static boolean isCalSuccessful(List<String> output, String ErrorStr) {
		if (output == null) {
			return false;
		}
		for (int i = 0; i < output.size(); i++) {
			if (output.get(i).contains(ErrorStr)) {
				return false;
			}
		}
		return true;	
	}
	
	/**
	 * Determine the calculation status by checking whether certain strings exist in output file.
	 * Valid status include "Finished", "Running", and "Failed".
	 * 
	 * @param output List of all the lines of the output file. (e.g OUTCAR, log.lammps)
	 * @param finished The string in output file indicating that the calculation has finished.
	 * @param errStr The string in output file indicating the calculation failed.
	 * @return
	 */
	public static String getStatus(List<String> output, String finished, String errStr) {
		boolean hasFinished = false;
		if (output.size() > 0) {
			for (int i = output.size() - 1; i >= 0; i--) {
				String line = output.get(i);
				if (line.contains(finished)) {
					hasFinished = true;
					break;
				} else if (line.contains(errStr)) {
					return "Failed";
				}
			}
			if (hasFinished) {
				return "Finished";
			} else {
				return "Running";
			}
		} else {
			return "Running"; // output.size() == 0: no output file.
		}
	}
	
}
