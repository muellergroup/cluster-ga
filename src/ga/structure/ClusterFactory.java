package ga.structure;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import ga.HPC;
import ga.Input;
import ga.SimilarityCalculator2;
import ga.io.GAIOUtils;
import ga.io.Logger;
import ga.utils.GAMath;
import ga.utils.GAUtils;
import matsci.Element;
import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.structure.Structure;
import matsci.structure.Structure.Site;
import matsci.structure.StructureBuilder;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

/**
 * A class collects algorithms that directly generate cluster structures for GA or facilitate
 * this process. 
 * 
 * These algorithms apply to: random bare cluster, bi-metallic cluster, multi-component cluster,
 * and ligated cluster.
 * 
 * @author ywang393
 */
public class ClusterFactory {
	
	public static double PRECISION = 1E-8;
	public static double OVERLAPTHRESHOLD = 0.8;  // Small overlap
	public static double COLLAPSETHRESHOLD = 0.5; // Large overlap. VASP would have error relaxing.
	public static double EXPLODETHRESHOLD = 1.1; 
	public static double MUTATEDATOMRATIO = 0.2;  // Percentage cap of mutated atoms in "Mut" Operation.
	
	private ClusterFactory() {}
	
	public static void setPrecision(double prec) {
		PRECISION = prec;
	}
	
	/**
	 * Randomly scatter atoms in a box to generate a random cluster. Could be used in multi-component
	 * cluster. It uses a overlap-rejection method, which reject a random position if it leads to
	 * overlapping with existing atoms. Better than using fix-overlap later.
	 * 
	 * @param numAtoms
	 * @param eleNames
	 * @param eleNums
	 * @return
	 */
	public static Object[] getRandomBareClusterByScattering(int numAtoms, String[] eleNames, int[] eleNums) {
		Object[] clusList = new Object[numAtoms];
		double scale = Math.pow(numAtoms, 1. / 3.);
		double maxNNDistance = 0.0;
		for (String e: eleNames) {
			maxNNDistance = maxNNDistance < GAUtils.getNNDistance(e) ? GAUtils.getNNDistance(e) 
					                                                 : maxNNDistance;
		}
		double boxLength = maxNNDistance * scale * 1.25; // More flexible
		int counter = 0;
		int attempt = 0;
		for (int i = 0; i < eleNames.length; i++) {
			for (int j = 0; j < eleNums[i]; j++) {
				double x = GAMath.random() * boxLength;
				double y = GAMath.random() * boxLength;
				double z = GAMath.random() * boxLength;
				Object[] atom = new Object[] { eleNames[i], x, y, z };
				clusList[counter] = atom;
				attempt++;
				Object[] copy = Arrays.copyOfRange(clusList, 0, counter + 1);
				if (GAUtils.isOverlap(copy) || ClusterFactory.checkExplosionByContiguous(copy)) {
					j--;
				} else {
					counter++;
				}
			}
		}
		Logger.debug("Random generation of size " + numAtoms + " : attempted " + attempt + " times.");
		return clusList;
	}
	
	/**
	 * Each atom is randomly located around its previous atom, with the condition that it touches
	 * the previous one, but not overlapping with others. Could handle multi-component clusters.
	 * 
	 * @param numAtoms
	 * @param eleNames
	 * @param eleNums
	 * @return
	 */
	public static Object[] getRandomBareClusterByConcatenating(int numAtoms, String[] eleNames, int[] eleNums) {
		Object[] clusList = new Object[numAtoms];
		int counter = 0;
		double x = 0, y = 0, z = 0;
		for (int i = 0; i < eleNames.length; i++) {
			for (int j = 0; j < eleNums[i]; j++) {
				if (counter == 0) {
					x = y = z = 0.0; 
				} else {
					Object[] lastAtom = (Object[]) clusList[counter - 1];
					double distance = GAUtils.getNNDistance(eleNames[i], (String) lastAtom[0]);
					double[] location = GAMath.randomPointOnSphere(distance);
					x = (double) lastAtom[1] + location[0];
					y = (double) lastAtom[2] + location[1];
					z = (double) lastAtom[3] + location[2];
				}
				Object[] thisAtom = new Object[] { eleNames[i], x, y, z };
				clusList[counter] = thisAtom;
				if (GAUtils.isOverlap(Arrays.copyOfRange(clusList, 0, counter + 1))) {
					j--;
				} else {
					counter++;
				}
			}
		}
		return clusList;
	}
	
	public static Object[] getRandomBareClusterByConcatenating(int numAtoms, String eleName, int eleNum) {
		return getRandomBareClusterByConcatenating(numAtoms, new String[] {eleName}, new int[] {eleNum});
	}
	
	public static Object[] ringGenerator(String element, int numAtoms, double bondLength, double bondAngle) {
        // Compute geometric info
        double theta = 2.0 * Math.PI / numAtoms; // Central angle of a bond in the horizontal plane.
        double bondAngleRadius = bondAngle / 180.0 * Math.PI; // Bond angle in radius
        double ringRadius = (1/2.0 * bondLength * Math.sin(bondAngleRadius / 2.0)) / Math.sin(theta/2.0);
        // Height above the horizontal plane in a zigzag ring.
        double heightAbovePlane = bondLength / 2.0 * Math.cos(bondAngleRadius / 2.0);
        
        // Generate the ring
        Object[] clusList = new Object[numAtoms];
        for (int i = 0; i < numAtoms; i++) {
            double[] thisCoord = new double[] {ringRadius * Math.sin(i * theta),
                                               ringRadius * Math.cos(i * theta),
                                               Math.pow(-1, i) * heightAbovePlane};
            clusList[i] = new Object[] {element, thisCoord[0], thisCoord[1], thisCoord[2]};
            
        }
        return clusList;
    }
	
	/**
	 * Mutate by randomly rotating MUTATEDATOMRATIO percent of atoms of the argument cluster.
	 * @param clusList It's not change in-place.
	 * @return Reference to a new mutated object.
	 */
	public static Object[] mutateBareClusterRandomRotate(Object[] clusList) {
		Object[] copy = ClusterFactory.copyObjectArray(clusList);
		ClusterFactory.moveCGToOrigin(copy);
		double[][] rotateMatrix = GAMath.getRandomRotationMatrix();
		int rotateNum = GAMath.randomInt(1,(int) Math.ceil(MUTATEDATOMRATIO * copy.length) + 1);
		for (int i = 0; i < rotateNum; i++) {
			Object[] atom = (Object[]) copy[i];
			double[] xyz = new double[] { (double) atom[1], (double) atom[2], (double) atom[3]};
			double[] rotatedCoord = MSMath.matrixTimesVector(rotateMatrix, xyz);
			atom[1] = rotatedCoord[0];
			atom[2] = rotatedCoord[1];
			atom[3] = rotatedCoord[2];
			copy[i] = atom;
		}
		return copy;
	}
	
	/**
	 * Currently used in GA.
	 * Mutate by randomly displacing at most MUTATEDATOMRATIO percent of atoms of the argument 
	 * cluster globally. Each atom is randomly re-position in a sphere centered at CG and 
	 * with a diameter of longest dimension of the cluster.
	 * 
	 * @param clusList It's not changed in place.
	 * @return Reference to a new object with mutated cluster.
	 */
	public static Object[] mutateBareClusterRandomMoveGlobal(Object[] clusList) {
		Object[] copy = ClusterFactory.copyObjectArray(clusList);
		double[] CGCoords = GAUtils.getCGCoords(clusList);
		int numMove = GAMath.randomInt(1, (int) Math.ceil(MUTATEDATOMRATIO * clusList.length) + 1);
		int[] randomSamples = GAMath.randomSamplingInt(clusList.length, numMove);
		//int[] randomSamples = GAMath.randomSamplingInt(clusList.length, 1);
		int counter = 0;
		boolean valid = false;
		while (!valid) {
			for (int i = 0; i < randomSamples.length; i++) {
				Object[] atom = (Object[]) copy[randomSamples[i]];
				double scale = GAMath.max(GAUtils.interatomicDistances(copy));
				String e = (String) atom[0];
				double x = (double) atom[1];
				double y = (double) atom[2];
				double z = (double) atom[3];
				x = GAMath.randomDoubleExclusive(-0.5, 0.5) * scale + CGCoords[0];
				y = GAMath.randomDoubleExclusive(-0.5, 0.5) * scale + CGCoords[1];
				z = GAMath.randomDoubleExclusive(-0.5, 0.5) * scale + CGCoords[2];
				double distance = GAUtils.distanceFrom(copy, new Object[] {e, x, y, z});
				// overlap or explode. Reject this mutation.
				if (distance < GAUtils.getNNDistance(e) * OVERLAPTHRESHOLD 
				 || distance > GAUtils.getNNDistance(e) * EXPLODETHRESHOLD) {
					i--;
					counter++;
					continue;
				}
				atom[1] = x;
				atom[2] = y;
				atom[3] = z;
				copy[randomSamples[i]] = atom;
			}
			if (checkExplosionByContiguous(copy) || checkCollapse(copy)) { // Exploded or large overlap.
				copy = ClusterFactory.copyObjectArray(clusList);
			} else {
				valid = true;
			}
		}
		Logger.debug("Mutation by random move of size " + copy.length + " : attempted " + counter + " times");
		return copy;
	}
	
	/**
	 * [BETA]: Currently only used for ring structures (sulphur). We could improve it if we can have 
	 * an efficient way to pick-ou only surface atoms to move. Right now, it could stuck in 
	 * infinite loop if an internal atom is chosen to be mutated. A local move will never 
	 * get an internal cluster out of cluster core (therefore always overlapping).
	 * 
	 * Mutation by randomly displacing MUTATEDATOMRATIO percent of atoms of the argument cluster 
	 * locally (+- 0.3 * nearest neightbor distance). Each atoms moves in a sphere centered at 
	 * its original position. For atoms not on the surface of the cluster, there are chances 
	 * of the operation stuck in an infinity loop.
	 * 
	 * @param clusList Object[] representing the cluster, which will be changed in-place.
	 * @return Reference to the same argument object.
	 */
	public static Object[] mutateBareClusterRandomMoveLocal(Object[] clusList) {
		int numMove = GAMath.randomInt(1, (int) Math.ceil(MUTATEDATOMRATIO * clusList.length) + 1);
		int[] randomSamples = GAMath.randomSamplingInt(clusList.length, numMove);
		Object[] copy = ClusterFactory.copyObjectArray(clusList);
		int counter = 0;
		for (int i = 0; i < randomSamples.length; i++) {
			Object[] atom = (Object[]) copy[randomSamples[i]];
			double scale = GAUtils.getNNDistance((String) atom[0]) * 0.75;
			//String e = (String) atom[0];
			double x = (double) atom[1];
			double y = (double) atom[2];
			double z = (double) atom[3];
			x = x + GAMath.randomDoubleExclusive(-1.0, 1.0) * scale;
			y = y + GAMath.randomDoubleExclusive(-1.0, 1.0) * scale;
			z = z + GAMath.randomDoubleExclusive(-1.0, 1.0) * scale;
			// <TODO> Cause infinity loop for sulphur. Disable for now.
			//double distance = GAUtils.distanceFrom(copy, new Object[] {e, x, y, z});
			//if (distance < GAUtils.getNNDistance(e) * OVERLAPTHRESHOLD 
			// || distance > GAUtils.getNNDistance(e) * EXPLODETHRESHOLD) {
			//	// overlap or explode. Reject this mutation.
			//	i--;
			//	counter++;
			//	continue;
			//}
			atom[1] = x;
			atom[2] = y;
			atom[3] = z;
			copy[randomSamples[i]] = atom;
		}
		Logger.debug("Mutation by random move of size " + copy.length + " : attempted " + counter + " times");
		return copy;
	}
	
	/**
	 * Mutates the ligated cluster by rotating a randomly chosen segment.
	 * In nearly all circumstances, this method will preserve the outwardness of ligands,
	 * i.e. ligands will remain surface-oriented after mutation.
	 */	
	public static Object[] getMutatedLigatedCluster(Object[][] ligatedClusterObject2D) {
		Object[] mutatedLC = new Object[0];
		while (mutatedLC.length == 0) {
			int lengthLC = ligatedClusterObject2D.length; //E.g., for Au18-lig14, lengthLC = 15.
			int noOfLigands = Input.getNumLigands();
			int nLigand = Input.getLigandStructure().numDefiningSites();
			int nCluster = Input.getBareClusterStructure().numDefiningSites();
			
			Object[] LCobj1D = obj2DTo1D(ligatedClusterObject2D);
			double[] centerOfLC = GAUtils.getCGCoords(LCobj1D);
			
			//Apply a rotation to randomize the orientation. This rotation will later be reversed.
			double[][] rotation = MSMath.getRandomRotationMatrix3D();
			LCobj1D = negObjTranslator(LCobj1D, centerOfLC); //Move LC center to origin.
			LCobj1D = rotate(LCobj1D,rotation);
			
			//Sort the randomly oriented LC by Z.
			ligatedClusterObject2D = obj1DTo2D(LCobj1D, noOfLigands, nLigand, nCluster);
			ligatedClusterObject2D = sortLigatedClusterByZ(ligatedClusterObject2D);
			
			//Set the span of the mutation.
			Object[] bareCluster = ligatedClusterObject2D[0];		
			int atomsInSegment = GAMath.randomInt(1, (int) Math.ceil(bareCluster.length * MUTATEDATOMRATIO) + 1); 
			
			//Store the segment of the bare cluster to be mutated.
			Object[] clusterSegment = new Object[atomsInSegment];
			for (int j = 0; j < atomsInSegment; j++) {
				clusterSegment[j] = bareCluster[j];
			}
			
			//Store the rest of the bare cluster.
			Object[] remainder = new Object[bareCluster.length - atomsInSegment];
			int nextIndex = 0;
			for (int k = atomsInSegment; k < bareCluster.length; k++) {
				remainder[nextIndex] = bareCluster[k];
				nextIndex++;
			}
			
			//Identify ligands in segment, and those in remainder.
			Object[] lastAtomInSegment = (Object[]) clusterSegment[atomsInSegment-1];
			double cutoffZ = (double) lastAtomInSegment[3];
			ArrayList<Integer> segmentLigandIndex = new ArrayList<>();
			ArrayList<Integer> remainderLigandIndex = new ArrayList<>();
			for (int m = 1; m < lengthLC; m++) {
				double[] cgLig = GAUtils.getCGCoords((Object[]) ligatedClusterObject2D[m]);
				if (cgLig[2] < cutoffZ) {
					segmentLigandIndex.add(m);
				} else {
					remainderLigandIndex.add(m);
				}
			}
			
			//Store segment ligands.
			Object[][] segmentLig2D = new Object[segmentLigandIndex.size()][];
			for (int a = 0; a < segmentLigandIndex.size(); a++) {
				int index = segmentLigandIndex.get(a);
				Object[] ligand = ligatedClusterObject2D[index];
				segmentLig2D[a] = ligand;
			}
			Object[] segmentLig = obj2DTo1D(segmentLig2D);
			
			//Store remainder ligands.
			Object[][] remainderLig2D = new Object[remainderLigandIndex.size()][];
			for (int b = 0; b < remainderLigandIndex.size(); b++) {
				int ind = remainderLigandIndex.get(b);
				Object[] lig = ligatedClusterObject2D[ind];
				remainderLig2D[b] = lig;
			}
			Object[] remainderLig = obj2DTo1D(remainderLig2D);
			
			//Rotate the segment around the Z axis by a random angle.
			double[] zAxis = new double[] {0, 0, 1};
			double mutAngle = MSMath.RANDOM.nextDouble() * 180; //TODO: what range for angles?
			double[][] twist = GAMath.getRotationMatrixAxisAngle(zAxis, mutAngle);
			clusterSegment = rotate(clusterSegment,twist);
			segmentLig = rotate(segmentLig,twist);
			
			//Reconstruct the mutated ligated cluster.
			Object[] bareMut = new Object[bareCluster.length];
			for (int p = 0; p < bareCluster.length; p++) {
				if (p < clusterSegment.length) {
				bareMut[p] = clusterSegment[p];
				} else {
					bareMut[p] = remainder[p-clusterSegment.length];
				}
			}
			bareMut = fixOverlapCluster(bareMut);
			ArrayList <Object> mutant = convertToExpandableArray(bareMut);		
			mutant = appendAtomwise(mutant,segmentLig);
			mutant = appendAtomwise(mutant,remainderLig);
			
			//Return mutated LC to its original orientation and center position.
			LCobj1D = convertFromExpandableArray(mutant);
			double[][] rotationBack = MSMath.transpose(rotation);
			LCobj1D = rotate(LCobj1D,rotationBack);
			LCobj1D = posObjTranslator(LCobj1D, centerOfLC);
			
			//Repair ligand overlap.
			ligatedClusterObject2D = obj1DTo2D(LCobj1D, noOfLigands, nLigand, nCluster);
			mutatedLC = fixOverlapLigands(ligatedClusterObject2D);
		}
		return mutatedLC;
	}
	
	
	public static Object[] ligatedClusterCrossover(Object[][] chosen1, Object[][] chosen2, double ratio) {
		Object[] crossedOverLC = new Object[0];
		while (crossedOverLC.length == 0) {
			Object[][] obj1 = prepareLigandObjectArray(chosen1);
			Object[][] obj2 = prepareLigandObjectArray(chosen2);
			
			Object[][] xoverLigandObj = new Object[obj1.length][];
			int split = (int) Math.ceil((obj1.length-1) * ratio);
			int count = 0;
			// Get half ligands from the first structure and the second half from the other one.
			// Bare cluster is the first in the array. Ligands start from the second.
			for (int i = 1; i < split; i++) { // obj1.length / 2 + 1; i++) {
				xoverLigandObj[i] = obj1[i];
				count++;
			}
			for (int j = split; j < obj1.length; j++) {//obj2.length / 2 + 1; j < obj1.length; j++) {
				count++;
				xoverLigandObj[count] = obj2[j];
			}
		
			Object[] clusObj1 = chosen1[0];
			Object[] clusObj2 = chosen2[0];
			Object[][] coords2 = new Object[][] { clusObj1, clusObj2 };
		
			for (int i = 0; i < coords2.length; i++) {
				coords2[i] = sortByZ(coords2[i]);
			}
				
			Object[] clusObj = bareClusterCrossoverLC(coords2, ratio);
			xoverLigandObj[0] = clusObj;
			crossedOverLC = fixOverlapLigands(xoverLigandObj);
		}
		return crossedOverLC;
	}
	

	private static Object[] bareClusterCrossoverLC(Object[][] coords, double ratio) {
		Object[] clus1 = coords[0];
		Object[] clus2 = coords[1];
		int start = (int) Math.ceil(clus1.length * ratio) - 1;

		Object[] clusList = new Object[clus1.length];

		for (int i = 0; i < start; i++) {
			clusList[i] = clus1[i];
		}
		for (int i = start; i < clus1.length; i++) {
			clusList[i] = clus2[i];
		}

		clusList = fixOverlapCluster(clusList);
		return clusList;
	}
	
	/**
	 * Convert between matsci.Structure and old GA format clusList. Passed-in clusList are not
	 * changed.
	 * 
	 * @param clusList
	 * @param cellSize
	 * @return
	 */
	public static Structure objectToStructure(Object[] clusList, double cellSize) {
		Species[] species = new Species[clusList.length];
		Coordinates[] coords = new Coordinates[clusList.length];
		for (int i = 0; i < coords.length; i++) {
			Object[] atom = (Object[]) clusList[i];
			String element = (String) atom[0];
			double[] xyz = new double[] { (double) atom[1], (double) atom[2], (double) atom[3] };
			coords[i] = new Coordinates(xyz, CartesianBasis.getInstance());
			species[i] = Species.get(element);
		}
		
		double[][] latticeVectors = new double[][] { { cellSize, 0, 0 }, { 0, cellSize, 0 }, { 0, 0, cellSize } };
		Vector[] cellVectors = new Vector[] { new Vector(latticeVectors[0]),
				                              new Vector(latticeVectors[1]),
				                              new Vector(latticeVectors[2])};
		StructureBuilder sb = new StructureBuilder();
		sb.addSites(coords, species);
		sb.setCellVectors(cellVectors);
		return new Structure(sb);
	}
	
	public static Object[] structureToObjectArray(Structure structure) {
		Object[] returnArray = new Object[structure.numDefiningSites()];
		for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
			String ele = structure.getSiteSpecies(siteNum).getElementSymbol();
			double[] cartArray = structure.getSiteCoords(siteNum).getCartesianArray();
			double x = cartArray[0];
			double y = cartArray[1];
			double z = cartArray[2];
			Object[] atom = new Object[] { ele, x, y, z };
			returnArray[siteNum] = atom;
		}
		return returnArray;
	}
	
	/**
	 * @param clusList
	 * @return A deep copy of the argument.
	 */
	public static Object[] copyObjectArray(Object[] clusList) {
		Object[] returnArray = new Object[clusList.length];
		for (int i = 0; i < clusList.length; i++) {
			Object[] atom = (Object[]) clusList[i];
			Object[] newAtom = new Object[] { (String) atom[0], (double) atom[1], (double) atom[2], (double) atom[3] };
			returnArray[i] = newAtom;
		}
		return returnArray;
	}
	
	/**
	 * Get cellSize by adding distImage to the largest width among all dimensions. 
	 * It's written to prevent side effects on passed-in arguments. 
	 * 
	 * @param cluster
	 * @param distImage
	 * @return
	 */
	public static double getCellSize(Object[] cluster, double distImage) {

		Species[] species = new Species[cluster.length];
		Coordinates[] coords = new Coordinates[cluster.length];

		for (int i = 0; i < coords.length; i++) {
			Object[] atom = (Object[]) cluster[i];
			String element = (String) atom[0];
			double[] xyz = new double[] { (double) atom[1], (double) atom[2], (double) atom[3] };
			coords[i] = new Coordinates(xyz, CartesianBasis.getInstance());
			species[i] = Species.get(element);
		}

		double[] xCoords = new double[coords.length];
		double[] yCoords = new double[coords.length];
		double[] zCoords = new double[coords.length];

		for (int i = 0; i < coords.length; i++) {
			xCoords[i] = coords[i].cartCoord(0);
			yCoords[i] = coords[i].cartCoord(1);
			zCoords[i] = coords[i].cartCoord(2);
		}

		double xWidth = GAMath.max(xCoords) - GAMath.min(xCoords);
		double yWidth = GAMath.max(yCoords) - GAMath.min(yCoords);
		double zWidth = GAMath.max(zCoords) - GAMath.min(zCoords);

		double cellArray[] = { xWidth, yWidth, zWidth };

		double cellSize = GAMath.max(cellArray) + distImage;
		return cellSize;
	}
	
	/**
	 * Move the center of mass of a cluster to the origin. Site coordinates are
	 * changed accordingly. Argument is changed in-place.
	 * 
	 * @param clusList Object[] containing atomic species and coordinates.
	 * @param eleNames
	 * @return A new Object[] with modified site coordinates.
	 */
	public static void moveCGToOrigin(Object[] clusList) {
		double[] CGCoords = GAUtils.getCGCoords(clusList);
		negObjTranslator(clusList, CGCoords);
	}
	
	/**
	 * Move cluster to the center of the unit cell. Note: clusList is changed in-place.
	 * @param clusList
	 * @param cellSize
	 * @param CGCoords
	 */
	public static void moveCGToCenter(Object[] clusList, double cellSize) {
		double[] CGCoords = GAUtils.getCGCoords(clusList);
		double[] centerOfCell = new double[] {cellSize/2, cellSize/2, cellSize/2};
		double[] displacement = MSMath.arraySubtract(centerOfCell, CGCoords);
		posObjTranslator(clusList, displacement);
	}
	
	/**
	 * Argument is changed in-place.
	 * @param clusList
	 * @return
	 */
	public static Object[] fixOverlapCluster(Object[] clusList) {
		double validDistance, distance;
		double[] centroid; // Save memory and GC work.
		Object[] atom1, atom2;
		Object[] overlappingAtoms = new Object[2];
		double[] cg = GAUtils.getCGCoords(clusList);
		clusList = negObjTranslator(clusList,cg);
		
		try {
			boolean overlapped = true;
			while (overlapped) {
				overlapped = false;
				for (int i = 0; i < clusList.length; i++) {
					for (int j = 0; j < i; j++) {
						atom1 = (Object[]) clusList[i];
						atom2 = (Object[]) clusList[j];
						validDistance = OVERLAPTHRESHOLD * (atomicRadius(atom1) + atomicRadius(atom2));
						distance = GAUtils.distance(atom1, atom2);
						
						if (distance < validDistance) {
							overlappingAtoms[0] = atom1;
							overlappingAtoms[1] = atom2;
							centroid = GAUtils.getCGCoords(overlappingAtoms);
							clusList = negObjTranslator(clusList,centroid);
							atom1 = (Object[]) clusList[i];
							atom2 = (Object[]) clusList[j];
							
							while (distance < validDistance) {
								atom1 = moveAwayFromCenter(atom1);
								atom2 = moveAwayFromCenter(atom2);
								distance = GAUtils.distance(atom1, atom2);
							}
							
							clusList[i] = atom1;
							clusList[j] = atom2;
							clusList = posObjTranslator(clusList,centroid);
							overlapped = true;
							break;
						}
					}
				}
			}
			clusList = posObjTranslator(clusList,cg);
			return clusList;
		} catch (NullPointerException n) {
			Logger.error("Cluster being checked for overlap has too few atoms. Something's very wrong.");
		}
		clusList = posObjTranslator(clusList,cg);
		return clusList;
	}
	
	
	public static Object[] fixOverlapLigands(Object[][] ligatedClusterObject) {
		Object[] cluster = ligatedClusterObject[0];
		double[] centerOfCluster = GAUtils.getCGCoords(cluster);
		cluster = negObjTranslator(cluster,centerOfCluster);
		int ligandCount = ligatedClusterObject.length - 1;
		Object[] ligandList = new Object[ligandCount];
		Object[] ligand;
		ArrayList<Object> ligandPositions = new ArrayList<Object>();
		for (int i = 1; i < ligatedClusterObject.length; i++) {
			ligand = (Object[]) ligatedClusterObject[i];
			ligand = negObjTranslator(ligand,centerOfCluster);
			double[] cgLig = GAUtils.getCGCoords(ligand);
			ligandPositions.add(cgLig);
			ligand = negObjTranslator(ligand,cgLig);
			ligandList[i-1] = ligand;
		}
		
		ArrayList<Object> revisedClusterList = qualityControl(cluster,ligandList,ligandPositions);
		Object[] revisedCluster = convertFromExpandableArray(revisedClusterList);
		if (revisedCluster.length > 0) {
			revisedCluster = posObjTranslator(revisedCluster,centerOfCluster);
		}
		return revisedCluster;
	}
	
	/**
	 * Another method for fixing overlaps. Could be faster but less stable.
	 * @param clusList
	 * @return
	 */
	public static Object[] fixOverlapClusterPhil(Object[] clusList) {
		Logger.debug("Starting structure of fixOverlapCluster function: " + HPC.LINESPTR + GAIOUtils.toString(clusList));
		clusList = ClusterFactory.sortByZ(clusList); // Disentangle overlapping by a order.
		double legitDistance, distance; // Save memory and GC work.
		Object[] atom1, atom2;
		double[] dr = new double[3];

		long startTime = System.currentTimeMillis();
		boolean overlapped = true;
		while (overlapped) {
			if (System.currentTimeMillis() - startTime > 300000) {
				Logger.warning("fixOverlapCluster takes more than 5 minutes. Most likely it has solved overlapping. Better check the cluster.");
				Logger.warning("Cluster information: " + HPC.LINESPTR + GAIOUtils.toString(clusList));
				return clusList;
			}
			overlapped = false;
			for (int i = 0; i < clusList.length; i++) {
				for (int j = i + 1; j < clusList.length; j++) {
					atom1 = (Object[]) clusList[i];
					atom2 = (Object[]) clusList[j];
					// Use 0.8 * sum_of_atomic_radius to allow some extent of overlapping.
					legitDistance = (GAUtils.getAtomicRadius((String) atom1[0])
							       + GAUtils.getAtomicRadius((String) atom2[0])) * OVERLAPTHRESHOLD;
					dr[0] = (double) atom2[1] - (double) atom1[1]; // atom1 is the anchor atom.
					dr[1] = (double) atom2[2] - (double) atom1[2];
					dr[2] = (double) atom2[3] - (double) atom1[3];
					distance = MSMath.magnitude(dr);
					// Avoid infinite loop when distance is very close to validDistance.
					// E.g. distance = 0.899999999999 and validDistance = 0.9. Each time it will
					// only update 0.000000000001, which effectively send the code to infinite loop
					// if not use BigDecimal to round numbers up.
					BigDecimal bd = new BigDecimal(distance).setScale(2, RoundingMode.HALF_UP);
					if (bd.doubleValue() < legitDistance) {
						overlapped = true;
						double[] unitVector;
						if (bd.doubleValue() > PRECISION) {
							unitVector = MSMath.normalize(dr);
						} else {
							// When atoms are completely overlapped, randomly pick a repulsive direction.
							unitVector = GAMath.randomPointOnUnitSphere();
						}
						double diff = legitDistance - bd.doubleValue();
						diff = (diff >= PRECISION * 1000) ? diff : PRECISION * 1000; // avoid infinity loop.
						double[] dispVector = MSMath.arrayMultiply(unitVector, diff);
						double x = dispVector[0] + (double) atom2[1];
						double y = dispVector[1] + (double) atom2[2];
						double z = dispVector[2] + (double) atom2[3];
						atom2[1] = x;
						atom2[2] = y;
						atom2[3] = z;
						clusList[j] = atom2;
						break;
					}
				}
			}
		}
		return clusList;
	}

	/**
	 * 
	 * @param ligList ligList[0][] is the bare cluster; ligList[1][] ... ligList[n][] are ligands.
	 * @return
	 */
	public static Object[][] fixOverlapLigandsPhil (Object[][] ligList) {
		Logger.debug("Start structure of fixOverlapClusterLigands function: " + HPC.LINESPTR + GAIOUtils.toString(ligList));
		for (int i = 0; i < ligList.length; i++) {
			ligList[i] = ClusterFactory.sortByZ(ligList[i]);
		}
		double validDistance, distance; // Save memory and GC work.
		Object[] atom1, atom2;
		double[] dr = new double[3];
		
		long startTime = System.currentTimeMillis();
		boolean overlapped = true;
		while (overlapped) {
			if (System.currentTimeMillis() - startTime > 600000) {
				Logger.warning("fixOverlapLigands takes more than 10 minutes. Most likely it has solved overlapping. Better check the cluster.");
				Logger.warning("Cluster information: " + HPC.LINESPTR + GAIOUtils.toString(ligList));
				return ligList;
			}
			overlapped = false;
			for (int i = 0; i < ligList.length; i++) {
				for (int j = 0; j < ligList[i].length; j++) {
					for (int k = i + 1; k < ligList.length; k++) {
						for (int m = 0; m < ligList[k].length; m++) {
							atom1 = (Object[]) ligList[i][j];
							atom2 = (Object[]) ligList[k][m];
							validDistance = (GAUtils.getAtomicRadius((String) atom1[0])
								           + GAUtils.getAtomicRadius((String) atom2[0])) * OVERLAPTHRESHOLD;
							dr[0] = (double) atom2[1] - (double) atom1[1];
							dr[1] = (double) atom2[2] - (double) atom1[2];
							dr[2] = (double) atom2[3] - (double) atom1[3];
							distance = MSMath.magnitude(dr);
							BigDecimal bd = new BigDecimal(distance).setScale(2, RoundingMode.HALF_UP);
							if (bd.doubleValue() < validDistance) {
								double[] unitVector;
								if (bd.doubleValue() > PRECISION) {
									unitVector = MSMath.normalize(dr);
								} else {
									// When atoms are completely overlapped, randomly pick a repulsive direction.
									unitVector = GAMath.randomPointOnUnitSphere();
								}
								double diff = validDistance - bd.doubleValue();
								diff = (diff >= PRECISION * 1000) ? diff : PRECISION * 1000; // avoid infinity loop.
								double[] dispVector = MSMath.arrayMultiply(unitVector, diff);
								for (int p = 0; p < ligList[k].length; p++) {
									Object[] atom3 = (Object[]) ligList[k][p];
									atom3[1] = (double) atom3[1] + dispVector[0];
									atom3[2] = (double) atom3[2] + dispVector[1];
									atom3[3] = (double) atom3[3] + dispVector[2];
									ligList[k][p] = atom3;
								}
								overlapped = true;
								break;
							}
						}
					}
				}
			}
		}
		return ligList;
	}

	
	/**A method to detect and correct overlap in ligated systems that have undergone genetic operations. Called by fixOverlapLigands.
	 * 
	 * @param cluster Bare cluster object centered at origin.
	 * @param ligandList An object comprised of rotated/oriented ligand objects, each centered at the origin.
	 * @param ligandPositions	A list of coordinates of each ligand's position relative to the cluster.
	 * @return The ligated cluster object, free of ligand-ligand or ligand-cluster overlap.
	 */
	public static ArrayList<Object> qualityControl(Object[] cluster, Object[] ligandList, ArrayList<Object> ligandPositions){
		long startTime, endTime;
		startTime = System.currentTimeMillis();
			
		ArrayList<Object> clusterSoFar = convertToExpandableArray(cluster);
		
		Object[] ligand = (Object[]) ligandList[0];

		double[] currentLigPosition = (double[]) ligandPositions.get(0);
		Object[] ligToCheck = posObjTranslator(ligand,currentLigPosition);
			
		int NumberOfCorrectionAttempts = 0;
		int thresholdAttempts = 150; //Increment is 0.05 Angstrom (see updatePosition).
			
		Object[] overlap = checkOverlapLigands(clusterSoFar,ligToCheck);
		boolean overlapped = (boolean) overlap[0];
		
		correctingOverlap: for (int i = 0; i<ligandList.length; i++) { //Iterate over all ligands.
				
			while (overlapped) {
				double[] overlappedLigandPosition = (double[]) ligandPositions.get(i);
				double[] newPosition = updatePosition(overlappedLigandPosition);
				ligandPositions.set(i, newPosition);
				ligToCheck = negObjTranslator(ligToCheck, currentLigPosition);
				ligToCheck = posObjTranslator(ligToCheck, newPosition);
					
				overlap = checkOverlapLigands(clusterSoFar,ligToCheck);
					
				overlapped = (boolean) overlap[0];
				NumberOfCorrectionAttempts=NumberOfCorrectionAttempts+1;
				currentLigPosition = newPosition;
					
				if (NumberOfCorrectionAttempts>thresholdAttempts) {
					clusterSoFar.clear();
					Logger.detail("Structure rejected because of ligand overlap.");
					break correctingOverlap;
				}
			}
		  
			if (NumberOfCorrectionAttempts>0) {
			    Logger.detail("Overlap on ligand " + (i+1) +" corrected in " + NumberOfCorrectionAttempts + " iterations.");
			}
			
			clusterSoFar = appendAtomwise(clusterSoFar,ligToCheck);
			    
			//Setting up for next iteration:
			if (i==(ligandList.length-1)) {
				break correctingOverlap;
			} else {
			ligToCheck = (Object[]) ligandList[i+1];
			currentLigPosition = (double[]) ligandPositions.get(i+1);
			ligToCheck = posObjTranslator(ligToCheck,currentLigPosition);
			overlap = checkOverlapLigands(clusterSoFar,ligToCheck);
			overlapped = (boolean) overlap[0];
			NumberOfCorrectionAttempts = 0;
			}
		} 
		
		endTime = System.currentTimeMillis();
		if (clusterSoFar.size() > 0) {
			Logger.detail("Overlap successfully corrected in " + GAIOUtils.formatElapsedTime(endTime - startTime));
		} else {
			Logger.detail("Overlap correction failed; attempt took " + GAIOUtils.formatElapsedTime(endTime - startTime) + ". Regenerating ligated cluster.");
			//Returning an empty array induces a new call to this method from Cluster.java.
		}
		return clusterSoFar; 
	}
	
	/**
	 * Subtract a vector from all atoms in argument. Argument is changed in-place.
	 * @param clusList
	 * @param disp
	 * @return
	 */
	public static Object[] negObjTranslator(Object[] clusList, double[] disp) {
		for (int i = 0; i < clusList.length; i++) {
			Object[] atom = (Object[]) clusList[i];
			atom[1] = (double) atom[1] - disp[0];
			atom[2] = (double) atom[2] - disp[1];
			atom[3] = (double) atom[3] - disp[2];
		}
		return clusList;
	}
	
	public static Object[] negAtomTranslator(Object[] atom, double[] disp) {
		atom[1] = (double) atom[1] - disp[0];
		atom[2] = (double) atom[2] - disp[1];
		atom[3] = (double) atom[3] - disp[2];
		return atom;
	}

	/**
	 * Add a vector to all atoms in argument. Argument is changed in-place.
	 * @param clusList
	 * @param disp
	 * @return
	 */
	public static Object[] posObjTranslator(Object[] clusList, double[] disp) {
		for (int i = 0; i < clusList.length; i++) {
			Object[] atom = (Object[]) clusList[i];
			atom[1] = (double) atom[1] + disp[0];
			atom[2] = (double) atom[2] + disp[1];
			atom[3] = (double) atom[3] + disp[2];
			clusList[i] = atom;
		}
		return clusList;
	}
	
	public static Object[] posAtomTranslator(Object[] atom, double[] disp) {
		atom[1] = (double) atom[1] + disp[0];
		atom[2] = (double) atom[2] + disp[1];
		atom[3] = (double) atom[3] + disp[2];
		return atom;
	}

	public static boolean checkOverlapLigands(ArrayList<Object> cluster, Object[] ligand, int placedClusterIndex, int placedLigandIndex) {
		boolean overlap = false;
		double[] cgLig = GAUtils.getCGCoords(ligand);
		double buffer = Input.getLigBuffer();
		Object[] farthestLigIndex = GAUtils.findFarthest(ligand,cgLig); 
		double farthestLigAtomRadius = atomicRadius((Object[])ligand[(int)farthestLigIndex[0]]);
		for (int k = 0; k<cluster.size(); k++) {
			Object[] atomToCheck = (Object[]) cluster.get(k);
			double[] positionToCheck = {(double) atomToCheck[1], (double) atomToCheck[2], (double) atomToCheck [3]};
			double relativePosition = GAMath.distance(cgLig, positionToCheck);
			double regionCloseToLigand = (1+buffer)*farthestLigAtomRadius + atomicRadius(atomToCheck) + (double)farthestLigIndex[1];
			if (relativePosition <= regionCloseToLigand) {
				for (int m = 0; m<ligand.length; m++) {
					if (m == placedLigandIndex && k == placedClusterIndex) { 
						continue;
					}
					Object[] ligAtom = (Object[]) ligand[m];
					double ligRadius = atomicRadius(ligAtom);
					double[] ligAtomPosition = getCoords(ligand,m);
					double spaceBetween = GAMath.distance(ligAtomPosition,positionToCheck);
					if (spaceBetween < (atomicRadius(atomToCheck) + ligRadius*(1+buffer))) {
						overlap = true;
					}
				}
			}
		}
		return overlap;
		}
	
	public static Object[] checkOverlapLigands(ArrayList<Object> cluster, Object[] ligand) {
		boolean overlap = false;
		int activeSite = Input.getActiveSite();
		String coreAtom = Input.getEleNames()[0];
		Object[] overlapData = {overlap, -1, -1};
		double[] cgFinal = GAUtils.getCGCoords(ligand);
		double buffer = Input.getLigBuffer();
		Object[] farthestLigIndex = GAUtils.findFarthest(ligand,cgFinal); 
		double farthestLigAtomRadius = atomicRadius((Object[])ligand[(int)farthestLigIndex[0]]);
		for (int k = 0; k<cluster.size(); k++) {
			Object[] atomToCheck = (Object[]) cluster.get(k);
			String elementToCheck = (String) atomToCheck[0];
			double[] positionToCheck = {(double) atomToCheck[1], (double) atomToCheck[2], (double) atomToCheck [3]};
			double relativePosition = GAMath.distance(cgFinal, positionToCheck);
			double regionCloseToLigand = (1+buffer)*farthestLigAtomRadius + atomicRadius(atomToCheck) + (double)farthestLigIndex[1];
			if (relativePosition <= regionCloseToLigand) {
				for (int m = 0; m<ligand.length; m++) {
					Object[] ligAtom = (Object[]) ligand[m];
					double ligRadius = atomicRadius(ligAtom);
					double[] ligAtomPosition = getCoords(ligand,m);
					double spaceBetween = GAMath.distance(ligAtomPosition,positionToCheck);
					double acceptableDistance = (atomicRadius(atomToCheck) + ligRadius*(1+buffer));
					if (activeSite > 0 && m==(activeSite-1) && elementToCheck.equalsIgnoreCase(coreAtom)) {
						acceptableDistance = 0.85*(atomicRadius(atomToCheck) + ligRadius);
					}
					if (spaceBetween < acceptableDistance) {
						overlap = true;
						int overlappedLigIndex = m;
						int overlappedClusterIndex = k;
						overlapData[0] = overlap;
						overlapData[1] = overlappedLigIndex;
						overlapData[2] = overlappedClusterIndex;
					}
				}
			}
		}
		return overlapData;
		}
		
	public static ArrayList<Object> convertToExpandableArray(Object[] structure) {
		ArrayList<Object> expandableStructureArray = new ArrayList<Object>();
		for (int i = 0; i<structure.length; i++) {
			Object[] row = (Object[]) structure[i];
			expandableStructureArray.add(row);
		}
		return expandableStructureArray;
	}
	
	public static Object[] convertFromExpandableArray(ArrayList<Object> structureArray) {
		Object[] structureObject = new Object[structureArray.size()];
		for (int i = 0; i<structureArray.size(); i++) {
			Object[] row = (Object[]) structureArray.get(i);
			structureObject[i] = row;
		}
		return structureObject;
	}
	/**
	 * A method for randomly attaching ligands to a bare cluster. 
	 * Avoids ligand overlap and other defects via a careful placement algorithm in combination with rejection sampling.
	 * 
	 * @param ligandStructure Ligand read in from $ROOT/LIGAND file.
	 * @param cluster         Bare cluster to be ligated.
	 * @param numLigands      Number of ligands to be attached.
	 * @return Object[] representing the ligated cluster.
	 */

	
	public static Object[] randomlyLigate(Structure ligandStructure, Object[] cluster, int numLigands) {
		long st, et;
		st = System.currentTimeMillis();
		
		Object[] ligand = structureToObjectArray(ligandStructure);
		double[] cgLigand = GAUtils.getCGCoords(ligand);
		double[] cgCluster = GAUtils.getCGCoords(cluster);
		ligand = negObjTranslator(ligand,cgLigand);
		cluster = negObjTranslator(cluster, cgCluster);
		int bareClusterSize = cluster.length;
		int ActiveSite = Input.getActiveSite();
				
		if (numLigands > bareClusterSize) {
			Logger.error("Only "+bareClusterSize+" of "+numLigands+" requested ligands can be added.");
			numLigands = bareClusterSize;
		}
		
		int numPlaced = 0;
		int numAttempt = 0;
		int maxAttempts = 1000*numLigands; //Intended to be a generous limit.
		
		Object[][] ligatedCluster = new Object[numLigands + 1][];
		ligatedCluster[0] = cluster;
		
		//Identify the cluster atom farthest from the center of gravity in order to set a safe starting distance for ligands.
		Object[] farthestClusterFromCenter = GAUtils.findFarthest(cluster,cgCluster);
		double startingRadius = 1.5*((double) farthestClusterFromCenter[1]);
		
		//To add ligands dynamically, we initialize the ligated cluster as an expandable list of atoms. 
		//The second list, "occupiedRegister", records which atoms in the bare cluster already have ligands attached.
		//This helps us to prevent overlap.
		ArrayList<Object> clusterSoFar = convertToExpandableArray(cluster);
		ArrayList<Integer> occupiedRegister = new ArrayList<Integer>();
		
		while (numPlaced < numLigands) {
			if (occupiedRegister.size()==bareClusterSize) {
				break;
			}
			//Position a randomly rotated ligand at a randomly chosen point orbiting the bare cluster,
			//and determine which cluster atom the positioned ligand is nearest to.
			Object[] ligAttempt = ligand;
			double[][] randomRotation = MSMath.getRandomRotationMatrix3D();
			double[] randomPosition = GAMath.randomPointOnSphere(startingRadius);
			ligAttempt = rotate(ligand,randomRotation);
			ligAttempt = posObjTranslator(ligAttempt,randomPosition);
			double[] cgPosition = GAUtils.getCGCoords(ligAttempt);
			Object[] minClustertoLigand = GAUtils.findNearest(cluster,cgPosition);
			int nearestClusterAtomtoLigand = (int) minClustertoLigand[0];
			
			
			//Check if the nearest cluster atom to the ligand already has a ligand attached to it.
			//If this is the case, find the nearest unoccupied cluster atom to the ligand.
			ArrayList<Object> clusterExcluding = convertToExpandableArray(cluster);
			ArrayList<Integer> indices = new ArrayList<Integer>();
			for (int i = 0; i<cluster.length; i++) {
				indices.add(i);
			}
			for (int r = 0; r<occupiedRegister.size(); r++) {
				int occAtom = occupiedRegister.get(r);
				int excluded = indices.indexOf(occAtom);
				clusterExcluding.remove(excluded);
				indices.remove(excluded);
			}
			while (occupiedRegister.contains(nearestClusterAtomtoLigand)) {
				Object[] minRemainingtoLigand = GAUtils.findNearest(clusterExcluding,cgPosition);
				nearestClusterAtomtoLigand = indices.get((int) minRemainingtoLigand[0]);
			}
			
			
			//Once the nearest viable cluster site to the ligand is found, we set the ligand active site.
			//If the active/binding site is unspecified in INGA, we identify the nearest ligand atom
			//to the viable cluster site and set this atom to be the ligand active site.
			occupiedRegister.add(nearestClusterAtomtoLigand);
			Object[] nearest = (Object[]) cluster[nearestClusterAtomtoLigand];
			double[] nearestClusterCoords = getCoords(cluster,nearestClusterAtomtoLigand);
			int ligActiveSiteIndex = 0;
			if (ActiveSite > 0) {
				ligActiveSiteIndex = ActiveSite-1;
			} else {
				Object[] nearestLigandIndex = GAUtils.findNearest(ligAttempt,nearestClusterCoords);
				ligActiveSiteIndex = (int) nearestLigandIndex[0];
			}
			Object[] boundLigAtom = (Object[]) ligAttempt[ligActiveSiteIndex];
			double[] boundLigCoords = getCoords(ligAttempt,ligActiveSiteIndex);
			
			
			//Next, the ligand is moved towards the cluster site. The ligand is moved so that the active site
			//is just in contact with the surface, i.e. the ligand binding site and cluster site are just touching.
			double ligRadius = atomicRadius(boundLigAtom);
			double clusterRadius = atomicRadius(nearest);
			double[] destination = MSMath.arraySubtract(boundLigCoords,nearestClusterCoords);
			destination = MSMath.normalize(destination);
			destination = MSMath.vectorTimesScalar(destination,((ligRadius)+clusterRadius));
			destination = MSMath.arrayAdd(destination,nearestClusterCoords);
			destination = MSMath.arraySubtract(destination, boundLigCoords);
			ligAttempt = posObjTranslator(ligAttempt,destination);
			
			
			//Finally, check for overlap caused by the addition of this ligand. If there is overlap, rather than
			//correcting it, simply reject the placement and try again. Repeat this process until all ligands
			//are placed or the maximum number of attempts has been exceeded.
			boolean overlap = checkOverlapLigands(clusterSoFar,ligAttempt,nearestClusterAtomtoLigand,ligActiveSiteIndex);
			if (overlap == true) {
				int rejectedPlacement = occupiedRegister.indexOf(nearestClusterAtomtoLigand);
				occupiedRegister.remove(rejectedPlacement);
			} else {
				numPlaced = numPlaced+1;
				clusterSoFar = appendAtomwise(clusterSoFar,ligAttempt);
				ligatedCluster[numPlaced] = ligAttempt;
			}
			numAttempt = numAttempt+1;
			if (numAttempt > maxAttempts) {
				Logger.error("Cluster could not be ligated; number of ligands requested may exceed available surface sites. Regenerating.");
				Object[] ligatedClusterObject = new Object[0]; 
				return ligatedClusterObject;
				//Returning an empty array induces a new call to this method from Cluster.java.
			}
		}
		Logger.detail(numPlaced + " ligands added; " + numAttempt + " placements attempted.");
		Object[] ligatedClusterObject = obj2DTo1D(ligatedCluster);
		ligatedClusterObject = posObjTranslator(ligatedClusterObject, cgCluster);
		et = System.currentTimeMillis();
		Logger.detail("Ligating cluster takes "+ GAIOUtils.formatElapsedTime(et - st));
		return ligatedClusterObject;
	}
	
	public static ArrayList<Object> appendAtomwise(ArrayList<Object> clusterList, Object[] ligand){
		for (int atom = 0; atom<ligand.length; atom++) {
			Object[] row = (Object[]) ligand[atom];
			clusterList.add(row);
		}
		return clusterList;
	}
	
	public static double[] updatePosition(double[] currentLigPosition) {
		double move = 0.05; // 1/20th of the base unit, e.g. Angstroms
		double scaling = 1+(move/GAUtils.getMagnitude(currentLigPosition));
		double[] newPosition = MSMath.arrayMultiply(currentLigPosition, scaling);
		return newPosition;
	}
	
	public static Object[] moveAwayFromCenter(Object[] atom) { 
		double[] atomPosition = getCoords(atom);
		double[] newPosition = updatePosition(atomPosition);
		atom = negAtomTranslator(atom,atomPosition);
		atom = posAtomTranslator(atom,newPosition);
		return atom;
	}
	

	public static double[] getCoords(Object[] structure, int index) {
		Object[] atom = (Object[]) structure[index];
		double[] coordinates = {(double) atom[1],(double) atom[2],(double) atom[3]};
		return coordinates;
	}
	
	public static double[] getCoords(Object[] atom) {
		double[] coordinates = {(double) atom[1],(double) atom[2],(double) atom[3]};
		return coordinates;
	}
		
	public static double atomicRadius(Object[] atom) { //Gets the atomic radius of object from hashtable in Seeder.
		double radius = GAUtils.getAtomicRadius((String) atom[0]);
		return radius;
	}
	
	/**
	 * @param coords A list of coordinates with the format "Element x y z"
	 * @param rotMat
	 * @return A new object containing the rotated structure.
	 */
	public static Object[] rotate(Object[] coords, double[][] rotMat) {
		Object[] rotatedStruct = new Object[coords.length];
		for (int i = 0; i < coords.length; i++) {
			Object[] row = (Object[]) coords[i];
			String ele = (String) row[0];
			double[] rowVec = {(double)row[1], (double)row[2], (double)row[3]};
			double[] rotated = MSMath.matrixTimesVector(rotMat,rowVec);
			double r1 = rotated[0];
			double r2 = rotated[1];
			double r3 = rotated[2];
			Object[] newrow = {ele, r1, r2, r3};
			rotatedStruct[i] = newrow;
		}
		return rotatedStruct;		
	}	

	public static Object[][] prepareLigandObjectArray(Object[][] obj) {
		int len = obj.length;
		Structure[] structArray = new Structure[obj.length - 1];
		double[] cgOfZ = new double[obj.length];
		for (int i = 1; i < len; i++) {
			structArray[i-1] = objectToStructure(obj[i], 30.0); // TODO: remove this magic number (cell size for ligand)
			cgOfZ[i] = getZCentroid(structArray[i-1]); // structArray index is one smaller than that of the obj.
		}
		double tempCg;
		Object[] tempObj;
		// Ligands start from index 1 in the obj matrix.
		for (int i = 1; i < cgOfZ.length; i++) {
			for (int j = i + 1; j < cgOfZ.length; j++) {
				if (cgOfZ[i] > cgOfZ[j]) {
					tempCg = cgOfZ[i];
					tempObj = obj[i];
					cgOfZ[i] = cgOfZ[j];
					obj[i] = obj[j];
					cgOfZ[j] = tempCg;
					obj[j] = tempObj;
				}
			}
		}
		return obj;
	}

	private static double getZCentroid(Structure str) {
		double sumZ = 0.0;
		double cg;
		Coordinates[] Coords = new Coordinates[str.numDefiningSites()];
		for (int i = 0; i < str.numDefiningSites(); i++) {
			Coords[i] = str.getSiteCoords(i);
			sumZ = sumZ + Coords[i].cartCoord(2);
		}
		cg = sumZ / str.numDefiningSites();
		return cg;
	}
	
	/**
	 * 
	 * @param obj The ligated cluster object.
	 * @param noofLigand
	 * @param nLigand
	 * @param nCluster
	 * @return
	 */
	public static Object[][] obj1DTo2D(Object[] obj, int noofLigand, int nLigand, int nCluster) {
		Object[][] newObj = new Object[noofLigand + 1][];
		int count = 0;
		// Add bare cluster
		newObj[0] = new Object[nCluster];
		for (int j = 0; j < nCluster; j++) {
			newObj[0][j] = obj[count];
			count++;
		}
		// Add ligands
		// TODO: initialization??
		for (int i = 1; i <= noofLigand; i++) {
			newObj[i] = new Object[nLigand];
			for (int j = 0; j < nLigand; j++) {
				newObj[i][j] = obj[count];
				count++;
			}
		}

		return newObj;
	}

	public static Object[] obj2DTo1D(Object[][] obj) {
		ArrayList<Object> newObj = new ArrayList<>();
		int k = 0;
		for (int i = 0; i < obj.length; i++) {
			for (int j = 0; j < obj[i].length; j++) {
				newObj.add(obj[i][j]);
				k++;
			}
		}
		Object[] obj2 = new Object[k];
		int i = 0;
		for (Object str : newObj) {
			obj2[i] = str;
			i++;
		}
		return obj2;
	}

	/**
	 * Sort list of coordinates in-place by z coordinates in descending order.
	 * @param coords In the format of "Element x y z".
	 * @return Reference to the same object of argument
	 */
	public static Object[] sortByZ(Object[] coords) {
		boolean swapped = true;
		while (swapped) {
			swapped = false;
			for (int i = 0; i < coords.length - 1; i++) {
				double z1 = (double) ((Object[]) coords[i])[3];
				double z2 = (double) ((Object[]) coords[i + 1])[3];
				if (z1 > z2) {
					Object[] temp = (Object[]) coords[i];
					coords[i] = coords[i + 1];
					coords[i + 1] = temp;
					swapped = true;
				}
			}
		}
		return coords;
	}
	
	/**
	 * Sort the structure by z coordinates in descending order. Construct a new structure object
	 * and return. Argument is unaffected.
	 * @param coords In the format of "Element x y z".
	 * @return A new object of the sorted structure.
	 */
	public static Structure sortByZ(Structure s) {
		StructureBuilder sb = new StructureBuilder(s);
		sb.removeAllSites();
		Site[] sites = s.getDefiningSites(); // It's a deep copy.
		double z1, z2;
		Site temp;
		for (int i = 0; i < sites.length - 1; i++) {
			for (int j = 0; j < sites.length - i - 1; j++) {
				z1 = sites[j].getCoords().cartesianCoord(2);
				z2 = sites[j+1].getCoords().cartesianCoord(2);
				if (z1 > z2) {
					temp = sites[j];
					sites[j] = sites[j+1];
					sites[j+1] = temp;
				}
			}
		}
		sb.addSites(sites);
		return new Structure(sb);
	}
	
	public static Object[][] sortLigatedClusterByZ(Object[][] LCobj) {
		boolean swapped = true;
		boolean ligSwapped = true;
		//First sort bare cluster by Z:
		Object[] bareCluster = LCobj[0];
		while (swapped) {
			swapped = false;
			for (int i = 0; i < bareCluster.length - 1; i++) {
				double z1 = (double) ((Object[]) bareCluster[i])[3];
				double z2 = (double) ((Object[]) bareCluster[i + 1])[3];
				if (z1 > z2) {
					Object[] temp = (Object[]) bareCluster[i];
					bareCluster[i] = bareCluster[i + 1];
					bareCluster[i + 1] = temp;
					swapped = true;
				}
			}
		}
		LCobj[0] = bareCluster;
		//Then sort ligands by Z of centers of gravity:
		while (ligSwapped) {
			ligSwapped = false;
			for (int j = 1; j < LCobj.length - 1; j++) {
				double[] cg1 = GAUtils.getCGCoords((Object[]) LCobj[j]);
				double[] cg2 = GAUtils.getCGCoords((Object[]) LCobj[j+1]);
				double cgz1 = (double) cg1[2];
				double cgz2 = (double) cg2[2];
				if (cgz1 > cgz2) {
					Object[] placeholder = (Object[]) LCobj[j];
					LCobj[j] = LCobj[j + 1];
					LCobj[j + 1] = placeholder;
					ligSwapped = true;
				}
			}
		}
		return LCobj;
	}
	
	
	/**
	 * getContiguousSites() uses an depth-first tree search algorithm to find the largest contiguous
	 * cluster. If there are parts of structure (include flying away segments) not connected to
	 * the main body, it's deemed exploded and return true.
	 * @param clusList
	 * @return true if not contiguous; false otherwise.
	 */
	public static boolean checkExplosionByContiguous(Object[] clusList) {
		double cellSize = ClusterFactory.getCellSize(clusList, 15); // Use a safe image distance.
		Structure tempStruct = ClusterFactory.objectToStructure(clusList, cellSize);
		return checkExplosionByContiguous(tempStruct);
	}
	
	public static boolean checkExplosionByContiguous(Structure s) {
		double nnDistance = GAUtils.getNNDistance(s.getDefiningSite(0).getSpecies().getSymbol());
		return checkExplosionByContiguous(s, nnDistance);
	}
	
	/**
	 * Allow user to set a customized value of nearest neighbor distance, other than the values
	 * stored in GAUtils.
	 * @param s
	 * @param nnDistance Nearest neighbor distance within which atoms are considered as in touch.
	 * @return
	 */
	public static boolean checkExplosionByContiguous(Structure s, double nnDistance) {
		Structure.Site[] contiguousSites = SimilarityCalculator2.getContiguousSites(s, nnDistance * EXPLODETHRESHOLD);
		return (contiguousSites == null);
	}
	
	public static boolean checkExplosionByContiguous(Object[] clusList, double nnDistance) {
		double cellSize = ClusterFactory.getCellSize(clusList, 15); // Use a safe image distance.
		Structure tempStruct = ClusterFactory.objectToStructure(clusList, cellSize);
		return checkExplosionByContiguous(tempStruct, nnDistance);
	}
	
	/**
	 * Check whether the relax structure is exploded, by comparing all bond lengths with
	 * 1.2 * nearest neighbor distance. If there is one atom having bond lengths to 
	 * all the other atoms greater than 1.2 * nearest neighbor distance, then this atom 
	 * is detached away from the cluster, and the structure is not relaxed properly.
	 * 
	 * TODO: when the structure is separated into parts (a small cluster is detached from the others,
	 * it should also be considered as "exploded."
	 * 
	 * @return True if all atoms are within 1.2 * nearest neighbor distance with all the other atoms
	 *         False if at least one atom is separated far away from all the other atoms.
	 */
	/*
	public static boolean checkExplosion(Object[] clusList) {
		for (int i = 0; i < clusList.length; i++) {
			boolean exploded = true;
			for (int j = 0; j < clusList.length; j++) {
				if (i == j) {
					continue;
				}
				Object[] atom1 = (Object[]) clusList[i];
				Object[] atom2 = (Object[]) clusList[j];
				double distance = GAUtils.distance(atom1, atom2);
				double nearestNeighbor = GAUtils.getNNDistance((String) atom1[0], (String) atom2[0]);
				if (distance < nearestNeighbor * EXPLODETHRESHOLD) {
					exploded = false;
					break;
				}
			}
			// If one atom is flying away from other atoms, exploded.
			if (exploded) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean checkExplosion(Structure struct) {
		return checkExplosion(structureToObjectArray(struct));
	}
	*/
	
	/**
	 * Check whether a relaxed structure having atoms completely overlapped (i.e. distance too small).
	 * If there are, VASP will have trouble relaxing it (malloc() error).
	 * 
	 * Note: small overlap will return false (not collapsed).
	 * 
	 * @param clusList
	 * @return true if have large overlap; false otherwise.
	 */
	public static boolean checkCollapse(Object[] clusList) {
		for (int i = 0; i < clusList.length; i++) {
			for (int j = i + 1; j < clusList.length; j++) {
				Object[] atom1 = (Object[]) clusList[i];
				Object[] atom2 = (Object[]) clusList[j];
				double distance = GAUtils.distance(atom1, atom2);
				double collapseThreshold = GAUtils.getNNDistance((String) atom1[0], (String) atom2[0]) * COLLAPSETHRESHOLD;
				if (distance < collapseThreshold) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean checkCollapse(Structure struct) {
		return checkCollapse(structureToObjectArray(struct));
	}

	public static String[] getEleNames(Structure struct) {
		Species[] distinctSpecies = struct.getDistinctSpecies();
		String[] eleNames = new String[distinctSpecies.length];
		for (int i = 0; i < distinctSpecies.length; i++) {
			eleNames[i] = distinctSpecies[i].getElementSymbol();
		}
		return eleNames;
	}

	public static int[] getEleNums(Structure struct) {
		Species[] distinctSpecies = struct.getDistinctSpecies();
		int[] eleNums = new int[distinctSpecies.length];
		for (int i = 0; i < distinctSpecies.length; i++) {
			eleNums[i] = struct.numDefiningSitesWithSpecies(distinctSpecies[i]);
		}
		return eleNums;
	}
	
	/** 
	 * For debug purposes.
	 * @return
	 */
	public static String toString(Object[] clusList) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < clusList.length; i++) {
			Object[] atom = (Object[]) clusList[i];
			String species = (String) atom[0];
			String coords = String.format(Cluster.COORDINATEFORMAT, (double) atom[1], (double) atom[2], (double) atom[3]);
			String l = species + " " + coords + System.getProperty("line.separator");
			sb.append(l);
		}
		return sb.toString();
	}
	
}
