package ga.structure;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;

import matsci.Species;
import matsci.io.vasp.POSCAR;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class Surface {
	
	private int m_slabThickness = 1;
	private int m_minGap = 12;
	private int m_DistanceBetweenPeriodicImages = 15;
	private double m_ClusHeight = 1.5;
	private Structure m_slabStructure = null;
	
	public Structure placeCluster(Structure cluster){
		
		Structure surfaceStructure = m_slabStructure;
		Coordinates[] coords = getCoordinates(cluster, surfaceStructure);
		Vector zVector = findZVector(surfaceStructure);
		Coordinates[] shiftedCoordinates = CoM(coords, cluster, surfaceStructure);
		double clusHeight = m_ClusHeight;
		clusHeight /= zVector.length();

		double[][] cartArray = coordinatesToDouble(shiftedCoordinates);
		cartArray = MSMath.transpose(cartArray);
		double minZCluster = ArrayUtils.minElement(cartArray[2]);
				
		Coordinates[] surfaceCoords = getCoordinates(surfaceStructure, surfaceStructure);
		double[][] surfaceCartArray = coordinatesToDouble(surfaceCoords);
		surfaceCartArray = MSMath.transpose(surfaceCartArray);
		double maxZSurface = ArrayUtils.maxElement(surfaceCartArray[2]);

		double zDiff = maxZSurface - minZCluster + clusHeight;

		StructureBuilder sb = new StructureBuilder(surfaceStructure);
		for(int i = 0; i < cluster.numDefiningSites(); i++){
			Coordinates oldPosition = shiftedCoordinates[i].getCoordinates(surfaceStructure.getDirectBasis());
			Vector translationVector = new Vector(new Coordinates(new double[]{0.5,0.5, zDiff}, surfaceStructure.getDirectBasis()));
			Coordinates newPosition = oldPosition.translateBy(translationVector).getCoordinates(surfaceStructure.getDirectBasis());
			Species s = cluster.getSiteSpecies(i);
			sb.addSite(newPosition, s);
		}
		return new Structure(sb);
	}

	private Vector findZVector(Structure surface) {
		Vector[] cellVectors = surface.getCellVectors();
		Vector zNormal = new Vector(new double[]{0,0,1});
		for(int i = 0; i < cellVectors.length; i++){
			if(zNormal.isParallelTo(cellVectors[i])){
				return cellVectors[i];
			}
		}
		return null;
	}

	private Coordinates[] CoM(Coordinates[] clusList, Structure cluster, Structure basisSet) {
		Coordinates[] returnArray = new Coordinates[clusList.length];
		Coordinates clusterCoM = cluster.getDefiningCenterOfMass();
		Vector translationVector = new Vector(clusterCoM).reverse();
		for(int i = 0; i < clusList.length; i++){
			Coordinates old = clusList[i];
			Coordinates newCoords = old.translateBy(translationVector).getCoordinates(basisSet.getDirectBasis());
			returnArray[i] = newCoords;
		}
		return returnArray;
	}

	String[] bulk = new String[]{
			"TiO2","MgO","Graphene","SiO2"
	};
	
	double[][][] lat = new double[][][]{
			{
				{4.653272, 0, 0},
				{0, 4.653272, 0},
				{0, 0, 2.969203}
			},
			{
				{4.256483, 0, 0},
				{0, 4.256483, 0},
				{0, 0, 4.256483}
			},
			{
				{-1.2205231197, 2.1140080551,0},
				{2.4410462393,0,0,},
				{0.0000000000,0.0000000000,10.0000000000},
			},
			{
			    { 5.0531966654619751,    0.0000000000000000,    0.0000000000000000},
			    { 0.0000000000000000,    5.0531966654619751,    0.0000000000000000},
			    {-0.0000000000000000,    0.0000000000000000,    7.3718453796655989},
			}
	};
	
	double[][][] bulkCoords = new double[][][]{
		{
			{0.500000, 0.500000, 0.500000},
			{0.000000, 0.000000, 0.000000},
			{0.804580, 0.195420, 0.500000},
			{0.195420, 0.804580, 0.500000},
			{0.304580, 0.304580, 0.000000},
			{0.695420, 0.695420, 0.000000},
		},
		{
			{0.000000, 0.000000, 0.000000},
			{0.000000, 0.500000, 0.500000},
			{0.500000, 0.000000, 0.500000},
			{0.500000, 0.500000, 0.000000},
			{0.500000, 0.000000, 0.000000},
			{0.500000, 0.500000, 0.500000},
			{0.000000, 0.000000, 0.500000},
			{0.000000, 0.500000, 0.000000},
		},
		{
			{0.0000000000,  +0.0000000000,  +0.0000000000}, 
			{0.3333333333,  +0.6666666667,  +0.0000000000}, 
		},
		{
			 { 0.5000000000000000,  0.0000000000000000,  0.2500000000000000},
			 {-0.0000000000000000,  0.0000000000000000,  0.5000000000000000},
			 {-0.0000000000000000,  0.5000000000000000,  0.7500000000000000},
			 { 0.5000000000000000,  0.5000000000000000,  0.0000000000000000},
			 { 0.9121551392396904,  0.7500000000000000,  0.6250000000000000},
			 { 0.7500000000000000,  0.0878448607603096,  0.3750000000000000},
			 { 0.0878448607603096,  0.2500000000000000,  0.6250000000000000},
			 { 0.2500000000000000,  0.9121551392396904,  0.3750000000000000},
			 { 0.4121551392396904,  0.2500000000000000,  0.1250000000000000},
			 { 0.2500000000000000,  0.5878448607603096,  0.8750000000000000},
			 { 0.5878448607603096,  0.7500000000000000,  0.1250000000000000},
			 { 0.7500000000000000,  0.4121551392396904,  0.8750000000000000},
		}
	};
	
	String[][] m_Elements = new String[][]{
			{"Ti","Ti","O","O","O","O"},
			{"Mg","Mg","Mg","Mg","O","O","O","O"},
			{"C","C"},
			{"Si","Si","Si","Si","O","O","O","O","O","O","O","O"}
	};
	
	private Structure[] m_BulkStructures = null;
	
	public Structure getStructure(){
		return m_slabStructure;
	}

	public Surface(String material){
		if(material == null){System.out.println("Need surface type to continue"); System.exit(1);}
		if(m_BulkStructures == null){
			getBulkStructures();
		}
		if(m_slabStructure == null){
			constructSlab(material);
		}
	}
	
	private static Coordinates[] getCoordinates(Structure s, Structure basisSet) {
		Coordinates[] returnArray = new Coordinates[s.numDefiningSites()];
		for(int i = 0; i < returnArray.length; i++){
			returnArray[i] = s.getSiteCoords(i).getCoordinates(basisSet.getDirectBasis());
		}
		return returnArray;
	}

	private void getBulkStructures() {
		Structure[] bulkStructures = new Structure[bulk.length];
		for(int i = 0; i < bulk.length; i++){
			double[][] vals = bulkCoords[i];
			double[][] cellVals = lat[i];
			String[] elements = m_Elements[i];
			bulkStructures[i] = build(vals, cellVals, elements);
			print(bulkStructures[i], bulk[i]);
		}
		m_BulkStructures = bulkStructures;
	}
	
	private static void print(Structure structure, String name) {
		POSCAR p = new POSCAR(structure);
		p.writeFile("C:/Users/Peter Lile/Documents/Research/Input/GA structures/Surfaces/" + name + ".vasp");
	}

	private Structure build(double[][] coordArray, double[][] latticeArray, String[] labels){
		StructureBuilder sb = new StructureBuilder();
		assert(coordArray.length == labels.length);
		Vector[] cellVectors = convertToVectorArray(latticeArray);
		Coordinates[] coords = convertToCoordinatesArray(coordArray, cellVectors);
		Species[] species = convertToSpeciesArray(labels);
		sb.addSites(coords, species);
		sb.setCellVectors(cellVectors);
		return new Structure(sb).getConventionalStructure();
	}

	private Species[] convertToSpeciesArray(String[] labels) {
		Species[] returnArray = new Species[labels.length];
		for(int i = 0; i < returnArray.length; i++){
			returnArray[i] = Species.get(labels[i]);
		}
		return returnArray;
	}

	private Vector[] convertToVectorArray(double[][] latticeArray) {
		Vector[] returnArray = new Vector[latticeArray.length];
		for(int i = 0; i < returnArray.length; i++){
			double[] array = latticeArray[i];
			Vector v = new Vector(array);
			returnArray[i] = v;
		}
		return returnArray;
	}

	private static Coordinates[] convertToCoordinatesArray(double[][] coordArray, Vector[] cellVectors) {
		Coordinates[] returnArray = new Coordinates[coordArray.length];
		BravaisLattice bl = new BravaisLattice(cellVectors);
		Structure s = new Structure(bl);
		for(int i = 0; i < coordArray.length; i++){
			returnArray[i] = new Coordinates(coordArray[i], s.getDirectBasis());
		}
		return returnArray;
	}

	private int getIndex(String material){
		return ArrayUtils.findIndex(bulk, material);
	}
	
	private static double[][] coordinatesToDouble(Coordinates[] shiftedCoordinates) {
		double[][] returnArray = new double[shiftedCoordinates.length][];
		for(int i = 0; i < returnArray.length; i++){
			double[] array = shiftedCoordinates[i].getArrayCopy();
			returnArray[i] = array;
		}
		return returnArray;
	}

	private void constructSlab(String material) {
		int index = getIndex(material);
		Structure bulkStructure = m_BulkStructures[index];
		
		int z = m_slabThickness;
		double v = m_minGap;
		double distanceBetweenPeriodicImages = m_DistanceBetweenPeriodicImages;
		int size = getSize(bulkStructure, distanceBetweenPeriodicImages);
		
		Structure surface = bulkStructure.makeSurface(new int[]{0,0,1}, z, v, 1.);
		
		StructureBuilder sb = new StructureBuilder(surface);
		
		Vector[] cellVectors = sb.getCellVectors();
		
		for(int i = 0; i < cellVectors.length; i++){
			double[] array = cellVectors[i].getCartesianDirection();
			for(int dimNum = 0; dimNum < array.length; dimNum++){
				double dim = array[dimNum];
				if(dim < 0){
					dim = -dim;
				}
				array[dimNum] = dim;
			}
			cellVectors[i] = new Vector(array);
//			System.out.println(Arrays.toString(sb.getCellVectors()[i].getCartesianDirection()));
		}
		
		sb.setCellVectors(cellVectors);
		
		sb.setVectorPeriodicity(new boolean[]{true, true, false});
		surface = new Structure(sb);
		int[][] superLattice = new int[][]{{size,0},{0,size}};
		SuperStructure t = new SuperStructure(surface, superLattice);
		m_slabStructure = t;
	}

	private int getSize(Structure bulkStructure, double distanceBetweenPeriodicImages) {
		double latticeParam = getLatticeParam(bulkStructure);
		int ratio = (int) (distanceBetweenPeriodicImages / latticeParam) + 1;
		return ratio;
	}
	
	private double getLatticeParam(Structure bulkStructure){
		Vector[] cellVectors = bulkStructure.getCellVectors();
		return new BigDecimal(cellVectors[0].length()).round(new MathContext(10, RoundingMode.HALF_UP)).doubleValue();
	}
	
	
}
