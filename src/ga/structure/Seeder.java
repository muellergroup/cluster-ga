package ga.structure;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import ga.HPC;
import ga.Input;
import ga.io.GAIOUtils;
import ga.io.Logger;
import ga.utils.GAMath;
import ga.utils.GAUtils;
import matsci.Species;
import matsci.Element;
import matsci.location.Coordinates;
import matsci.location.basis.CartesianBasis;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;
import matsci.io.vasp.POSCAR;
import matsci.structure.Structure.Site;

/**
 * Building clusters from seed structures. 
 * 
 * All functions are written as static methods and don't need to create seeder objects.
 */
public class Seeder {
	
	// upper bound of nearest distance of newly added atoms to a seed structure (atomic radius
	// is taken into consideration.
	private static final double BUFFER = 0.01;
	static boolean generateRandomAtoms = true;
	public static ArrayList<Structure> seeds;
	private static boolean[] selectable; // Flags to singal whether we can use this seed.
	// Having this flag to avoid re-select the seed that already have
	// the desired number of atoms.
	private static int targetNumAtoms;
	private static String[] targetEleNames;
	private static int[] targetEleNums;
	
	//static int[] strides = new int[0];
	//static List<String> seedList = new ArrayList<>();
	//static Structure[] groundStateStructures;
	
	//public Seeder(int numAtoms, String[] eleNames, int[] eleNums) {
	//	if (groundStateStructures == null) {
	//		groundStateStructures = getGroundStateStructures(eleNames, eleNums);
	//	}
	//	size = numAtoms;
	//}

	private Seeder() {}
	
	/**
	 * Load seed clusters and initialize seeder configurations.
	 * 
	 * @param seedDir Folder containing subfolders in the format of "$i/POSCAR". Presumably,
	 *                user should have a "seeds" folder at the same location where INGA is.
	 * @param eleNames Array of element symbols in the target cluster
	 * @param eleNums Array of number of elements corresponding to the elements of eleNames.
	 *                Should have eleNames.length = eleNums.length.
	 */
	public static void configSeeder(String seedDir, String[] eleNames, int[] eleNums) {
		File[] subfolders = new File(seedDir).listFiles((file, name) -> name.matches("[0-9]+"));
		subfolders = HPC.sortFolderByNumber(subfolders);
		seeds = new ArrayList<Structure>();
		for (File f: subfolders) {
			File poscar = new File(f, "POSCAR");
			if (poscar.exists()) {
				seeds.add(new Structure(new POSCAR(poscar.getPath())));
			}
		}
		selectable = new boolean[seeds.size()];
		Arrays.fill(selectable, true);
		targetNumAtoms = GAMath.arraySum(eleNums);
		targetEleNames = eleNames;
		targetEleNums = eleNums;
	}
	
	public static Structure build() {
		Structure seed = chooseSeed();
		Structure structure = buildStructure(seed);
		return structure;
	}

	private static Structure chooseSeed(){
		int index = GAMath.randomInt(seeds.size());
		while (!selectable[index]) {
			index = GAMath.randomInt(seeds.size());
		}
		return seeds.get(index);
	}
	
	public static Structure buildStructure(Structure seed){
		int seedNumAtoms = seed.numDefiningSites();
		if (targetNumAtoms >= seedNumAtoms) {
			return addAtoms(seed);
		} else if (targetNumAtoms < seedNumAtoms) {
			return subtractAtoms(seed);
		} else {
			// Seed structure already have desired number of atoms.
			// Perform DFT on it only once. Avoid re-selecting this seed again.
			// Don't simple remove the seed since we want to keep the index of the seeds consistent
			// in the log (chosen.dat).
			selectable[seeds.indexOf(seed)] = false;
			return new Structure(seed);
		}
	}

	private static Structure subtractAtoms(Structure seed){
		if (generateRandomAtoms) {
			return randomSubtractAtoms(seed);
		} else {
			// TODO: not yet implemented
			return scoopedSubtractAtoms(seed);
		}
	}
	
	/**
	 * Randomly select atoms of desired elements and remove them from the seed cluster to create new
	 * cluster.
	 * 
	 * Note:
	 * 1. Don't guarantee the new cluster is still connected, e.g. for rings of sulphur clusters.
	 * 
	 * @param seed
	 * @return Newly created cluster.
	 */
	private static Structure randomSubtractAtoms(Structure seed) {
		StructureBuilder builder = new StructureBuilder(seed);
		
		// Capable of dealing with multi-element clusters.
		// Add missing atoms for each element one-by-one.
		for (int i = 0; i < targetEleNames.length; i++) {
			String targetElement = targetEleNames[i];
			Element e = Element.getElement(targetElement);
			int redundantNumAtoms = Math.abs(seed.numDefiningSitesWithElement(e) - targetEleNums[i]);
			
			for (int j = 0; j < redundantNumAtoms; j++) {
				int removeAtomIndex = -1;
				boolean valid = false;
				while (!valid) {
					removeAtomIndex = GAMath.randomInt(seed.numDefiningSites());
					Site s = seed.getDefiningSite(removeAtomIndex);
					valid = (s.getSpecies().getElementSymbol().equals(targetElement));
				}
				builder.removeSite(removeAtomIndex);

				// Update seed and builder (since the atom index changes)
				seed = new Structure(builder);
				builder = new StructureBuilder(seed);
			}
		}

		return new Structure(builder);
	}
	
	/**
	 * <TODO>
	 */
	private static Structure scoopedSubtractAtoms(Structure seed) {
		return null;
	}
	
	private static Structure addAtoms(Structure seed){
		if (generateRandomAtoms) {
			long st = System.currentTimeMillis();
			//Structure struct = randomAddAtoms2(seed);
			Structure struct = randomAddAtoms3(seed);
			Logger.debug("Randomly adding " + (targetNumAtoms - seed.numDefiningSites()) + " atoms "
			             + "takes " + GAIOUtils.formatElapsedTime(System.currentTimeMillis() - st)); 
			return struct;
		} else {
			// TODO: Not completed yet.
			return combinedAddAtoms(seed);
		}
	}

	/*
	private static Structure randomAddAtoms(Structure seed){
		double[][] maxMinCoordsByDimension = getCoordsByDimension(seed);
		StructureBuilder sb = new StructureBuilder(seed);
		Random rand = new Random();
		Coordinates[] coords = new Coordinates[Math.abs(size - sb.numDefiningSites())];
		Species[] species = new Species[coords.length];
		double[] widthDimensions = new double[]{
				maxMinCoordsByDimension[0][1] - maxMinCoordsByDimension[0][0],
				maxMinCoordsByDimension[1][1] - maxMinCoordsByDimension[1][0],
				maxMinCoordsByDimension[2][1] - maxMinCoordsByDimension[2][0]
		};
		Arrays.fill(species, seed.getSiteSpecies(0)); // "0" means cluster has same type of element.
		int[] state = getState(widthDimensions);
		
		for(int i = 0; i < coords.length; i++){
			double wX = widthDimensions[0] * state[0];
			double wY = widthDimensions[1] * state[1];
			double wZ = widthDimensions[2] * state[2];

			int[] indices = new int[]{0,0,0};
			for(int j = 0; j < indices.length; j++){
				if(state[j] == 0){
					indices[j] = new Random().nextInt(2);
				}
			}
			
			double x = maxMinCoordsByDimension[0][indices[0]] + rand.nextDouble()*wX;
			double y = maxMinCoordsByDimension[1][indices[1]] + rand.nextDouble()*wY;
			double z = maxMinCoordsByDimension[2][indices[2]] + rand.nextDouble()*wZ;
			Logger.detail("New atom added to the seed: " + Arrays.toString(new double[] { x, y, z }));
			coords[i] = new Coordinates(new double[] { x, y, z }, CartesianBasis.getInstance());
		}
		sb.addSites(coords, species);
		return new Structure(sb);
	}
	*/
	
	/**
	 * Procedure to randomly add an atom:
	 *  1. randomly chose a direction;
	 *  2. Use a Newton's bisection method to determine closest location to add an atom;
	 * 
	 * Note: 
	 *  1. This procedure doesn't trace whether a site has already been attached by a new
	 * atom.
	 *  2. The first atom is alway added at the CG of the cluster, and therefore this method 
	 * favors convex structure (i.e. might miss out concave structure or ring structures).
	 *  3. It works for both single-element clusters and multi-element clusters.
	 *  4. Don't consider ligated clusters yet.
	 * @param seed Structure as a seed to add atoms onto.
	 * @return newly created structure
	 */
	private static Structure randomAddAtoms(Structure seed) {
		StructureBuilder builder = new StructureBuilder(seed);
		double[] cg = GAUtils.getCGCoords(seed);
		
		// Capable of dealing with multi-element clusters.
		// Add missing atoms for each element one-by-one.
		for (int i = 0; i < targetEleNames.length; i++) {
			String newElement = targetEleNames[i];
			Element e = Element.getElement(newElement);
			int numNewAtoms = Math.abs(targetEleNums[i] - seed.numDefiningSitesWithElement(e));
			
			for (int j = 0; j < numNewAtoms; j++) {
				int counter = 0;
				boolean validAttach = false;
				double[] randomDirection = GAMath.randomPointOnUnitSphere();
				double inPoint = 0.;
				double outPoint = GAUtils.getFarthestCartDistFromCG(seed, true) 
								+ GAUtils.getAtomicRadius(newElement);
				double dist = inPoint; // distance
				double[] location = MSMath.arrayAdd(MSMath.arrayMultiply(randomDirection, dist), cg);
				double nd = GAUtils.distanceFrom(seed, location, newElement, true);  // nearest distance;
				counter++;
				if (0 <= nd && nd <= BUFFER) {
					// A concave structure. Enough space even at the center of mass. By adding the atom
					// at the center of mass, the structure will prefer convex shapes.
					validAttach = true;
					builder.addSite(new Coordinates(location, CartesianBasis.getInstance()), 
										Species.get(newElement)); 
				}
				while (!validAttach) {
					// valid location if 0 <= nd <= BUFFER
					if (nd < 0) {
						inPoint = dist;
					} else if (nd > BUFFER) {
						outPoint = dist;
					}
					dist = (inPoint + outPoint) * 0.5;
					location = MSMath.arrayAdd(MSMath.arrayMultiply(randomDirection, dist), cg);
					nd = GAUtils.distanceFrom(seed, location, newElement, true);
					counter++;
					if (0 <= nd && nd <= BUFFER) {
						validAttach = true;
						builder.addSite(new Coordinates(location, CartesianBasis.getInstance()), 
										Species.get(newElement)); 
					}
				}
				Logger.debug("Randomly adding atom-" + i + " takes " + counter + " attempts.");
			}
		}
		return new Structure(builder);
	}

	/**
	 * New procedure. Updated on 2021-07-08.
	 * 
	 * Procedure to randomly add an atom:
	 *  1. Randomly pick an atom of seed cluster;
	 *  2. Randomly chose a direction;
	 *  3. Attempt to attach a new atom to the select atom of the seed by putting it
	 *     at a distance of the sum of atomic radii of the two atoms along the randomly selected
	 *     direction.
	 *  4. Check overlap. If overlapping, restart from step 1. Otherwise, continue to next atom.
	 * 
	 * Note: 
	 *  1. It works for both single-element cluster and multi-element clusters. For clusters with
	 * multiple elements, it assumes the number of atoms for each element of the seed cluster
	 * is less or equal to the target number of atom for that element.
	 *  2. It tracks the newly added atoms. Subsequent attachment could happen on newly added atoms.
	 *  3. Don't have a preference over concave or convex configuration. Attachment direction
	 * is totally random.
	 *  4. Don't consider ligated clusters yet.
	 *  5. Unit cell size are adjusted later by Cluster.getCellSize(). Don't need to worry about it
	 *     here.
	 * @param seed Structure as a seed to add atoms onto.
	 * @return newly created structure
	 */
	private static Structure randomAddAtoms2(Structure seed) {
		StructureBuilder builder = new StructureBuilder(seed);
		
		// Capable of dealing with multi-element clusters.
		// Add missing atoms for each element one-by-one.
		int totalNumAtomDiff = targetNumAtoms - seed.numDefiningSites();
		int counter = 0;
		for (int i = 0; i < targetEleNames.length; i++) {
			String newElement = targetEleNames[i];
			Element e = Element.getElement(newElement);
			int numNewAtoms = Math.abs(targetEleNums[i] - seed.numDefiningSitesWithElement(e));
			
			for (int j = 0; j < numNewAtoms; j++) {
				boolean validAttach = false;
				while(!validAttach) {
					counter++;
					int randomIndex = GAMath.randomInt(seed.numDefiningSites());
					double[] randomDirection = GAMath.randomPointOnUnitSphere();
					Site s = seed.getDefiningSite(randomIndex);
					double NNdistance = GAUtils.getAtomicRadius(s.getSpecies().getElementSymbol()) 
									  + GAUtils.getAtomicRadius(e.getSymbol());
					
					double[] location = MSMath.arrayAdd(s.getCoords().getCartesianArray(),
						MSMath.arrayMultiply(randomDirection, NNdistance));
					
					// nearest distance
					double nd = GAUtils.distanceFrom(seed, location, newElement, true);
					if (0 <= nd && nd <= BUFFER) {
						validAttach = true;
						builder.addSite(new Coordinates(location, CartesianBasis.getInstance()), 
										Species.get(newElement)); 
					}
				}
				// The newly added atom could be target of next valid attachment.
				seed = new Structure(builder);
				builder = new StructureBuilder(seed);
			}
		}
		Logger.debug("Randomly adding atom-" + totalNumAtomDiff + " takes " + counter + " attempts.");
		return new Structure(builder);
	}

	/**
	 * A variation of randomAddAtoms2. This function don't trace the atoms that are just added
	 * to the seed cluster. All attempts to add atoms are made to atoms that are already in the
	 * original cluster. So the created clusters won't have chains growing out of the original 
	 * seed by just added atoms.
	 * 
	 * @param seed
	 * @return
	 */
	private static Structure randomAddAtoms3(Structure seed) {
		Structure currStruct = new Structure(seed);
		StructureBuilder builder = new StructureBuilder(currStruct);
		
		// Capable of dealing with multi-element clusters.
		// Add missing atoms for each element one-by-one.
		int totalNumAtomDiff = targetNumAtoms - seed.numDefiningSites();
		int counter = 0;
		for (int i = 0; i < targetEleNames.length; i++) {
			String newElement = targetEleNames[i];
			Element e = Element.getElement(newElement);
			int numNewAtoms = Math.abs(targetEleNums[i] - seed.numDefiningSitesWithElement(e));
			
			for (int j = 0; j < numNewAtoms; j++) {
				boolean validAttach = false;
				while(!validAttach) {
					counter++;
					// Use the original seed cluster to find the attaching point, but check
					// overlap with the current structure.
					int randomIndex = GAMath.randomInt(seed.numDefiningSites());
					double[] randomDirection = GAMath.randomPointOnUnitSphere();
					Site s = seed.getDefiningSite(randomIndex);
					double NNdistance = GAUtils.getAtomicRadius(s.getSpecies().getElementSymbol()) 
									  + GAUtils.getAtomicRadius(e.getSymbol());
					
					double[] location = MSMath.arrayAdd(s.getCoords().getCartesianArray(),
						MSMath.arrayMultiply(randomDirection, NNdistance));
					
					// nearest distance
					double nd = GAUtils.distanceFrom(currStruct, location, newElement, true);
					if (0 <= nd && nd <= BUFFER) {
						validAttach = true;
						builder.addSite(new Coordinates(location, CartesianBasis.getInstance()), 
										Species.get(newElement)); 
					}
				}
				// The newly added atom could be target of next valid attachment.
				currStruct = new Structure(builder);
				builder = new StructureBuilder(currStruct);
			}
		}
		Logger.debug("Randomly adding atom-" + totalNumAtomDiff + " takes " + counter + " attempts.");
		return new Structure(builder);
	}
	
	/*
	private static int[] getState(double[] widths){
		int[] numStates = new int[widths.length];
		Arrays.fill(numStates, 2);
		ArrayIndexer indexer = new ArrayIndexer(numStates);
		int[] currentState = indexer.getInitialState();
		List<int[]> returnList = new ArrayList<>();
		while(indexer.increment(currentState)){
			int sum = MSMath.arraySum(currentState);
			if(sum > 1){
				int[] thisState = ArrayUtils.copyArray(currentState);
				returnList.add(thisState);
			}
			if(sum == 3){
				int adder = 0;
				int[] thisState = ArrayUtils.copyArray(currentState);
				while(adder < 2){
					adder++;
					returnList.add(thisState);
				}
			}
		}
		
		Random rand = new Random();
		int index = rand.nextInt(returnList.size());
		return returnList.get(index);
	}
	*/
	
	private static Structure combinedAddAtoms(Structure seed){
		int numAtoms = targetNumAtoms - seed.numDefiningSites();
		int[][] getCombos = generateCombos(numAtoms);
		for(int comboNum = 0; comboNum < getCombos.length; comboNum++) {
			//<TODO>
		}
		return null;
	}
	
	//private static int[] times(int[] sizes, int[] states){
	//	int[] returnArray = new int[sizes.length];
	//	for(int i = 0; i < sizes.length; i++){
	//		returnArray[i] = sizes[i] * states[i];
	//	}
	//	return returnArray;
	//}
	
	private static int[][] generateCombos(int sum){
				
		int[] sortedSizes = getSizes();
		int[][] combos = new int[0][];
		int[][] comboIndices = new int[0][];
		int[] states = new int[sortedSizes.length];
		Arrays.fill(states, 1);
		int counter = 0; 
		int[] initialState = new int[states.length];
		for(int i = 0; i < initialState.length; i++){
			initialState[i] = counter;
			counter++;
		}
		int[][] insufficientCombos = new int[][]{sortedSizes};
		int[][] insufficientIndices = new int[][]{initialState};
		insufficientCombos = MSMath.transpose(insufficientCombos);
		insufficientIndices = MSMath.transpose(insufficientIndices);
		while(!(insufficientIndices.length == 0)){
						
			int[][] insufficientCombosClone = ArrayUtils.copyArray(insufficientCombos);
			int[][] insufficientIndicesClone = ArrayUtils.copyArray(insufficientIndices);
			insufficientCombos = new int[0][];
			insufficientIndices = new int[0][];
			
			for(int i = 0; i < insufficientCombosClone.length; i++){
				int sumSoFar = MSMath.arraySum(insufficientCombosClone[i]);
				int[] origCombo = insufficientCombosClone[i];
				int[] origIndices = insufficientIndicesClone[i];
				for(int j = 0; j < sortedSizes.length; j++){
					int sizeB = sortedSizes[j];
					if((sumSoFar + sizeB) > sum){continue;}
					else if(sumSoFar + sizeB == sum){
						combos = ArrayUtils.appendElement(combos, ArrayUtils.appendElement(origCombo, sortedSizes[j]));
						int[] returnArray = ArrayUtils.appendElement(origIndices, j);
						if(!contains(origIndices, comboIndices)){
							comboIndices =  ArrayUtils.appendElement(comboIndices, returnArray);
						}
					}
					else{
						insufficientCombos = ArrayUtils.appendElement(insufficientCombos, ArrayUtils.appendElement(origCombo, sortedSizes[j]));
						int[] returnArray = ArrayUtils.appendElement(origIndices, j);
						if(!contains(origIndices, comboIndices)){
							insufficientIndices = ArrayUtils.appendElement(insufficientIndices, returnArray);
						}
					}
				}
			}
			
		}
	
		return comboIndices;
	}
	
	private static boolean contains(int[] testArray, int[][] collectiveArray){
		testArray = sort(testArray);
		for(int index = 0; index < collectiveArray.length; index++){
			int[] stateArray = collectiveArray[index];
			stateArray = sort(stateArray);
			if(isSame(testArray, stateArray)){
				return true;
			}
		}
		return false;
	}
	
	private static boolean isSame(int[] a, int[] b){
		if(a.length != b.length) {return false;}
		for(int i = 0; i < a.length; i++){
			if(a[i] != b[i]){
				return false;
			}
		}
		return true;
	}
	
	private static int[] sort(int[] i){
		int[] map = ArrayUtils.getSortPermutation(i);
		int[] returnArray = new int[i.length];
		for(int ind = 0; ind < returnArray.length; ind++){
			returnArray[ind] = i[map[ind]]; 
		}
		return returnArray;
	}
	
	private static int[] getSizes(){
		int[] sizes = new int[seeds.size()];
		for(int i = 0; i < seeds.size(); i++){
			sizes[i] = seeds.get(i).numDefiningSites();
		}
		int[] map = ArrayUtils.getSortPermutation(sizes);
		int[] sortedSizes = new int[map.length];
		for(int i = 0; i < map.length; i++){
			sortedSizes[i] = sizes[map[i]];
		}
		return sortedSizes;
	}
	
	private static double[][] getCoordsByDimension(Structure seed){
		double[][] returnArray = new double[3][];
		double[][] xyz = new double[seed.numDefiningSites()][];
		for(int site = 0; site < seed.numDefiningSites(); site++){
			xyz[site] = seed.getSiteCoords(site).getCartesianArray();
		}
		xyz = MSMath.transpose(xyz);
		for(int dimNum = 0; dimNum < returnArray.length; dimNum++){
			double min = ArrayUtils.minElement(xyz[dimNum]);
			double max = ArrayUtils.maxElement(xyz[dimNum]);
			returnArray[dimNum] = new double[]{min, max};
		}
		System.out.println(Arrays.deepToString(returnArray));
		return returnArray;
	}

	//private static Structure[] getGroundStateStructures(String[] eleNames, int[] eleNums) {
	//	if (strides.length == 0) {
	//		findStrides();
	//	}
	//	Structure[] structures = new Structure[strides.length];
	//	int lastSeen = 0;
	//	String newElement = eleNames[0];
	//	for(int structureNumber = 0; structureNumber < strides.length; structureNumber++){
	//		int numberOfAtoms = strides[structureNumber] - 2;
	//		Coordinates[] coords = new Coordinates[numberOfAtoms];
	//		Species[] species = new Species[numberOfAtoms];
	//		int adder = 0;
	//		String oldElement = getElementName(lastSeen+2);
	//		for(int indexNumber = lastSeen + 2; indexNumber < lastSeen + strides[structureNumber]; indexNumber++){
	//			coords[adder] = getCoords(indexNumber, oldElement, newElement); // read coordinates from seed.dat
	//			species[adder++] = Species.get(newElement);
	//		}
	//		Vector[] cellVectors = getVectors(cellSize(coords));
	//		structures[structureNumber] = buildStructure(coords, species, cellVectors);
	//		lastSeen += strides[structureNumber];
	//	}
	//	return structures;
	//}
	
	//private static Coordinates getCoords(int indexNumber, String oldElement, String newElement){
	//	double oldElementNN = GAUtils.getNNDistance(oldElement);
	//	double newElementNN = GAUtils.getNNDistance(newElement);
	//	double ratio = newElementNN / oldElementNN;
	//	String line = seedList.get(indexNumber);
	//	String[] split = line.split("\\s+");
	//	double x = Double.parseDouble(split[1]);
	//	double y = Double.parseDouble(split[2]);
	//	double z = Double.parseDouble(split[3]);
	//	double[] cartArray = new double[]{x,y,z};
	//	cartArray = MSMath.arrayMultiply(cartArray, ratio);
	//	return new Coordinates(cartArray, CartesianBasis.getInstance());
	//}
	
	/**
	 * @param index The list index of the first coordinate line.
	 * @return The element name in the first line of coordinate.
	 */
	//private static String getElementName(int index){
	//	String name = seedList.get(index);
	//	String element = name.split("\\s+")[0];
	//	return element;
	//}
	
	//private static Structure buildStructure(Coordinates[] coords, Species[] species, Vector[] cellVectors){
	//	StructureBuilder sb = new StructureBuilder();
	//	sb.setCellVectors(cellVectors);
	//	sb.addSites(coords, species);
	//	return new Structure(sb);
	//}
	
	//private static Vector[] getVectors(double cellSize){
	//	double[][] abc = new double[][]{
	//		{cellSize,0,0},
	//		{0,cellSize,0},
	//		{0,0,cellSize}
	//	};
	//	Vector[] returnArray = new Vector[abc.length];
	//	for(int i = 0; i < abc.length; i++){
	//		returnArray[i] = new Vector(abc[i]);
	//	}
	//	return returnArray;
	//}
	
	/**
	 * Get the largest difference of coordinates of all atoms in all dimensions (x, y, z). Add 
	 * 10 to this value as the cellSize. It makes sure that periodic images have a separation of
	 * at least DISTIMAGE.
	 * 
	 * @param coords
	 * @return
	 */
	private static double cellSize(Coordinates[] coords){
		double[][] cartesianArray = toCartesianMatrix(coords);
		double[][] transposedMatrix = MSMath.transpose(cartesianArray);
		double largestWidth = Double.NEGATIVE_INFINITY;
		for(int dimNum = 0; dimNum < transposedMatrix.length; dimNum++){
			double min = ArrayUtils.minElement(transposedMatrix[dimNum]);
			double max = ArrayUtils.maxElement(transposedMatrix[dimNum]);
			double width = max - min;
			if(width > largestWidth){
				largestWidth = width;
			}
		}
		return largestWidth + Input.getDistImage();
	}
	
	private static double[][] toCartesianMatrix(Coordinates[] coords){
		double[][] xyz = new double[coords.length][];
		for(int i = 0; i < xyz.length; i++){
			xyz[i] = coords[i].getCartesianArray();
		}
		return xyz;
	}

	/**
	 * Find the strides (length of lines representing each structure) in seed.dat. The seed
	 * could have different number of atoms.
	 * @throws IOException
	 */
	//private static void findStrides() {
	//	if (seedList.size() == 0) {
	//		seedList = HPC.read(Input.getRoot() + HPC.FILESPTR + "seed.dat");
	//	}
	//	int[] energyIndices = searchEnergies();
	//	energyIndices = ArrayUtils.appendElement(energyIndices, seedList.size() + 1);		
	//	for(int i = 0; i < energyIndices.length - 1; i++){
	//		strides = ArrayUtils.appendElement(strides, energyIndices[i + 1] - energyIndices[i]);
	//	}
	//}
	
	/**
	 * 
	 * @return The array of indices of lines containing "Energy".
	 */
	//private static int[] searchEnergies(){
	//	int[] indices = new int[0];
	//	for(int i = 0; i < seedList.size(); i++){
	//		if (seedList.get(i).contains("Energy")) {
	//			indices = ArrayUtils.appendElement(indices, i);
	//		}
	//	}
	//	return indices;
	//}
}