package ga.structure;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import matsci.Species;
import matsci.io.vasp.POSCAR;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;
import ga.HPC;
import ga.Input;
import ga.Start;
import ga.io.GAIOUtils;
import ga.io.Logger;
import ga.minimizer.EnergyMinimizer;
import ga.minimizer.LAMMPS;
import ga.minimizer.VASP;
import ga.multithreaded.Candidates;
import ga.multithreaded.NewPool;
import ga.utils.GAMath;
import ga.utils.GAUtils;
import ga.utils.FileLock;

public class Cluster {

	// Necessary variables to represent a cluster
	private String m_CalcDir;
	private String[] m_EleNames;
	private int[] m_EleNums;
	private double m_CellSize = 0;
	private Object[] m_ClusList; // Favor using Structure type instead of Object[].
	private Structure m_Structure = null;
	
	// Useful variables to query info. Can be obtained from necessary variables. Should be initiated.
	private int m_NumAtoms;
	private int m_CandidateIndex = -1; // Start from 1.
	
	// From Input class. Automatically initiated.
	//private double m_Rij = Input.getRij();
	private boolean m_isSurfaceCalc = Input.isSurfaceCalc();
	private boolean m_isLigatedCluster = Input.isLigatedCluster();
	private double m_EnergyLimit = Input.getEnergyLimit();
	
	// Optional variables: after evaluation and post-processing
	private String m_ClusterType = null;
	private String m_ClusterSubType; // For debugging purposes.
	private double m_Energy = 0.0;
	private static double[][] m_Bonds;
	private int m_StatusLine = -1; // List index of the first line of this cluster in candidates.dat
	// TODO: what for?
	// For now, set it to be the order it's added to the pool.
	private int m_PoolIndex;
	private int[] m_Parents = {-1, -1}; // Candidate index of parent clusters.
	                                    // For mutation, only first index is set.
	private String m_Status = null;
	private double m_Fitness;
	//private String m_Thread = Thread.currentThread().getName();
	private SupportedCluster m_SupportedCluster = null;
	private EnergyMinimizer m_Minimizer = null;
	private Double m_MaxvolGrade = Double.MAX_VALUE;
	private boolean m_IsExtrapolating = true;
	private int[] m_POSCARLookup;
	
	// Flags signaling state of cluster
	private boolean m_IsCharged = false;
	private boolean m_Relaxed = false;   // help decide use initial structure for retrain or relaxed one.
	private boolean m_Exploded = false;  // There exists at least one atom not touching any other atoms.
	private boolean m_Collapsed = false; // There exists at least one pair of atoms that are totally 
	                                     // overlapping with each other. (distance less than 1E-5)

	// Constants
	public static final String COORDINATEFORMAT = "%19.15f %19.15f %19.15f";
	public static final String CHOSEN = Start.WorkDir + HPC.FILESPTR + "chosen.dat";
	public static final String MUTATION = Start.WorkDir + HPC.FILESPTR + "mutation.dat";
	public static final String MAXVOLGRADES = Start.WorkDir + HPC.FILESPTR + "maxvolGrades.dat";
	
	/**
	 * Generate a new cluster positioned at the center of the unit cell.
	 * 
	 * @param clusterType Type of cluster: "Random" "Seed"
	 * @param numAtoms    Total number of atoms in the cluster, including ligands.
	 * @param rij         Maximum allowed bond length between any pair of atoms.
	 * @param distImage   distance between periodic cluster image.
	 * @param eleNames    Array of element symbols.
	 * @param eleNums     Array of number of atoms for each element, in the same
	 *                    order of eleNames.
	 * @param isCharged   TODO: ?? charged ligands
	 * @param pool		  The globally shared pool object. Use it only in this constructor and its
	 *                    called functions to make minimize the exposure of it, and lower the 
	 *                    chance of deadlock.
	 * @throws FileNotFoundException
	 */
	public Cluster(String clusterType, int numAtoms, double distImage, String[] eleNames, 
			       int[] eleNums, boolean isCharged, NewPool pool) {
		m_ClusterType = clusterType;
		m_NumAtoms = numAtoms;
		m_EleNames = eleNames;
		m_EleNums = eleNums;
		m_IsCharged = isCharged;
		m_ClusList = this.generateCluster(m_NumAtoms, m_EleNames, m_EleNums, pool);
		m_CellSize = ClusterFactory.getCellSize(m_ClusList, distImage);
		// Ensure structure is at center of unit cell before relaxation.
		ClusterFactory.moveCGToCenter(m_ClusList, m_CellSize);
		m_Structure = ClusterFactory.objectToStructure(m_ClusList, m_CellSize);
		m_POSCARLookup = this.buildLookup(m_ClusList,m_EleNames);
		m_Status = "Running";
		m_Relaxed = false;
	}
	
	/**
	 * Used in recover candidate list when restarting from a stopped run. Create cluster object 
	 * from block of lines in candidates.dat or pool.dat. 
	 * 
	 * CalcDir is checked in "DOptimality" mode for situations that clusters are relaxed but added 
	 * to Start.m_NewData waiting for training. In these cases, m_Relaxed should be true, but
	 * candidates.dat still records a status of "Running".
	 * 
	 * @param lines The lines in candidates.dat or pool.dat depicting one cluster object, including
	 *              the two header lines.
	 * @param source Which .dat file the lines are from. "candidates", "pool".
	 */
	public Cluster(List<String> lines, String source) {
		if (source == "candidates") {
			m_CellSize = Double.parseDouble(lines.get(0).trim());
			m_Structure = buildStructure(m_CellSize, lines.subList(2, lines.size()).toArray(new String[0]));
			String[] secondLine = lines.get(1).trim().split("\\s+");
			m_ClusterType = secondLine[0];
			m_CalcDir = secondLine[3];
			m_Status = secondLine[4];
			m_CandidateIndex = Cluster.calcDirToIndex(m_CalcDir);
			this.updateStatusLineNumber();
			m_Minimizer = GAUtils.findMinimizer(Input.getMode(), this);
			if (secondLine.length == 9) { // Finished.
				m_Energy = Double.parseDouble(secondLine[7]);
				m_Status = m_Status + " Energy = " + m_Energy + " eV";
				m_Relaxed = true;
			} else if (Input.getRetrainMode() == Input.RetrainMode.DOptimality && m_Minimizer.existOutput()){
				// Relaxed but extrapolating / unrealistic. candidates.dat was not updated.
				String status = EnergyMinimizer.getStatus(m_Minimizer.readOutput(), 
						m_Minimizer.getFinishStr(), m_Minimizer.getErrorStr());
				if (status.contains("Finished")) {
					m_Relaxed = true;
					m_Status = status; // "Finished"
					m_Structure = m_Minimizer.readStructure(true);
				}
			}
		} else if (source == "pool") {
			// Less important tags for pool clusters: m_ClusterType, m_Parents, m_StatusLine
			m_NumAtoms = Integer.parseInt(lines.get(0).trim());
			String[] secondLine = lines.get(1).trim().split("\\s+");
			if (secondLine.length != 6) {
				Logger.error("Incomplete pool.dat file: " + lines.get(1));
				System.exit(3);
			}
			m_CalcDir = secondLine[5];
			m_CandidateIndex = Cluster.calcDirToIndex(m_CalcDir);
			// Read m_CellSize from candidates.dat file. We shouldn't use getCellSize() function
			// since the relaxation has changed the atomic coordinates and adding m_DistImage to
			// the widest size could give inconsistent value as that stored in candidates.dat.
			// TODO: potential dead lock?? We use readWriteLock. Should be fine.
			List<String> candidates = HPC.read(Candidates.CANDIDATES);
			m_CellSize = Double.parseDouble(candidates.get((m_CandidateIndex-1)*Input.getStride()));
			m_Structure = buildStructure(m_CellSize, lines.subList(2, lines.size()).toArray(new String[0]));
			m_Energy = Double.parseDouble(secondLine[2]);
			m_Status = "Finished" + " Energy = " + m_Energy + " eV";
			m_Relaxed = true;
		} else {
			Logger.error("Unrecoganized type of file to read cluster from: " + source + ".dat.");
			System.exit(3);
		}
		
		m_ClusList = ClusterFactory.structureToObjectArray(m_Structure); // TODO: Use solely Strucutre type.
		m_Exploded = ClusterFactory.checkExplosionByContiguous(m_ClusList);
		m_Collapsed = ClusterFactory.checkCollapse(m_ClusList);
		m_EleNames = ClusterFactory.getEleNames(m_Structure);
		m_EleNums = ClusterFactory.getEleNums(m_Structure);
		m_NumAtoms = m_Structure.numDefiningSites();
		m_POSCARLookup = buildLookup(m_ClusList,m_EleNames);
	}

	/**
	 * Used in Re-evaluation engine.
	 * @param struct
	 * @param calcDir
	 */
	public Cluster(Structure struct, String calcDir) {
		m_CalcDir = calcDir;
		m_CandidateIndex = calcDirToIndex(calcDir);
		m_EleNames = ClusterFactory.getEleNames(struct);
		m_EleNums = ClusterFactory.getEleNums(struct);
		m_CellSize = struct.getCellVectors()[0].length();
		m_ClusList = ClusterFactory.structureToObjectArray(struct);
		m_Structure = struct;
		m_NumAtoms = struct.numDefiningSites();
		m_POSCARLookup = buildLookup(m_ClusList,m_EleNames);
		m_Status = "Running";
		m_Relaxed = false;
	}
	
	private Cluster(double cellSize, String clusterType, String calcNum, int numAtoms, double energy, int statusLine,
			Structure clusterStructure, String status) {
		m_CellSize = cellSize;
		m_ClusterType = clusterType;
		m_CalcDir = calcNum;
		m_NumAtoms = numAtoms;
		m_Energy = energy;
		m_StatusLine = statusLine;
		m_Structure = clusterStructure;
		Species[] distinctSpecies = m_Structure.getDistinctSpecies();
		m_EleNames = new String[distinctSpecies.length];
		m_EleNums = new int[distinctSpecies.length];
		for (int i = 0; i < distinctSpecies.length; i++) {
			m_EleNames[i] = distinctSpecies[i].getElementSymbol();
			m_EleNums[i] = m_Structure.numDefiningSitesWithSpecies(distinctSpecies[i]);
		}
		m_ClusList = ClusterFactory.structureToObjectArray(m_Structure);
		m_POSCARLookup = buildLookup(m_ClusList,m_EleNames);
		m_Status = status;
	}
	
	/**
	 * Dummy constructor to create empty cluster object as place-holder. Used to fill
	 * candidates list in multithreading execution.
	 */
	public Cluster() {}
	
	/** 
	 * The main routine for generating new structures by GA operations. m_Pool is only used in this
	 * function and its called functions. To make Cluster thread-safe, we should lock m_Pool when
	 * generating new clusters.
	 * 
	 * Cluster is not positioned at center after this function since m_CellSize is not yet 
	 * determined. Move to the center in constructor.
	 * 
	 * @param numAtoms
	 * @param rij
	 * @param eleNames
	 * @param eleNums
	 * @return
	 */
	private Object[] generateCluster(int numAtoms, String[] eleNames, int[] eleNums, NewPool pool) {
		Object[] clusList = new Object[numAtoms];
		// Initial population generation
		if (!m_isLigatedCluster) {
			if (m_ClusterType.equalsIgnoreCase("Random")) {
				clusList = ClusterFactory.getRandomBareClusterByScattering(numAtoms, eleNames, eleNums);
				//clusList = ClusterFactory.getRandomBareClusterByConcatenating(numAtoms, eleNames, eleNums);
			} else if (m_ClusterType.equalsIgnoreCase("Incremental")) {

			} else if (m_ClusterType.equalsIgnoreCase("Swap")) {

			} else if (m_ClusterType.equalsIgnoreCase("Seed")) {
				Structure seededStructure = Seeder.build();
				clusList = ClusterFactory.structureToObjectArray(seededStructure);
			} else if (m_ClusterType.equalsIgnoreCase("Ring")) {
			    // Warning: Beta function only for sulfur. Elemental for now.
			    if (! eleNames[0].equalsIgnoreCase("S")) {
			        Logger.warning("Initializing as rings is a BETA function only for sulphur. " + eleNames[0]);
			        // TODO: Allow user to add customized value in INGA.
			        System.exit(1);
			    }
			    double[] parameters = GAUtils.m_RingParameters.get(eleNames[0]);
			    clusList = ClusterFactory.ringGenerator(eleNames[0], numAtoms, parameters[0], parameters[1]);
			} else if (m_ClusterType.equalsIgnoreCase("Off") || m_ClusterType.equalsIgnoreCase("RBG")) {
				// Genetic operations
				// "RBG" case is only when a calculation has been restarted without a
				// candidates.dat file, so this shouldn't be too many.
				if (m_ClusterType.equals("RBG")) {
					m_ClusterType = "Off"; // Reassign as cross-over.
				}
				// New clustList objects. Can be safely changes without side effects.
				Object[][] coords = this.findPair(pool);
				coords = this.prepare(coords);
				clusList = this.mate(coords, pool);
				//this.writeChosen();
			} else if (m_ClusterType.equalsIgnoreCase("Mut")) {
				String[] mutations = Input.getMutationType();
				m_ClusterSubType = mutations[GAMath.randomInt(mutations.length)];
				
				Cluster parent = pool.getPool().get(GAMath.randomInt(pool.getSize()));
				m_Parents[0] = parent.getCandidateIndex(); // Second index default to -1.
				Logger.detail("Mutated from cluster: " + m_Parents[0] + " with type: " + m_ClusterSubType);
				
				clusList = ClusterFactory.copyObjectArray(parent.getClusList());
				if (m_ClusterSubType.equalsIgnoreCase("Rotate")) {
					clusList = ClusterFactory.mutateBareClusterRandomRotate(clusList);
				} else if (m_ClusterSubType.equalsIgnoreCase("Move")) {
					clusList = ClusterFactory.mutateBareClusterRandomMoveGlobal(clusList);
				} else if (m_ClusterSubType.equalsIgnoreCase("Move_local")) {
				    // Warning: BETA function. Only for sulphur.
				    clusList = ClusterFactory.mutateBareClusterRandomMoveLocal(clusList);
				}
			}
			clusList = ClusterFactory.fixOverlapCluster(clusList);
		} else {
		//********************************** Ligated structure ***********************************//
			// In output structure file, write the bare cluster first and then the ligands.
			if (m_ClusterType.equalsIgnoreCase("Random")) {
				// randomly generated cluster
				clusList = randomGenerationLigated();
			} else if (m_ClusterType.equalsIgnoreCase("Off") || m_ClusterType.equalsIgnoreCase("RBG")) {
				// crossover operation
				m_ClusterType = "Off"; // Reassign RBG to Off.
				clusList = crossoverLigated(pool);
			} else if (m_ClusterType.equalsIgnoreCase("Mut")) {
				// mutation operation
				clusList = selectCandidateFromPool(pool);
				clusList = mutationLigated(clusList);
			} else if (m_ClusterType.equalsIgnoreCase("Seed")) {
				// seed operation
				clusList = seedLigated();
			}
		}

		if (m_isSurfaceCalc) {
			// TODO: ??
			SupportedCluster supportedCluster = new SupportedCluster(clusList);
			m_SupportedCluster = supportedCluster;
			// m_Structure = m_SupportedCluster.getSupportedClusterStructure();
			clusList = supportedCluster.getSupportedClusterList();
		}
		return clusList;
	}

	/**
	 * Initialize m_Minimizer, subprocess parameters like "mpirun -np 4" and write 
	 * input files for calculation.
	 * 
	 * Input files include KPOINTS, POSCAR, POTCAR, INCAR for VASP; and input.lmp, mlip.ini,
	 * mlip.mtp, and struct.lmp for LAMMPS with MTP;
	 * 
	 * Constructor is only used to generate structure. I split the two initialization for
	 * multi-threading, since I don't want m_Candidates is always locked by one thread 
	 * (avoid starvation).
	 * 
	 */
	public void initializeCalculation() {
		File calc = new File(m_CalcDir);
		if (!calc.exists()) {
			calc.mkdirs();
		}
		
		if (Input.getMode().equalsIgnoreCase("VASP")) {
			m_Minimizer = new VASP(this, Input.getVASPEXE(), Input.isParallel(), Input.getVASPMPI());
		} else if (Input.getMode().equalsIgnoreCase("LAMMPS")) {
			m_Minimizer = new LAMMPS(this, Input.getLAMMPSEXE(), Input.isParallel(), Input.getLAMMPSMPI());
		} else if (Input.getMode().equalsIgnoreCase("Active")) {
			m_Minimizer = new LAMMPS(this, Input.getLAMMPSEXE(), Input.isParallel(), Input.getLAMMPSMPI());
		}
		m_Minimizer.createInputs(false);
	}
	
	public void setPoolIndex(int i) {
		m_PoolIndex = i;
	}

	public String getStatus() {
		return m_Status;
	}
	
	public void setStatus(String status) {
		m_Status = status;
	}

	public int getPoolIndex() {
		return m_PoolIndex;
	}

	public Structure getStructure() {
		if (m_Structure == null) {
			m_Structure = ClusterFactory.objectToStructure(m_ClusList, m_CellSize);
		}
		return m_Structure;
	}
	
	/**
	 * Pre-processing for similarity evaluations of ligated clusters. 
	 * Returns a copy of the ligated cluster with ligand side chains removed. 
	 * This method should only be used if ACTIVESITE is specified in INGA; otherwise, the bare cluster is returned.
	 */
	public Structure getStructureWithoutSideChains() {
		ArrayList<Object> clusterNoSideChains = new ArrayList<Object>();
		int activeSite = Input.getActiveSite();
		String coreAtom = m_EleNames[0];
		String bindingAtom = (activeSite > 0) ? m_EleNames[activeSite] : m_EleNames[0];
		
		for (int i = 0; i < m_ClusList.length; i++) {
			Object[] atom = (Object[]) m_ClusList[i];
			String element = (String) atom[0];
			if (element.equalsIgnoreCase(coreAtom) || element.equalsIgnoreCase(bindingAtom)) {
				clusterNoSideChains.add(atom);
			}
		}
		Object[] clusterObj = ClusterFactory.convertFromExpandableArray(clusterNoSideChains);
		Structure structure = ClusterFactory.objectToStructure(clusterObj, m_CellSize);
		return structure;
	}
	
	/**
	 * 
	 * @param struct
	 */
	public void setStructure(Structure struct) {
		this.m_Structure = struct;
		m_ClusList = ClusterFactory.structureToObjectArray(m_Structure);
		m_CellSize = m_Structure.getCellVectors()[0].length(); // Since cells are cubic.
	}

	public void setFitness(double f) {
		m_Fitness = f;
	}

	public String numDefiningSites() {
		return Integer.toString(m_NumAtoms);
	}

	public double getEnergy() {
		return m_Energy;
	}
	
	public void setEnergy(double energy) {
		m_Energy = energy;
		if (m_Status.contains("Finished Energy")) {
			m_Status = "Finished Energy = " + m_Energy + " eV";
		}
	}

	public String getCalcDir() {
		return m_CalcDir;
	}
	
	public void setCalcDir(String calDir) {
		this.m_CalcDir = calDir;
	}
	
	public int getCandidateIndex() {
		if (m_CandidateIndex == -1) {
			m_CandidateIndex = Cluster.calcDirToIndex(m_CalcDir);
		}
		return m_CandidateIndex;
	}
	
	public void setCandidateIndex(int index) {
		m_CandidateIndex = index;
	}
	
	/**
	 * Calculate and update m_StatusLine according to m_CandidateIndex.
	 */
	public void updateStatusLineNumber() {
		if (m_CandidateIndex == -1) {
			Logger.error("Set correct m_CandidateIndex before update m_StatusLine!");
			return;
		} else {
			m_StatusLine = (m_CandidateIndex - 1) * Input.getStride();
		}
	}

	/**
	 * Be careful when accessing the internal variable. If changes will be made to them in-place,
	 * better call ClusterFactory.copyObjectArray() to get a deep copy.
	 * @return
	 */
	public Object[] getClusList() {
		return m_ClusList;
	}

	public String[] getEleNames() {
		return m_EleNames; // Should be the same as Input.m_EleNames
	}

	public int[] getEleNums() {
		return m_EleNums;
	}
	
	public int getNumAtoms() {
		if (m_NumAtoms == 0) {
			m_NumAtoms = MSMath.arraySum(m_EleNums);
		}
		return m_NumAtoms;
	}

	public boolean isCharged() {
		return m_IsCharged;
	}

	public double getCellSize() {
		if (m_CellSize == 0) {
			m_CellSize = ClusterFactory.getCellSize(m_ClusList, Input.getDistImage());
		}
		return m_CellSize;
	}

	public EnergyMinimizer getEnergyMinimizer() {
		return this.m_Minimizer;
	}
	
	/**
	 * It's used in many if-loops, for simplification of code, add this helper function.
	 * @return whether m_Minimizer is of type ga.minimizer.VASP
	 */
	public boolean relaxedByVASP() {
		return m_Minimizer.getName().equals("VASP");
	}
	
	public void setEnergyMinimizer(String minimizer) {
		if (minimizer == "VASP") {
			this.m_Minimizer = new VASP(this, Input.getVASPEXE(), Input.isParallel(), Input.getVASPMPI());
		} else if (minimizer == "LAMMPS") {
			this.m_Minimizer = new LAMMPS(this, Input.getLAMMPSEXE(), Input.isParallel(), Input.getLAMMPSMPI());
		}
	}
	
	public String getClusterType() {
		return m_ClusterType;
	}
	
	public String getClusterSubType() {
		return m_ClusterSubType;
	}
	
	public void setClusterType(String type) {
		m_ClusterType = type;
	}
	
	public void setClusterSubType(String type) {
		m_ClusterSubType = type;
	}
	
	public void setParents(int one, int two) {
		m_Parents[0] = one;
		m_Parents[1] = two;
	}
	
	public int[] getParents() {
		return m_Parents;
	}
	
	public boolean relaxed() {
		return m_Relaxed;
	}
	
	public void setRelaxed(boolean relaxed, boolean resetStructure) {
		m_Relaxed = relaxed;
		if (!relaxed) {
			m_Status = "Running";
			m_Energy = 0.0;
		}
		if (resetStructure) { 
			this.setStructure(m_Minimizer.readStructure(relaxed ? true : false));
		}
	}
	
	public boolean isExploded() {
		return m_Exploded;
	}
	
	public boolean isCollapsed() {
		return m_Collapsed;
	}
	
	public boolean realistic() {
		return (!m_Exploded && !m_Collapsed);
	}
	
	public boolean extrapolating() {
		return m_IsExtrapolating;
	}
	
	public boolean valid() {
		return !m_IsExtrapolating && this.realistic();
	}
	
	public void setExtrapolating(boolean isExtrapolating) {
		m_IsExtrapolating = isExtrapolating;
	}
	
	public Double getMaxvolGrade() {
		return m_MaxvolGrade;
	}
	
	public void setMaxvolGrade(double grade) {
		m_MaxvolGrade = grade;
	}
	
	/**
	 * Write maxvol grade to maxvolGrades.dat file if not written. Otherwise, update with new score.
	 * Order of maxvol grades is not guaranteed in the order of candidateIndex, especially for
	 * multi-threaded calculations. Use grep to retrieve grades of clusters. 
	 */
	public void writeMaxvolGrade() {
		String line = m_CalcDir + " " + m_MaxvolGrade + " " + m_IsExtrapolating;
		List<String> fileLines = HPC.read(MAXVOLGRADES);
		boolean seen = false;
		for (int i = 0; i < fileLines.size(); i++) {
			if (fileLines.get(i).matches("^" + m_CalcDir + "\\s+.*")) { // matches() check the entire line.
				if (!seen) {
					seen = true;
					fileLines.set(i, line);
				} else {
					fileLines.remove(i);
					i--; // index will change if removing one line.
				}
			}
		}
		if (!seen) {
			HPC.write(Arrays.asList(line), MAXVOLGRADES, true); // Append
		} else {
			HPC.write(fileLines, MAXVOLGRADES, false); // Overwrite
		}
	}
	
/**Below are the current operable methods for ligated cluster structure generation.**/	

	private Object[] randomGenerationLigated() {
		int noOfLigands = Input.getNumLigands();
		Structure ligand = Input.getLigandStructure();
		int numAtoms = m_EleNums[0];
		double distImage = Input.getDistImage();
		
		Object[] ligatedCluster = new Object[0];
		Logger.detail("Randomly generate bare cluster core.");
		while (ligatedCluster.length == 0) {
			//cluster = ClusterFactory.getRandomBareClusterByScattering(numAtoms, m_EleNames[0], m_Rij);
			Object[] cluster = ClusterFactory.getRandomBareClusterByConcatenating(numAtoms, m_EleNames[0], m_EleNums[0]);
			double cellSize = ClusterFactory.getCellSize(cluster,distImage);
			ClusterFactory.moveCGToCenter(cluster, cellSize);
			ligatedCluster = ClusterFactory.randomlyLigate(ligand, cluster, noOfLigands);
		}
		return ligatedCluster;
	}
	
	private Object[] crossoverLigated(NewPool pool) {
		long startTime, endTime;
		startTime = System.currentTimeMillis();
		
		int noOfLigands = Input.getNumLigands();
		int nLigand = Input.getLigandStructure().numDefiningSites();
		int nCluster = Input.getBareClusterStructure().numDefiningSites();
		
		Object[][] coords = this.findPair(pool);
		Object[][] obj1 = ClusterFactory.obj1DTo2D(coords[0], noOfLigands, nLigand, nCluster);
		Object[][] obj2 = ClusterFactory.obj1DTo2D(coords[1], noOfLigands, nLigand, nCluster);
	
		double ratio;
		String crossType = this.selectRandom(Input.getCrossType());
		if (crossType.equalsIgnoreCase("Random")) {
			ratio = GAMath.random();
		} else if (crossType.equalsIgnoreCase("Even")) {
			ratio = 0.5;
		} else { // Weighted by fitness.
			double[] fitPair = this.getFitnessPair(pool);
			double fit1 = fitPair[0];
			double fit2 = fitPair[1];
			ratio = fit1 / (fit1 + fit2);
		}
		Logger.detail("Crossover mode: " + crossType + ", "
					  + Math.round(ratio*100) + "% from " + m_Parents[0] + ", "
				      + Math.round((1-ratio)*100) + "% from " + m_Parents[1]);
		
		Object[] objCross = ClusterFactory.ligatedClusterCrossover(obj1, obj2, ratio);
			
		endTime = System.currentTimeMillis();
		Logger.detail("Ligated cluster crossover done. Takes "+ GAIOUtils.formatElapsedTime(endTime - startTime));
		return objCross;
	}

	private Object[] mutationLigated(Object[] Cluster) {
		long startTime, endTime;
		startTime = System.currentTimeMillis();
		int noOfLigands = Input.getNumLigands();
		int nLigand = Input.getLigandStructure().numDefiningSites();
		int nCluster = Input.getBareClusterStructure().numDefiningSites();
		Object[][] obj1 = ClusterFactory.obj1DTo2D(Cluster, noOfLigands, nLigand, nCluster);
		
		Object[] objMut = ClusterFactory.getMutatedLigatedCluster(obj1);
		
		endTime = System.currentTimeMillis();
		Logger.detail("Mutating ligated cluster done. Takes "+ GAIOUtils.formatElapsedTime(endTime - startTime));
		return objMut;
	}
	
	/**
	 * Use bare clusters from the folder clusterFiles as seed and attach ligands on them.
	 * TODO: combine seed.dat file from bare cluster.
	 * @return
	 */
	private Object[] seedLigated() {
		Structure[] clusterArray = Input.getBareClusterStructureArray();
		int index = GAMath.randomInt(clusterArray.length);

		Logger.detail("Select bare cluster core from clusterFiles.");
		Structure ligand = Input.getLigandStructure();
		Object[] cluster = ClusterFactory.structureToObjectArray(clusterArray[index]);
		Object[] ligatedCluster = ClusterFactory.randomlyLigate(ligand, cluster, Input.getNumLigands());
		return ligatedCluster;
	}

	private Object[] selectCandidateFromPool(NewPool pool) {
		List<String> lines = pool.getPoolDatLines();
		Object[] clusList = new Object[0];
		int stride = GAMath.arraySum(m_EleNums) + 2;
		int numIndividuals = lines.size() / stride;
		Random rand = new Random();
		int ranCluster = rand.nextInt(numIndividuals);
		int ranPoolPos = ranCluster * stride;
		for (int i = ranPoolPos + 2; i < ranPoolPos + stride; i++) {
			String[] atom = lines.get(i).split("\\s+");
			String ele = atom[0];
			double x = Double.parseDouble(atom[1]);
			double y = Double.parseDouble(atom[2]);
			double z = Double.parseDouble(atom[3]);
			clusList = ArrayUtils.appendElement(clusList, new Object[] { ele, x, y, z });
		}
		return clusList;
	}

	private Object[] mate(Object[][] coords, NewPool pool) {
		//Return offspring based on crosstype
		String crossType = this.selectRandom(Input.getCrossType());
		if (m_EleNames.length == 1) {
			// Monometallic
			if (crossType.equalsIgnoreCase("Random")) {
				return this.monoRandom(coords);
			} else if (crossType.equalsIgnoreCase("Even")) {
				return this.monoEven(coords);
			} else {
				return this.monoWeighted(coords, pool);
			}
		} else {
			// Bimetallic
			if (crossType.equalsIgnoreCase("Random")) {
				return this.biRandom(coords);
			} else if (crossType.equalsIgnoreCase("Even")) {
				return this.biEven(coords);
			} else {
				// add bi weighted
				return this.biWeighted(coords);
			}
		}
	}

	private Object[] monoEven(Object[][] coords) {
		Object[] clus1 = coords[0];
		Object[] clus2 = coords[1];

		int start = (int) clus1.length / 2;

		Object[] clusList = new Object[m_NumAtoms];

		for (int i = 0; i < start; i++) {
			clusList[i] = clus1[i];
		}
		for (int i = start; i < m_NumAtoms; i++) {
			clusList[i] = clus2[i];
		}

		return clusList;

	}

	private Object[] biWeighted(Object[][] coords) {
		return new Object[m_NumAtoms];
	}

	private Object[] biEven(Object[][] coords) {
		Object[] clus1 = coords[0];
		Object[] clus2 = coords[1];

		Object[] clusList = new Object[m_NumAtoms];

		int start = (int) clus1.length / 2;

		boolean compositionWrong = true;

		while (compositionWrong) {

			for (int i = 0; i < start; i++) {
				clusList[i] = clus1[i];
			}
			for (int i = start; i < m_NumAtoms; i++) {
				clusList[i] = clus2[i];
			}

			// Check diff in composition

			for (int ele = 0; ele < m_EleNames.length; ele++) {
				String elementSymbol = m_EleNames[ele];
				int eleCount = 0;
				for (int atom = 0; atom < clusList.length; atom++) {
					String atomElement = (String) ((Object[]) clusList[atom])[0];
					if (atomElement.equals(elementSymbol)) {
						eleCount++;
					}
				}
				int diff = m_EleNums[ele] - eleCount;
				if (diff < 0) { // Remove extraneous atoms
					int numberOfAtomsToCorrectComposition = Math.abs(diff);
					for (int i = 0; i < numberOfAtomsToCorrectComposition; i++) {
						for (int atom = clusList.length - 1; atom >= 0; atom--) {
							String atomElement = (String) ((Object[]) clusList[atom])[0];
							if (atomElement.equals(elementSymbol)) {
								clusList = ArrayUtils.removeElement(clusList, atom);
								break;
							}
						}
					}
				} else { // Add atoms from either parent cluster
					Object[] parentClusterCoords = coords[new Random().nextInt(coords.length)];
					int numberOfAtomsToCorrectComposition = diff;
					for (int i = 0; i < numberOfAtomsToCorrectComposition; i++) {
						for (int atom1 = 0; atom1 < parentClusterCoords.length; atom1++) {
							if (ArrayUtils.arrayContains(clusList, parentClusterCoords[atom1])) {
								continue;
							}
							String atomElement = (String) ((Object[]) parentClusterCoords[atom1])[0];
							if (atomElement.equals(elementSymbol)) {
								boolean tooClose = false;
								for (int atom2 = 0; atom2 < clusList.length; atom2++) {
									double r = GAUtils.distance((Object[]) parentClusterCoords[atom1], 
											                    (Object[]) clusList[atom2]);
									if (r < 0.9) {
										tooClose = true;
										break;
									}
								}
								if (tooClose) {
									continue;
								} else {
									clusList = ArrayUtils.appendElement(clusList, parentClusterCoords[atom1]);
									break;
								}
							}
						}
					}
				}
			}

			int[] eleCounts = new int[m_EleNames.length];

			for (int ele = 0; ele < m_EleNames.length; ele++) {
				String elementSymbol = m_EleNames[ele];
				int eleCount = 0;
				for (int atom = 0; atom < clusList.length; atom++) {
					String atomElement = (String) ((Object[]) clusList[atom])[0];
					if (atomElement.equals(elementSymbol)) {
						eleCount++;
					}
				}
				eleCounts[ele] = eleCount;
			}

			for (int i = 0; i < eleCounts.length; i++) {
				if (eleCounts[i] != m_EleNums[i]) {
					compositionWrong = true;
					clus1 = ClusterFactory.rotate(clus1, GAMath.getRandomRotationMatrix());
					clus2 = ClusterFactory.rotate(clus2, GAMath.getRandomRotationMatrix());
					clusList = new Object[m_NumAtoms];
					break;
				} else {
					compositionWrong = false;
				}
			}
		}

//		for(int i = 0; i < clusList.length; i++){
//			Object[] a = (Object[]) clusList[i];
//			for(int j = 1; j < a.length; j++){
//				double v = (double) a[j];
//				if(Double.isNaN(v)){
//					System.out.println("What");
//				}
//			}
//		}
		return clusList;
	}



	private Object[] biRandom(Object[][] coords) {

		boolean compositionWrong = true;

		Object[] clusList = null;

		while (compositionWrong) {
			clusList = new Object[m_NumAtoms];

			Object[] clus1 = coords[0];
			Object[] clus2 = coords[1];

			int start = GAMath.randomInt(1, m_NumAtoms + 1); // [1, m_NumAtoms]

			for (int i = 0; i < start; i++) {
				clusList[i] = clus1[i];
			}
			for (int i = start; i < m_NumAtoms; i++) {
				clusList[i] = clus2[i];
			}

			int[] checkEle = new int[m_EleNums.length];

			for (int ele = 0; ele < m_EleNames.length; ele++) {
				int eleCount = 0;
				for (int atom = 0; atom < clusList.length; atom++) {
					if (((Object[]) clusList[atom])[0].equals(m_EleNames[ele])) {
						eleCount++;
					}
					checkEle[ele] = eleCount;
				}
			}
			for (int i = 0; i < checkEle.length; i++) {
				if (checkEle[i] == m_EleNums[i]) {
					compositionWrong = false;
				} else {
					compositionWrong = true;
				}
			}
		}
		return clusList;
	}

	private Object[] monoWeighted(Object[][] coords, NewPool pool) {

		Object[] clus1 = coords[0];
		Object[] clus2 = coords[1];

		double[] fitPair = this.getFitnessPair(pool);
		double fit1 = fitPair[0];
		double fit2 = fitPair[1];
		int start = (int) (m_NumAtoms * (fit1 / (fit1 + fit2)));

		Object[] clusList = new Object[m_NumAtoms];

		for (int i = 0; i < start; i++) {
			clusList[i] = clus1[i];
		}
		for (int i = start; i < m_NumAtoms; i++) {
			clusList[i] = clus2[i];
		}

		return clusList;
	}

	private double[] getFitnessPair(NewPool pool) {
		double[] fitness = pool.fitnesses(pool.getEnergies());
		int[] poolCandidates = new int[(int) pool.getSize()];
		for (int c = 0; c < pool.getSize(); c++) {
				poolCandidates[c] = pool.getPool().get(c).getCandidateIndex();
		}
		int parent1 = ArrayUtils.findIndex(poolCandidates, m_Parents[0]);
		int parent2 = ArrayUtils.findIndex(poolCandidates, m_Parents[1]);
		
		return new double[] { fitness[parent1], fitness[parent2] };
	}

	private Object[] monoRandom(Object[][] coords) {

		Object[] clus1 = coords[0];
		Object[] clus2 = coords[1];

		int start = GAMath.randomInt(1, m_NumAtoms + 1); // [1, m_NumAtoms]

		Object[] clusList = new Object[m_NumAtoms];

		for (int i = 0; i < start; i++) {
			clusList[i] = clus1[i];
		}
		for (int i = start; i < m_NumAtoms; i++) {
			clusList[i] = clus2[i];
		}

		return clusList;
	}

	private String selectRandom(String[] a) {
		Random rand = new Random();
		return a[rand.nextInt(a.length)];
	}

	/**
	 * Note that returned object[][] are newly created clusLists. They don't share reference to
	 * these of clusters in pool, and can be safely changed without any side effects.
	 * 
	 * TODO: Deep copy clusList from pool. Reduce IO.
	 * @param pool
	 * @return a pair of clusters stored in object[0][] and object[1][];
	 */
	private Object[][] findPair(NewPool pool) {
		int[] pair = this.select("Roulette", pool);
		List<String> poolList = pool.getPoolDatLines();
		int stride = Input.getStride();

		int c1 = pair[0] * stride;
		int c2 = pair[1] * stride;
		m_Parents[0] = Cluster.calcDirToIndex(HPC.fields(poolList.get(c1 + 1), "=", -1));
		m_Parents[1] = Cluster.calcDirToIndex(HPC.fields(poolList.get(c2 + 1), "=", -1));

		Object[] clus1 = this.convert(c1 + 2, c1 + stride, poolList);
		Object[] clus2 = this.convert(c2 + 2, c2 + stride, poolList);

		return new Object[][] { clus1, clus2 };
	}

	

	private Object[][] prepare(Object[][] coords) {

		// According to Deaven and Ho cut and splice method, for each cluster:
		// 1 - Rotate
		// 2 - Center two structures
		// 3 - Sort by Z (for cut plane)

		for (int i = 0; i < coords.length; i++) {
			coords[i] = ClusterFactory.rotate(coords[i], GAMath.getRandomRotationMatrix());
			ClusterFactory.moveCGToOrigin(coords[i]);
			coords[i] = ClusterFactory.sortByZ(coords[i]);
		}
		return coords;
	}

	private Object[] convert(int start, int end, List<String> p) {
		Object[] coords = new Object[end - start];
		int adder = 0;
		for (int i = start; i < end; i++) {
			String[] split = p.get(i).split("\\s+");
			String ele = split[0];
			double x = Double.parseDouble(split[1]);
			double y = Double.parseDouble(split[2]);
			double z = Double.parseDouble(split[3]);
			if (Double.isNaN(x) || Double.isNaN(y) || Double.isNaN(z)) {
				Logger.error("Cluster.convert() has NaN arguments: " + x + " " + y + " " + z);
			}
			coords[adder] = new Object[] { ele, x, y, z };
			adder++;
		}
		return coords;
	}

	private int[] select(String type, NewPool pool) {
		int[] pair = new int[2];
		if (type.equalsIgnoreCase("Roulette")) {
			double[] selectabilities = pool.selectabilities(false);
			Arrays.fill(pair, -1);
			int index = 0;
			while (ArrayUtils.arrayContains(pair, -1)) {

				Random rand = new Random();
				int[] map = ArrayUtils.getSortPermutation(selectabilities);
				int ranPos = map[rand.nextInt(selectabilities.length)]; // Number of individuals in pool should be same
																		// as num indices in array.
				double ranFit = rand.nextDouble();

				if (ranFit < selectabilities[ranPos] && !ArrayUtils.arrayContains(pair, ranPos)) {
					pair[index] = ranPos;
					index++;
				}
			}

		}

		return pair;
	}

	private double[] getFitness(double[] energies, double[] copy) {
		double energyRange = energies[energies.length - 1] - energies[0];
		double[] fitness = new double[energies.length];
		for (int i = 0; i < copy.length; i++) {
			double f = 0.5 * (1 - Math.tanh(2. * ((copy[i] - energies[0]) / energyRange) - 1.));
			fitness[i] = f;
		}
		return fitness;
	}

	private void writeSurfacePoscar() {

		Surface surface = m_SupportedCluster.getSurface();
		String[] eleNames = m_SupportedCluster.getEleNames();
		String[] eleNums = m_SupportedCluster.getEleNums();
		Structure surfaceStructure = surface.getStructure();
		Vector[] cellVectors = surfaceStructure.getCellVectors();
		String[] vectors = toStringVector(cellVectors);

		List<String> poscar = new ArrayList<>();

		poscar.add(m_CalcDir);
		poscar.add("1.0");

		poscar.add(vectors[0]);
		poscar.add(vectors[1]);
		poscar.add(vectors[2]);

		poscar.add(String.join(" ", Arrays.toString(eleNames).replace("[", "").replace("]", "").replace(", ", " ")));
		poscar.add(String.join(" ", Arrays.toString(eleNums).replace("[", "").replace("]", "").replace(", ", " ")));

		poscar.add("S");
		poscar.add("C");

		for (int ele = 0; ele < eleNames.length; ele++) {
			for (int i = 0; i < m_ClusList.length; i++) {
				Object[] atom = (Object[]) m_ClusList[i];
				String element = (String) atom[0];
				double x = (double) atom[1];
				double y = (double) atom[2];
				double z = (double) atom[3];

				if (element.equals(eleNames[ele])) {
					String line = x + " " + y + " " + z;
					String selectiveDynamics;
					if (ArrayUtils.arrayContains(m_EleNames, element)) {
						selectiveDynamics = " T T T";
					} else {
						selectiveDynamics = " F F F";
					}
					String atomLine = line + selectiveDynamics;
					poscar.add(atomLine);
				}
			}
		}
		HPC.write(poscar, m_CalcDir + "POSCAR", false);

	}

	private String[] toStringVector(Vector[] cellVectors) {
		String[] returnArray = new String[cellVectors.length];
		for (int i = 0; i < cellVectors.length; i++) {
			double[] array = cellVectors[i].getCartesianDirection();
			String results = Arrays.toString(array).replace("[", "").replace("]", "").replace(", ", " ");
			returnArray[i] = results;
		}
		return returnArray;
	}

	public int[] buildLookup(Object[] clusList, String[] eleNames) {
		int sortedIndex = 0;
		Object[] atom; String atomType;
		
		int[] POSCARLookup = new int[clusList.length];
		for (int elementType = 0; elementType < eleNames.length; elementType++) {
			String currElement = eleNames[elementType];
			for (int line = 0; line < clusList.length; line++) {
				atom = (Object[]) clusList[line];
				atomType = (String) atom[0];
				if (atomType.equalsIgnoreCase(currElement)) {
					POSCARLookup[sortedIndex++] = line;
				}
			}
		}
		return POSCARLookup;
	}
	
	
	/*
	private void restart(boolean retrain) {
		Logger.basic("Calc failed. Restarting calculation");

		if (m_Restart) {
			m_Status = "RestartNotFinished";
			this.writeStatus();
		} else {
			m_Status = "NotFinished";
			this.writeStatus();
		}
		m_ClusList = this.generateCluster(m_NumAtoms, m_Rij, m_EleNames, m_EleNums);
		// It's important to re-calculate the cell size.
		m_CellSize = this.getCellSize(m_ClusList, m_EleNames); // Note: cluster has been moved to the corner.
		m_Structure = this.convertToStructure();
		m_Minimizer.createInputs(true); // Move cluster to the center.

		this.minimize(retrain);
	}
	*/
	
	/**
	 * Perform structural relaxation by m_Minizer. Structure is updated and ground-state energy
	 * is read.
	 * 
	 * If m_Relaxed is already set true, skip relaxation but still update structure and energy.
	 * 
	 * Side effects: m_Structure, m_Relaxed, m_Status, m_Energy, m_Collapsed and m_Exploded 
	 * are updated.
	 * 
	 * @return true if executable doesn't stop with a non-zero code and 
	 *              there is no m_Minimizer.ErrorStr in output.
	 *         false otherwise.
	 */
	public boolean minimize() {
		int attempt = 0, exitCode = 1;
		boolean isSuccessful = false;
		// If calculation is copied from training folder to calcDir in active learning mode,
		// m_Relaxed should have been set to true.
		if (m_Relaxed) {
			isSuccessful = true;
			exitCode = 0;
		}
		while (!isSuccessful || exitCode != 0) {
			attempt++;
			if (attempt > 3) {
				Logger.error("Failed to complete relaxation in three attempts!");
				m_Status = "Failed";
				return false;
			}
			if (attempt > 1) {
				m_Minimizer.attemptToFix(attempt);
				Logger.basic("Calculation failed! Attempt the " + attempt + " time. Submitting calculation.");
			}
			Logger.basic("Submitting relaxation calculation.");
			exitCode = m_Minimizer.relax();
			Logger.basic("Calculation finished. Relaxation exit code: " + exitCode);
			List<String> newLines = Arrays.asList("Thread-" + Thread.currentThread().getName() + ": " + 
					m_CalcDir + " Exitcode =  " + exitCode);
			HPC.write(newLines, Start.WorkDir + HPC.FILESPTR + "exitcodes.dat", true);
			
			Logger.detail(m_Minimizer.getName() + " done. Checking for errors.");
			isSuccessful = m_Minimizer.isCalSuccessful();
		}
		
		m_Structure = m_Minimizer.readStructure(true);
		m_ClusList = ClusterFactory.structureToObjectArray(m_Structure);
		if (m_Minimizer.getName().equalsIgnoreCase("VASP")) {
			m_ClusList = this.reorganizeFromPOSCAR();
			m_Structure = ClusterFactory.objectToStructure(m_ClusList, m_CellSize);
		}
		m_Bonds = this.getBonds(); // TODO: useless;
		m_Exploded = ClusterFactory.checkExplosionByContiguous(m_ClusList);
		m_Collapsed = ClusterFactory.checkCollapse(m_ClusList);
		m_Energy = m_Minimizer.readEnergy();
		m_Relaxed = true;
		m_Status = "Finished Energy = " + m_Energy + " eV";
		if (m_isSurfaceCalc) {
			m_SupportedCluster.set(m_ClusList);
		}
			
		if (m_Exploded) {
			// Failed the bond length check. Some atoms are flying away from all the other atoms.
			Logger.error("Certain atoms are too far away!");
			Logger.debug("Bonds = " + HPC.LINESPTR + GAIOUtils.toString(m_Bonds));
			HPC.write(this.toStringList(), Start.WorkDir + HPC.FILESPTR + "exploded.dat", true);
		}
		
		if (m_Collapsed) {
			Logger.error("Relaxed structure collapsed after relaxation!");
		}
		if (!m_Collapsed && !m_Exploded) {
			Logger.basic("Calculation succeeded.");
		} else {
			Logger.basic("Calculation succeeded, but relaxed to unrealistic configuration!");
		}
		return true;
	}

	public void writeChosen() {
		Logger.detail("Writing parent cluster data in chosen.dat");
		FileLock lock = HPC.fileLocks.get("chosen.dat");
		lock.writeLock();
		
		// Lines start with m_CalcDir.
		List<Integer> lineNumbers = HPC.grepLineNumbers("^" + m_CalcDir + "\\s+", CHOSEN);
		List<String> lines = HPC.read(CHOSEN);
		try {
			if (lineNumbers.size() <= 1) {
				if (lineNumbers.size() == 1) {
					// Incomplete write. Parents always exist in pairs. Line number starts from 1.
					lines.remove(lineNumbers.get(0) - 1);
				}
				lines.add(m_CalcDir + " = " + Candidates.ALLCANDIDATES + HPC.FILESPTR + m_Parents[0]);
				lines.add(m_CalcDir + " = " + Candidates.ALLCANDIDATES + HPC.FILESPTR + m_Parents[1]);
			} else {
				// Line numbers start from 1. Overwrite the first two entries.
				lines.set(lineNumbers.get(0) - 1, m_CalcDir + " = " + Candidates.ALLCANDIDATES + HPC.FILESPTR + m_Parents[0]);
				lines.set(lineNumbers.get(1) - 1, m_CalcDir + " = " + Candidates.ALLCANDIDATES + HPC.FILESPTR + m_Parents[1]);
				for (int i = 2; i < lineNumbers.size(); i++) {
					// Redundant writes. Will affect Pool.isConvergence() if not cleaned.
					lines.remove(lineNumbers.get(i) - 1 - (i - 2));
				}
			}
			HPC.write(lines, CHOSEN, false);
		} catch (IndexOutOfBoundsException e) {
			Logger.error("Parent indices = " + m_Parents[0] + " and " + m_Parents[1]);
			e.printStackTrace();
			System.exit(1);
		} finally {
			lock.writeUnLock();
		}
	}

	/**
	 * An atomic function to write structure with specified status to candidates.dat. 
	 * Note: when a calculation is finished, the relaxed structure is the one written to 
	 * candidates.dat.
	 * 
	 */
	public void writeToCandidates() {
		//Logger.detail("Updating status for cluster-" + m_CandidateIndex + ".");
		Logger.debug("Status line number for cluster-" + m_CandidateIndex + " is " + (m_StatusLine + 1));
		List<String> lines = this.toStringList();
		HPC.replace(m_StatusLine, lines, Candidates.CANDIDATES);
	}
		
	// This method reads POSCAR file and returns it in a Structure format
	public static Structure stringToStructure(String name) {
		POSCAR file = new POSCAR(name);
		Structure ligand = new Structure(file);
		return ligand;
	}

	public Object[] reorganizeFromPOSCAR() {
		Object[] organizedClusList = new Object[m_ClusList.length];
		for (int index = 0; index < m_ClusList.length; index++) {
			Object POSCARLine = m_ClusList[index];
			organizedClusList[(int) m_POSCARLookup[index]] = POSCARLine;
		}
		return organizedClusList;
	}

	private static double[][] toMatrix(Object[] clusList) {
		double[][] returnArray = new double[clusList.length][];
		for (int i = 0; i < clusList.length; i++) {
			Object[] atom = (Object[]) clusList[i];
			double x = (double) atom[1];
			double y = (double) atom[2];
			double z = (double) atom[3];
			double[] cartArray = new double[] { x, y, z };
			returnArray[i] = cartArray;
		}
		return returnArray;
	}

	/**
	 * Calculate distance between each pair of atoms in the cluster. Atoms in
	 * ligands are treated the same way as metal atoms.
	 * 
	 * @return A 2D array representing distances between each pair of atoms. It
	 *         should be a symmetry square matrix.
	 */
	private double[][] getBonds() {
		Structure clusterStructure = m_Structure;
		if (m_isSurfaceCalc) {
			clusterStructure = m_SupportedCluster.getClusterStructure(m_EleNames);
		}
		return GAUtils.getBonds(clusterStructure);
	}


	
	/*
	private String findCalcNum() {
		int num = HPC.findLastDir() + 1;
		return Cluster.getCandidateDir() + HPC.FILESPTR + num;
	}
	*/

	/*
	private int findStatusLine() {
		List<String> cands = HPC.read(Candidates.CANDIDATES);
		return cands.size();
//		String[] split = m_CalcNum.split("/");
//		return (Integer.parseInt(split[split.length - 1]) - 1) * (m_NumAtoms + 2);
	}
	*/

	/**
	 * Rebuild cluster from the given atomic coordinates.
	 * 
	 * @param cellSize
	 * @param c        List of atomic coordinates in the format of: "Element_Symbol
	 *                 x y z"
	 * @return
	 */
	private static Structure buildStructure(double cellSize, String[] c) {
		StructureBuilder sb = new StructureBuilder();
		Coordinates[] coords = new Coordinates[c.length];
		Species[] species = new Species[c.length];
		for (int i = 0; i < coords.length; i++) {
			String[] split = c[i].split("\\s+");
			species[i] = Species.get(split[0]);
			double[] xyz = new double[] { Double.parseDouble(split[1]), 
					                      Double.parseDouble(split[2]),
					                      Double.parseDouble(split[3]) };
			coords[i] = new Coordinates(xyz, CartesianBasis.getInstance());
		}
		double[][] abc = new double[][] { { cellSize, 0, 0 }, { 0, cellSize, 0 }, { 0, 0, cellSize }, };
		Vector[] cellVectors = new Vector[] { new Vector(abc[0]), new Vector(abc[1]), new Vector(abc[2]) };
		sb.addSites(coords, species);
		sb.setCellVectors(cellVectors);
		return new Structure(sb);
	}
	
	/*
	public void relax(boolean retrain) {
		m_Restart = true;
		File contcar = new File(m_CalcDir + "CONTCAR");
		Logger.basic("Relaxing " + m_CalcDir);
		if (contcar.exists() && contcar.length() > 0D) {
			HPC.copy(m_CalcDir + HPC.FILESPTR + "CONTCAR", m_CalcDir + HPC.FILESPTR + "POSCAR", false);
		}
		m_Status = "RestartRunning";
		this.writeStatus();
		Logger.basic("Relaxing unrelaxed restart structure");
		HPC.unlock("db", true);
		this.minimize();
		Logger.basic("Exiting relaxation of " + m_CalcDir);
	}
	*/
	
	public Cluster removeClusterFromSurface() {
		SupportedCluster supportedCluster = m_SupportedCluster;
		Structure clusterStructure = supportedCluster.getClusterStructure(m_EleNames);
		return new Cluster(m_CellSize, m_ClusterType, m_CalcDir, m_NumAtoms, m_Energy, m_StatusLine,
				clusterStructure, m_Status);
	}

	public boolean getIsSurfaceCalc() {
		return m_isSurfaceCalc;
	}

	public SupportedCluster getSupportedCluster() {
		return m_SupportedCluster;
	}
	
	//*****************Newly added functions for refactoring Peter's code*************************//
	
	/**
	 * 
	 * @return A list of Strings in the format of candidates.dat.
	 */
	public List<String> toStringList() {
		ArrayList<String> lines = new ArrayList<>(m_NumAtoms + 2);
		Object[] clusList = ArrayUtils.copyArray(m_ClusList);

		if (m_isSurfaceCalc) {
			SupportedCluster sc = m_SupportedCluster;
			Structure clusterStructure = sc.getClusterStructure(m_EleNames);
			clusList = ClusterFactory.structureToObjectArray(clusterStructure);
			m_CellSize = ClusterFactory.getCellSize(clusList, Input.getDistImage());
		}

		// Write the new cluster structure to String array.
		lines.add(Double.toString(m_CellSize));
		lines.add(m_ClusterType + " Dir = " + m_CalcDir + " " + m_Status);
		
		for (int i = 0; i < clusList.length; i++) {
			Object[] atom = (Object[]) clusList[i];
			String species = (String) atom[0];
			String coords = String.format(COORDINATEFORMAT, (double) atom[1], (double) atom[2], (double) atom[3]);
			String l = species + " " + coords;
			lines.add(l);
		}
		
		return lines;
	}
	
	public static List<String> toStringList(List<Cluster> clusters) {
		List<String> lines = new ArrayList<>(100);
		for (Cluster c: clusters) {
			lines.addAll(c.toStringList());
		}
		return lines;
	}
	
	/**
	 * Convert a calcDir to its corresponding candidate index.
	 * @param calcDir folder of a candidate cluster.
	 * @return "m_CandidateIndex"
	 */
	public static int calcDirToIndex(String calcDir){
		String[] split = calcDir.split(HPC.FILESPTR);
		return Integer.parseInt(split[split.length - 1]);
	}
	
	@Override
	/**
	 * To allow List.contains() to compare content instead of reference.
	 * Essential info: candidate index; site coordiates;
	 * 
	 * Note that once calculation finishes, the structure is updated to the relaxed one.
	 */
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else if (! (o instanceof Cluster)) {
			return false;
		} else if (o == this) {
			return true;
		}
		Cluster c = (Cluster) o;
		boolean equal = true;
		equal &= (this.getCalcDir().equals(c.getCalcDir()));
		// Energy might have numerical discrepancies; 
		// If read from file system, status might not be initiated.
		//equal &= (this.getStatus().equals(c.getStatus()));
		//if (this.getStatus() == "Finished") {
		//	equal &= (this.getEnergy() == c.getEnergy());
		//}
		
		Structure struct1 = this.getStructure();
		Structure struct2 = c.getStructure();
		equal &= (struct1.numDefiningSites() != struct2.numDefiningSites());
		
		for (int i = 0; i < struct1.numDefiningSites(); i++) {
			if (!equal) {
				break;
			}
			equal &= struct1.getSiteCoords(i).isCloseEnoughTo(struct2.getSiteCoords(i));
		}
		
		return equal;
	}
	
	/**
	 * Read cluster from candidate folder.
	 * 
	 * @param calcDir
	 * @return Relaxed/initial structure 
	 */
	public static Cluster getClusterFromCalcDir(String calcDir){
		Structure struct = null;
		String minimizer = null;
		double energy = 0.0;
		if (Files.exists(Paths.get(calcDir + HPC.FILESPTR + VASP.getStructureFileName(true)))) { // CONTCAR
			struct = VASP.readStructure(calcDir, VASP.getStructureFileName(true));
			minimizer = "VASP";
			energy = VASP.readEnergy(HPC.read(calcDir + HPC.FILESPTR + VASP.getOutputFileName()));
		} else if (Files.exists(Paths.get(calcDir + HPC.FILESPTR + LAMMPS.getStructureFileName(true)))) { // end.cfg
			struct = LAMMPS.readStructure(calcDir, LAMMPS.getStructureFileName(true));
			minimizer = "LAMMPS";
			energy = LAMMPS.readEnergy(HPC.read(calcDir + HPC.FILESPTR + LAMMPS.getOutputFileName()));
		} else if (Files.exists(Paths.get(calcDir + HPC.FILESPTR + VASP.getInputFileName()))){
			 // CONTCAR doesn't exist, but INCAR exists --> read POSCAR
			struct = VASP.readStructure(calcDir, VASP.getStructureFileName(false));
			minimizer = "VASP";
		} else if (!Files.exists(Paths.get(calcDir + HPC.FILESPTR + LAMMPS.getOutputFileName()))){
			 // log.lammps doesn't exist, but input.lmp exists --> read struct.lmp
			struct = VASP.readStructure(calcDir, LAMMPS.getStructureFileName(false));
			minimizer = "LAMMPS";
		}
		
		if (struct == null) {
			Logger.error("Cannot read cluster from" + calcDir);
			Logger.error("FATAL ERROR GETTING CLUSTER. EXITING");
			System.exit(1);
		}
		
		Cluster cluster = new Cluster(struct, calcDir);
		cluster.setEnergyMinimizer(minimizer);
		if (energy != 0) {
			cluster.setEnergy(energy);
			cluster.setStatus("Finished" + " Energy = " + energy + " eV");
			cluster.setRelaxed(true, false);
		}
		return cluster;
	}
}
