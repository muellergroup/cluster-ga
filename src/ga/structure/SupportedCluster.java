package ga.structure;
import ga.Input;
import matsci.Element;
import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.structure.Structure;
import matsci.structure.Structure.Site;
import matsci.structure.StructureBuilder;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class SupportedCluster {
	
	private String m_SurfaceType = Input.getSurfaceType();
	private Surface m_Surface = new Surface(m_SurfaceType);
	private Structure m_SupportedCluster = null;
	private Structure m_ClusterStructure = null;
	private String[] m_EleNames = null;
	private String[] m_EleNums = null;
	
	public Surface getSurface(){
		return m_Surface;
	}
	
	public Structure getSupportedClusterStructure(){
		return m_SupportedCluster;
	}
	
	public void set(Object[] clusList){
		m_SupportedCluster = listToOptimizedStructure(clusList);
	}
	
	private Structure listToOptimizedStructure(Object[] clusList) {
		Species[] species = new Species[clusList.length];
		Coordinates[] coords = new Coordinates[clusList.length];
		
		for(int i = 0; i < coords.length; i++){
			Object[] atom = (Object[]) clusList[i];
			String element = (String) atom[0];
			double[] xyz = new double[]{(double) atom[1], (double) atom[2], (double) atom[3]};
			coords[i] = new Coordinates(xyz, CartesianBasis.getInstance());
			species[i] = Species.get(element);
		}
		
		Vector[] cellVectors = m_SupportedCluster.getCellVectors();
		
		StructureBuilder sb = new StructureBuilder();
		sb.addSites(coords, species);
		sb.setCellVectors(cellVectors);
		return new Structure(sb);
	}

	public String[] getEleNames(){
		if(m_EleNames != null){return m_EleNames;}
		Structure supportedCluster = m_SupportedCluster;
		Element[] distinctElements = supportedCluster.getDistinctElements();
		String[] returnArray = new String[distinctElements.length - 1];
		int adder = 0;
		for(int i = 0; i < distinctElements.length; i++){
			if(distinctElements[i] == Element.vacancy){continue;} // don't write the vacancies
			returnArray[adder++] = distinctElements[i].getSymbol();
		}
		m_EleNames = returnArray;
		return returnArray;
	}
	
	public String[] getEleNums(){
		if(m_EleNums != null){return m_EleNums;}
		String[] returnArray = new String[m_EleNames.length];
		Structure structure = m_SupportedCluster;
		for(int i = 0; i < m_EleNames.length; i++){
			int numSpecies = structure.numDefiningSitesWithElement(Element.getElement(m_EleNames[i]));
			returnArray[i] = Integer.toString(numSpecies);
		}
		m_EleNums = returnArray;
		return returnArray;
	}
	
	public Object[] getSupportedClusterList(){
		Structure supportedCluster = m_SupportedCluster;
		Object[] returnArray = new Object[supportedCluster.numDefiningSites()];
		for(int atomNum = 0; atomNum < supportedCluster.numDefiningSites(); atomNum++){
			Site site = supportedCluster.getDefiningSite(atomNum);
			String element = site.getSpecies().getElementSymbol();
			double[] c = site.getCoords().getCartesianCoords().getCartesianArray();
			double x = c[0];
			double y = c[1];
			double z = c[2];
			Object[] atom = new Object[]{element, x, y, z};
			returnArray[atomNum] = atom;
		}
		return returnArray;
	}

	public SupportedCluster(Object[] clusList){
		Structure clusterStructure = listToStructure(clusList);
		if(m_SupportedCluster == null){
			m_SupportedCluster = m_Surface.placeCluster(clusterStructure);
		}
	}
	
	private double cellSize(Coordinates[] coords){
		double[][] cartesianArray = toCartesianMatrix(coords);
		double[][] transposedMatrix = MSMath.transpose(cartesianArray);
		double largestWidth = Double.NEGATIVE_INFINITY;
		for(int dimNum = 0; dimNum < transposedMatrix.length; dimNum++){
			double min = ArrayUtils.minElement(transposedMatrix[dimNum]);
			double max = ArrayUtils.maxElement(transposedMatrix[dimNum]);
			double width = max - min;
			if(width > largestWidth){
				largestWidth = width;
			}
		}
		return largestWidth + 10;
	}
	
	private static double[][] toCartesianMatrix(Coordinates[] coords){
		double[][] xyz = new double[coords.length][];
		for(int i = 0; i < xyz.length; i++){
			xyz[i] = coords[i].getCartesianArray();
		}
		return xyz;
	}

	private Structure listToStructure(Object[] clusList) {
		Species[] species = new Species[clusList.length];
		Coordinates[] coords = new Coordinates[clusList.length];
		
		for(int i = 0; i < coords.length; i++){
			Object[] atom = (Object[]) clusList[i];
			String element = (String) atom[0];
			double[] xyz = new double[]{(double) atom[1], (double) atom[2], (double) atom[3]};
			coords[i] = new Coordinates(xyz, CartesianBasis.getInstance());
			species[i] = Species.get(element);
		}
		
		double cellSize = cellSize(coords);
		
		double[][] abc = new double[][]{
			{cellSize, 0, 0},
			{0, cellSize, 0},
			{0, 0, cellSize}
		};
		
		Vector[] cellVectors = new Vector[]{
				new Vector(abc[0]),
				new Vector(abc[1]),
				new Vector(abc[2]),
		};
		
		StructureBuilder sb = new StructureBuilder();
		sb.addSites(coords, species);
		sb.setCellVectors(cellVectors);
		return new Structure(sb);
	}

	public Structure getClusterStructure(String[] eleNames) {
		if(m_ClusterStructure != null){return m_ClusterStructure;}
		Structure supportedCluster = m_SupportedCluster;
		StructureBuilder sb = new StructureBuilder(supportedCluster);
		for(int i = supportedCluster.numDefiningSites() - 1; i >= 0; i--){
			String element = supportedCluster.getSiteSpecies(i).getElementSymbol();
			if(ArrayUtils.arrayContains(eleNames, element)){
				continue;
			}
			else{
				sb.removeSite(i);
			}
		}
		m_ClusterStructure = new Structure(sb);
		
		return m_ClusterStructure;
	}
}
