package ga;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import ga.multithreaded.Candidates;
import ga.structure.Cluster;
import ga.utils.GAUtils;
import ga.io.Logger;
import matsci.util.arrays.ArrayUtils; 

/**
 * To make it thread-safe, Tables should always be used within synchronized block of m_Candidates.
 * 
 */
public class Tables {
	
	//private static List<String> similarityTableLines;
	//private static List<String> scoreTableLines;
	// "true", "false" or "none" (if calculation hasn't finished yet)
	private static ArrayList<ArrayList<String>> similarityTable = new ArrayList<ArrayList<String>>();
	// "-1" (if calculation hasn't finished yet) or other positive number.
	private static ArrayList<ArrayList<Double>> scoreTable = new ArrayList<ArrayList<Double>>();
	private static SimilarityCalculator2 m_SimilarityCalculator = new SimilarityCalculator2(false, false, GAUtils.getNNDistanceMap());
	private static List<Cluster> m_Candidates;
	private static boolean m_isLigatedCluster = Input.isLigatedCluster();
	private static final String SCOREFORMAT = " %-6.4f"; 	// left-justified
	private static final String SIMILARITYFORMAT = "%-6s";	// left-justified
	private static final double SIMILARITYTHRESHOLD = Input.getSimilarityThreshold();
	
	public static void setCandiates(List<Cluster> candidates) {
		m_Candidates = candidates;
	}
	
	/**
	 * Read the similarity_table.dat and score_table.dat into Class variable.
	 * @param checkTableParity Whether check correctness of the two tables. i.e. If some
	 *                         clusters are not recorded in the two tables, add them.
	 */
	public synchronized static void read(boolean checkTableParity, boolean restart) {
		if (restart) {
			Logger.basic("Recovering tables from score_table.dat and similarity_table.dat ...");
		}
		List<String> similarityLines = HPC.read(Start.WorkDir + HPC.FILESPTR + "similarity_table.dat");
		List<String> scoreLines = HPC.read(Start.WorkDir + HPC.FILESPTR + "score_table.dat");
		if (similarityLines.size() != scoreLines.size()) {
			Logger.error("Tables don't have equal size!");
			System.exit(5);
		}
		
		// Parse the score table;
		scoreTable = new ArrayList<ArrayList<Double>>(m_Candidates.size());
		for (int i = 0; i < m_Candidates.size(); i++) {
			ArrayList<Double> scores = new ArrayList<Double>(i);
			// have a line and it's not empty;
			if (scoreLines.size() > i && scoreLines.get(i).length() > 0) {
				String[] terms = scoreLines.get(i).trim().split("\\s+");
				// First term is candidate index.
				for (int j = 1; j < terms.length; j++) {
					scores.add(Double.parseDouble(terms[j]));
				}
			}
			
			// Not having a line for the corresponding candidate, or having an empty line, or
			// having an incomplete line. Either way, we should repair this line;
			if (scores.size() < i) { // scores.size() == i is right one.
				for (int j = scores.size(); j < i; j++) {
					scores.add(repairOneScore(m_Candidates.get(i), m_Candidates.get(j)));
				}
			} else if (scores.size() > i) {
				// some lines are missing in the middle. Therefore, line i of score_table.dat is
				// actually for a later candidate. Add a new line for the current cluster.
				scores = new ArrayList<Double>(i);
				for (int j = 0; j < i; j++) {
					scores.add(repairOneScore(m_Candidates.get(i), m_Candidates.get(j)));
				}
				// Insert an empty line to as a place holder. Make the current i-th line the i+1-th
				// line until scores.size() == i.
				scoreLines.add(i,"");
			}
			scoreTable.add(i, scores);
		}
		
		// Parse the similarity table;
		similarityTable = new ArrayList<ArrayList<String>>(m_Candidates.size());
		for (int i = 0; i < m_Candidates.size(); i++) {
			ArrayList<String> similarities = new ArrayList<String>(i);
			
			if (similarityLines.size() > i && similarityLines.get(i).length() > 0) {
				String[] terms = similarityLines.get(i).trim().split("\\s+");
				// First term is candidate index.
				for (int j = 1; j < terms.length; j++) {
					similarities.add(terms[j]);
				}
			}
			
			if (similarities.size() < i) {
				for (int j = similarities.size(); j < i; j++) {
					double score = scoreTable.get(i).get(j);
					similarities.add(Tables.getSimilarity(score));
				}
			} else if (similarities.size() > i) {
				similarities = new ArrayList<String>(i);
				for (int j = 0; j < i; j++) {
					double score = scoreTable.get(i).get(j);
					similarities.add(Tables.getSimilarity(score));
				}
				similarityLines.add(i, "");
			}
			similarityTable.add(similarities);
		}
		
		// TODO: the following lines might be redundant.
		String[][] finishedCalcNums = findDiscrepancyFinishedCalcNums();
		String[][] unfinishedCalcNums = findDiscrepancyUnfinishedCalcNums();
		if (checkTableParity) {
			check(unfinishedCalcNums, finishedCalcNums);
		}
	}
	
	/**
	 * Decide one score in scoreTable for a pair of clusters. It considers calculation mode and
	 * status of clusters. Extract from read() to avoid code repetition.
	 * @param c1
	 * @param c2
	 * @return
	 */
	private static double repairOneScore(Cluster c1, Cluster c2) {
		if (!c1.relaxed() || !c2.relaxed()) {
			return -1.0;
		} else if (Input.getRetrainMode() == Input.RetrainMode.DOptimality) {
			// Been added to training list because of being extrapolating or unrealistic
			if ((!c1.relaxedByVASP() && (c1.extrapolating() || !c1.realistic())) 
			 || (!c2.relaxedByVASP() && (c2.extrapolating() || !c2.realistic()))) {
				return -1.0;
			}
		}
		
		if (m_isLigatedCluster) {
			return m_SimilarityCalculator.getScore(c1.getStructureWithoutSideChains(), 
                    							   c2.getStructureWithoutSideChains());
		} else {
			return m_SimilarityCalculator.getScore(c1.getStructure(), c2.getStructure());
		}
	}
	
	/**
	 * @return An array of paths to clusters that are not recorded in similarity table or 
	 *         score table.
	 */
	private static String[][] findDiscrepancyFinishedCalcNums(){
		String[][] returnArray = new String[2][];
		String[] booleanArray = new String[0];
		String[] scoreArray = new String[0];
		//List<String> finishedCalcNums = Engine.getCandidateCrawler().getFinishedCandidates();
		//for(int i = 0; i < finishedCalcNums.size(); i++){
		//	String calcNum = finishedCalcNums.get(i);
		//	int index = Cluster.calcDirToIndex(calcNum) - 1;
		for (int i = 0; i < m_Candidates.size(); i++) {
			int index = m_Candidates.get(i).getCandidateIndex();
			String calcNum = m_Candidates.get(i).getCalcDir();
			try {
				similarityTable.get(index - 1);
			} catch (IndexOutOfBoundsException e) {
				booleanArray = (String[]) ArrayUtils.appendElement(booleanArray, calcNum);
			}
			try {
				//scoreTableLines.get(index);
				scoreTable.get(index - 1);
			} catch (IndexOutOfBoundsException e) {
				scoreArray = (String[]) ArrayUtils.appendElement(scoreArray, calcNum);
			}
		}
		returnArray[0] = booleanArray;
		returnArray[1] = scoreArray;
		return returnArray;
	}
	
	/**
	 * @return List of paths of clusters of which relaxation hasn't yet finished.
	 */
	private static List<String> getUnfinishedCandidates(){
		List<String> candidates = HPC.read(Candidates.CANDIDATES);
		List<String> returnArray = new ArrayList<>();
		for(int i = 0; i < candidates.size(); i++){
			String line = candidates.get(i);
			if(line.contains(" RestartRunning") ||
					line.contains(" RestartNotFinished") ||
					line.contains(" NotFinished") ||
					line.contains(" Running")){
				String calcNum = candidates.get(i).split("\\s+")[3];
				returnArray.add(calcNum);
			}
		}
		return returnArray;
	}
	
	private static String[][] findDiscrepancyUnfinishedCalcNums(){
		String[][] returnArray = new String[2][];
		String[] booleanArray = new String[0];
		String[] scoreArray = new String[0];
		List<String> unfinishedCalcNums = getUnfinishedCandidates();
		for(int i = 0; i < unfinishedCalcNums.size(); i++){
			String calcNum = unfinishedCalcNums.get(i);
			int index = Cluster.calcDirToIndex(calcNum);
			try {
				similarityTable.get(index - 1);  // Assume candidates are in order of their indices.
			} catch (IndexOutOfBoundsException e) {
				booleanArray = (String[]) ArrayUtils.appendElement(booleanArray, calcNum);
			}
			try {
				//scoreTableLines.get(index);
				scoreTable.get(index - 1);
			} catch (IndexOutOfBoundsException e) {
				scoreArray = (String[]) ArrayUtils.appendElement(scoreArray, calcNum);
			}
		}
		returnArray[0] = booleanArray;
		returnArray[1] = scoreArray;
		return returnArray;
	}
	
	/**
	 * Check whether the similarity_table.dat and score_table.dat correctly recorded all 
	 * candidate clusters. If not, add them in and update the tables.
	 * 
	 * @param unfinishedCalcNums
	 * @param finishedCalcNums
	 */
	private static void check(String[][] unfinishedCalcNums, String[][] finishedCalcNums){
		Logger.detail("Checking discrepancy between score and boolean tables.");
		if (finishedCalcNums.length == 0) {
			return;
		} // No candidates directory exists

		for (int type = 0; type < finishedCalcNums.length; type++) { // boolean / score
			for (int i = 0; i < finishedCalcNums[type].length; i++) {
				String calcDir = finishedCalcNums[type][i];
				int index = Cluster.calcDirToIndex(calcDir);
				try {
					if (type == 0) {
						similarityTable.get(index - 1);
					} else {
						//scoreTableLines.get(index);
						scoreTable.get(index - 1);
					}
				} catch (IndexOutOfBoundsException e) {
					Cluster c = Cluster.getClusterFromCalcDir(calcDir); // recover a cluster.
					//Logger.detail("Adding finished cluster to table");
					//addNewLine(index);
					Logger.detail("Finished candidate-" + index + " is missing in tables!");
					Logger.detail("Updating finished cluster to table");
					newUpdate(c); // Repair the tables.
					// Recursive until all lines have been repaired.
					check(unfinishedCalcNums, finishedCalcNums);
				}
			}
		}
		for (int type = 0; type < unfinishedCalcNums.length; type++) {
			for (int i = 0; i < unfinishedCalcNums[type].length; i++) {
				String calcNum = unfinishedCalcNums[type][i];
				int index = Cluster.calcDirToIndex(calcNum);
				try {
					if (type == 0) {
						similarityTable.get(index - 1);
					} else {
						//scoreTableLines.get(index);
						scoreTable.get(index - 1);
					}
				} catch (IndexOutOfBoundsException e) {
					Logger.detail("Unfinished candidate-" + index + " is missing in tables!");
					Logger.detail("Adding unfinished cluster to table");
					addNewLine(index - 1);
					check(unfinishedCalcNums, finishedCalcNums);
				}
			}
		}
	}
	
	public static void write(){
		List<String> similarityLines = new ArrayList<String>(similarityTable.size());
		for (int i = 0; i < similarityTable.size(); i++) {
			StringBuilder sb = new StringBuilder(String.format("%-5d", i + 1)); // Candidate index could be up to 10000.
			for (String s: similarityTable.get(i)) {
				sb.append(String.format(SIMILARITYFORMAT, s));
			}
			similarityLines.add(sb.toString());
		}
		HPC.write(similarityLines, Start.WorkDir + HPC.FILESPTR + "similarity_table.dat", false);
		
		List<String> scoreLines = new ArrayList<String>(scoreTable.size());
		for (int i = 0; i < scoreTable.size(); i++) {
			StringBuilder sb = new StringBuilder(String.format("%-5d", i + 1));
			for (Double d: scoreTable.get(i)) {
				sb.append(String.format(SCOREFORMAT, d));
			}
			scoreLines.add(sb.toString());
		}
		HPC.write(scoreLines, Start.WorkDir + HPC.FILESPTR + "score_table.dat", false);
	}
	
	/**
	 * Add an empty line as place holder to represent this cluster to the similarity_table.dat 
	 * and score_table.dat.
	 * 
	 * @param lineIndex = candidateIndex + 1.
	 */
	public synchronized static void addNewLine(int lineIndex) {
		similarityTable.add(lineIndex, new ArrayList<String>()); // Insert one to the list.
		scoreTable.add(lineIndex, new ArrayList<Double>());
		write();
	}
	
	/**
	 * Add an empty line representing this cluster to the similarity_table.dat and score_table.dat.
	 * @param c A newly generated cluster.
	 * @param read
	 */
	/*
	public static void addString(String calcNum){
		String booleanResult = " ";
		String scoreResult = " ";
		similarityTableLines.add(booleanResult);
		scoreTableLines.add(scoreResult);
		write();
	}
	*/
	/**
	 * Update the lines in similarity and score table corresponding to this cluster, by checking
	 * all clusters with a smaller candidate index than it, even if there are candidates with larger
	 * indices already been evaluated. The candidateIndex doesn't indicate the chronological order 
	 * of the time at which relaxation finishes. 
	 * 
	 * Note:
	 * 	 1. m_Candidates should be locked when calling this function.
	 *   2. A cluster is not compared with itself. I.e. the i-th candidate only has i - 1 socres.
	 * 
	 * @param c
	 * @param isRestart
	 */
	public synchronized static void update(Cluster c){
		//if (isRestart) {
		//	read(false);
		//}
		String calcDir = c.getCalcDir();
		int listIndex = Cluster.calcDirToIndex(calcDir) - 1; // listIndex to distinguish with candidatesIndex
		double score;
		
		// When one cluster finishes relaxation, its structure is replaced by the relaxed one. 
		// Therefore, all the lines after this candidate should also be updated. Candidates
		// with a larger index might finish earlier because of multi-threading.
		for (int i = listIndex; i < m_Candidates.size(); i++) {
			while (scoreTable.size() <= i) { // Put place holder for candidates that haven't update tables.
				scoreTable.add(new ArrayList<Double>(i));
				similarityTable.add(new ArrayList<String>(i));
			}
			ArrayList<Double> scores = scoreTable.get(i);
			ArrayList<String> similarities = similarityTable.get(i);
			while (scores.size() < i) { // Fill the lines up when they are empty.
				scores.add(-1.0);
				similarities.add("none");
			}
			
			if (m_Candidates.get(i).relaxed()) { // only update when structure has been relaxed.
				for (int j = 0; j < i; j++) {
					if (m_Candidates.get(j).relaxed()) {
						if (m_isLigatedCluster) {
						score = m_SimilarityCalculator.getScore(m_Candidates.get(j).getStructureWithoutSideChains(), 
							    								m_Candidates.get(i).getStructureWithoutSideChains());	
						} else {
						score = m_SimilarityCalculator.getScore(m_Candidates.get(j).getStructure(), 
																m_Candidates.get(i).getStructure());
						}
						scores.set(j, score);
						similarities.set(j, Tables.getSimilarity(score));
					}
				}
			}
			
			try {
				if (similarityTable.size() <= i) {
					similarityTable.add(similarities);
					scoreTable.add(scores);
				} else {
					similarityTable.set(i, similarities);
					scoreTable.set(i, scores);
				}
			} catch (IndexOutOfBoundsException e) {
			    Logger.error("Fail to update tables! Tables don't have a line for cluster: " + (i + 1));
				e.printStackTrace();
				System.exit(2);
			}
		}
		write();
	}
	
	public synchronized static void newUpdate(Cluster c) {
		// Put place-holders to tables when m_Candidates is increased.
		for (int i = scoreTable.size(); i < m_Candidates.size(); i++) {
			ArrayList<Double> scores = new ArrayList<Double>(i);
			ArrayList<String> similarities = new ArrayList<String>(i);
			for (int j = 0; j < i; j++) {
				scores.add(-1.0);
				similarities.add("none");
			}
			scoreTable.add(scores);
			similarityTable.add(similarities);
		}
		
		if (c.relaxed()) {
			int candidateIndex = c.getCandidateIndex();
			// Update the line corresponding to this cluster; cluster 1 to cluster "candidateIndex - 1";
			for (int i = 0; i < candidateIndex - 1; i++) {
				if (m_Candidates.get(i).relaxed()) {
					double score;
					if (m_isLigatedCluster) {
					score =  m_SimilarityCalculator.getScore(m_Candidates.get(i).getStructureWithoutSideChains(), 
                                c.getStructureWithoutSideChains());
					} else {
					score =  m_SimilarityCalculator.getScore(m_Candidates.get(i).getStructure(), 
							                                        c.getStructure());
					}
					scoreTable.get(candidateIndex - 1).set(i, score);
					similarityTable.get(candidateIndex - 1).set(i, Tables.getSimilarity(score));
				}
			}
			
			// Update scores for lines corresponding cluster from candidateIndex + 1 to the last one.
			for (int i = candidateIndex; i < m_Candidates.size(); i++) {
				if (m_Candidates.get(i).relaxed()) {
					double score;
					if (m_isLigatedCluster) {
					score =  m_SimilarityCalculator.getScore(c.getStructureWithoutSideChains(), 
							               m_Candidates.get(i).getStructureWithoutSideChains());
					} else {
					score =  m_SimilarityCalculator.getScore(c.getStructure(), 
                                m_Candidates.get(i).getStructure());
					}
					scoreTable.get(i).set(candidateIndex - 1, score);
					similarityTable.get(i).set(candidateIndex - 1, Tables.getSimilarity(score));
				}
			}
		} else { // Overwrite the values for this cluster.
			int candidateIndex = c.getCandidateIndex();
			// Update the line corresponding to this cluster.
			Collections.fill((List<Double>) scoreTable.get(candidateIndex - 1), -1.0);
			Collections.fill((List<String>) similarityTable.get(candidateIndex - 1), "none");
			
			// Update entries of following lines corresponding to this cluster.
			for (int i = candidateIndex; i < m_Candidates.size(); i++) {
				scoreTable.get(i).set(candidateIndex - 1, -1.0);
				similarityTable.get(i).set(candidateIndex - 1, "none");
			}
		}
		write();
	}
	
	/**
	 * After retrain the potential and re-evaluated the clusters in the pool, regenerate the
	 * similarity_table.dat and score_table.dat. The full table needs to be regenerated because
	 * that the updated pool cluster could affect the similarity values for all subsequent clusters.
	 */
	public synchronized static void reset() {
		Logger.basic("Reseting tables ...");
		double score;
		String similar;
		synchronized (m_Candidates) {
			// i, j are list indices. list index = candidate_index - 1;
			for (int i = 0; i < m_Candidates.size(); i++) {
				//Tables.update(m_Candidates.get(i), false);
				for (int j = 0; j < i; j++) {
					if (m_isLigatedCluster) {
					score = m_SimilarityCalculator.getScore(m_Candidates.get(j).getStructureWithoutSideChains(),
                                						    m_Candidates.get(i).getStructureWithoutSideChains());	
					} else {
					score = m_SimilarityCalculator.getScore(m_Candidates.get(j).getStructure(),
							                                m_Candidates.get(i).getStructure());
					}
					similar = Tables.getSimilarity(score);
					// Don't allocate new ones, simple change the values.
					scoreTable.get(i).set(j, score);
					similarityTable.get(i).set(j, similar);
				}
			}
			Tables.write();
		}
	}
	
	public static boolean exists(String calcDir){
		int index = Cluster.calcDirToIndex(calcDir) - 1;
		Logger.detail("Checking whether cluster-" + index + " exists in tables.");
		Logger.detail("Table size = " + similarityTable.size());
		Logger.detail("Table calcDir = " + calcDir);
	
		if(similarityTable == null || scoreTable == null){
			return false;
		}
		
		try {
			similarityTable.get(index);
		} catch (IndexOutOfBoundsException e) {
			return false;
		}
		try {
			scoreTable.get(index);
		} catch (IndexOutOfBoundsException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Calculate similarity scores for the cluster in the specified folder against all clusters
	 * that have a smaller candidate index than it.
	 * 
	 * E.g. for the 5-th cluster, only compare it with 1 ~ 4, even if there are 6, 7, 8-th clusters
	 * been created and calculated.
	 * 
	 * @param calcDir1
	 * @return The lines to be added in similarity_table.dat and score_table.dat, corresponding
	 *         to this cluster.
	 */
	/*
	private static List<List<String>> compare(Cluster c) {
		List<String> booleans = new ArrayList<String>();
		List<String> scores = new ArrayList<String>();
		int index = c.getCandidateIndex() - 1; // Index of this cluster in a list.
		// Don't compare this one with itself.
		for (int i = 0; i < index; i++) {
			double s = m_SimilarityCalculator.getScore(c, m_Candidates.get(i));
			boolean bool = false;
			if (s < 0.72) {
				bool = true;
			}
			booleans.add(Boolean.toString(bool));
			scores.add(Double.toString(s));
		}
		List<List<String>> returnList = new ArrayList<>();
		returnList.add(booleans);
		returnList.add(scores);
		return returnList;
	}
	*/
	
	public static List<String> getSimilarityRow(String calcNum){
		//read(false);
		try {
			return similarityTable.get(Cluster.calcDirToIndex(calcNum) - 1);
		} catch (IndexOutOfBoundsException n) {
			Logger.error("Trying to get similarity for non-existing cluster: " + calcNum);
			return new ArrayList<String>();
		}
	}
	
	public static List<String> getSimilarityCol(String calcNum){
		//read(false);
		List<String> column = new ArrayList<>(m_Candidates.size());
		int index = Cluster.calcDirToIndex(calcNum) - 1;
		for(int i = index + 1; i < similarityTable.size(); i++){
			column.add(similarityTable.get(i).get(index));
		}
		return column;
	}
	
	/**
	 * Get score between clusters from score table.
	 * 
	 * @param calcNum1
	 * @param calcNum2
	 * @return similarity score in score table for give pair of clusters.
	 */
	public static double getScore(String calcNum1, String calcNum2){
		int index1 = Cluster.calcDirToIndex(calcNum1) - 1;
		int index2 = Cluster.calcDirToIndex(calcNum2) - 1;
		return Tables.getScore(index1, index2);
	}
	
	/**
	 * Read score from scoreTable. Note candidateIndex should subtract 1 to get listIndex.
	 * 
	 * @param listIndex1 Index in a list, starting from 0.
	 * @param listIndex2 Index in a list, starting from 0.
	 * @return
	 */
	public static double getScore(int listIndex1, int listIndex2) {
		if (listIndex1 > listIndex2) {
			return scoreTable.get(listIndex1).get(listIndex2);
		} else if (listIndex1 < listIndex2) {
			return scoreTable.get(listIndex2).get(listIndex1);
		}
		Logger.basic("Trying to get similarity score for the same cluster: " + (listIndex1 + 1));
		return -1.0;
	}
	
	/**
	 * Get the similarity between two clusters.
	 * 
	 * @param calcNum1
	 * @param calcNum2
	 * @return true/false/none
	 */
	public static String getSimilarity(String calcNum1, String calcNum2){
		int index1 = Cluster.calcDirToIndex(calcNum1) - 1;
		int index2 = Cluster.calcDirToIndex(calcNum2) - 1;
		return getSimilarity(index1, index2);
	}
	
	/**
	 * 
	 * @param listIndex1 candidateIndex - 1 
	 * @param listIndex2 candidateIndex - 1
	 * @return The similarity string between two clusters.
	 */
	public static String getSimilarity(int listIndex1, int listIndex2) {
		String similar = "none";
		if (listIndex1 > listIndex2) {
			similar = similarityTable.get(listIndex1).get(listIndex2);
		} else if (listIndex1 < listIndex2) {
			similar = similarityTable.get(listIndex2).get(listIndex1);
		} else {
			Logger.error("Trying to get similarity between the same clusters: " + listIndex1);
		}
		return similar;
	}
	
	public static String getSimilarity(double score) {
		if (0 <= score && score <= SIMILARITYTHRESHOLD) {
			return "true";
		} else if (score > SIMILARITYTHRESHOLD) {
			return "false";
		}
		return "none"; // Negative values.
	}
}
