package ga;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.net.InetAddress;
import java.net.UnknownHostException;

import ga.io.GAIOUtils;
import ga.io.Logger;
import ga.io.mtp.CFG;
import ga.minimizer.LAMMPS;
import ga.minimizer.VASP;
import ga.multithreaded.Candidates;
import ga.multithreaded.NewPool;
import ga.multithreaded.ParallelStopper;
import ga.potential.MTP;
import ga.structure.Cluster;
import ga.utils.GAUtils;
import matsci.io.app.log.Status;
import matsci.util.arrays.ArrayUtils;

public class Start {

    private static boolean m_ChargedSystem;
    
    // Global variables shared among processes to tweak workflow. Name them 
	// in UpperCamal for distinguishment.
    // General:
    private static Candidates AllCandidates;
    private static NewPool Pool;
    public static long GlobalStartTime = System.currentTimeMillis();
    public static SimilarityCalculator2 SimCalctor = new SimilarityCalculator2(false, false, GAUtils.getNNDistanceMap());
	public static String WorkDir = null;

	// Active Learning:
    public static int TrainedTimes = 0; // Number of training performed.
    public static AtomicBoolean Retrain = new AtomicBoolean(false);
    public static AtomicBoolean Complete = new AtomicBoolean(false);
    public static int lastRetrainedIndex = 0; // index of last cluster when retraining happens.
	public static boolean useWeightedDataset = false;
    // clusters needed to be retrained on
    public static Queue<Cluster> NewData = new LinkedList<>();
    // A list of candidate indices that have been evaluated by DFT as training data.
	public static ArrayList<Integer> TrainedList = new ArrayList<> (100);

	// IO Monitor
	public static ArrayList<Boolean> Zipped = new ArrayList<>(5000);
	
    /**
     * How to call the main function:
     *     java -jar ga.jar &> stdout
     * @param args
     */
    public static void main(String[] args) {
    	//********************************** INITIALIZATION **************************************//
    	Logger.setIncludeHeader(true);
    	Logger.basic("Initiating GA ...");
        Input.initialize();
        Start.initialize();
        Start.checkFiles();
        boolean restart = Start.checkStartMode(); // Decide it's a restart calculation or a new calculation.
        if (restart) {
        	Logger.basic("Restart from previous run. Recovering ...");
        } else {
        	Logger.basic("Fresh calculation. Start a new run.");
        }
        //long st, et;
        
        Handler handler = new Handler();
        Thread.setDefaultUncaughtExceptionHandler(handler);
        
        //******************************** Initialize or recover *********************************//
		if (restart) {
			// Decompress calculations
			Start.unarchive();
			Logger.basic("Finish decompressing calculations. Elapsed: " + GAIOUtils.elapsedTime());
		}

		AllCandidates = new Candidates(restart);
		//Tables.setCandiates(m_Candidates.getCandidates());
		if (restart) {
			// GA operation history.
			Start.recoverParents(AllCandidates);
			AllCandidates.writeToFile();
			if (Input.getRetrainMode() == Input.RetrainMode.DOptimality) {
				Logger.basic("Recovering extrapolation grades ...");
				Start.recoverMaxvolGrades(AllCandidates);
				Logger.basic("Finish recovering extrapolation grades. Elapsed: " + GAIOUtils.elapsedTime());
			}
			
			/*
			Logger.basic("Recovering tables ...");
			st = System.currentTimeMillis();
			Tables.read(true, true);   // Recover tables from files. Read two table files.
			et = System.currentTimeMillis();
			Logger.detail("Recovering tables takes " + GAIOUtils.formatElapsedTime(et - st));
			Tables.write();            // Remove empty lines or redundant lines.
			*/
			
			Pool = new NewPool(AllCandidates);
			
			// Active Learning: training history.
			if (Input.getRetrainMode() == Input.RetrainMode.DOptimality 
					&& AllCandidates.getSize() > Input.getPreprocessing()) {
				Logger.basic("Recovering training history ... ");
				Start.recoverTrainingHistory(); // Recover m_TrainedList and m_TrainedTimes;
				Logger.basic("Finish recovering training history. Elapsed: " + GAIOUtils.elapsedTime());
				
				/*
				if (m_NewData.size() >= Input.getSelectPoolSize()) {
					// Enough number of extrapolating clusters.
					m_Retrain.set(true);
					Start.addPoolClusterToNewData();
					// In retrainMTP(), m_TrainedTimes will be incremented at the beginning.
					if (m_TrainedTimes > 0) { m_TrainedTimes--; }
					Logger.basic("GA was stopped at MTP retraining. Restart from retraining.");
				} else if (m_Candidates.getSize() > Input.getLAMMPSRatioOffset() + Start.lastRetrainedIndex
						&& m_Pool.getNonVASPClusterRatio() >= Input.getMaxLAMMPSPoolRatio()) {
					// Too many LAMMPS relaxed structures in pool.
					m_Retrain.set(true);
					Start.addPoolClusterToNewData();
					// In retrainMTP(), m_TrainedTimes will be incremented at the beginning.
					if (m_TrainedTimes > 0) { m_TrainedTimes--; }
					Logger.basic("GA was stopped at MTP retraining. Non-VASP pool clusters exceed "
							   + "maximum allowed ratio: " + m_Pool.getNonVASPClusterRatio()
							   + ". Restart from retraining.");
				} else {
					// GA was not stopped at retraining. Update the value set in recoverTrainingHistory().
					lastRetrainedIndex = Collections.max(m_TrainedList);
				}
				*/
			} else if (Input.getRetrainMode() == Input.RetrainMode.Batch 
					&& AllCandidates.getSize() % Input.getRetrainStep() == 0) {
				Retrain.set(true);
			} else { // Pure "VASP" or "LAMMPS" calculation.
				Start.recoverUnfinishedCandidates();
				Logger.basic("Finish recovering unfinished candidates. Elapsed: " + GAIOUtils.elapsedTime());
			}

			if (Input.zip()) {
				for (int i = 0; i < AllCandidates.getSize(); i++) { Zipped.add(false); }
				Engine.archiveCalculations(AllCandidates, Pool);
				Logger.basic("Finish archiving calculations. Elapsed: " + GAIOUtils.elapsedTime());
			}
		} else {
			Pool = new NewPool();
		}
		
		// Only recover .dat files. Don't execute GA.
		if (args.length != 0 && args[0].equals("recover")) {
			System.exit(0);
		}
		
		//*****************************  SET UP THREADS AND CALCULATION **************************//
		Logger.detail("Number of engine threads to be initiated: " + Input.getEngineThreads());
        Thread[] threads = new Thread[Input.getEngineThreads()];
        Engine[] engines = new Engine[Input.getEngineThreads()];
        //AtomicBoolean complete = new AtomicBoolean(false); // TODO: make it a global variable.
        if (AllCandidates.getSize() > Input.getNumOperations() || Pool.isConverged()) {
        	Complete.set(true);
        }
       
        // While not complete, loops over generate new clusters and retrain.
        Logger.detail("Starting GA ...");
        while(!Complete.get()) {
		    for (int i = 0; i < threads.length; i++) {
		    	engines[i] = new Engine(AllCandidates, Pool, m_ChargedSystem);
		    	threads[i] = new Thread(engines[i], Integer.toString(i));
		    }
		    Runtime.getRuntime().addShutdownHook(new ShutDownHook(threads));
		    
		    for (Thread t: threads) {
		    	Logger.detail("Start engine thread-" + t.getName());
		    	t.start();
		    }
        
	        for (Thread t: threads) {
	        	try {
	        		t.join(); 
	        	} catch (InterruptedException e) {
	        		Logger.error("Interrupted. Exit.");
	        		System.exit(10);
	        	}
	        }
	        // It's possible engines terminate because reached convergence or maximum number of
	        // candidates.
	        if (!Complete.get() && Input.getMode().equalsIgnoreCase("Active") && Retrain.get()) {
	        	
	        	// backup the initial potential.
	        	if (TrainedTimes == 0) {
	        		String backup = WorkDir + HPC.FILESPTR + "train" + HPC.FILESPTR + "0";
	        		HPC.mkdir(WorkDir + HPC.FILESPTR + "train" + HPC.FILESPTR + "0");
	        		HPC.copy(Input.getRoot() + HPC.FILESPTR + LAMMPS.getPotentialFileName(), backup, false);
	        		HPC.copy(Input.getRoot() + HPC.FILESPTR + MTP.getCFGOutput(), backup, false);
	        		if (Files.exists(Paths.get(Input.getRoot() + HPC.FILESPTR + MTP.ALSFile))) {
	        			HPC.copy(Input.getRoot() + HPC.FILESPTR + MTP.ALSFile, backup, false);
	        		}
	        	}
	        	
	        	TrainedTimes++;
	        	if (Input.getRetrainMode() == Input.RetrainMode.Batch) {
		        	Logger.basic("Start retraining in Batch mode with pool clusters.");
	        	} else if (Input.getRetrainMode() == Input.RetrainMode.DOptimality) {
	        		Logger.basic("Start retraining in DOptimality mode with extrapolating clusters.");
	        	}
	        	Start.retrainMTP();
	        	
	        	lastRetrainedIndex = Collections.max(TrainedList);
	        	Logger.detail("Latest retrained cluster is " + lastRetrainedIndex);
	        	
	        	// Re-evaluate energies for the selected percentage of lowest energy clusters.
				if (Input.reevaluate()) {
					Start.reevaluate();
				} else {
					Logger.basic("REEVALUTE is set to \"False\". Skip re-evaluation of "
							   + "past structures with new potential.");
				}
	        	Retrain.set(false);
	        }
        }
        
        // Check reasons for exiting the above loop.
        if (Pool.getConverged()) {
        	Logger.basic("GA converged! Convergence = " + Pool.getConvergence());
        } else if (AllCandidates.getSize() == Input.getNumOperations()) {
        	Logger.basic("Reached maximum number of candidates: " + Input.getNumOperations() 
        	           + ". Exit.");
        } else {
        	Logger.basic("GA finished for unknown reasons.");
        }
    }

	// Initialization
	public static void initialize() {
		// Compose working folder according to m_EleNames and m_EleNums
		WorkDir = Input.getWorkDirectory();
		File workDirectory = new File(WorkDir);
		if (!workDirectory.exists()) {
			workDirectory.mkdirs();
		}
		Logger.basic("Working directory: " + WorkDir);
		
		try {
			String hostName = InetAddress.getLocalHost().getHostName();
			Logger.basic("Host name: " + hostName);
		} catch (UnknownHostException e) {
			Logger.basic("Host name: Unknown");
		}
	}

    /**
     * Check whether the "RESTART" folder or "candidates.dat" exists.
     */
    private static boolean checkStartMode() {
    	Logger.basic("Checking start mode ...");
    	File restart = new File(WorkDir + HPC.FILESPTR + "RESTART");
        File candidates = new File(Candidates.CANDIDATES);
        File allCandidates = new File(Candidates.ALLCANDIDATES);
        boolean isRestart = false;
        if (candidates.exists() || restart.exists()) { // Deal with "SIGKILL".
            isRestart = true;
        } else if (allCandidates.exists() && allCandidates.listFiles((file, name) -> name.matches("[0-9]+")).length != 0) {
        	isRestart = true;
        }
        Logger.basic("Restart: " + isRestart);
        restart.delete();
        return isRestart;
    }

    /**
     * When GA is terminated by Ctrl-C or scancel, create a "RESTART" folder to denote that the
     * GA run is interrupted. Next time, the calculation will restart from where it's left.
     * 
     * Note: "SIGKILL" will immediately abort the process and the shutdown hook won't be called
     * in this situation. So m_Restart should be also check existence of "candidates.dat".
     * 
     * TODO: what about time-out in a batch job?
     */
    private static void dataDump() {
        File restartFile = new File(Start.WorkDir + HPC.FILESPTR + "RESTART");
        restartFile.mkdirs();
    }

    /**
     * A sanity-check function to ensure input files exist. Otherwise, issue warnings
	 * to user and exit.
     */
    private static void checkFiles() {
    	Logger.basic("Checking existence of necessary input files ... ");
        ArrayList<String> inFiles = new ArrayList<String>(10); // Prevent dynamic expansion.

        inFiles.add("INGA");
        if (Input.getMode().equalsIgnoreCase("VASP")) {
        	for (String i: VASP.getInputFiles()) {
        		inFiles.add(i);
        	}
        } else if (Input.getMode().equalsIgnoreCase("LAMMPS")) {
        	for (String i: LAMMPS.getInputFiles()) {
        		inFiles.add(i);
        	}
        } else { // Active learning
        	for (String i: LAMMPS.getInputFiles()) {
        		inFiles.add(i);
        	}
        	for (String i: VASP.getInputFiles()) {
        		inFiles.add(i);
        	}
        }
        
        if (Input.isLigatedCluster()) {
        	inFiles.add("LIGAND");
        	inFiles.add("CLUSTER");
        }
        
        if (Input.getMode().equalsIgnoreCase("Active") && Input.runMD()) {
        	inFiles.add("INCAR_MD");
        }

        if (Input.isLigatedCluster() && Input.useStaticINCAR()) {
        	inFiles.add("INCAR_static");
        }
        
        for (String file: inFiles) {
            String dir = Input.getRoot() + HPC.FILESPTR + file;
            File f = new File(dir);
            if (!f.exists()) {
                Logger.error("No " + file + " file !! Exiting.");
                Logger.error(f.getAbsolutePath());
                System.exit(1);
            } else {
                Logger.detail(file + " exists");
            }
        }
    }

    private static void checkCharge() throws IOException {
        List<String> incarLines = HPC.read(Input.getRoot() + HPC.FILESPTR + "INCAR");
        System.out.println(incarLines.get(0));
        List<String> potcarLines = HPC.read(Input.getRoot() + HPC.FILESPTR + "POTCAR");
        System.out.println(potcarLines.get(0));

        boolean isCharged = checkIncar(incarLines);
        if (isCharged) {
            if (getNelect(incarLines, potcarLines) - getNeutral(potcarLines) > 0) {
                m_ChargedSystem = true;
            }
        } else {
            m_ChargedSystem = false;
        }

    }

    private static int getNeutral(List<String> potcarLines) {
        int[] valenceElectrons = getValenceElectrons(potcarLines);
        int[] numIons = Input.getEleNums();
        assert (numIons.length == valenceElectrons.length);
        int sum = 0;
        for (int i = 0; i < valenceElectrons.length; i++) {
            sum += valenceElectrons[i] * numIons[i];
        }
        return sum;
    }

    private static int[] getValenceElectrons(List<String> potcarLines) {
        int[] returnArray = new int[0];
        for (int i = 0; i < potcarLines.size(); i++) {
            String line = potcarLines.get(i);
            if (line.contains("PAW_PBE") && !line.contains("TITEL")) {
                returnArray = ArrayUtils.appendElement(returnArray,
                        (int) Double.parseDouble(potcarLines.get(i + 1).trim()));
            }
        }
        return returnArray;
    }

    private static int getNelect(List<String> l, List<String> potcarLines) {
        for (int lineNum = 0; lineNum < l.size(); lineNum++) {
            if (l.get(lineNum).contains("NELECT")) {
                String[] split = l.get(lineNum).split("=");
                return Integer.parseInt(split[split.length - 1]);
            }
        }
        return getNeutral(potcarLines);
    }
    
    private static boolean checkIncar(List<String> l) throws IOException {
        for (int lineNum = 0; lineNum < l.size(); lineNum++) {
            if (l.get(lineNum).contains("NELECT")) {
                if (l.get(lineNum).contains("#")) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }
    
    /**
	 * Collect the code for retrain the MTP potential, instead of putting it all in run().
	 * 
	 */
	private static void retrainMTP() {
		long st, et;
		String rootDir = Input.getRoot(); // For not call this function every time.
		// e.g. <root>/Al/8/train/2;
		String lastTrainDir = WorkDir + HPC.FILESPTR + "train"
				                      + HPC.FILESPTR + (TrainedTimes - 1);
		String trainDir = WorkDir + HPC.FILESPTR + "train" 
		                          + HPC.FILESPTR + TrainedTimes;
		RetrainEngine.updateTrainDir(trainDir);
		RetrainEngine.updateTrainedTime(TrainedTimes);
		if (!HPC.mkdir(trainDir)) {
			Logger.error("Failed to create " + trainDir + ".");
			System.exit(3);
		}
		
		// Copy the potential and training set used last time here for re-train.
		// Not use the one in root since it might has been contaminated by abortion of last run.
		HPC.copy(lastTrainDir + HPC.FILESPTR + LAMMPS.getPotentialFileName(), trainDir, false);
		HPC.copy(lastTrainDir + HPC.FILESPTR + MTP.getCFGOutput(), trainDir, false);
		if (Files.exists(Paths.get(lastTrainDir + HPC.FILESPTR + MTP.ALSFile))) {
			HPC.copy(lastTrainDir + HPC.FILESPTR + MTP.ALSFile, trainDir, false);
		}
		
		// No matter we're using batch mode or DOptimality, we want accurate description of 
		// low-energy clusters --> always add pool cluster to retraining.
		List<Cluster> poolClusters = Pool.getPool(); // relaxed pool clusters from pool.dat
		for (Cluster c: poolClusters) {
			if (!TrainedList.contains(c.getCandidateIndex()) && !NewData.contains(c)) {
				NewData.add(c);
			}
		}
		
		// Always re-evaluate clusters in the new data set, since they either extrapolate w.r.t to 
		// previous training data in D-Optimality mode or have been evaluated by VASP.
		// VASP results are copied to calcDir. This way, we guarantee that energies of clusters 
		// in pool.dat always come from VASP.
		for (Cluster c: NewData) {
			AllCandidates.addToUnfinishedCandidates(c);
		}
		
		List<String> newCFGLines = new ArrayList<>();
		Thread[] retrainThreads = new Thread[Input.getTrainingThreads()];
		for (int i = 0; i < retrainThreads.length; i++) {
			retrainThreads[i] = new Thread(new RetrainEngine(NewData, TrainedList, newCFGLines, Pool), 
					                       "Retrain-" + Integer.toString(i));
			Logger.basic("Start thread Retrain-" + i);
			retrainThreads[i].start();
		}
		
		for (Thread t: retrainThreads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				Logger.error("Interrupted. Exit.");
				System.exit(10);
			}
		}
		
		if (newCFGLines.size() != 0) {
			HPC.append(newCFGLines, trainDir + HPC.FILESPTR + MTP.getCFGOutput());
			Logger.basic("Retrain MTP potential ...");
			useWeightedDataset = false;
			
			// First actively select the best CFGs from training data and new data. Then add
			// topMultiple copy of topSelection CFG images of the entire data set to the actively
			// selected ones.
			if (Input.scaleTopCandidates()) {
				int topSelection = Input.getTopSelection();
				int topMultiple = Input.getTopMultiple();
				Logger.basic("Constructing weighted training set with " +
							 Integer.toString(topMultiple) + " copies each of the " +
							 Integer.toString(topSelection) + " best candidates.");
				
				String preSelectDir = trainDir + HPC.FILESPTR + "pre-select";
				HPC.mkdir(preSelectDir);
				HPC.write(newCFGLines, preSelectDir + HPC.FILESPTR + "new.cfg", false);
				st = System.currentTimeMillis();
				int preSelectExitCode = MTP.selectAdd(preSelectDir,
													  trainDir + HPC.FILESPTR + LAMMPS.getPotentialFileName(), 
													  lastTrainDir + HPC.FILESPTR + MTP.getCFGOutput(), 
													  preSelectDir + HPC.FILESPTR + "new.cfg");
				et = System.currentTimeMillis();
				Logger.detail("Active selection of new configurations takes " + GAIOUtils.formatElapsedTime(et - st));
				
				if (preSelectExitCode != 0) {
					Logger.error("Select-add in " + preSelectDir + " failed!" +
								 "Training will proceed with " + MTP.getCFGOutput() + ".");
				} else {
					st = System.currentTimeMillis();
					/* Use CFG class.
					List<String[]> sortedConfigs = MTP.sortCFG(trainDir + HPC.FILESPTR + MTP.getCFGOutput());
					List<String> topConfigs = new ArrayList<String>();
					
					for (int s = 0; s < topSelection; s++) {
						String[] CFGArray = sortedConfigs.get(s);
						for (String line : CFGArray) {
							topConfigs.add(line);
						}
					}
					*/
					CFG topCFGs = new CFG(trainDir + HPC.FILESPTR + MTP.getCFGOutput());
					topCFGs.sortByEnergy();
					topCFGs.writeFile(0, topSelection, trainDir + HPC.FILESPTR + "top.cfg");
					List<String> topConfigs = HPC.read(trainDir + HPC.FILESPTR + "top.cfg");
					
					HPC.copy(preSelectDir + HPC.FILESPTR + MTP.SELECTED, trainDir + HPC.FILESPTR + MTP.WEIGHTEDDATASET, false);
					for (int m = 0; m < topMultiple; m++) {
						HPC.write(topConfigs, trainDir + HPC.FILESPTR + MTP.WEIGHTEDDATASET, true);
					}
					et = System.currentTimeMillis();
					Logger.detail("Constructing weighted dataset takes " + GAIOUtils.formatElapsedTime(et - st));
					useWeightedDataset = true;
				}
			}
			
			st = System.currentTimeMillis();
			int trainExitCode;
			if (useWeightedDataset) {
				trainExitCode = MTP.train(trainDir, 
		                 	              trainDir + HPC.FILESPTR + LAMMPS.getPotentialFileName(), 
		                 	              trainDir + HPC.FILESPTR + MTP.WEIGHTEDDATASET);
			} else {
				trainExitCode = MTP.train(trainDir, 
						                  trainDir + HPC.FILESPTR + LAMMPS.getPotentialFileName(), 
						                  trainDir + HPC.FILESPTR + MTP.getCFGOutput());
			}
			et = System.currentTimeMillis();
			Logger.detail("Retraining by MTP takes " + GAIOUtils.formatElapsedTime(et - st));
			Logger.basic("Finish retraining. Elapsed: " + GAIOUtils.elapsedTime());

			if (trainExitCode != 0) {
				Logger.error("Retrain in " + trainDir + " failed! Please check mlip output. Potential is not updated.");
				return;
			}

			// select-add and copy state.als to Input.getRoot().
			Logger.basic("Update state.als file ...");
			String selectAddDir = trainDir + HPC.FILESPTR + "select-add";
			if (useWeightedDataset) {
				selectAddDir = trainDir + HPC.FILESPTR + "post-select";
			}
			String emptyCFG = selectAddDir + HPC.FILESPTR + "empty.cfg";
			HPC.mkdir(selectAddDir);
			HPC.write(new ArrayList<String>(), emptyCFG, false); // otherwise, mlp would report error.
			st = System.currentTimeMillis();
			String trainingData = (useWeightedDataset) ? MTP.WEIGHTEDDATASET : MTP.getCFGOutput();
			int selectExitCode = MTP.selectAdd(selectAddDir, trainDir + HPC.FILESPTR + LAMMPS.getPotentialFileName(),
					                           trainDir + HPC.FILESPTR + trainingData, emptyCFG);
			et = System.currentTimeMillis();
			Logger.detail("Updating state.als file takes " + GAIOUtils.formatElapsedTime(et - st));
			Logger.basic("Finish updating state.als. Elapsed: " + GAIOUtils.elapsedTime());
			if (selectExitCode != 0) {
				Logger.error("Select-add in " + selectAddDir + " failed! State.als file is deleted!"
						+ " A new one will be generated when grade is calculated next time.");
				HPC.rm(trainDir + HPC.FILESPTR + MTP.ALSFile, false, false, false); // report error
				HPC.rm(Input.getRoot() + HPC.FILESPTR + MTP.ALSFile, false, false, false); // report error
			} else {
				HPC.copy(selectAddDir + HPC.FILESPTR + MTP.ALSFile, trainDir, false);
			}
		} else {
			Logger.basic("No new training data added. Skip retraining potential.");
		}
		// Copy the new potential back to wkDir.
		HPC.copy(trainDir + HPC.FILESPTR + LAMMPS.getPotentialFileName(), rootDir, false);
		HPC.copy(trainDir + HPC.FILESPTR + MTP.getCFGOutput(), rootDir, false);
		HPC.copy(trainDir + HPC.FILESPTR + MTP.ALSFile, rootDir, false);
	}
	
	/**
	 * Recover m_TrainedList, m_NewData and Candidates.unfinishedCluster by scanning through
	 * "train" folder, and by checking cluster tags m_Status and m_isInterpolating.
	 * 
	 * If previous calculation was stopped at VASP relaxation / MD, they will be restarted from
	 * scratch from VASP relaxation. If it was stopped at MTP training, training will be restarted
	 * from scratch. Determine correctly which cluster ab initio calculation has finished upon is
	 * important for avoid repetitive work, especially when SELECTPOOLSIZE > 1.
	 * 
	 */
	private static void recoverTrainingHistory() {
		List<Cluster> candidates = AllCandidates.getCandidates();
		for (Cluster c: candidates) {
			if (c.getEnergyMinimizer().getName().equals("VASP")) {
				continue;
			} else if (c.relaxed()) {
				if (c.extrapolating()) { // Relaxed and extrapolating
					NewData.add(c);
					Logger.detail("Cluster-" + c.getCandidateIndex() + " is added to training pool.");
				} else if (!c.realistic()) { // Relaxed but to either collapsed or exploded configuration.
					NewData.add(c);
					Logger.detail("Cluster-" + c.getCandidateIndex() + " is added to training pool.");
				}
			} else {
				// Initial structure is extrapolating
				if (c.extrapolating() && c.getMaxvolGrade() != Double.MAX_VALUE) {
					NewData.add(c);
					Logger.detail("Cluster-" + c.getCandidateIndex() + " is added to training pool.");
				} else { // unfinished candidates
					AllCandidates.addToUnfinishedCandidates(c);
					Logger.detail("Cluster-" + c.getCandidateIndex() + " is added to unfinished list.");
				}
			}
		}
		
		File trainDir = new File(WorkDir + HPC.FILESPTR + "train");
		// If folder "train" doesn't exist, assume it hasn't been re-trained even once.
		if (trainDir.exists()) {
			File[] trainDirs = trainDir.listFiles((file, name) -> name.matches("[0-9]+"));			
			if (trainDirs != null) {
				// the folder with largest index is the last training index.
				trainDirs = HPC.sortFolderByNumber(trainDirs);
				TrainedTimes = Integer.parseInt(trainDirs[trainDirs.length - 1].getName());
				for (int i = 0; i < trainDirs.length; i++) {
					// Lost the sequence of DFT evaluation here. But it's less important.
					File[] calculations = trainDirs[i].listFiles((file, name) -> name.matches("[0-9]+"));
					if (calculations.length == 0) { // No VASP calculations in folder. E.g. train/0
						continue;
					}
					calculations = HPC.sortFolderByNumber(calculations);
					for (File calc: calculations) {
						if (VASP.isCalcSuccessful(calc.getAbsolutePath())) {
							if (!Input.runMD()) {
								// Don't run MD. Relaxation succeeded.
								TrainedList.add(Integer.parseInt(calc.getName()));
							} else if (VASP.isCalcSuccessful(calc.getAbsolutePath() + HPC.FILESPTR + "MD")) {
								// Run MD. Both relaxation and MD should succeed. Otherwise, rerun them.
								TrainedList.add(Integer.parseInt(calc.getName()));
							}
						}
					}
					
					// Use files in last training folder to determine retrain status.
					// If retrain finished, it should have train/i/stdout and train/i/select-add/state.als
					if (i != 0 && i == trainDirs.length - 1) {
						File trainingOutput = new File(trainDirs[i].getAbsolutePath() + HPC.FILESPTR + "stdout");
						File selectAddOutput = new File(trainDirs[i].getAbsolutePath() + HPC.FILESPTR
								+ "select-add" + HPC.FILESPTR + MTP.ALSFile);
						if (!trainingOutput.exists() || !selectAddOutput.exists()) {
							Logger.basic("Trainig output or select-add output don't exist in " 
									+ trainDirs[i].getAbsolutePath() + ". GA was stopped at retraining. "
									+ "Restart from retraining.");
							Retrain.set(true);
							Start.addPoolClusterToNewData(); // Note it uses the un-decreased m_TrainedTimes.
							TrainedTimes--; // Since it will be incremented at the beginning of retrainMTP();
						}
					}
					
					// If was stopped at retraining, use the largest index of the second to last dir,
					// i.e. don't update lastRetrainedIndex in this loop.
					if (!Retrain.get()) {
						lastRetrainedIndex = Integer.parseInt(calculations[calculations.length - 1].getName());
					}
				}
			} else {
				TrainedTimes = 0;
				lastRetrainedIndex = 0;
			}
		}
		
	}
	
	/**
	 * Add pool cluster to list of retraining clusters only when GA is determined as being
	 * stopped as retraining step.
	 */
	private static void addPoolClusterToNewData() {
		File trainDir = new File(WorkDir + HPC.FILESPTR + "train");
		for (Cluster c: Pool.getPool()) {
			if (!GAUtils.contains(NewData, c)) {
				if (HPC.exists(trainDir + HPC.FILESPTR + TrainedTimes + HPC.FILESPTR + c.getCandidateIndex())) {
					// Add pool clusters that had been retrained in this cycle (finished or unfinished). 
					// CFGs need to be re-read in retrainMTP() when restarting.
					NewData.add(c);
				} else if (!TrainedList.contains(c.getCandidateIndex())) {
					// Add pool clusters that haven't been retrained by to training pool.
					NewData.add(c);
				}
			}
		}
	}
	
	/**
	 * Try to recover maxvol grade for each cluster. For the ones that are extrapolating, they will
	 * be added to unfinished cluster list. Most likely, in Engine.run(), they will be added to
	 * retraining pool.
	 * 
	 * @param m_Candidates
	 */
	private static void recoverMaxvolGrades(Candidates m_Candidates) {
		List<Cluster> candidates = m_Candidates.getCandidates();
		boolean hasFile = Files.exists(Paths.get(Cluster.MAXVOLGRADES));
		if (!hasFile) {
			Logger.basic("maxvolGrades.dat doesn't exist! Try to read from mlp outputs.");
		}
		List<String> gradeLines = HPC.read(Cluster.MAXVOLGRADES); // Empty list if not exist.
		for (int i = 0; i < candidates.size(); i++) {
			Cluster c = candidates.get(i);
			double grade = Double.MAX_VALUE;
			
			if (hasFile) { // Read from maxvolGrades.dat
				List<Integer> lineNumbers = HPC.grepLineNumbers(c.getCalcDir() + "\\s+", Cluster.MAXVOLGRADES);
				if (!lineNumbers.isEmpty()) {
					try {
						grade = Double.parseDouble(gradeLines.get(lineNumbers.get(0) - 1).split("\\s+")[1]);
					} catch (NumberFormatException e) {
						Logger.error("Maxvol grade is not parsable. " + e.getLocalizedMessage()
								   + "Set grade to Double.MAX_VALUE.");
					}
				} else {
					Logger.error("MaxvolGrades.dat misses line for cluster-" + c.getCandidateIndex()
							   + ". Set grade to Double.MAX_VALUE.");
				}
			} else { // Recover from output files.
				List<Double> grades = new ArrayList<>();
				if (c.getEnergyMinimizer().getName().equals("LAMMPS")) {
					if (c.relaxed() & c.realistic()) {
						grades = MTP.readMaxvolGrade(c.getCalcDir() + HPC.FILESPTR + "graded_end.cfg");
						if (grades.isEmpty()) {
							Logger.error("Cannot recover maxvol grade for LAMMPS relaxed cluster-"
								       + c.getCandidateIndex() + ". Set grade to Double.MAX_VALUE.");
							// Unfinished cluster stopped after LAMMPS relaxation, but before
							// maxvol grade calculation.
							c.setRelaxed(false, true);
						}
					} else {
						// Unrelaxed or relaxed but unrealistic
						grades = MTP.readMaxvolGrade(c.getCalcDir() + HPC.FILESPTR + "graded_struct.cfg");
					}
				} else if (c.getEnergyMinimizer().getName().equals("VASP")) {
					// Must have finished retrained. Otherwise wouldn't be copied to all_candidates.
					grades = MTP.readMaxvolGrade(c.getCalcDir() + HPC.FILESPTR + MTP.CFGOutput);
					if (grades.isEmpty()) {
						Logger.error("Cannot recover maxvol grade for VASP relaxed cluster-"
								   + c.getCandidateIndex() + ". Set grade to Double.MAX_VALUE.");
					}
				}
				
				if (!grades.isEmpty()) {
					grade = grades.get(grades.size() - 1);
				}
			}
			
			c.setMaxvolGrade(grade);
			if (c.relaxed() & c.realistic()) {
				c.setExtrapolating(grade > Input.getSelectThreshold());
			} else {
				c.setExtrapolating(grade > Input.getBreakThreshold());
			}
			c.writeMaxvolGrade();
		}
	}
	
	/**
	 * Recover type of GA operation and parent(s) of clusters from chosen.dat and mutation.dat.
	 * Cluster type might have been recovered at candidates.dat, but parents info can only be 
	 * recovered here.
	 * 
	 * @param m_Candidates
	 */
	private static void recoverParents(Candidates m_Candidates) {
		List<Cluster> candidateList = m_Candidates.getCandidates();
		List<String> parentLines = HPC.read(Cluster.CHOSEN);
		List<String> mutationLines = HPC.read(Cluster.MUTATION);
		for (Cluster c: candidateList) {
			// Check chosen.dat first.
			List<Integer> lineNumbers = HPC.grepLineNumbers("^" + c.getCalcDir() + "\\s+", Cluster.CHOSEN);
			if (!lineNumbers.isEmpty()) {
				c.setClusterType("Off");				
				// Repair parents info.
				// Should be in pairs. When there are more than two lines, trust the first pair,
				// because of Cluster.writeChosen() overwrites first two lines when there are multiple.
				if (lineNumbers.size() >= 2) {
					int one = GAIOUtils.convertToIndex(parentLines.get(lineNumbers.get(0) - 1).split("\\s+")[2]);
					int two = GAIOUtils.convertToIndex(parentLines.get(lineNumbers.get(1) - 1).split("\\s+")[2]);
					c.setParents(one, two);
				} else if (lineNumbers.size() == 1) {
					// Only one line for parents. Incomplete, set both to default, -1.
					// TODO: should we also change clustertype if parents are not recoverred correctly?
					c.setParents(-1, -1);
				}
				continue;
			} else if (c.getCandidateIndex() <= Input.getInitSize()) {
				c.setClusterType(Input.getInitType());
			}
			
			// Check mutation.dat
			lineNumbers = HPC.grepLineNumbers("->\\s*" + c.getCandidateIndex() + "\\s+", Cluster.MUTATION);
			if (!lineNumbers.isEmpty()) {
				// Use the last matching string since mutation.dat concatenate lines without
				// removing duplicates.
				String[] terms = mutationLines.get(lineNumbers.get(lineNumbers.size() - 1) - 1).trim().split("\\s+");
				c.setClusterType("Mut");
				if (terms[0].matches("[0-9]+")) {
					c.setParents(Integer.parseInt(terms[0]), -1);
				}
				// Used the new mutation.dat format, but check to comply with old format.
				if (terms.length == 7) {
					c.setClusterSubType(terms[6]);
				}
				continue;
			}
			
			// If not in chosen.dat or mutation.dat, leave it be. It could be RBG or seed.
		}
	}
	
	private static void recoverUnfinishedCandidates() {
		for (Cluster c: AllCandidates.getCandidates()) {
			if (!c.relaxed()) {
				Logger.detail("Cluster-" + c.getCandidateIndex() + " is added to unfinished list.");
				AllCandidates.addToUnfinishedCandidates(c);
			}
		}
	}

	/**
	 * Decompress any archived calculation at the beginning of a restart calculation. It scans
	 * through all_candidates folder and decompress .zip files with name index.zip into each
	 * calculation folder. 
	 * 
	 * Note it assumes that entry names of index.zip are relative to calcDir, not java "user.dir".
	 */
	private static void unarchive() {
		File[] calcDirs = new File(Candidates.ALLCANDIDATES).listFiles(file -> file.getName().matches("[0-9]+"));
		Arrays.sort(calcDirs);
		if (calcDirs.length == 0) { return; }
		for (File calcDir: calcDirs) {
			// Abstract pathname is absolute here.
			File[] archives = calcDir.listFiles(file -> file.getName().endsWith(".zip"));
			for (File archive: archives) {
				HPC.unzip(archive.getPath(), calcDir.getPath());
			}
		}
	}
	
	/**
	 * Re-evaluate the past calculation after a new potential is trained. Tables and the pool will
	 * be subsequently re-generated.
	 */
	private static void reevaluate() {
		long st, et;
		Logger.basic("Start re-evaluation of the lowest " 
		           + String.format("%6.2f%%", Input.getReCalcFraction() * 100.0) + " clusters");
		// Only relaxed candidates are sorted.
		List<Integer> sortedClusters = AllCandidates.getSortedClusterIndices();
		int reCalcNumber = (int) Math.floor(Input.getReCalcFraction() * sortedClusters.size());
		Logger.basic("There are " + reCalcNumber + " clusters to be re-evaluated.");
		SortedSet<Integer> reCalcSet = new TreeSet<>(sortedClusters.subList(0, reCalcNumber)); // Exclusive.
		// Skip VASP evaluated ones.
		Queue<Cluster> reCalcClusters = new LinkedList<>();
		for (Integer i: reCalcSet) {
			Cluster c = AllCandidates.getCandidate(i - 1);
			// Skip VASP relaxed clusters.
			if (c.getEnergyMinimizer().getName().equals("LAMMPS") 
					&& TrainedList.contains(c.getCandidateIndex())) {
				reCalcClusters.add(c); 
			}
		}

		Thread[] reCalcThreads = new Thread[Input.getEngineThreads()];
		for (int i = 0; i < reCalcThreads.length; i++) {
			// reCalcClusters share objects with m_Candidates. Changes will be reflected.
			reCalcThreads[i] = new Thread(new ReEvaluationEngine(reCalcClusters, TrainedTimes, 
					AllCandidates, Pool), "ReEvaluate-" + Integer.toString(i));
			reCalcThreads[i].start();
		}
		
		for (Thread t: reCalcThreads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				Logger.error("Interrupted. Exit.");
				System.exit(10);
			}
		}
	}

	private static class ShutDownHook extends Thread {
		private Thread[] threads;
		
		public ShutDownHook(Thread[] threads) {
			this.threads = threads;
		}
		
		@Override
	    public void run()

	    {
	    	Status.basic("Running shutdown hook!");
	    	Thread[] stoppers = new Thread[threads.length];
	            
	        for (int i = 0; i < threads.length; i++) {
	        	Logger.debug("Stopping Thread-" + threads[i].getName());
	            stoppers[i] = new Thread(new ParallelStopper(threads[i]));
	            stoppers[i].start();
	        }
	        boolean stopped = false;
	        while (!stopped) {
	        	stopped = true;
	        	for (Thread stopper: stoppers) {
	        		stopped &= !stopper.isAlive();
	        	}
	        }
	        Start.dataDump();
	        System.out.flush();
	        System.err.flush();
	    }
	}
	
	private static class Handler implements Thread.UncaughtExceptionHandler {
		public void uncaughtException(Thread t, Throwable e) {
			Status.error("Thread-" + t.getName() + ": Uncaught exception thrown!");
			e.printStackTrace();
			System.exit(10);
		}
	}
	
}
