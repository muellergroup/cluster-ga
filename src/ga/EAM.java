package ga;

import matsci.Species;

public class EAM {
	
	static double a = 4.08;
	static double n = 10;
	static double m = 8;
	static double c = 34.408;
	static double epsilon = 0.012793;	
	static Object[] cluster;

	public EAM(Object[] c){
		cluster = c;
	}
	
	public double calculate(String type){
		if(type.equals("EAM")){
			double e_tot = 0;
			double P = 0;
			double A = 0;
			for(int i = 0; i < cluster.length; i++){
				double pot = 0;
				double att = 0;
				for(int j = 0; j < cluster.length; j++){
					if(i == j){continue;}
					double rij = this.getRij(cluster[i], cluster[j]);
					pot += Math.pow(a / rij, n);
					att += Math.pow(a / rij, m);
				}
				e_tot += epsilon*(pot / 2 - c*Math.sqrt(att));
			}
			return e_tot;
		}
		else{
			double e_tot = 0;
			for(int i = 0; i < cluster.length; i++){
				double e = 0;
				for(int j = 0; j < cluster.length; j++){
					if(j == i){continue;}
					double rij = this.getRij(cluster[i], cluster[j]);
					double att = Math.pow(rij, -6);
					double rep = Math.pow(rij, -12);
					e += (rep - att);
				}
				e_tot += 0.5 * e;
			}
			return 4*e_tot;
		}
	}
	
	public double getRij(Object object1, Object object2){
		Object[] atom1 = (Object[]) object1;
		Object[] atom2 = (Object[]) object2;
		double x = (double) atom1[1] - (double) atom2[1];
		double y = (double) atom1[2] - (double) atom2[2];
		double z = (double) atom1[3] - (double) atom2[3];
	
		double r = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
		
		return r;
	}
	
}
