package ga.utils;

import java.util.Map;

import ga.io.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * This class parses the command-line arguments to groups of options and sort them in the order of 
 * options, arguments.
 * 
 * @author ywang393
 */
public class ArgParser {
	
	public static List<String> SINGULAROPTIONS = Arrays.asList("-h", "-v", "-d", "--sortZ", 
			"--energy", "--grade");
	Map<String, String> options = new HashMap<>(10);
	List<String> arguments = new ArrayList<>(10);
	
	public ArgParser (String[] args){
		for (int i = 0; i < args.length; i++) {
			if (args[i].matches("[-]+[a-zA-Z-]+")) {
				if (SINGULAROPTIONS.contains(args[i])) {
					// Common singular option: Help info, verbose, debug, sort.
					options.put(args[i], "");
			    } else if (i + 1 < args.length) {
			    	// Options with one arguments
					options.put(args[i], args[i + 1]);
					i++;
				} else {
					options.put(args[i], ""); // Other options without args.
				}
			} else {
				// entries not following an option is viewed as argument.
				arguments.add(args[i]);
			}
		}
		
		if (options.containsKey("-d")) {
			Logger.setLogLevelDebug();
		} else if (options.containsKey("-v")) {
			Logger.setLogLevelDetail();
		}
	}
	
	public Map<String, String> getOptions() {
		return options;
	}
	
	public List<String> getArguments() {
		return arguments;
	}
	
}
