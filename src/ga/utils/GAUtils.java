package ga.utils;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import ga.HPC;
import ga.Input;
import ga.io.Logger;
import ga.minimizer.EnergyMinimizer;
import ga.minimizer.LAMMPS;
import ga.minimizer.VASP;
import ga.structure.Cluster;
import ga.structure.ClusterFactory;
import matsci.Element;
import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.basis.CartesianBasis;
import matsci.structure.Structure;
import matsci.structure.Structure.Site;
import matsci.structure.StructureBuilder;
import matsci.util.MSMath;

public class GAUtils {
	/**
	 * Calculate the distance of the farthest atom from the center of mass (i.e. size of structure).
	 * 
	 * @param struct
	 * @param radius whether consider atomic radius.
	 * @return The distance of the farthest atom from the center of mass (i.e. size of structure).
	 */
	public static double getFarthestCartDistFromCG(Structure struct, boolean useRadius) {
		double maxDistance = Double.NEGATIVE_INFINITY;
		double newDistance;
		double[] centerOfMass = GAUtils.getCGCoords(struct);
		
		for (int i = 0; i < struct.numDefiningSites(); i++) {
			Coordinates coord = struct.getDefiningSite(i).getCoords();
			newDistance = coord.distanceFrom(centerOfMass, CartesianBasis.getInstance());
			if (useRadius) {
				newDistance += GAUtils.getAtomicRadius(struct.getDefiningSite(i));
			}
				
			maxDistance = Math.max(maxDistance, newDistance);
		}
		return maxDistance;
	}

	/**
	 * Atomic weight is counted in. getCentroid() won't take account of atomic weights.
	 * @param struct
	 * @return The Cartesian coordinates of the center of mass of given structure.
	 */
	public static double[] getCGCoords(Structure struct) {
		double[] centerOfMass = {0., 0., 0.};
		double totalMass = 0.;
		
		double weight; // temporary variable to avoid defining new local variables;
		double[] cartCoords;
		for (int i = 0; i < struct.numDefiningSites(); i++) {
			weight = struct.getDefiningSite(i).getSpecies().getElement().getAtomicWeight();
			cartCoords = struct.getDefiningSite(i).getCoords().getCartesianArray();
			totalMass += weight;
			centerOfMass = MSMath.arrayAdd(centerOfMass, MSMath.arrayMultiply(cartCoords, weight));
		}
		centerOfMass = MSMath.arrayMultiply(centerOfMass, 1. / totalMass);
		return centerOfMass;
	}
	
	/**
	 * Get center of mass of the given cluster. Don't change the given list.
	 * @param clusList
	 * @return
	 */
	public static double[] getCGCoords(Object[] clusList) {
		double[] centerOfMass = {0., 0., 0.};
		double totalMass = 0., weight;
		double[] cartCoords = new double[3];
		for (int i = 0; i < clusList.length; i++) {
			Object[] atom = (Object[]) clusList[i];
			weight = Element.getElement((String) atom[0]).getAtomicWeight();
			cartCoords[0] = (double) atom[1];
			cartCoords[1] = (double) atom[2];
			cartCoords[2] = (double) atom[3];
			totalMass += weight;
			centerOfMass = MSMath.arrayAdd(centerOfMass, MSMath.arrayMultiply(cartCoords, weight));
		}
		centerOfMass = MSMath.arrayMultiply(centerOfMass, 1. / totalMass);
		return centerOfMass;
	}
	
	/**
	 * Calculate interatomic distance between any pair of atoms. 
	 * @param structure
	 * @return A square matrix, whose (i,j) term represent distance between atom_i and atom_j.
	 */
	public static double[][] getBonds(Structure structure) {
		int numSites = structure.numDefiningSites();
		double[][] bonds = new double[numSites][numSites];
		for (int i = 0; i < numSites; i++) {
			bonds[i][i] = 0;
			for (int j = 0; j < i; j++) {
				double d = structure.getSiteCoords(i).distanceFrom(structure.getSiteCoords(j));
				bonds[i][j] = bonds[j][i] = d;
			}
		}
		return bonds;
	}
	
	public static boolean isOverlapped(Structure struct, double[] cartesianCoord, String symbol) {
		return isOverlapped(struct, new Coordinates(cartesianCoord, CartesianBasis.getInstance()), symbol);
	}
	
	public static boolean isOverlapped(Structure struct, Coordinates coord, String symbol) {
		StructureBuilder builder = new StructureBuilder(struct);
		builder.addSite(coord, Species.get(symbol));
		return isOverlapped(new Structure(builder));
	}
	
	/**
	 * Allow to some extent overlapping (70% nearest neighbor distance)
	 * @param struct
	 * @return
	 */

	public static boolean isOverlapped(Structure struct) {
		double minimumDistance = Double.POSITIVE_INFINITY;
		for (int i = 0; i < struct.numDefiningSites(); i++) {
			for(int j = 0; j < struct.numDefiningSites(); j++) {
				Site site1 = struct.getDefiningSite(i);
				Site site2 = struct.getDefiningSite(j);
				
				minimumDistance = getNNDistance(site1.getSpecies().toString(), site2.getSpecies().toString()) * 0.90;
				if (site1.getCoords().distanceFrom(site2.getCoords()) < minimumDistance) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isOverlap(Object[] clusList) {
		for (int i = 0; i < clusList.length; i++) {
			for (int j = i + 1; j < clusList.length; j++) {
				Object[] atom1 = (Object[]) clusList[i];
				Object[] atom2 = (Object[]) clusList[j];
				if (GAUtils.isOverlap(atom1, atom2)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Allow to some extent overlapping (70% nearest neighbor distance)
	 * @param atom1
	 * @param atom2
	 * @return
	 */
	public static boolean isOverlap(Object[] atom1, Object[] atom2) {
		double allowedDistance = getNNDistance((String) atom1[0], (String) atom2[0]);
		double[] r1 = new double[]{ (double) atom1[1], (double) atom1[2], (double) atom1[3] };
		double[] r2 = new double[]{ (double) atom2[1], (double) atom2[2], (double) atom2[3] };
		return isOverlap(r1, r2, allowedDistance);
	}

	public static boolean isOverlap(double[] r1, double[] r2, double allowedDistance) {
		if (GAMath.distance(r1, r2) < allowedDistance * ClusterFactory.OVERLAPTHRESHOLD - ClusterFactory.PRECISION) {
			return true;
		}
		return false;
	}
	
	/**
	 * Calculate the minimum distance of a coordinate to a structure. Negative value indicates the
	 * coordinate is within the contour of the structure.
	 * 
	 * @param struct
	 * @param coords Cartesian coordinates
	 * @param symbol
	 * @param useRadius if false, treat atom as size-less point. Otherwise, treat as balls.
	 * @return
	 */
	public static double distanceFrom(Structure struct, double[] coords, String symbol, boolean useRadius) {
		Coordinates c = new Coordinates(coords, CartesianBasis.getInstance());
		return distanceFrom(struct, c, symbol, useRadius);
	}
	
	public static double distanceFrom(Structure struct, Coordinates coord, String symbol, boolean useRadius) {
		double minimumDistance = Double.POSITIVE_INFINITY, newDistance;
		for (int i = 0; i < struct.numDefiningSites(); i++) {
			Site site = struct.getDefiningSite(i);
			newDistance = coord.distanceFrom(site.getCoords());
			if (useRadius) {
				newDistance -= (getAtomicRadius(site) + getAtomicRadius(symbol));
			}
			minimumDistance = Math.min(minimumDistance, newDistance);
		}
		return minimumDistance;
	}
	
	/**
	 * Calculate minimal distance between one atom from a cluster. Not atomic radius is not subtracted.
	 * Treat coordinates as points.
	 * 
	 * @param coords
	 * @param atom
	 */
	public static double distanceFrom(Object[] clusList, Object[] atom) {
		double distance = Double.MAX_VALUE;
		for (int i = 0; i < clusList.length; i++) {
			Object[] clusterAtom = (Object[]) clusList[i];
			distance = Math.min(distance, distance(clusterAtom, atom));
		}
		return distance;
	}
	
	/**
	 * Distance between two atoms. TODO: use matsci for atoms.
	 * @param atom1 Coordinates are in Cartesian system.
	 * @param atom2 Coordinates are in Cartesian system.
	 * @return
	 */
	public static double distance(Object[] atom1, Object[] atom2) {
		double[] r1 = new double[]{ (double) atom1[1], (double) atom1[2], (double) atom1[3] };
		double[] r2 = new double[]{ (double) atom2[1], (double) atom2[2], (double) atom2[3] };
		return GAMath.distance(r1, r2);
	}
	
	public static Map<String, Double> getNNDistanceMap() {
		return m_NearestNeighborDistance;
	}
	
	public static double getNNDistance(String symbol) {
		Double dist = m_NearestNeighborDistance.get(symbol);
		if (dist == null) {
			Logger.error("No nearest neighbor distance for " + symbol);
			System.exit(6); // Cluster generation error.
		}
		return dist;
	}
	
	public static double getNNDistance(String e1, String e2) {
		// Might be wrong for pair of elements with large electronegativity difference.
		return getAtomicRadius(e1) + getAtomicRadius(e2);
	}
	
	/**
	 * Find the index of the minimum element in the array.
	 * @param arr Has to have at least one element.
	 * @return
	 */
	public static int locateMin(double[] arr) {
		int index = -1;
		double min = Double.MAX_VALUE;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < min) {
				index = i;
				min = arr[i];
			}
		}
		return index;
	}
	
	/**
	 * Find the index of the maximum element in the array.
	 * @param arr Has to have at least one element.
	 * @return
	 */
	public static int locateMax(double[] arr) {
		int index = -1;
		double max = -Double.MAX_VALUE;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > max) {
				index = i;
				max = arr[i];
			}
		}
		return index;
	}
	
	public static Object[] getMax(double[] inputArray) {
		// This method returns the maximum element of an array
		double maxValue = inputArray[0];
		int maxIndex = 0;
		for (int i = 1; i < inputArray.length; i++) {
			if (inputArray[i] > maxValue) {
				maxValue = inputArray[i];
				maxIndex = i;
			}
		}
		Object[] maxOut = {maxIndex,maxValue};
		return maxOut;
	}

	public static Object[] getMin(double[] inputArray) {
		// This method returns the minimum element of an array
		double minValue = inputArray[0];
		int minIndex = 0;
		for (int i = 1; i < inputArray.length; i++) {
			if (inputArray[i] < minValue) {
				minValue = inputArray[i];
				minIndex = i;
			}
		}
		Object[] minOut = {minIndex,minValue};
		return minOut;
	}

	/**
	 * 
	 * @param str Object[] or ArrayList representing a structure, with each entry an atom in the structure.
	 * @param point double[] giving Cartesian coordinates of a point.
	 * @return An object, maxData. maxData[0] contains the index of the farthest atom
	 *  in the provided structure to the specified point, and maxData[1] contains
	 *  the distance between the farthest atom and the specified point.
	 */
	public static Object[] findFarthest(Object[] str, double[] point) {
		double[] distArray = new double[str.length];
		for (int i = 0; i < str.length; i++) {
			Object[] atom = (Object[]) str[i];
			double[] coord = { (double) atom[1], (double) atom[2], (double) atom[3] };
			distArray[i] = GAMath.distance(coord, point);
		}
		Object[] maxData = getMax(distArray);
		return maxData;
	}
	
	public static Object[] findFarthest(ArrayList<Object> str, double[] point) {
		double[] distArray = new double[str.size()];
		for (int i=0; i<str.size(); i++) {
			Object[] atom = (Object[]) str.get(i);
			double[] coord = { (double) atom[1], (double) atom[2], (double) atom[3] };
			distArray[i] = GAMath.distance(coord, point);
		}
		Object[] maxData = getMax(distArray);
		return maxData;
	}
	
	/**
	 * 
	 * @param str Object[] or ArrayList representing a structure, with each entry an atom in the structure.
	 * @param point double[] giving Cartesian coordinates of a point.
	 * @return An object, minData. minData[0] contains the index of the nearest atom
	 *  in the provided structure to the specified point, and minData[1] contains
	 *  the distance between the nearest atom and the specified point.
	 */
	public static Object[] findNearest(Object[] str, double[] point) {
		double[] distArray = new double[str.length];
		for (int i = 0; i < str.length; i++) {
			Object[] atom = (Object[]) str[i];
			double[] coord = { (double) atom[1], (double) atom[2], (double) atom[3] };
			distArray[i] = GAMath.distance(coord, point);
		}
		Object[] minData = getMin(distArray);
		return minData;
	}

	public static Object[] findNearest(ArrayList<Object> str, double[] point) {
		double[] distArray = new double[str.size()];
		for (int i=0; i<str.size(); i++) {
			Object[] atom = (Object[]) str.get(i);
			double[] coord = { (double) atom[1], (double) atom[2], (double) atom[3] };
			distArray[i] = GAMath.distance(coord, point);
		}
		Object[] minData = getMin(distArray);
		return minData;
	}
	
	public static double[] interatomicDistances(Object[] coords) {
		double[] distArray = new double[coords.length * (coords.length - 1)/2];
		int counter = 0;
		for (int i = 0; i < coords.length; i++) {
			Object[] atom1 = (Object[]) coords[i];
			for (int j = 0; j < i; j++) {
				Object[] atom2 = (Object[]) coords[j];
				distArray[counter] = GAUtils.distance(atom1, atom2);
				counter++;
			}
		}
		return distArray;
	}
	
	public static double[] getNormalizeVector(double[] u) {
		double uMag = getMagnitude(u);
		double[] uNorm = new double[] { u[0] / uMag, u[1] / uMag, u[2] / uMag };
		return uNorm;
	}

	public static double getMagnitude(double[] u) {
		double uMag = Math.sqrt(u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
		return uMag;
	}

	public static double[] getCrossProduct(double[] u, double v[]) {
		double[] uv = new double[3];
		uv[0] = u[1] * v[2] - v[1] * u[2];
		uv[1] = v[0] * u[2] - u[0] * v[2];
		uv[2] = u[0] * v[1] - v[0] * u[1];
		return uv;
	}
	
	public static double getAtomicRadius(String symbol) {
		return getNNDistance(symbol) * 0.5;
	}
	
	public static double getAtomicRadius(Site s) {
		return getAtomicRadius(s.getSpecies().getSymbol());
	}
	
	/**
	 * Test whether a list of cluster contains the given one. Candidate index is better than the
	 * List.contains(), since the latter compares references. When relaxation fails (as in 
	 * Engine.run()), a new cluster is generated and the reference changes. Using List.contains()
	 * would give false in this case, even if the old cluster sharing the same index is already in
	 * the list.
	 * 
	 * TODO: Make it thread-safe. 
	 * 
	 * @param list
	 * @param target
	 * @return
	 */
	public static boolean contains(Collection<Cluster> list, Cluster target) {
		int targetIndex = target.getCandidateIndex();
		for (Cluster c: list) {
			if (c.getCandidateIndex() == targetIndex) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Determine calculation type of given folder by mode and checking files in calcDir.
	 * VASP precedes LAMMPS. Unless mode doesn't match neither of "VASP", "LAMMPS" or "Active",
	 * a minimizer type can always be found.
	 * 
	 * @param mode
	 * @param calcDir
	 * @return VASP or LAMMPS, if corresponding output/input structure files have been found;
	 *         null, if mode doesn't match "VASP", "LAMMPS", or "Active".
	 */
	public static String findMinimizer(String mode, String calcDir) {
		if (mode.equals("VASP")) {
			return "VASP";
		} else if (mode.equals("LAMMPS")) {
			return "LAMMPS";
		} else if (mode.equalsIgnoreCase("Active")) {
			if (Files.exists(Paths.get(calcDir + HPC.FILESPTR + VASP.getOutputFileName()))) {
				return "VASP";
			} else if (Files.exists(Paths.get(calcDir + HPC.FILESPTR + LAMMPS.getOutputFileName()))){
				return "LAMMPS";
			} else if (Files.exists(Paths.get(calcDir + HPC.FILESPTR + VASP.getInputFileName()))) {
	    		return "VASP";
	    	} else if (Files.exists(Paths.get(calcDir + HPC.FILESPTR + LAMMPS.getStructureFileName(false)))) {
	    		// input.lmp might be deleted.
	    		return "LAMMPS";
	    	} else {
	    		return "LAMMPS";
	    	}
		}
		return null;
	}
	
	public static EnergyMinimizer findMinimizer(String mode, Cluster c) {
		String minimizer = GAUtils.findMinimizer(mode, c.getCalcDir());
		if (minimizer != null) {
			if (minimizer.equals("VASP")) {
				return (EnergyMinimizer) new VASP(c, Input.getVASPEXE(), Input.isParallel(), Input.getVASPMPI());
			} else if (minimizer.equals("LAMMPS")) {
				return (EnergyMinimizer) new LAMMPS(c, Input.getLAMMPSEXE(), Input.isParallel(), Input.getLAMMPSMPI());
			}
		}
		return null;
	}
	
	public static boolean equal(Object[] cluster1, Object[] cluster2) {
		double cellSize = ClusterFactory.getCellSize(cluster1, 10); // Use a safe value for distance of periodic image.
		Structure s1 = ClusterFactory.objectToStructure(cluster1, cellSize);
		Structure s2 = ClusterFactory.objectToStructure(cluster2, cellSize);
		return equal(s1, s2);
	}
	
	/**
	 * Check site species and site coordinates for equality between two structures.
	 * If sites are re-ordered, they are still considered as equal.
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static boolean equal(Structure s1, Structure s2) {
		if (s1.numDefiningSites() != s2.numDefiningSites()) {
			return false;
		}
		
		for (int i = 0; i < s1.numDefiningSites(); i++) {
			Site a = s1.getDefiningSite(i);
			boolean found = false;
			for (int j = 0; j < s2.numDefiningSites(); j++) {
				Site b = s2.getDefiningSite(j);
				if (!a.getSpecies().getSymbol().equals(b.getSpecies().getSymbol())) {
					continue;
				}
				if (!a.getCoords().isCloseEnoughTo(b.getCoords())) {
					continue;
				}
				found = true;
				break;
			}
			if (!found) {
				return false;
			}
		}
		return true;
	}
	
	public static void assertion(boolean condition) {
		if (!condition) {
			throw new AssertionError();
		}
	}
	
	public static void assertion(boolean condition, String m1, String m2) {
		if (!condition) {
			throw new AssertionError(m1 + " " + m2);
		}
	}
	
	/**
	 * Remove unnecessary files from each calculation to reduce file count.
	 * This function should be called whenever exiting a calculation folder, e.g. add to retraining
	 * list because of extrapolation.
	 */
	public static void cleanFolder(String minimizerType, String calcDir) {
		String[] targets = VASP.REDUNDANTFILELIST; // default
		if (minimizerType.equals("LAMMPS")) {
			targets = LAMMPS.REDUNDANTFILELIST;
		}
		for (String target: targets) {
			HPC.delete(calcDir + HPC.FILESPTR + target, true);
		}
	}
	
	/**
	 * Get the index of the highest energy clusters in the list. 
	 * 
	 * @param clusters
	 * @return index, the index of most energetic cluster in the list.
	 * 	       -1, if the list is null or empty.
	 */
	public static int getHighestEnergyClusterIndex(List<Cluster> clusters) {
		int index = -1;
		double highestEnergy = Double.NEGATIVE_INFINITY;
		if (clusters == null || clusters.size() == 0) {
			return index;
		}
		for (int i = 0; i < clusters.size(); i++) {
			double energy = clusters.get(i).getEnergy();
			if (energy > highestEnergy) {
				index = i;
				highestEnergy = energy;
			}
		}
		return index;
	}
	
	public static final HashMap<String, double[]> m_RingParameters = new HashMap<>();
	
	static {
	    // Octasulfur ring from NIST: https://cccbdb.nist.gov/listgeomexp.asp?casno=10544500&charge=0
	    m_RingParameters.put("S", new double[] {2.059, 107.9});
	}
	
	public static final HashMap<String, Double> m_NearestNeighborDistance = new HashMap<>();
	
	// Alberto's update on 2020-07-24.
	static {
		m_NearestNeighborDistance.put("Ag",2.895912588);
		m_NearestNeighborDistance.put("Al",2.855954246);
		m_NearestNeighborDistance.put("As",2.550183123);
		m_NearestNeighborDistance.put("Au",2.915931463);
		m_NearestNeighborDistance.put("B",1.666024657);
		m_NearestNeighborDistance.put("Ba",4.356367038);
		m_NearestNeighborDistance.put("Be",2.20136404);
		m_NearestNeighborDistance.put("Bi",3.099440673);
		m_NearestNeighborDistance.put("C",1.421377872);
		m_NearestNeighborDistance.put("Ca",3.817938091);
		m_NearestNeighborDistance.put("Cd",2.978816317);
		m_NearestNeighborDistance.put("Co",2.458546714);
		m_NearestNeighborDistance.put("Cr",2.446798984);
		m_NearestNeighborDistance.put("Cs",4.631935958);
		m_NearestNeighborDistance.put("Cu",2.548771005);
		m_NearestNeighborDistance.put("Fe",2.442529461);
		m_NearestNeighborDistance.put("Ga",2.525537973);
		m_NearestNeighborDistance.put("Ge",2.500974237);
		// Aevrage of NIST CH bond database. https://cccbdb.nist.gov/expbondlengths2x.asp?descript=rCH&all=0
		// TODO: find a better one.
		m_NearestNeighborDistance.put("H",1.091);
		m_NearestNeighborDistance.put("Hf",3.122809934);
		m_NearestNeighborDistance.put("Hg",3.291588741);
		m_NearestNeighborDistance.put("In",3.279844457);
		m_NearestNeighborDistance.put("Ir",2.731445828);
		m_NearestNeighborDistance.put("K",4.145087132);
		m_NearestNeighborDistance.put("Li",2.967710914);
		m_NearestNeighborDistance.put("Mg",3.150177715);
		m_NearestNeighborDistance.put("Mn",2.127518436);
		m_NearestNeighborDistance.put("Mo",2.721703902);
		m_NearestNeighborDistance.put("Na",3.383385287);
		m_NearestNeighborDistance.put("Nb",2.870737754);
		m_NearestNeighborDistance.put("Ni",2.468509786);
		m_NearestNeighborDistance.put("Os",2.684333914);
		m_NearestNeighborDistance.put("P",2.197731703);
		m_NearestNeighborDistance.put("Pb",3.555145202);
		m_NearestNeighborDistance.put("Pd",2.774875113);
		m_NearestNeighborDistance.put("Pt",2.794794806);
		m_NearestNeighborDistance.put("Rb",4.492154138);
		m_NearestNeighborDistance.put("Re",2.747669514);
		m_NearestNeighborDistance.put("Rh",2.694162508);
		m_NearestNeighborDistance.put("Ru",2.64001643);
		m_NearestNeighborDistance.put("S",2.05942439);
		m_NearestNeighborDistance.put("Sb",3.083375591);
		m_NearestNeighborDistance.put("Sc",3.210087076);
		m_NearestNeighborDistance.put("Se",2.361712782);
		m_NearestNeighborDistance.put("Si",2.366088229);
		m_NearestNeighborDistance.put("Sn",2.877918);
		m_NearestNeighborDistance.put("Sr",4.236609066);
		m_NearestNeighborDistance.put("Ta",2.858983888);
		m_NearestNeighborDistance.put("Te",2.903174431);
		m_NearestNeighborDistance.put("Ti",2.627394975);
		m_NearestNeighborDistance.put("Tl",3.421144669);
		m_NearestNeighborDistance.put("V",2.570187729);
		m_NearestNeighborDistance.put("W",2.740305481);
		m_NearestNeighborDistance.put("Y",3.54112206);
		m_NearestNeighborDistance.put("Zn",2.611720936);
		m_NearestNeighborDistance.put("Zr",3.162049349);
	}
	
	/* 2019.09-2020.07: Used in initial GA calculations
	static{
	    m_NearestNeighborDistance.put("Li", 3.052288103);
	    m_NearestNeighborDistance.put("H", 1.09); //very approximate value--find a better one.
	    m_NearestNeighborDistance.put("Be", 2.253787048);
	    m_NearestNeighborDistance.put("B", 1.758949964);
	    m_NearestNeighborDistance.put("C", 1.424415172);
	    m_NearestNeighborDistance.put("Na", 3.667617585);
	    m_NearestNeighborDistance.put("Mg", 3.182702404);
	    m_NearestNeighborDistance.put("Al", 2.856480393);
	    m_NearestNeighborDistance.put("Si", 2.368359252);
	    m_NearestNeighborDistance.put("P", 2.211288895);
	    m_NearestNeighborDistance.put("S", 2.064449522);
	    m_NearestNeighborDistance.put("K", 4.56690264);
	    m_NearestNeighborDistance.put("Ca", 3.893484771);
	    m_NearestNeighborDistance.put("Sc", 3.252455607);
	    m_NearestNeighborDistance.put("Ti", 2.882881953);
	    m_NearestNeighborDistance.put("V", 2.76940869);
	    m_NearestNeighborDistance.put("Cr", 2.630176752);
	    m_NearestNeighborDistance.put("Mn", 2.634143013);
	    m_NearestNeighborDistance.put("Fe", 2.620746238);
	    m_NearestNeighborDistance.put("Co", 2.487239034);
	    m_NearestNeighborDistance.put("Ni", 2.492056429);
	    m_NearestNeighborDistance.put("Cu", 2.557080896);
	    m_NearestNeighborDistance.put("Zn", 2.767854217);
	    m_NearestNeighborDistance.put("Ga", 2.530369679);
	    m_NearestNeighborDistance.put("Ge", 2.449444601);
	    m_NearestNeighborDistance.put("As", 2.558618459);
	    m_NearestNeighborDistance.put("Se", 2.40800687);
	    m_NearestNeighborDistance.put("Rb", 4.887644536);
	    m_NearestNeighborDistance.put("Sr", 4.296380802);
	    m_NearestNeighborDistance.put("Y", 3.531404656);
	    m_NearestNeighborDistance.put("Zr", 3.17927016);
	    m_NearestNeighborDistance.put("Nb", 3.068578181);
	    m_NearestNeighborDistance.put("Mo", 2.731611457);
	    m_NearestNeighborDistance.put("Ru", 2.665702009);
	    m_NearestNeighborDistance.put("Rh", 2.708049615);
	    m_NearestNeighborDistance.put("Pd", 2.788789918);
	    m_NearestNeighborDistance.put("Ag", 2.931930289);
	    m_NearestNeighborDistance.put("Cd", 3.136498438);
	    m_NearestNeighborDistance.put("In", 3.38958482);	    
	    m_NearestNeighborDistance.put("Sn", 2.881365722);	    
	    m_NearestNeighborDistance.put("Sb", 3.176964962);	    
	    m_NearestNeighborDistance.put("Te", 2.892380633);	    
	    m_NearestNeighborDistance.put("Cs", 5.457903378);	    
	    m_NearestNeighborDistance.put("Ba", 4.356620227);	    
	    m_NearestNeighborDistance.put("Hf", 3.13249076);	    
	    m_NearestNeighborDistance.put("Ta", 2.87584524);	    
	    m_NearestNeighborDistance.put("W", 2.940696097);	    
	    m_NearestNeighborDistance.put("Re", 2.740007497);	    
	    m_NearestNeighborDistance.put("Os", 2.726898516);	    
	    m_NearestNeighborDistance.put("Ir", 2.737567141);	    
	    m_NearestNeighborDistance.put("Pt", 2.805971474);	    
	    m_NearestNeighborDistance.put("Au", 2.939816494);	    
	    m_NearestNeighborDistance.put("Hg", 3.538772091);	    
	    m_NearestNeighborDistance.put("Tl", 3.487923461);	    
	    m_NearestNeighborDistance.put("Pb", 3.563703704);	    
	    m_NearestNeighborDistance.put("Bi", 3.337898568);	    
	}
	*/

	/*
	// Peter's orginal values:
    static {
        Element.lithium.m_nearestNeighborDistance = 3.052288103;
        Element.beryllium.m_nearestNeighborDistance = 2.253787048;
        Element.boron.m_nearestNeighborDistance = 1.758949964;
        Element.carbon.m_nearestNeighborDistance = 1.424415172;
        Element.sodium.m_nearestNeighborDistance = 3.667617585;
        Element.magnesium.m_nearestNeighborDistance = 3.182702404;
        Element.aluminum.m_nearestNeighborDistance = 2.856480393;
        Element.silicon.m_nearestNeighborDistance = 2.368359252;
        Element.phosphorus.m_nearestNeighborDistance = 2.211288895;
        Element.sulfur.m_nearestNeighborDistance = 2.064449522;
        Element.potassium.m_nearestNeighborDistance = 4.56690264;
        Element.calcium.m_nearestNeighborDistance = 3.893484771;
        Element.scandium.m_nearestNeighborDistance = 3.252455607;
        Element.titanium.m_nearestNeighborDistance = 2.882881953;
        Element.vanadium.m_nearestNeighborDistance = 2.76940869;
        Element.chromium.m_nearestNeighborDistance = 2.630176752;
        Element.manganese.m_nearestNeighborDistance = 2.634143013;
        Element.iron.m_nearestNeighborDistance = 2.620746238;
        Element.cobalt.m_nearestNeighborDistance = 2.487239034;
        Element.nickel.m_nearestNeighborDistance = 2.492056429;
        Element.copper.m_nearestNeighborDistance = 2.557080896;
        Element.zinc.m_nearestNeighborDistance = 2.767854217;
        Element.gallium.m_nearestNeighborDistance = 2.530369679;
        Element.germanium.m_nearestNeighborDistance = 2.449444601;
        Element.arsenic.m_nearestNeighborDistance = 2.558618459;
        Element.selenium.m_nearestNeighborDistance = 2.40800687;
        Element.rubidium.m_nearestNeighborDistance = 4.887644536;
        Element.strontium.m_nearestNeighborDistance = 4.296380802;
        Element.yttrium.m_nearestNeighborDistance = 3.531404656;
        Element.zirconium.m_nearestNeighborDistance = 3.17927016;
        Element.niobium.m_nearestNeighborDistance = 3.068578181;
        Element.molybdenum.m_nearestNeighborDistance = 2.731611457;
        Element.ruthenium.m_nearestNeighborDistance = 2.665702009;
        Element.rhodium.m_nearestNeighborDistance = 2.708049615;
        Element.palladium.m_nearestNeighborDistance = 2.788789918;
        Element.silver.m_nearestNeighborDistance = 2.931930289;
        Element.cadmium.m_nearestNeighborDistance = 3.136498438;
        Element.indium.m_nearestNeighborDistance = 3.38958482;
        Element.tin.m_nearestNeighborDistance = 2.881365722;
        Element.antimony.m_nearestNeighborDistance = 3.176964962;
        Element.tellurium.m_nearestNeighborDistance = 2.892380633;
        Element.cesium.m_nearestNeighborDistance = 5.457903378;
        Element.barium.m_nearestNeighborDistance = 4.356620227;
        Element.hafnium.m_nearestNeighborDistance = 3.13249076;
        Element.tantalum.m_nearestNeighborDistance = 2.87584524;
        Element.tungsten.m_nearestNeighborDistance = 2.940696097;
        Element.rhenium.m_nearestNeighborDistance = 2.740007497;
        Element.osmium.m_nearestNeighborDistance = 2.726898516;
        Element.iridium.m_nearestNeighborDistance = 2.737567141;
        Element.platinum.m_nearestNeighborDistance = 2.805971474;
        Element.gold.m_nearestNeighborDistance = 2.939816494;
        Element.mercury.m_nearestNeighborDistance = 3.538772091;
        Element.thallium.m_nearestNeighborDistance = 3.487923461;
        Element.lead.m_nearestNeighborDistance = 3.563703704;
        Element.bismuth.m_nearestNeighborDistance = 3.337898568;
        // Made with tolerance 0.75

//    Element.lithium.m_nearestNeighborDistance = 3.054411157840862;
//    Element.beryllium.m_nearestNeighborDistance = 2.239369331778049;
//    Element.boron.m_nearestNeighborDistance = 1.758949963641892;
//    Element.carbon.m_nearestNeighborDistance = 1.4244151718258145;
//    Element.sodium.m_nearestNeighborDistance = 3.910781477158341;
//    Element.magnesium.m_nearestNeighborDistance = 3.1827024035011653;
//    Element.aluminum.m_nearestNeighborDistance = 2.8564803926778324;
//    Element.silicon.m_nearestNeighborDistance = 2.3683592522264947;
//    Element.phosphorus.m_nearestNeighborDistance = 2.211288895265709;
//    Element.sulfur.m_nearestNeighborDistance = 2.0644495215600687;
//    Element.potassium.m_nearestNeighborDistance = 4.869689338272209;
//    Element.calcium.m_nearestNeighborDistance = 3.9163950214018253;
//    Element.scandium.m_nearestNeighborDistance = 3.27293585108445;
//    Element.titanium.m_nearestNeighborDistance = 2.9725176396673296;
//    Element.vanadium.m_nearestNeighborDistance = 2.7694086900054287;
//    Element.chromium.m_nearestNeighborDistance = 2.6301767518704673;
//    Element.manganese.m_nearestNeighborDistance = 2.6497398367817677;
//    Element.iron.m_nearestNeighborDistance = 2.6207462377245068;
//    Element.cobalt.m_nearestNeighborDistance = 2.487239033964297;
//    Element.nickel.m_nearestNeighborDistance = 2.4789549822280956;
//    Element.copper.m_nearestNeighborDistance = 2.5570808956002935;
//    Element.zinc.m_nearestNeighborDistance = 2.790131587978324;
//    Element.gallium.m_nearestNeighborDistance = 2.7292109808774074;
//    Element.germanium.m_nearestNeighborDistance = 2.496049855347901;
//    Element.arsenic.m_nearestNeighborDistance = 2.8527915296961126;
//    Element.selenium.m_nearestNeighborDistance = 2.4080068701967603;
//    Element.rubidium.m_nearestNeighborDistance = 4.887644536183991;
//    Element.strontium.m_nearestNeighborDistance = 4.253324731739055;
//    Element.yttrium.m_nearestNeighborDistance = 3.5837770081428686;
//    Element.zirconium.m_nearestNeighborDistance = 3.2111049784796664;
//    Element.niobium.m_nearestNeighborDistance = 3.0685781813163886;
//    Element.molybdenum.m_nearestNeighborDistance = 2.9127179267716916;
//    Element.ruthenium.m_nearestNeighborDistance = 2.690330599003738;
//    Element.rhodium.m_nearestNeighborDistance = 2.708049614604508;
//    Element.palladium.m_nearestNeighborDistance = 2.788789917660843;
//    Element.silver.m_nearestNeighborDistance = 2.9360728222051695;
//    Element.cadmium.m_nearestNeighborDistance = 3.2028526387335607;
//    Element.indium.m_nearestNeighborDistance = 3.3895848201012733;
//    Element.tin.m_nearestNeighborDistance = 2.8813657223574847;
//    Element.antimony.m_nearestNeighborDistance = 3.176964961946613;
//    Element.tellurium.m_nearestNeighborDistance = 3.3063713896498452;
//    Element.cesium .m_nearestNeighborDistance = 5.275903377940726;
//    Element.barium.m_nearestNeighborDistance = 4.645465153208435;
//    Element.hafnium.m_nearestNeighborDistance = 3.1671496648178965;
//    Element.tantalum.m_nearestNeighborDistance = 3.0665144426564614;
//    Element.tungsten.m_nearestNeighborDistance = 2.9406960967893783;
//    Element.rhenium.m_nearestNeighborDistance = 2.770837341958717;
//    Element.osmium.m_nearestNeighborDistance = 2.726898515613693;
//    Element.iridium.m_nearestNeighborDistance = 2.7375671414839746;
//    Element.platinum.m_nearestNeighborDistance = 2.8059714743694504;
//    Element.gold.m_nearestNeighborDistance = 2.9398164935668825;
//    Element.mercury.m_nearestNeighborDistance = 3.5387720905404465;
//    Element.thallium.m_nearestNeighborDistance = 3.5440523479286803;
//    Element.lead.m_nearestNeighborDistance = 3.5637037035516363;
//    Element.bismuth.m_nearestNeighborDistance = 3.3378985675136827;
//
//    
//    
        // Made with tolerance 0.5

//    Element.lithium.m_nearestNeighborDistance = 3.054411157840862;
//    Element.beryllium.m_nearestNeighborDistance = 2.239369331778049;
//    Element.boron.m_nearestNeighborDistance = 1.758949963641892;
//    Element.carbon.m_nearestNeighborDistance = 1.4244151718258145;
//    Element.sodium.m_nearestNeighborDistance = 3.6676175850270982;
//    Element.magnesium.m_nearestNeighborDistance = 3.1827024035011653;
//    Element.aluminum.m_nearestNeighborDistance = 2.8564803926778324;
//    Element.silicon.m_nearestNeighborDistance = 2.3683592522264947;
//    Element.phosphorus.m_nearestNeighborDistance = 2.211288895265709;
//    Element.sulfur.m_nearestNeighborDistance = 2.0644495215600687;
//    Element.potassium.m_nearestNeighborDistance = 4.566902639531702;
//    Element.calcium.m_nearestNeighborDistance = 3.9163950214018253;
//    Element.scandium.m_nearestNeighborDistance = 3.27293585108445;
//    Element.titanium.m_nearestNeighborDistance = 2.9725176396673296;
//    Element.vanadium.m_nearestNeighborDistance = 2.7694086900054287;
//    Element.chromium.m_nearestNeighborDistance = 2.6301767518704673;
//    Element.manganese.m_nearestNeighborDistance = 2.6497398367817677;
//    Element.iron.m_nearestNeighborDistance = 2.6207462377245068;
//    Element.cobalt.m_nearestNeighborDistance = 2.487239033964297;
//    Element.nickel.m_nearestNeighborDistance = 2.4789549822280956;
//    Element.copper.m_nearestNeighborDistance = 2.5570808956002935;
//    Element.zinc.m_nearestNeighborDistance = 2.790131587978324;
//    Element.gallium.m_nearestNeighborDistance = 2.7292109808774074;
//    Element.germanium.m_nearestNeighborDistance = 2.496049855347901;
//    Element.arsenic.m_nearestNeighborDistance = 2.5586184591102463;
//    Element.selenium.m_nearestNeighborDistance = 2.4080068701967603;
//    Element.rubidium.m_nearestNeighborDistance = 4.887644536183991;
//    Element.strontium.m_nearestNeighborDistance = 4.253324731739055;
//    Element.yttrium.m_nearestNeighborDistance = 3.5837770081428686;
//    Element.zirconium.m_nearestNeighborDistance = 3.2111049784796664;
//    Element.niobium.m_nearestNeighborDistance = 3.0685781813163886;
//    Element.molybdenum.m_nearestNeighborDistance = 2.9127179267716916;
//    Element.ruthenium.m_nearestNeighborDistance = 2.690330599003738;
//    Element.rhodium.m_nearestNeighborDistance = 2.708049614604508;
//    Element.palladium.m_nearestNeighborDistance = 2.788789917660843;
//    Element.silver.m_nearestNeighborDistance = 2.9360728222051695;
//    Element.cadmium.m_nearestNeighborDistance = 3.2028526387335607;
//    Element.indium.m_nearestNeighborDistance = 3.3895848201012733;
//    Element.tin.m_nearestNeighborDistance = 2.8813657223574847;
//    Element.antimony.m_nearestNeighborDistance = 3.176964961946613;
//    Element.tellurium.m_nearestNeighborDistance = 2.892380633394989;
//    Element.cesium .m_nearestNeighborDistance = 5.275903377940726;
//    Element.barium.m_nearestNeighborDistance = 4.356620226941927;
//    Element.hafnium.m_nearestNeighborDistance = 3.1671496648178965;
//    Element.tantalum.m_nearestNeighborDistance = 3.0665144426564614;
//    Element.tungsten.m_nearestNeighborDistance = 2.9406960967893783;
//    Element.rhenium.m_nearestNeighborDistance = 2.770837341958717;
//    Element.osmium.m_nearestNeighborDistance = 2.726898515613693;
//    Element.iridium.m_nearestNeighborDistance = 2.7375671414839746;
//    Element.platinum.m_nearestNeighborDistance = 2.8059714743694504;
//    Element.gold.m_nearestNeighborDistance = 2.9398164935668825;
//    Element.mercury.m_nearestNeighborDistance = 3.5387720905404465;
//    Element.thallium.m_nearestNeighborDistance = 3.5440523479286803;
//    Element.lead.m_nearestNeighborDistance = 3.5637037035516363;
//    Element.bismuth.m_nearestNeighborDistance = 3.3378985675136827;

        // Made with tolerance 0.3

//    Element.lithium.m_nearestNeighborDistance = 3.054411157840862;
//    Element.beryllium.m_nearestNeighborDistance = 2.239369331778049;
//    Element.boron.m_nearestNeighborDistance = 1.758949963641892;
//    Element.carbon.m_nearestNeighborDistance = 1.4244151718258145;
//    Element.sodium.m_nearestNeighborDistance = 3.6676175850270982;
//    Element.magnesium.m_nearestNeighborDistance = 3.1827024035011653;
//    Element.aluminum.m_nearestNeighborDistance = 2.8564803926778324;
//    Element.silicon.m_nearestNeighborDistance = 2.3683592522264947;
//    Element.phosphorus.m_nearestNeighborDistance = 2.211288895265709;
//    Element.sulfur.m_nearestNeighborDistance = 2.0644495215600687;
//    Element.potassium.m_nearestNeighborDistance = 4.566902639531702;
//    Element.calcium.m_nearestNeighborDistance = 3.9163950214018253;
//    Element.scandium.m_nearestNeighborDistance = 3.27293585108445;
//    Element.titanium.m_nearestNeighborDistance = 2.9725176396673296;
//    Element.vanadium.m_nearestNeighborDistance = 2.5972128769954086;
//    Element.chromium.m_nearestNeighborDistance = 2.4666380781518176;
//    Element.manganese.m_nearestNeighborDistance = 2.6497398367817677;
//    Element.iron.m_nearestNeighborDistance = 2.4577939328781464;
//    Element.cobalt.m_nearestNeighborDistance = 2.487239033964297;
//    Element.nickel.m_nearestNeighborDistance = 2.4789549822280956;
//    Element.copper.m_nearestNeighborDistance = 2.5570808956002935;
//    Element.zinc.m_nearestNeighborDistance = 2.790131587978324;
//    Element.gallium.m_nearestNeighborDistance = 2.7292109808774074;
//    Element.germanium.m_nearestNeighborDistance = 2.496049855347901;
//    Element.arsenic.m_nearestNeighborDistance = 2.5586184591102463;
//    Element.selenium.m_nearestNeighborDistance = 2.4080068701967603;
//    Element.rubidium.m_nearestNeighborDistance = 4.887644536183991;
//    Element.strontium.m_nearestNeighborDistance = 4.253324731739055;
//    Element.yttrium.m_nearestNeighborDistance = 3.5837770081428686;
//    Element.zirconium.m_nearestNeighborDistance = 3.2111049784796664;
//    Element.niobium.m_nearestNeighborDistance = 2.877780659584214;
//    Element.molybdenum.m_nearestNeighborDistance = 2.7316114569034506;
//    Element.ruthenium.m_nearestNeighborDistance = 2.690330599003738;
//    Element.rhodium.m_nearestNeighborDistance = 2.708049614604508;
//    Element.palladium.m_nearestNeighborDistance = 2.788789917660843;
//    Element.silver.m_nearestNeighborDistance = 2.9360728222051695;
//    Element.cadmium.m_nearestNeighborDistance = 3.2028526387335607;
//    Element.indium.m_nearestNeighborDistance = 3.3895848201012733;
//    Element.tin.m_nearestNeighborDistance = 2.8813657223574847;
//    Element.antimony.m_nearestNeighborDistance = 2.9596044456446364;
//    Element.tellurium.m_nearestNeighborDistance = 2.892380633394989;
//    Element.cesium .m_nearestNeighborDistance = 5.275903377940726;
//    Element.barium.m_nearestNeighborDistance = 4.356620226941927;
//    Element.hafnium.m_nearestNeighborDistance = 3.1671496648178965;
//    Element.tantalum.m_nearestNeighborDistance = 2.8758452397086067;
//    Element.tungsten.m_nearestNeighborDistance = 2.75785000512024;
//    Element.rhenium.m_nearestNeighborDistance = 2.770837341958717;
//    Element.osmium.m_nearestNeighborDistance = 2.726898515613693;
//    Element.iridium.m_nearestNeighborDistance = 2.7375671414839746;
//    Element.platinum.m_nearestNeighborDistance = 2.8059714743694504;
//    Element.gold.m_nearestNeighborDistance = 2.9398164935668825;
//    Element.mercury.m_nearestNeighborDistance = 3.5387720905404465;
//    Element.thallium.m_nearestNeighborDistance = 3.5440523479286803;
//    Element.lead.m_nearestNeighborDistance = 3.5637037035516363;
//    Element.bismuth.m_nearestNeighborDistance = 3.1127316878105433;

        // Made with tolerance 0.2

//    Element.lithium.m_nearestNeighborDistance = 3.054411157840862;
//    Element.beryllium.m_nearestNeighborDistance = 2.239369331778049;
//    Element.boron.m_nearestNeighborDistance = 1.758949963641892;
//    Element.carbon.m_nearestNeighborDistance = 1.4244151718258145;
//    Element.sodium.m_nearestNeighborDistance = 3.6676175850270982;
//    Element.magnesium.m_nearestNeighborDistance = 3.1827024035011653;
//    Element.aluminum.m_nearestNeighborDistance = 2.8564803926778324;
//    Element.silicon.m_nearestNeighborDistance = 2.3683592522264947;
//    Element.phosphorus.m_nearestNeighborDistance = 2.211288895265709;
//    Element.sulfur.m_nearestNeighborDistance = 2.0644495215600687;
//    Element.potassium.m_nearestNeighborDistance = 4.566902639531702;
//    Element.calcium.m_nearestNeighborDistance = 3.9163950214018253;
//    Element.scandium.m_nearestNeighborDistance = 3.27293585108445;
//    Element.titanium.m_nearestNeighborDistance = 2.9725176396673296;
//    Element.vanadium.m_nearestNeighborDistance = 2.5972128769954086;
//    Element.chromium.m_nearestNeighborDistance = 2.4666380781518176;
//    Element.manganese.m_nearestNeighborDistance = 2.6497398367817677;
//    Element.iron.m_nearestNeighborDistance = 2.4577939328781464;
//    Element.cobalt.m_nearestNeighborDistance = 2.487239033964297;
//    Element.nickel.m_nearestNeighborDistance = 2.4789549822280956;
//    Element.copper.m_nearestNeighborDistance = 2.5570808956002935;
//    Element.zinc.m_nearestNeighborDistance = 2.790131587978324;
//    Element.gallium.m_nearestNeighborDistance = 2.6563519592814937;
//    Element.germanium.m_nearestNeighborDistance = 2.496049855347901;
//    Element.arsenic.m_nearestNeighborDistance = 2.5586184591102463;
//    Element.selenium.m_nearestNeighborDistance = 2.4080068701967603;
//    Element.rubidium.m_nearestNeighborDistance = 4.887644536183991;
//    Element.strontium.m_nearestNeighborDistance = 4.253324731739055;
//    Element.yttrium.m_nearestNeighborDistance = 3.5837770081428686;
//    Element.zirconium.m_nearestNeighborDistance = 3.2111049784796664;
//    Element.niobium.m_nearestNeighborDistance = 2.877780659584214;
//    Element.molybdenum.m_nearestNeighborDistance = 2.7316114569034506;
//    Element.ruthenium.m_nearestNeighborDistance = 2.690330599003738;
//    Element.rhodium.m_nearestNeighborDistance = 2.708049614604508;
//    Element.palladium.m_nearestNeighborDistance = 2.788789917660843;
//    Element.silver.m_nearestNeighborDistance = 2.9360728222051695;
//    Element.cadmium.m_nearestNeighborDistance = 3.2028526387335607;
//    Element.indium.m_nearestNeighborDistance = 3.3895848201012733;
//    Element.tin.m_nearestNeighborDistance = 2.8813657223574847;
//    Element.antimony.m_nearestNeighborDistance = 2.9596044456446364;
//    Element.tellurium.m_nearestNeighborDistance = 2.892380633394989;
//    Element.cesium .m_nearestNeighborDistance = 5.275903377940726;
//    Element.barium.m_nearestNeighborDistance = 4.356620226941927;
//    Element.hafnium.m_nearestNeighborDistance = 3.1671496648178965;
//    Element.tantalum.m_nearestNeighborDistance = 2.8758452397086067;
//    Element.tungsten.m_nearestNeighborDistance = 2.75785000512024;
//    Element.rhenium.m_nearestNeighborDistance = 2.770837341958717;
//    Element.osmium.m_nearestNeighborDistance = 2.726898515613693;
//    Element.iridium.m_nearestNeighborDistance = 2.7375671414839746;
//    Element.platinum.m_nearestNeighborDistance = 2.8059714743694504;
//    Element.gold.m_nearestNeighborDistance = 2.9398164935668825;
//    Element.mercury.m_nearestNeighborDistance = 3.5387720905404465;
//    Element.thallium.m_nearestNeighborDistance = 3.5440523479286803;
//    Element.lead.m_nearestNeighborDistance = 3.5637037035516363;
//    Element.bismuth.m_nearestNeighborDistance = 3.1127316878105433;

        // Made with tolerance 1E-4

//    Element.lithium.m_nearestNeighborDistance = 3.0522881032793836;
//    Element.beryllium.m_nearestNeighborDistance = 2.213561188353312;
//    Element.boron.m_nearestNeighborDistance = 1.6728861693276875;
//    Element.carbon.m_nearestNeighborDistance = 1.4244151718258145;
//    Element.sodium.m_nearestNeighborDistance = 3.6676175850270982;
//    Element.magnesium.m_nearestNeighborDistance = 3.1747681405362544;
//    Element.aluminum.m_nearestNeighborDistance = 2.8564803926778324;
//    Element.silicon.m_nearestNeighborDistance = 2.3683592522264947;
//    Element.phosphorus.m_nearestNeighborDistance = 2.2022510260020476;
//    Element.sulfur.m_nearestNeighborDistance = 2.0636374064079055;
//    Element.potassium.m_nearestNeighborDistance = 4.566902639531702;
//    Element.calcium.m_nearestNeighborDistance = 3.9163950214018253;
//    Element.scandium.m_nearestNeighborDistance = 3.2251802017661486;
//    Element.titanium.m_nearestNeighborDistance = 2.9725176396673296;
//    Element.vanadium.m_nearestNeighborDistance = 2.5972128769954086;
//    Element.chromium.m_nearestNeighborDistance = 2.4666380781518176;
//    Element.manganese.m_nearestNeighborDistance = 2.6341430126167116;
//    Element.iron.m_nearestNeighborDistance = 2.4577939328781464;
//    Element.cobalt.m_nearestNeighborDistance = 2.487239033964297;
//    Element.nickel.m_nearestNeighborDistance = 2.4789549822280956;
//    Element.copper.m_nearestNeighborDistance = 2.5570808956002935;
//    Element.zinc.m_nearestNeighborDistance = 2.790131587978324;
//    Element.gallium.m_nearestNeighborDistance = 2.530369679389842;
//    Element.germanium.m_nearestNeighborDistance = 2.496049855347901;
//    Element.arsenic.m_nearestNeighborDistance = 2.5586184591102463;
//    Element.selenium.m_nearestNeighborDistance = 2.4080068701967603;
//    Element.rubidium.m_nearestNeighborDistance = 4.887644536183991;
//    Element.strontium.m_nearestNeighborDistance = 4.253324731739055;
//    Element.yttrium.m_nearestNeighborDistance = 3.531261836613782;
//    Element.zirconium.m_nearestNeighborDistance = 3.1864300283034614;
//    Element.niobium.m_nearestNeighborDistance = 2.877780659584214;
//    Element.molybdenum.m_nearestNeighborDistance = 2.7316114569034506;
//    Element.ruthenium.m_nearestNeighborDistance = 2.659337156910127;
//    Element.rhodium.m_nearestNeighborDistance = 2.708049614604508;
//    Element.palladium.m_nearestNeighborDistance = 2.788789917660843;
//    Element.silver.m_nearestNeighborDistance = 2.9350360377542284;
//    Element.cadmium.m_nearestNeighborDistance = 3.1726389182286776;
//    Element.indium.m_nearestNeighborDistance = 3.3086959028722998;
//    Element.tin.m_nearestNeighborDistance = 2.8813657223574847;
//    Element.antimony.m_nearestNeighborDistance = 2.9596044456446364;
//    Element.tellurium.m_nearestNeighborDistance = 2.892380633394989;
//    Element.cesium .m_nearestNeighborDistance = 5.251305636527872;
//    Element.barium.m_nearestNeighborDistance = 4.356620226941927;
//    Element.hafnium.m_nearestNeighborDistance = 3.130889124432643;
//    Element.tantalum.m_nearestNeighborDistance = 2.8758452397086067;
//    Element.tungsten.m_nearestNeighborDistance = 2.75785000512024;
//    Element.rhenium.m_nearestNeighborDistance = 2.7604626352337513;
//    Element.osmium.m_nearestNeighborDistance = 2.6967683445992527;
//    Element.iridium.m_nearestNeighborDistance = 2.7375671414839746;
//    Element.platinum.m_nearestNeighborDistance = 2.8059714743694504;
//    Element.gold.m_nearestNeighborDistance = 2.9398164935668825;
//    Element.mercury.m_nearestNeighborDistance = 3.466722531494292;
//    Element.thallium.m_nearestNeighborDistance = 3.486701627316117;
//    Element.lead.m_nearestNeighborDistance = 3.5637037035516363;
//    Element.bismuth.m_nearestNeighborDistance = 3.1127316878105433;

        // Made with tolerance 10

//    Element.lithium.m_nearestNeighborDistance = 9.78252590273339;
//    Element.beryllium.m_nearestNeighborDistance = 9.110426209673886;
//    Element.boron.m_nearestNeighborDistance = 8.77014158305476;
//    Element.carbon.m_nearestNeighborDistance = 8.376438719096175;
//    Element.sodium.m_nearestNeighborDistance = 10.291275047172238;
//    Element.magnesium.m_nearestNeighborDistance = 9.943185562247118;
//    Element.aluminum.m_nearestNeighborDistance = 9.743887439949056;
//    Element.silicon.m_nearestNeighborDistance = 9.245673547562543;
//    Element.phosphorus.m_nearestNeighborDistance = 9.182884619771244;
//    Element.sulfur.m_nearestNeighborDistance = 9.051592094203423;
//    Element.potassium.m_nearestNeighborDistance = 10.780763268310475;
//    Element.calcium.m_nearestNeighborDistance = 10.263399124727997;
//    Element.scandium.m_nearestNeighborDistance = 10.030400696496772;
//    Element.titanium.m_nearestNeighborDistance = 9.780653228855114;
//    Element.vanadium.m_nearestNeighborDistance = 9.398805070926235;
//    Element.chromium.m_nearestNeighborDistance = 9.462465256142506;
//    Element.manganese.m_nearestNeighborDistance = 9.510734743155544;
//    Element.iron.m_nearestNeighborDistance = 9.428537531555117;
//    Element.cobalt.m_nearestNeighborDistance = 9.454441400695634;
//    Element.nickel.m_nearestNeighborDistance = 9.422952235146742;
//    Element.copper.m_nearestNeighborDistance = 9.342356181907604;
//    Element.zinc.m_nearestNeighborDistance = 9.70558363204461;
//    Element.gallium.m_nearestNeighborDistance = 9.412851556456133;
//    Element.germanium.m_nearestNeighborDistance = 9.532107280595708;
//    Element.arsenic.m_nearestNeighborDistance = 9.546528269108679;
//    Element.selenium.m_nearestNeighborDistance = 9.17731880745149;
//    Element.rubidium.m_nearestNeighborDistance = 11.537915923176776;
//    Element.strontium.m_nearestNeighborDistance = 10.76198125684166;
//    Element.yttrium.m_nearestNeighborDistance = 10.211632325703961;
//    Element.zirconium.m_nearestNeighborDistance = 9.888227059202828;
//    Element.niobium.m_nearestNeighborDistance = 9.939609534589808;
//    Element.molybdenum.m_nearestNeighborDistance = 9.47037316513582;
//    Element.ruthenium.m_nearestNeighborDistance = 9.451261332579637;
//    Element.rhodium.m_nearestNeighborDistance = 9.613626118101887;
//    Element.palladium.m_nearestNeighborDistance = 9.773465179450845;
//    Element.silver.m_nearestNeighborDistance = 9.765629727802015;
//    Element.cadmium.m_nearestNeighborDistance = 10.099214835770555;
//    Element.indium.m_nearestNeighborDistance = 9.966808898203805;
//    Element.tin.m_nearestNeighborDistance = 10.081986317197725;
//    Element.antimony.m_nearestNeighborDistance = 9.668754092079885;
//    Element.tellurium.m_nearestNeighborDistance = 9.666228138532803;
//    Element.cesium .m_nearestNeighborDistance = 11.575047709953596;
//    Element.barium.m_nearestNeighborDistance = 10.547317658766001;
//    Element.hafnium.m_nearestNeighborDistance = 9.752772650675926;
//    Element.tantalum.m_nearestNeighborDistance = 9.932924759019794;
//    Element.tungsten.m_nearestNeighborDistance = 9.561341023063187;
//    Element.rhenium.m_nearestNeighborDistance = 9.566552105563245;
//    Element.osmium.m_nearestNeighborDistance = 9.447187188001239;
//    Element.iridium.m_nearestNeighborDistance = 9.593952188461769;
//    Element.platinum.m_nearestNeighborDistance = 9.57159386637091;
//    Element.gold.m_nearestNeighborDistance = 9.8869172642835;
//    Element.mercury.m_nearestNeighborDistance = 10.185526444914089;
//    Element.thallium.m_nearestNeighborDistance = 10.097466753762843;
//    Element.lead.m_nearestNeighborDistance = 10.128875213233032;
//    Element.bismuth.m_nearestNeighborDistance = 9.768335396915084;

        // Made with tolerance 1

//    Element.lithium.m_nearestNeighborDistance = 3.054411157840862;
//    Element.beryllium.m_nearestNeighborDistance = 2.5486314343132697;
//    Element.boron.m_nearestNeighborDistance = 1.758949963641892;
//    Element.carbon.m_nearestNeighborDistance = 1.4244151718258145;
//    Element.sodium.m_nearestNeighborDistance = 3.910781477158341;
//    Element.magnesium.m_nearestNeighborDistance = 3.1827024035011653;
//    Element.aluminum.m_nearestNeighborDistance = 2.8564803926778324;
//    Element.silicon.m_nearestNeighborDistance = 2.3683592522264947;
//    Element.phosphorus.m_nearestNeighborDistance = 2.211288895265709;
//    Element.sulfur.m_nearestNeighborDistance = 2.0644495215600687;
//    Element.potassium.m_nearestNeighborDistance = 4.869689338272209;
//    Element.calcium.m_nearestNeighborDistance = 3.9163950214018253;
//    Element.scandium.m_nearestNeighborDistance = 3.27293585108445;
//    Element.titanium.m_nearestNeighborDistance = 2.9725176396673296;
//    Element.vanadium.m_nearestNeighborDistance = 2.7694086900054287;
//    Element.chromium.m_nearestNeighborDistance = 2.6301767518704673;
//    Element.manganese.m_nearestNeighborDistance = 2.6497398367817677;
//    Element.iron.m_nearestNeighborDistance = 2.6207462377245068;
//    Element.cobalt.m_nearestNeighborDistance = 2.487239033964297;
//    Element.nickel.m_nearestNeighborDistance = 2.4789549822280956;
//    Element.copper.m_nearestNeighborDistance = 2.5570808956002935;
//    Element.zinc.m_nearestNeighborDistance = 2.790131587978324;
//    Element.gallium.m_nearestNeighborDistance = 2.7292109808774074;
//    Element.germanium.m_nearestNeighborDistance = 2.496049855347901;
//    Element.arsenic.m_nearestNeighborDistance = 2.8527915296961126;
//    Element.selenium.m_nearestNeighborDistance = 2.4080068701967603;
//    Element.rubidium.m_nearestNeighborDistance = 5.211696496678592;
//    Element.strontium.m_nearestNeighborDistance = 4.253324731739055;
//    Element.yttrium.m_nearestNeighborDistance = 3.5837770081428686;
//    Element.zirconium.m_nearestNeighborDistance = 3.2111049784796664;
//    Element.niobium.m_nearestNeighborDistance = 3.0685781813163886;
//    Element.molybdenum.m_nearestNeighborDistance = 2.9127179267716916;
//    Element.ruthenium.m_nearestNeighborDistance = 2.690330599003738;
//    Element.rhodium.m_nearestNeighborDistance = 2.708049614604508;
//    Element.palladium.m_nearestNeighborDistance = 2.788789917660843;
//    Element.silver.m_nearestNeighborDistance = 2.9360728222051695;
//    Element.cadmium.m_nearestNeighborDistance = 3.2028526387335607;
//    Element.indium.m_nearestNeighborDistance = 3.3895848201012733;
//    Element.tin.m_nearestNeighborDistance = 2.8813657223574847;
//    Element.antimony.m_nearestNeighborDistance = 3.176964961946613;
//    Element.tellurium.m_nearestNeighborDistance = 3.3063713896498452;
//    Element.cesium .m_nearestNeighborDistance = 5.275903377940726;
//    Element.barium.m_nearestNeighborDistance = 4.645465153208435;
//    Element.hafnium.m_nearestNeighborDistance = 3.1671496648178965;
//    Element.tantalum.m_nearestNeighborDistance = 3.0665144426564614;
//    Element.tungsten.m_nearestNeighborDistance = 2.9406960967893783;
//    Element.rhenium.m_nearestNeighborDistance = 2.770837341958717;
//    Element.osmium.m_nearestNeighborDistance = 2.726898515613693;
//    Element.iridium.m_nearestNeighborDistance = 2.7375671414839746;
//    Element.platinum.m_nearestNeighborDistance = 2.8059714743694504;
//    Element.gold.m_nearestNeighborDistance = 2.9398164935668825;
//    Element.mercury.m_nearestNeighborDistance = 3.5387720905404465;
//    Element.thallium.m_nearestNeighborDistance = 3.5440523479286803;
//    Element.lead.m_nearestNeighborDistance = 3.5637037035516363;
//    Element.bismuth.m_nearestNeighborDistance = 3.3378985675136827;

        // Made with tolerance 0.1

//    Element.lithium.m_nearestNeighborDistance = 3.054411157840862;
//    Element.beryllium.m_nearestNeighborDistance = 2.239369331778049;
//    Element.boron.m_nearestNeighborDistance = 1.720742514406367;
//    Element.carbon.m_nearestNeighborDistance = 1.4244151718258145;
//    Element.sodium.m_nearestNeighborDistance = 3.6676175850270982;
//    Element.magnesium.m_nearestNeighborDistance = 3.1827024035011653;
//    Element.aluminum.m_nearestNeighborDistance = 2.8564803926778324;
//    Element.silicon.m_nearestNeighborDistance = 2.3683592522264947;
//    Element.phosphorus.m_nearestNeighborDistance = 2.211288895265709;
//    Element.sulfur.m_nearestNeighborDistance = 2.0644495215600687;
//    Element.potassium.m_nearestNeighborDistance = 4.566902639531702;
//    Element.calcium.m_nearestNeighborDistance = 3.9163950214018253;
//    Element.scandium.m_nearestNeighborDistance = 3.27293585108445;
//    Element.titanium.m_nearestNeighborDistance = 2.9725176396673296;
//    Element.vanadium.m_nearestNeighborDistance = 2.5972128769954086;
//    Element.chromium.m_nearestNeighborDistance = 2.4666380781518176;
//    Element.manganese.m_nearestNeighborDistance = 2.6497398367817677;
//    Element.iron.m_nearestNeighborDistance = 2.4577939328781464;
//    Element.cobalt.m_nearestNeighborDistance = 2.487239033964297;
//    Element.nickel.m_nearestNeighborDistance = 2.4789549822280956;
//    Element.copper.m_nearestNeighborDistance = 2.5570808956002935;
//    Element.zinc.m_nearestNeighborDistance = 2.790131587978324;
//    Element.gallium.m_nearestNeighborDistance = 2.530369679389842;
//    Element.germanium.m_nearestNeighborDistance = 2.496049855347901;
//    Element.arsenic.m_nearestNeighborDistance = 2.5586184591102463;
//    Element.selenium.m_nearestNeighborDistance = 2.4080068701967603;
//    Element.rubidium.m_nearestNeighborDistance = 4.887644536183991;
//    Element.strontium.m_nearestNeighborDistance = 4.253324731739055;
//    Element.yttrium.m_nearestNeighborDistance = 3.531261836613782;
//    Element.zirconium.m_nearestNeighborDistance = 3.2111049784796664;
//    Element.niobium.m_nearestNeighborDistance = 2.877780659584214;
//    Element.molybdenum.m_nearestNeighborDistance = 2.7316114569034506;
//    Element.ruthenium.m_nearestNeighborDistance = 2.690330599003738;
//    Element.rhodium.m_nearestNeighborDistance = 2.708049614604508;
//    Element.palladium.m_nearestNeighborDistance = 2.788789917660843;
//    Element.silver.m_nearestNeighborDistance = 2.9360728222051695;
//    Element.cadmium.m_nearestNeighborDistance = 3.2028526387335607;
//    Element.indium.m_nearestNeighborDistance = 3.3086959028722998;
//    Element.tin.m_nearestNeighborDistance = 2.8813657223574847;
//    Element.antimony.m_nearestNeighborDistance = 2.9596044456446364;
//    Element.tellurium.m_nearestNeighborDistance = 2.892380633394989;
//    Element.cesium .m_nearestNeighborDistance = 5.275903377940726;
//    Element.barium.m_nearestNeighborDistance = 4.356620226941927;
//    Element.hafnium.m_nearestNeighborDistance = 3.1671496648178965;
//    Element.tantalum.m_nearestNeighborDistance = 2.8758452397086067;
//    Element.tungsten.m_nearestNeighborDistance = 2.75785000512024;
//    Element.rhenium.m_nearestNeighborDistance = 2.770837341958717;
//    Element.osmium.m_nearestNeighborDistance = 2.726898515613693;
//    Element.iridium.m_nearestNeighborDistance = 2.7375671414839746;
//    Element.platinum.m_nearestNeighborDistance = 2.8059714743694504;
//    Element.gold.m_nearestNeighborDistance = 2.9398164935668825;
//    Element.mercury.m_nearestNeighborDistance = 3.466722531494292;
//    Element.thallium.m_nearestNeighborDistance = 3.487923461458887;
//    Element.lead.m_nearestNeighborDistance = 3.5637037035516363;
//    Element.bismuth.m_nearestNeighborDistance = 3.1127316878105433;

        // Made with tolerance 1E-9

//    Element.lithium.m_nearestNeighborDistance = 3.0522881032793836;
//    Element.beryllium.m_nearestNeighborDistance = 2.213561188353312;
//    Element.boron.m_nearestNeighborDistance = 1.6728861693276875;
//    Element.carbon.m_nearestNeighborDistance = 1.4244151718258145;
//    Element.sodium.m_nearestNeighborDistance = 3.6676175850270982;
//    Element.magnesium.m_nearestNeighborDistance = 3.1747681405362544;
//    Element.aluminum.m_nearestNeighborDistance = 2.8564803926778324;
//    Element.silicon.m_nearestNeighborDistance = 2.3683592522264947;
//    Element.phosphorus.m_nearestNeighborDistance = 2.2022510260020476;
//    Element.sulfur.m_nearestNeighborDistance = 2.0636374064079055;
//    Element.potassium.m_nearestNeighborDistance = 4.566902639531702;
//    Element.calcium.m_nearestNeighborDistance = 3.9163950214018253;
//    Element.scandium.m_nearestNeighborDistance = 3.2251802017661486;
//    Element.titanium.m_nearestNeighborDistance = 2.9725176396673296;
//    Element.vanadium.m_nearestNeighborDistance = 2.5972128769954086;
//    Element.chromium.m_nearestNeighborDistance = 2.4666380781518176;
//    Element.manganese.m_nearestNeighborDistance = 2.6341430126167116;
//    Element.iron.m_nearestNeighborDistance = 2.4577939328781464;
//    Element.cobalt.m_nearestNeighborDistance = 2.487239033964297;
//    Element.nickel.m_nearestNeighborDistance = 2.4789549822280956;
//    Element.copper.m_nearestNeighborDistance = 2.5570808956002935;
//    Element.zinc.m_nearestNeighborDistance = 2.790131587978324;
//    Element.gallium.m_nearestNeighborDistance = 2.530369679389842;
//    Element.germanium.m_nearestNeighborDistance = 2.496049855347901;
//    Element.arsenic.m_nearestNeighborDistance = 2.5586184591102463;
//    Element.selenium.m_nearestNeighborDistance = 2.4080068701967603;
//    Element.rubidium.m_nearestNeighborDistance = 4.887644536183991;
//    Element.strontium.m_nearestNeighborDistance = 4.253324731739055;
//    Element.yttrium.m_nearestNeighborDistance = 3.531261836613782;
//    Element.zirconium.m_nearestNeighborDistance = 3.1864300283034614;
//    Element.niobium.m_nearestNeighborDistance = 2.877780659584214;
//    Element.molybdenum.m_nearestNeighborDistance = 2.7316114569034506;
//    Element.ruthenium.m_nearestNeighborDistance = 2.659337156910127;
//    Element.rhodium.m_nearestNeighborDistance = 2.708049614604508;
//    Element.palladium.m_nearestNeighborDistance = 2.788789917660843;
//    Element.silver.m_nearestNeighborDistance = 2.9350360377542284;
//    Element.cadmium.m_nearestNeighborDistance = 3.1726389182286776;
//    Element.indium.m_nearestNeighborDistance = 3.3086959028722998;
//    Element.tin.m_nearestNeighborDistance = 2.8813657223574847;
//    Element.antimony.m_nearestNeighborDistance = 2.9596044456446364;
//    Element.tellurium.m_nearestNeighborDistance = 2.892380633394989;
//    Element.cesium .m_nearestNeighborDistance = 5.251305636527872;
//    Element.barium.m_nearestNeighborDistance = 4.356620226941927;
//    Element.hafnium.m_nearestNeighborDistance = 3.1308784015380673;
//    Element.tantalum.m_nearestNeighborDistance = 2.8758452397086067;
//    Element.tungsten.m_nearestNeighborDistance = 2.75785000512024;
//    Element.rhenium.m_nearestNeighborDistance = 2.7604626352337513;
//    Element.osmium.m_nearestNeighborDistance = 2.6967652835701363;
//    Element.iridium.m_nearestNeighborDistance = 2.7375671414839746;
//    Element.platinum.m_nearestNeighborDistance = 2.8059714743694504;
//    Element.gold.m_nearestNeighborDistance = 2.9398164935668825;
//    Element.mercury.m_nearestNeighborDistance = 3.466722531494292;
//    Element.thallium.m_nearestNeighborDistance = 3.486701627316117;
//    Element.lead.m_nearestNeighborDistance = 3.5637037035516363;
//    Element.bismuth.m_nearestNeighborDistance = 3.1127316878105433;
//

////      Element.hydrogen.m_nearestNeighborDistance = ??;
////      Element.helium.m_nearestNeighborDistance = ??;
//    Element.lithium.m_nearestNeighborDistance = 3.046326996179318; 
//    Element.beryllium.m_nearestNeighborDistance = 2.2537870482926654; 
//    Element.boron.m_nearestNeighborDistance = 1.7392281316152305; 
//    Element.carbon.m_nearestNeighborDistance = 1.4203254283790028; // Calculated from icsd-28417
//    Element.nitrogen.m_nearestNeighborDistance = 0.44997200000000026; // Calculated from icsd-16386
//    Element.oxygen.m_nearestNeighborDistance = 1.183096391686715; // Calculated from icsd-236855
//    Element.fluorine.m_nearestNeighborDistance = 1.5648125427104687; // Calculated from icsd-16262
//    Element.neon.m_nearestNeighborDistance = 3.196122650963192; // Calculated from icsd-24602
//    Element.sodium.m_nearestNeighborDistance = 3.667617585027097; // Calculated from icsd-44757
//    Element.magnesium.m_nearestNeighborDistance = 3.1967294152328596; // Calculated from icsd-52260
//    Element.aluminum.m_nearestNeighborDistance = 2.8703232015314923; 
//    Element.silicon.m_nearestNeighborDistance = 2.351566410293094; // Calculated from icsd-29287
//    Element.phosphorus.m_nearestNeighborDistance = 2.199; // Calculated from icsd-391323
//    Element.sulfur.m_nearestNeighborDistance = 2.0512470682144626; // Calculated from icsd-419593
//    Element.chlorine.m_nearestNeighborDistance = 1.9742426284486916; // Calculated from icsd-18154
//    Element.argon.m_nearestNeighborDistance = 3.798903202755591; // CAlculated from icsd-77918
//    Element.potassium.m_nearestNeighborDistance = 4.614183351363489; // Calculated from icsd-44670 ;
//    Element.calcium.m_nearestNeighborDistance = 3.9385847712090674; // Calculated from icsd-53768
//    Element.scandium.m_nearestNeighborDistance = 3.252455607165748; // Calculated from icsd-43587
//    Element.titanium.m_nearestNeighborDistance = 2.8828819533744734; // Calculated from icsd-52521
//    Element.vanadium.m_nearestNeighborDistance = 2.620419666770955; // Calculated from icsd-43420
//    Element.chromium.m_nearestNeighborDistance = 2.498431328393878; // Calculated from icsd-44731
//    Element.manganese.m_nearestNeighborDistance = 2.6203157637623344; 
//    Element.iron.m_nearestNeighborDistance = 2.5387534711940827; // Calculated from icsd-44863
//    Element.cobalt.m_nearestNeighborDistance = 2.4416538190998507;
//    Element.nickel.m_nearestNeighborDistance = 2.492056428935751; // Calculated from icsd-52231
//    Element.copper.m_nearestNeighborDistance = 2.5562263693284284; // Calculated from icsd-43493
//    Element.zinc.m_nearestNeighborDistance = 2.7678542174223986; // Calculated from icsd-247147
//    Element.gallium.m_nearestNeighborDistance = 2.4839579308186437; // Calculated from icsd-43388
//    Element.germanium.m_nearestNeighborDistance = 2.44944460142881; // Calculated from icsd-43422
//    Element.arsenic.m_nearestNeighborDistance = 2.64182878743; 
//    Element.selenium.m_nearestNeighborDistance = 2.847969475970069; // Calculated from icsd-164261
//    Element.bromine.m_nearestNeighborDistance = 2.300993368038859; // Calculated from icsd-201692;
//    Element.krypton.m_nearestNeighborDistance = 3.999597366415173; // Calculated from icsd-9785
//    Element.rubidium.m_nearestNeighborDistance = 4.854072388211778; // Calculated from icsd-44755
//    Element.strontium.m_nearestNeighborDistance = 4.296380802489465; // Calculated from icsd-44721
//    Element.yttrium.m_nearestNeighborDistance = 3.555903985806859; // Calculated from icsd-52539
//    Element.zirconium.m_nearestNeighborDistance = 3.179270160132855; // Calculated from icsd-76042
//    Element.niobium.m_nearestNeighborDistance = 2.8675833170110328; // Calculated from icsd-76011
//    Element.molybdenum.m_nearestNeighborDistance = 2.7257023751090292; // Calculated from icsd-52267
//    Element.technetium.m_nearestNeighborDistance = 2.7107224897679782; // Calculated from icsd-52498
//    Element.ruthenium.m_nearestNeighborDistance = 2.6657020086311047; // Calculated from icsd-40354
//    Element.rhodium.m_nearestNeighborDistance = 2.6891977995305605; // Calculated from icsd-64991
//    Element.palladium.m_nearestNeighborDistance = 2.7507868001719085; // Calculated from icsd-52251
//    Element.silver.m_nearestNeighborDistance = 2.93264368605; 
//    Element.cadmium.m_nearestNeighborDistance = 3.1364984380743777; // Calculated from icsd-52264
//    Element.indium.m_nearestNeighborDistance = 3.3366675797056673; // Calculated from icsd-182782
//    Element.tin.m_nearestNeighborDistance = 2.8099060251189907; // Calculated from icsd-40039
//    Element.antimony.m_nearestNeighborDistance = 3.279924288490203;
//    Element.tellurium.m_nearestNeighborDistance = 3.217524228719827;
//    Element.iodine.m_nearestNeighborDistance = 2.78190453419811; // Calculated from icsd-10084
//    Element.xenon.m_nearestNeighborDistance = 4.335766650201551; // Calculated from icsd-43428
//    Element.cesium.m_nearestNeighborDistance = 5.462449508991765; 
//    Element.barium.m_nearestNeighborDistance = 4.3502421709; 
//    Element.lanthanum.m_nearestNeighborDistance = 3.732785772403133; // Calculated from icsd-43573
//    Element.cerium.m_nearestNeighborDistance = 3.633514362245487; // Calculated from icsd-43381
//    Element.praseodymium.m_nearestNeighborDistance = 3.637852624459089; // Calculated from icsd-43576
//    //Element.promethium.m_nearestNeighborDistance = ??;
//    Element.samarium.m_nearestNeighborDistance = 3.590862297500991; // Calculated from icsd-52482
//    Element.europium.m_nearestNeighborDistance = 3.9646642985251592; // Calculated from icsd-43578
//    Element.gadolinium.m_nearestNeighborDistance = 3.3889719819731856; // Calculated from icsd-53607
//    Element.terbium.m_nearestNeighborDistance = 3.532010186951302; // Calculated from icsd-52497
//    Element.dysprosium.m_nearestNeighborDistance = 3.5147060588434074; // Calculated from icsd-52518
//    Element.holmium.m_nearestNeighborDistance = 3.4810416784028106; // Calculated from icsd-52519
//    Element.erbium.m_nearestNeighborDistance = 3.4698381046682685; // Calculated from icsd-43583
//    Element.thulium.m_nearestNeighborDistance = 3.4536407438329255; // Calculated from icsd-52527
//    Element.ytterbium.m_nearestNeighborDistance = 3.875652267683469; // Calculated from icsd-43585
//    Element.lutetium.m_nearestNeighborDistance = 3.4453675960263976; // Calculated from icsd-168439
//    Element.hafnium.m_nearestNeighborDistance = 3.1324907601900294; // Calculated from icsd-53022
//    Element.tantalum.m_nearestNeighborDistance = 2.8629067798305967; // Calculated from icsd-76152
//    Element.tungsten.m_nearestNeighborDistance = 2.740753896626802; // Calculated from icsd-43421
//    Element.rhenium.m_nearestNeighborDistance = 2.7400074971416863; // Calculated from icsd-40355
//    Element.osmium.m_nearestNeighborDistance = 2.661794194085503; // Calculated from icsd-40323
//    Element.iridium.m_nearestNeighborDistance = 2.697612370226679; // Calculated from icsd-53813
//    Element.platinum.m_nearestNeighborDistance = 2.774474877341658; // Calculated from icsd-52250
//    Element.gold.m_nearestNeighborDistance = 2.9367754644666237; // Calculated from icsd-44362
//    Element.mercury.m_nearestNeighborDistance = 3.4830673216506574;
//    Element.thallium.m_nearestNeighborDistance = 3.4960859957928236;
//    Element.lead.m_nearestNeighborDistance = 3.4998957241609325; // Calculated from icsd-52253
//    Element.bismuth.m_nearestNeighborDistance = 3.4470583633632677;
//    Element.polonium.m_nearestNeighborDistance = 3.34;
////      Element.astatine.m_nearestNeighborDistance = ??
////      Element.radon.m_nearestNeighborDistance = ??
////      Element.francium.m_nearestNeighborDistance = ??
////      Element.radium.m_nearestNeighborDistance = ??
//    Element.actinium.m_nearestNeighborDistance = 3.755444114881754; // Calculated from icsd-43491
//    Element.thorium.m_nearestNeighborDistance = 2.9234898223380608; // Calculated from icsd-104198
//    Element.protactinium.m_nearestNeighborDistance = 3.5574542161495226; // Calculated from icsd-77862
//    Element.uranium.m_nearestNeighborDistance = 2.7537671261339436; // Calculated from icsd-43419
//    Element.neptunium.m_nearestNeighborDistance = 2.599006912227245; // Calculated from icsd-105489
////      Element.plutonium.m_nearestNeighborDistance = 3.10, 2.59??;
//    Element.americium.m_nearestNeighborDistance = 3.61;
////      Element.curium.m_nearestNeighborDistance = ??;
////      Element.berkelium.m_nearestNeighborDistance = ??;
////      Element.californium.m_nearestNeighborDistance = ??;
////      Element.einsteinium.m_nearestNeighborDistance = ??;
////      Element.fermium.m_nearestNeighborDistance = ??;
////      Element.mendelevium.m_nearestNeighborDistance = ??;
////      Element.nobelium.m_nearestNeighborDistance = ??;
////      Element.lawrencium.m_nearestNeighborDistance = ??;
////      Element.rutherfordium.m_nearestNeighborDistance = ??;
////      Element.dubnium.m_nearestNeighborDistance = ??;
////      Element.seaborgium.m_nearestNeighborDistance = ??;
////      Element.bohrium.m_nearestNeighborDistance = ??;
////      Element.hassium.m_nearestNeighborDistance = ??;
////      Element.meitnerium.m_nearestNeighborDistance = ??;
////      Element.darmstadtium.m_nearestNeighborDistance = ??;
////      Element.roentgenium.m_nearestNeighborDistance = ??;
////      Element.ununbium.m_nearestNeighborDistance = ??;
////      Element.ununtrium.m_nearestNeighborDistance = ??;
////      Element.ununquadium.m_nearestNeighborDistance = ??;
////      Element.ununpentium.m_nearestNeighborDistance = ??;
////      Element.ununhexium.m_nearestNeighborDistance = ??;
////      Element.ununseptium.m_nearestNeighborDistance = ??;
////      Element.ununoctium.m_nearestNeighborDistance = ??;

    }
    */
}
