package ga.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import ga.io.Logger;
import matsci.io.app.log.Status;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class GAMath {
	
	/**
	 * Pick a random, uniformly distributed point on the unit sphere. Coordinates are generated
	 * from the standard normal distribution, which is spherically symmetric.
	 * 
	 * @return The Cartesian coordinate of the randomly picked point on the unit sphere.
	 */
	public static double[] randomPointOnUnitSphere() {
		Random random = new Random();
		double[] r = new double[3];
		r[0] = random.nextGaussian();
		r[1] = random.nextGaussian();
		r[2] = random.nextGaussian();
		r = MSMath.normalize(r);
		return r;
	}
	
	public static double[] randomPointOnSphere(double radius) {
		if (radius <= 0) {
			Logger.error("GAMath.randomPointOnSphere: Radius=" + radius + ". Use a positive radius!");
			return null;
		}
		double[] r = randomPointOnUnitSphere();
		for (int i = 0; i < r.length; i++) {
			r[i] = r[i] * radius;
		}
		return r;
	}
	
	/**
	 * A wrapper of java.util.Random to generate a random number in (lowerBound, upperBound), both
	 * bounds are exclusive. Since Math.random() is thread-safe, random() should be thread-safe.
	 * 
	 * @param lowerBound
	 * @param upperBound
	 * @return A random number in (lowerBound, upperBound).
	 */
	public static double randomDoubleExclusive(double lowerBound, double upperBound) {
		double d = ThreadLocalRandom.current().nextDouble() * (upperBound - lowerBound) + lowerBound;
		if (d == lowerBound) {
			return randomDoubleExclusive(lowerBound, upperBound);
		}
		return d;
	}
	
	/**
	 * A thread-safe random double generator within [0, upperBound).
	 * @param upperBound
	 * @return
	 */
	public static double randomDouble(double upperBound) {
		return ThreadLocalRandom.current().nextDouble(upperBound);
	}
	
	public static double random() {
		return ThreadLocalRandom.current().nextDouble();
	}
	
	/**
	 * A thread-safe random integer generator within [0, upperBound). I.e. include 0, but 
	 * exclude upperBound.
	 * 
	 * @param upperBound
	 * @return
	 */
	public static int randomInt(int upperBound) {
		return ThreadLocalRandom.current().nextInt(upperBound);
	}
	
	/**
	 * A thread-safe random integer generator within [lowerBound, upperBound).
	 * @param lowerBound
	 * @param upperBound
	 * @return
	 */
	public static int randomInt(int lowerBound, int upperBound) {
		return ThreadLocalRandom.current().nextInt(lowerBound, upperBound);
	}
	
	/**
	 * Get distance between two points in Cartesian basis.
	 * @param u Cartesian coordinates.
	 * @param v Cartesian coordiantes.
	 * @return distance between two points.
	 */
	public static double distance(double[] u, double[] v) {
		if (u.length != v.length) {
			Logger.error("GAMath.distance: Points don't have same dimensions: "
					   + u.length + ", " + v.length);
			return Double.MAX_VALUE;
		}
		double dist = 0;
		double diff = 0;
		for (int i = 0; i < u.length; i++) {
			diff = u[i] - v[i];
			dist += diff * diff;
		}
		return Math.sqrt(dist);
	}
	
	public static int arraySum(int[] array){
		int sum = 0;
		for(int i = 0; i < array.length; i++){
			sum += array[i];
		}
		return sum;
	}
	
	public static double arraySum(double[] array){
		double sum = 0;
		for(int i = 0; i < array.length; i++){
			sum += array[i];
		}
		return sum;
	}
	
	public static double max(double[] array) {
		double max = -Double.MIN_VALUE;
		for (double d: array) {
			max = Math.max(d, max);
		}
		return max;
	}
	
	public static double min(double[] array) {
		double min = Double.MAX_VALUE;
		for (double d: array) {
			min = Math.min(d, min);
		}
		return min;
	}
	
	public static double[][] matrixPower (double[][] matrix, double power) {
		double[][] returnArray = new double[matrix.length][];
	    for (int i = 0; i < matrix.length; i++) {
	      returnArray[i] = MSMath.arrayPower(matrix[i], power);
	    }
	    return returnArray;
	}
	
	// Written by Sam.
	public static double[][] getRotationMatrixAxisAngle(double[] axis, double angleDegrees) {
		 double[][] rotationMatrix = new double[3][3];
		 double angleRadians = Math.toRadians(angleDegrees);
		 
		 axis = MSMath.normalize(axis);
		 double sqx = axis[0] * axis[0];
		 double sqy = axis[1] * axis[1];
		 double sqz = axis[2] * axis[2];
		 double x = axis[0];
		 double y = axis[1];
		 double z = axis[2];
		
		 //Applying the Rodrigues formula:
		 rotationMatrix[0][0] = Math.cos(angleRadians) + (sqx * (1 - Math.cos(angleRadians)));
		 rotationMatrix[0][1] = x * y * (1 - Math.cos(angleRadians)) - z * Math.sin(angleRadians);
		 rotationMatrix[0][2] = x * z * (1 - Math.cos(angleRadians)) + y * Math.sin(angleRadians);
		 rotationMatrix[1][0] = x * y * (1 - Math.cos(angleRadians)) + z * Math.sin(angleRadians);
		 rotationMatrix[1][1] = Math.cos(angleRadians) + (sqy * (1 - Math.cos(angleRadians)));
     	 rotationMatrix[1][2] = y * z * (1 - Math.cos(angleRadians)) - x * Math.sin(angleRadians);
     	 rotationMatrix[2][0] = x * z * (1 - Math.cos(angleRadians)) - y * Math.sin(angleRadians);
    	 rotationMatrix[2][1] = y * z * (1 - Math.cos(angleRadians)) + x * Math.sin(angleRadians);
     	 rotationMatrix[2][2] = Math.cos(angleRadians) + (sqz * (1 - Math.cos(angleRadians)));
		 
		 return rotationMatrix;
	}  
	
	public static double[][] getRandomRotationMatrix() {
		double theta = ThreadLocalRandom.current().nextDouble() * 2 * Math.PI;
		double phi = ThreadLocalRandom.current().nextDouble() * Math.PI;

		double[][] rotateMatrix = new double[][] {{ Math.cos(phi), 0.0, -Math.sin(phi) },
				{ Math.sin(theta) * Math.sin(phi), Math.cos(theta), Math.sin(theta) * Math.cos(phi) },
				{ Math.cos(theta) * Math.sin(phi), -Math.sin(theta), Math.cos(theta) * Math.cos(phi) } };

		return rotateMatrix;
	}
	
	/**
	 * Randomly pick numMove numbers from 0 - total (exclusive), and return them as an integer array.
	 * @param total
	 * @param numMove
	 * @return
	 */
	public static int[] randomSamplingInt(int total, int numMove) {
		int[] indices = new int[numMove];
		if (total == numMove) {
			for (int i = 0; i < numMove; i++) {
				indices[i] = i;
			}
		} else {
			Random rand = new Random();
			for (int i = 0; i < numMove; i++) {
				int randomIndex = rand.nextInt(total);
				if (!ArrayUtils.arrayContains(indices, randomIndex)) {
					indices[i] = randomIndex;
				} else {
					i--;
				}
			}
		}
		return indices;
	}
	
}
