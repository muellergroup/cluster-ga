package ga.utils.app;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ga.HPC;
import ga.SimilarityCalculator2;
import ga.io.LAMMPS.LAMMPSDataFile_MTP;
import ga.minimizer.LAMMPS;
import ga.minimizer.VASP;
import ga.utils.ArgParser;
import ga.utils.GAMath;
import ga.utils.GAUtils;
import matsci.Element;
import matsci.io.app.log.Status;
import matsci.io.vasp.POSCAR;
import matsci.structure.Structure;

/**
 * Analyze a GA calculation or a single cluster.
 * 
 * Usage: Get a list of index of clusters that are unique to each other. 
 *    java -jar Analyzer.jar uniqCluster ./similarity.dat ./candidates.dat
 * 
 * Acquire information for a calculation. The outputs are: calcDir, final
 * energy, total number of bonds, average Coordination number, average bond
 * length, shortest bond length, longest bond length. 
 *    java -jar Analyzer.jar [-e Au,S,C,H] -c 3.2 analyze ./folder_of_calculation
 * 
 * @author ywang393
 *
 */
public class Analyzer {

	private final static String FILESEPARATOR = System.getProperty("file.separator");
	private static SimilarityCalculator2 calculator = new SimilarityCalculator2(false, false, GAUtils.getNNDistanceMap());

	public static void main(String[] args) {
		ArgParser parser = new ArgParser(args);
		List<String> arguments = parser.getArguments();
		Map<String, String> options = parser.getOptions();

		if (arguments.size() == 0) {
			if (options.containsKey("-h")) {
				Analyzer.getHelp();
			} else {
				Status.basic("No arguments provided! Exit");
				System.exit(1);
			}
		} else {
			if (arguments.get(0).equals("uniqCluster")) {
				listUniqueCluster(arguments.get(1), arguments.get(2));
			} else if (arguments.get(0).equals("groupCluster")){
				groupCluster(arguments.get(1), Double.parseDouble(arguments.get(2)));
		    } else if (arguments.get(0).equals("analyze")) {
				String calcType = findCalcType(arguments.get(1));
				String information = Analyzer.analyzeCalculation(calcType, arguments.get(1), options);
				System.out.println(information);
			} else if (arguments.get(0).equalsIgnoreCase("calc-similarity")) {
				calculateSimilarity(arguments);
			}
		}
	}

	public static void listUniqueCluster(String similarityTable, String candidates) {
		// Parse similarity table.
		List<List<Boolean>> similarities = null;
		List<String> lines = HPC.read(similarityTable);
		// similarities.get(i).get(j) (i > j) will give similarity of two clusters i+1,
		// j+1.
		similarities = new ArrayList<>(lines.size());
		for (int i = 0; i < lines.size(); i++) {
			String[] values = lines.get(i).trim().split("\\s+");
			List<Boolean> booleans = new ArrayList<>(values.length - 1);
			for (int j = 1; j < values.length; j++) { // First term is cluster index;
				booleans.add(Boolean.parseBoolean(values[j]));
			}
			similarities.add(booleans);
		}

		// Parse energies of cluster;
		List<Double> energies = new ArrayList<Double>(similarities.size());
		lines = HPC.read(candidates);
		for (String line : lines) {
			if (line.contains("Dir")) {
				if (line.contains("Energy")) {
					energies.add(Double.parseDouble(line.split("\\s+")[7]));
				} else {
					energies.add(0.0); // Still Running;
				}
			}
		}
		for (int i = energies.size(); i < similarities.size(); i++) {
			energies.add(0.0); // For the ones that's not in candidates.dat.
		}

		// Sort indices into groups of similar clusters;
		List<List<Integer>> groups = new ArrayList<>(similarities.size());
		for (int i = 0; i < similarities.size(); i++) {
			List<Boolean> similar = similarities.get(i);
			boolean unique = true;
			for (int j = 0; j < similar.size(); j++) {
				if (similar.get(j)) {
					unique = false;
					addToGroups(groups, i + 1, j + 1);
					break;
				}
			}
			if (unique) {
				List<Integer> newGroup = new ArrayList<>();
				newGroup.add(i + 1);
				groups.add(newGroup);
			}
		}

		List<Integer> uniqueLowEnergyClusters = pickLowEnergyClusters(groups, energies);

		for (int i : uniqueLowEnergyClusters) {
			System.out.println(i);
		}
	}
	
	/**
	 * For now, only treat VASP calculations with CONTCARs.
	 * 
	 * TODO: expand to all types of calculations, include hybrid (VASP + LAMMPS)
	 * 
	 * @param pathFile A file containing paths to a list of calculations. 
	 */
	public static void groupCluster(String pathFile, double similarityThreshold) {
		List<String> lines = HPC.read(pathFile);
		List<Structure> clusters = new ArrayList<>();
		for (String line: lines) {
			clusters.add(new Structure(new POSCAR(line + "/CONTCAR")));
		}
		
		List<List<Double>> scoreTable = new ArrayList<>();
		for (int i = 0; i < clusters.size(); i++) {
			List<Double> scores = new ArrayList<>();
			for (int j = 0; j < clusters.size(); j++) {
				scores.add(calculator.getScore(clusters.get(i), clusters.get(j)));
			}
			scoreTable.add(scores);
		}
		
		// Each entry is line number of the file.
		List<List<Integer>> groups = new ArrayList<>();
		for (int i = 0; i < clusters.size(); i++) {
			boolean unique = true;
			double ss; // similarity score.
			double lowestSS = Double.MAX_VALUE;
			int mostSimilarTerm = -1;
			for (int j = 0; j < i; j++) { // Search similar cluster in preceding configurations.
				ss = scoreTable.get(i).get(j);
				if (ss < similarityThreshold) {
					unique = false;
					if (ss < lowestSS) {
						mostSimilarTerm = j;
						lowestSS = ss;
					}
				}
			}
			if (!unique) {
				addToGroups(groups, i + 1, mostSimilarTerm + 1);
			} else { // Create a new group;
				List<Integer> newGroup = new ArrayList<>();
				newGroup.add(i + 1);
				groups.add(newGroup);
			}
		}
		
		for (int i = 0; i < groups.size(); i++) {
			List<Integer> group = groups.get(i);
			// Group index start from 1.
			StringBuilder sb = new StringBuilder("Group " + (i+1) + " : ");
			for (int j : group) {
				sb.append(j + " ");
			}
			System.out.println(sb.toString());
		}
	}
	
	public static void calculateSimilarity(List<String> arguments) {
		if (arguments.size() != 3) {
			System.err.println("Error! Wrong number of arguments: " + arguments.size());
			getHelp();
			return;
		}
		Structure s1 = new Structure(new POSCAR(arguments.get(1)));
		Structure s2 = new Structure(new POSCAR(arguments.get(2)));
		double score = calculator.getScore(s1, s2);
		System.out.println("Similarity Score = " + score);
	}

	/**
	 * Add clusterIndex to the list of integer, whose members are index of clusters
	 * that are similar to clusterIndex.
	 * 
	 * @param groups              List of List of integers. Each integer is a
	 *                            cluster index.
	 * @param clusterIndex        Candidate index of the current cluster.
	 * @param similarClusterIndex Candidate index of a similar cluster of the
	 *                            current cluster.
	 */
	private static void addToGroups(List<List<Integer>> groups, int clusterIndex, int similarClusterIndex) {
		for (List<Integer> lists : groups) {
			if (lists.contains(similarClusterIndex)) {
				lists.add(clusterIndex);
			}
		}
	}

	/**
	 * 
	 * @param groups   Groups of candidates index of similar clusters.
	 * @param energies
	 * @return
	 */
	private static List<Integer> pickLowEnergyClusters(List<List<Integer>> groups, List<Double> energies) {
		List<Integer> uniqueLowEnergyClusters = new ArrayList<>(groups.size());
		for (int i = 0; i < groups.size(); i++) {
			List<Integer> group = groups.get(i);
			double lowestEnergy = Double.MAX_VALUE;
			int clusterIndex = -1;
			for (int j = 0; j < group.size(); j++) {
				if (energies.get(group.get(j) - 1) < lowestEnergy) {
					clusterIndex = group.get(j); // Start from 0;
				}
			}
			uniqueLowEnergyClusters.add(clusterIndex);
		}
		return uniqueLowEnergyClusters;
	}

	public static String findCalcType(String calcDir) {
		File incar = new File(calcDir + FILESEPARATOR + VASP.getInputFileName());
		File lammpsIn = new File(calcDir + FILESEPARATOR + LAMMPS.getInputFileName());
		if (incar.exists()) {
			return "VASP"; // VASP takes precedence.
		} else if (lammpsIn.exists()) {
			return "LAMMPS";
		} else {
			Status.error("Cannot find output of any calculation. Exit");
			System.exit(1);
		}
		return "NONE";
	}

	/**
	 * Collect the following list of info from the relaxed structure of a
	 * calculation: total number of bonds; average value of coordination number;
	 * average value of bond length; shortest bond length; longest bond length;
	 * 
	 * If calculation hasn't been finished, information on initial structure is
	 * collected.
	 * 
	 * @param type      "VASP" or "LAMMPS"
	 * @param calcDir   Folder containing a relaxation.
	 * @param options Map of options, containing cutoff (and Element list).
	 * @return
	 */
	public static String analyzeCalculation(String type, String calcDir, Map<String, String> options) {
		StringBuilder sb = new StringBuilder(calcDir + " ");
		double energy = 0.0;
		Structure struct = null;
		if (type.equals("VASP")) {
			File outcar = new File(calcDir + FILESEPARATOR + VASP.getOutputFileName());
			if (outcar.exists()) {
				struct = new Structure(new POSCAR(calcDir + FILESEPARATOR + VASP.getStructureFileName(true)));
				try {
					energy = VASP.readEnergy(HPC.readAllLines(outcar.getPath()));
				} catch (IOException e) {
					Status.error("Failed to read energy from log.lammps!");
					energy = 0.0;
				}
			} else {
				struct = new Structure(new POSCAR(calcDir + FILESEPARATOR + VASP.getStructureFileName(false)));
			}
		} else if (type.equals("LAMMPS")) {
			if (!options.containsKey("-e")) {
				Status.error("Need elements list for LAMMPS calculation! Exit");
				System.exit(1);
			}
			String[] elementsStr = options.get("-e").split(",");
			Element[] elements = new Element[elementsStr.length];
			for (int i = 0; i < elementsStr.length; i++) {
				elements[i] = Element.getElement(elementsStr[i]);
			}
			boolean[] isVectorPeriodic = { true, true, true };
			File lammpsOutput = new File(calcDir + FILESEPARATOR + LAMMPS.getOutputFileName());
			if (lammpsOutput.exists()) {
				struct = new Structure(new LAMMPSDataFile_MTP(
						calcDir + FILESEPARATOR + LAMMPS.getStructureFileName(true), elements, isVectorPeriodic));
				try {
					energy = LAMMPS.readEnergy(HPC.readAllLines(lammpsOutput.getPath()));
				} catch (IOException e) {
					Status.error("Failed to read energy from log.lammps!");
					energy = 0.0;
				}
			} else {
				struct = new Structure(new LAMMPSDataFile_MTP(
						calcDir + FILESEPARATOR + LAMMPS.getStructureFileName(false), elements, isVectorPeriodic));
			}
		}

		if (!options.containsKey("-c")) {
			Status.error("Need cutoff distance for command \"analyze\"! Exit");
			System.exit(1);
		}
		double cutoff = Double.parseDouble(options.get("-c"));
		double[][] interatomicDistance = GAUtils.getBonds(struct);

		List<Bond> bonds = Analyzer.findBonds(interatomicDistance, cutoff);

		int numBonds = bonds.size();
		// Both atoms in a bond acquire one increment in their coordination numbers.
		double avgCoordinationNumber = numBonds * 2 / (double) struct.numDefiningSites();

		double avgBondLength = 0, minBondLength = Double.MAX_VALUE, maxBondLength = -Double.MAX_VALUE;
		for (Bond bond : bonds) {
			double l = bond.getLength();
			avgBondLength += l;
			if (l < minBondLength) {
				minBondLength = l;
			}

			if (l > maxBondLength) {
				maxBondLength = l;
			}
		}
		avgBondLength = avgBondLength / bonds.size();

		sb.append(String.format("%12.7f ", energy) + String.format("%4d ", numBonds));
		sb.append(String.format("%4.2f ", avgCoordinationNumber));
		sb.append(String.format("%8.5f %8.5f % 8.5f", avgBondLength, minBondLength, maxBondLength));
		return sb.toString();
	}

	private static List<Bond> findBonds(double[][] distances, double cutoff) {
		List<Bond> bonds = new ArrayList<>();
		int numAtoms = distances.length;
		for (int i = 0; i < numAtoms; i++) {
			for (int j = i + 1; j < numAtoms; j++) {
				if (distances[i][j] <= cutoff) {
					bonds.add(new Bond(i, j, distances[i][j]));
				}
			}
		}
		return bonds;
	}

	private static void getHelp() {
		StringBuilder sb = new StringBuilder();
		String EOL = System.getProperty("line.separator");
		sb.append("Usage:" + EOL);
		sb.append("    Get a list of index of clusters that are unique to each other." + EOL);
        sb.append("        java -jar Analyzer.jar uniqCluster ./similarity.dat ./candidates.dat" + EOL);
        sb.append(EOL);
        sb.append("    Get groups of clusters, cluters within which are similar to each other but dissimliar" + EOL);
        sb.append("    to clusters in other groups." + EOL);
        sb.append("        java -jar Analyzer.jar groupCluster ./file_of_folder_paths similarityThreshold" + EOL);
        sb.append(EOL);
        sb.append("    Acquire information for a calculation. The outputs are: calcDir, final energy," + EOL);
        sb.append("    total number of bonds, average Coordination number, average bond length," + EOL);
        sb.append("    shortest bond length, longest bond length." + EOL);
        sb.append("        java -jar Analyzer.jar [-e Au,S,C,H] -c CUTOFF analyze ./folder_of_calculation" + EOL);
        sb.append(EOL);
        sb.append("    Calculate similarity score between two structures." + EOL);
        sb.append("        java -jar Analyzer.jar calc-similarity ./POSCAR_1 ./POSCAR_2");
        System.out.println(sb.toString());
	}

	/**
	 * A cheap helper class to store bonds. For a more sophisticated class, don't
	 * use this one.
	 * 
	 * @author ywang393
	 *
	 */
	private static class Bond {

		private int dim = 3;
		private double bondLength;

		private int site1Index = 0;
		private int site2Index = 0; // Index of site in a structure.
		private boolean hasIndex = false; // siteIndex has been assigned.
		private double[] site1; // Cartesian cooridnates.
		private double[] site2;
		private boolean hasCoord = false;

		public Bond() {
			site1 = new double[3];
			site2 = new double[3];
			bondLength = 0;
			hasCoord = true;
		}

		public Bond(double[] site1, double[] site2) {
			dim = site1.length;
			this.site1 = site1;
			this.site2 = site2;
			bondLength = GAMath.distance(site1, site2);
			hasCoord = true;
		}

		public Bond(int site1Index, int site2Index, double length) {
			this.site1Index = site1Index;
			this.site2Index = site2Index;
			this.bondLength = length;
			hasIndex = true;
		}

		public boolean containSite(int siteIndex) {
			if (site1Index == siteIndex || site2Index == siteIndex) {
				return true;
			}
			return false;
		}

		public double getLength() {
			return bondLength;
		}
	}

}
