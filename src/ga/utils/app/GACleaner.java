package ga.utils.app;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ga.HPC;
import ga.Input;
import ga.io.Logger;
import ga.minimizer.LAMMPS;
import ga.minimizer.VASP;
import ga.multithreaded.Candidates;
import ga.utils.GAUtils;

/**
 * Usage: In the root folder of GA calculation, i.e the one contains INGA.
 *     java -jar GACleaner.jar
 * 
 * @author ywang393
 * @version 2020-09-01
 */
public class GACleaner {
	
	private static List<String> LAMMPSTargets;
	private static List<String> VASPTargets;
	private static int counter = 0;
	
	public static void main(String[] args) {
		Logger.basic("Start cleaning calculation: " + System.getProperty("user.dir"));
		Logger.setLogLevelQuiet();
		Input.initialize();
		Logger.setLogLevelBasic();
		initTargets();
		File[] clusterFolders = new File(Candidates.ALLCANDIDATES).listFiles((file, name) -> name.matches("[0-9]+"));
		if (clusterFolders != null) { 
			clusterFolders = HPC.sortFolderByNumber(clusterFolders);
			for (int i = 0; i < clusterFolders.length; i++) {
				Logger.detail("Clean candidate-" + i);
				cleanFolder(clusterFolders[i]);
				File[] backupDirs = clusterFolders[i].listFiles((file, name) -> name.matches("[0-9]+"));
				for (File dir: backupDirs) {
					cleanFolder(dir);
				}
			}
		}
		
		File[] trainDirs = new File(Input.getWorkDirectory() + HPC.FILESPTR + "train").
				listFiles((file, name) -> name.matches("[0-9]+"));
		if (trainDirs != null && Input.getMode().equals("Active")) {
			for (int i = 0; i < trainDirs.length; i++) {
				Logger.detail("Clean training folder-" + i);
				File[] clusterDirs = trainDirs[i].listFiles((file, name) -> name.matches("[0-9]+"));
				for (File dir: clusterDirs) {
					cleanFolder(dir);
				}
			}
		}
		
		Logger.basic("Removed " + counter + " files.");
	}
	
	public static void cleanFolder(String path) {
		cleanFolder(new File(path));
	}
	
	public static void cleanFolder(File dir) {
		String type = GAUtils.findMinimizer(Input.getMode(), dir.getAbsolutePath());
		if (type.equals("LAMMPS")) {
			deleteTargets(dir.getAbsolutePath(), LAMMPSTargets);
		} else if (type.equals("VASP")) {
			deleteTargets(dir.getAbsolutePath(), VASPTargets);
		}
	}
	
	public static void deleteTargets(String path, List<String> targets) {
		for (String file: targets) {
			File f = new File(path + HPC.FILESPTR + file);
			if (f.exists()) { 
				f.delete(); 
				counter++;
			}
		}
	}
	
	public static void initTargets() {
		LAMMPSTargets = new ArrayList<String>();
		Collections.addAll(LAMMPSTargets, LAMMPS.REDUNDANTFILELIST);
		if (Input.getMode().equals("Active")) {
			Collections.addAll(LAMMPSTargets, "struct.cfg", "end.cfg", "stdout_calc_grade", "state.mvs", "state.als");
		}
		
		VASPTargets = new ArrayList<>();
		Collections.addAll(VASPTargets, VASP.REDUNDANTFILELIST);
		if (Input.getMode().equals("Active")) {
			Collections.addAll(VASPTargets, "graded_outcar.cfg", "state.mvs", "state.als", "stdout_calc_grade");
			if (Input.runMD()) {
				Collections.addAll(VASPTargets, "MD" + HPC.FILESPTR + "CHG", "MD" + HPC.FILESPTR + "CHGCAR",
						"MD" + HPC.FILESPTR + "REPORT", "MD" + HPC.FILESPTR + "WAVECAR", 
						"MD" + HPC.FILESPTR + "PENALTYPOT", "MD" + HPC.FILESPTR + "ICONST",
						"MD" + HPC.FILESPTR + "HILLSPOT");
			}
		}
	}
	
}
