package ga.utils.app;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import ga.HPC;
import ga.io.Logger;
import ga.io.VASP.INCAR;
import ga.io.VASP.OSZICAR;
import ga.io.mtp.CFG;
import ga.minimizer.VASP;
import ga.potential.MTP;
import matsci.Element;
import matsci.Species;
import matsci.io.vasp.POSCAR;
import matsci.structure.Structure;

/**
 * Deprecated
 * A app wrapper of MTP.prepareCFG(). Used to prepare CFG by different strategies.
 * @author ywang393
 * @version 2020-08-17
 */
public class CFGConvertor {
	
	private static String FILESPTR = System.getProperty("file.separator");
	
	public static void main(String[] args) {
		String calcDir = args[0];
		boolean includeMD = Boolean.parseBoolean(args[1]);
		boolean removeSimilar = Boolean.parseBoolean(args[2]);
		double HET = Double.parseDouble(args[3]);
		double LET = Double.parseDouble(args[4]);
		double similarityThreshold = Double.parseDouble(args[5]);
		String output = args[5];
		
		POSCAR contcar = new POSCAR(calcDir + FILESPTR + "CONTCAR");
		Species[] species = contcar.getSpeciesSet();
		Element[] elements = new Element[species.length];
		for (int i = 0; i < elements.length; i++) {
			elements[i] = species[i].getElement();
		}
		
		List<String> CFGLines = new ArrayList<>();
		// VASP output
		CFGLines.addAll(MTP.prepareCFG(calcDir, elements, "dummy", "dummy", "dummy", false, false, 
				                       removeSimilar, HET, LET, 1.01, similarityThreshold, 1, "RELAX"));
		
		if (includeMD) {
			CFGLines.addAll(MTP.prepareCFG(calcDir + System.getProperty("file.separator") + "MD", 
					        elements, "dummy", "dummy", "dummy", removeSimilar, false, false, HET, 
					        LET, 1.01, similarityThreshold, 1, "MD"));
		}
		HPC.write(CFGLines, calcDir + System.getProperty("file.separator") + output, false);
	}
	
	public static void getHelp() {
		Logger.basic("Usage: ");
		Logger.basic("    java -jar CFGConvertor.jar <calcDir> <include_MD> <removeSimilar> <HET>"
				   + " <includeAll> <LET> <similarityThreshold> <output>");
	}
	
	public static List<String> prepareCFG(String calcDir, double HET, String type, String output) {
		if (!new File(calcDir + HPC.FILESPTR + VASP.getOutputFileName()).exists()) {
			Logger.error("OUTCAR doesn't exist! VASP evaluation might have failed. No CFGs are added.");
			return new ArrayList<String>();
		} else if (MTP.convertCFG(calcDir, "OUTCAR", "outcar.cfg") != 0) {
			Logger.error("Converting OUTCAR in " + calcDir + " failed! No CFGs are added.");
			return new ArrayList<String>();
		}
		
		INCAR incar = new INCAR(calcDir + HPC.FILESPTR + VASP.getInputFileName());
		OSZICAR oszicar = new OSZICAR(calcDir + HPC.FILESPTR + "OSZICAR", type);
		CFG cfgs = new CFG(calcDir + HPC.FILESPTR + "outcar.cfg");
		
		// Remove ionic steps that reaches NELM. SCF calculation doesn't converge for these steps.
		cfgs.removeUnconvergedImages(oszicar.getSCFSteps(), incar.getNELM());
		// Use last magnetization value as the correct magnetization.
		cfgs.removeWrongMagImages(oszicar.getMagetization(), oszicar.getMagnetizationList());
		// remove configurations with energies exceeding the ground-state energy more than ENERGYTHRESHOLD
		cfgs.removeHighEnergyImages(HET);
		cfgs.writeFile(calcDir + HPC.FILESPTR + "outcar.cfg");
		
		// cfgs sets includeImage[i] to false, but didn't remove them. Thus, a reload is needed.
		return HPC.read(calcDir + HPC.FILESPTR + output);
	}
}
