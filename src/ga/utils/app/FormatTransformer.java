package ga.utils.app;

import ga.utils.ArgParser;
import ga.io.Logger;
import ga.io.LAMMPS.LAMMPSDataFile_MTP;
import ga.io.mtp.CFG;
import ga.structure.ClusterFactory;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import matsci.Element;
import matsci.io.structure.XYZFile;
import matsci.structure.Structure;
import matsci.io.vasp.POSCAR;

/**
 * An app for transforming structure files be\tween different format.
 * Supported formats for now:
 *      POSCAR -> LAMMPS data file, POSCAR, CFG, .xyz (for seed.dat)
 *      LAMMPS data file -> POSCAR, CFG, .xyz (for seed.dat)
 *      unregulated POSCAR -> regulated POSCAR
 * 
 * Input format: POSCAR; LAMMPS data file; CFG;
 * Output format: POSCAR, LAMMPS data file, .xyz, CFG;
 * 
 * For command "center", if OUTPUT_FORMAT is missing, use INPUT_FORMAT.
 * 
 * Usage:
 *      java -jar FormatTransfer.jar [-v] [-h] -i INPUT_FORMAT -o OUTPUT_FORMAT input_file output_file
 *      java -jar FormatTransfer.jar center -i INPUT_FORMAT [ -o OUTPUT_FORMAT ] input_file output_file
 * 		
 * Example:
 *      java -jar FormatTransfer.jar -h
 *      java -jar FormatTransfer.jar -i POSCAR -o LAMMPS ./POSCAR ./struct.lmp
 *      java -jar FormatTransfer.jar -i POSCAR ./POSCAR -o POSCAR ./POSCAR.regulated
 *      java -jar FormatTransfer.jar -v -i POSCAR ./POSCAR -o POSCAR ./POSCAR.regulated
 *      java -jar FormatTransfer.jar -i LAMMPS -e Au,S,C,H -o POSCAR ./end.lmp ./POSCAR
 *      java -jar FormatTransfer.jar -i LAMMPS -e Al -o CFG ./end.lmp ./relaxed.cfg
 *      java -jar FormatTransfer.jar -i CFG -e Au,S,C,H -o POSCAR ./outcar.cfg ./POSCAR
 *      java -jar FormatTransfer.jar center -i POSCAR ./CONTCAR ./CONTCAR_centered
 * @author ywang393
 *
 */
public class FormatTransformer {

	private static String INPUT_FORMAT = null;
	private static String OUTPUT_FORMAT = null;
	private static ArrayList<Element> ELEMENTS = new ArrayList<>(10);
	private static List<Structure> STRUCTURES = new ArrayList<Structure>();
	
	public static void main(String[] args) {

		ArgParser parser = new ArgParser(args);
		Map<String, String> options = parser.getOptions();
		List<String> arguments = parser.getArguments();
		
		if (options.containsKey("-h")) {
			getHelp();
			System.exit(0);
		}
		
		try {
			if (!parseOptions(options)) {
				System.exit(1);
			}
			
			if (arguments.get(0).equalsIgnoreCase("center")) {
				if (arguments.size() != 3) {
					Logger.error("Command \"center\" needs 3 arguments! Missing input/output file name.");
				}
				moveCGToCenter(arguments.get(1), arguments.get(2));
			} else {
				if (arguments.size() != 2) {
					Logger.error("Format transformation needs 2 arguments! Missing input file name.");
				}
				formatTransfer(arguments.get(0), arguments.get(1));
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	private static void formatTransfer(String input, String output) {
		if (readInput(input) != 1) {
			Logger.basic("Failed to read input file " + input);
		}
		
		writeOutput(output);
	}
	
	private static void moveCGToCenter(String input, String output) {
		if (readInput(input) != 1) {
			Logger.basic("Failed to read input file " + input);
		}
		for (int i = 0; i < STRUCTURES.size(); i++) {
			Object[] clusList = ClusterFactory.structureToObjectArray(STRUCTURES.get(i));
			double cellSize = STRUCTURES.get(i).getCellVectors()[0].length();
			ClusterFactory.moveCGToCenter(clusList, cellSize);
			STRUCTURES.set(i, ClusterFactory.objectToStructure(clusList, cellSize));
		}
		writeOutput(output);
	}
	
	private static boolean parseOptions(Map<String, String> options) {
		if (options.containsKey("-i")) {
			if (options.get("-i").length() == 0) {
				Logger.error("Option -i requires specify input format.");
				return false;
			}
			String inputFormat = options.get("-i");
			if (inputFormat.equalsIgnoreCase("POSCAR")) {
				INPUT_FORMAT = "POSCAR";
			} else if (inputFormat.equalsIgnoreCase("LAMMPS")) {
				INPUT_FORMAT = "LAMMPS";
			} else if (inputFormat.equalsIgnoreCase("CFG")) {
				INPUT_FORMAT = "CFG";
			} else {
				Logger.error("Input format not recognized: " + inputFormat + ". " 
			                +"Use \"POSCAR\" or \"LAMMPS\".");
				return false;
			}
		} else {
			Logger.error("Need an input format!");
		}
		
		// Output format: Commend "center" might not specify an output format.
		if (options.containsKey("-o")) {
			String outputFormat = options.get("-o");
			if (outputFormat.equalsIgnoreCase("POSCAR")) {
				OUTPUT_FORMAT = "POSCAR";
			} else if (outputFormat.equalsIgnoreCase("LAMMPS")) {
				OUTPUT_FORMAT = "LAMMPS";
			} else if (outputFormat.equals("XYZ")) {
				OUTPUT_FORMAT = "XYZ";
			} else if (outputFormat.equalsIgnoreCase("CFG")){
				OUTPUT_FORMAT = "CFG";
			} else {
				Logger.error("Output format not recognized: " + outputFormat + ". " 
			                +"Use \"POSCAR\", \"LAMMPS\" or \"XYZ\".");
				return false;
			}
		}
		
		if (options.containsKey("-e")) {
			if (options.get("-e").length() == 0) {
				Logger.error("Option -e requires specify element types.");
				return false;
			}
			String[] elements = options.get("-e").split(",");
			for (String e: elements) {
				ELEMENTS.add(Element.getElement(e));
			}
		}
		
		if (INPUT_FORMAT != null && (INPUT_FORMAT.equals("LAMMPS") || INPUT_FORMAT.equals("CFG")) 
				&& ELEMENTS.size() == 0) {
			Logger.error("Reading LAMMPS/CFG data file requires specifying elements!");
			return false;
		}
		
		// For command "center".
		if (OUTPUT_FORMAT == null) {
			OUTPUT_FORMAT = INPUT_FORMAT;
		}
		return true;
	}
	
	/**
	 * Initialize STRUCT from input file.
	 * 
	 * @param file Path to input file.
	 * @return 1 if succeed; 0 if failed
	 */
	private static int readInput(String file) {
		if (INPUT_FORMAT.equals("POSCAR")) {
			try {
				POSCAR poscar = new POSCAR(file); // POSCAR handles FileNotFoundException.
				STRUCTURES.add(new Structure(poscar));
			} catch (NumberFormatException e) {
				throw new RuntimeException("Cannot parse " + file + ". Wrong format!");
			}
		} else if (INPUT_FORMAT.equals("LAMMPS")) {
			try {
				LAMMPSDataFile_MTP lammps = new LAMMPSDataFile_MTP(file, 
						ELEMENTS.toArray(new Element[0]), new boolean[] {true, true, true});
				STRUCTURES.add(new Structure(lammps));
			} catch (NumberFormatException e) {
				throw new RuntimeException("Cannot parse " + file + ". Wrong format!");
			}
		} else if (INPUT_FORMAT.equals("CFG")) {
			try {
				CFG cfgs = new CFG(file, ELEMENTS.toArray(new Element[0]));
				STRUCTURES = cfgs.getStructures();
			} catch (NumberFormatException e) {
				throw new RuntimeException("Cannot parse " + file + ". Wrong format!");
			} 
		}
		if (STRUCTURES == null) {
			return 0;
		}
		return 1;
	}
	
	private static void writeOutput(String output) {
		if (OUTPUT_FORMAT.equals("POSCAR")) {
			if (STRUCTURES.size() == 1) {
				POSCAR poscar = new POSCAR(STRUCTURES.get(0));
				poscar.writeFile(output);
			} else {
				for (int i = 0; i < STRUCTURES.size(); i++) {
					POSCAR poscar = new POSCAR(STRUCTURES.get(i));
					poscar.writeFile(output + "_" + (i + 1));
				}
			}
		} else if (OUTPUT_FORMAT.equals("LAMMPS")) {
			if (STRUCTURES.size() == 1) {
				LAMMPSDataFile_MTP lammps = new LAMMPSDataFile_MTP(STRUCTURES.get(0));
				lammps.writeFile(output);
			} else {
				for (int i = 0; i < STRUCTURES.size(); i++) {
					LAMMPSDataFile_MTP lammps = new LAMMPSDataFile_MTP(STRUCTURES.get(i));
					lammps.writeFile(output + "_" + i);
				}
			}
		} else if (OUTPUT_FORMAT.equals("XYZ")) {
			if (STRUCTURES.size() == 1) {
				XYZFile xyz= new XYZFile(STRUCTURES.get(0));
				xyz.writeFile(output);
			} else {
				for (int i = 0; i < STRUCTURES.size(); i++) {
					XYZFile xyz= new XYZFile(STRUCTURES.get(i));
					xyz.writeFile(output + "_" + i);
				}
			}
		} else if (OUTPUT_FORMAT.equals("CFG")) {
			CFG cfgs = new CFG(STRUCTURES.toArray(new Structure[0]));
			cfgs.writeFile(output);
		}
	}
	
	private static void getHelp() {
		Logger.basic("An app for transforming structure files between formats.\n" + 
					 "Supported formats for now:\n" + 
					 "     POSCAR -> LAMMPS data file, POSCAR, CFG, .xyz (for seed.dat)\n" + 
					 "     LAMMPS data file -> POSCAR, CFG, .xyz (for seed.dat)\n" + 
					 "     unregulated POSCAR -> regulated POSCAR\n" + 
					 "\n" + 
					 "Input format: POSCAR; LAMMPS data file; CFG\n" + 
					 "Output format: POSCAR, LAMMPS data file, .xyz, CFG;\n" + 
					 "\n" +
					 "Note:\n" +
					 "   1.For LAMMPS structures, element types should be provided as a list delimited by comma.\n" +
					 "   2.For output as CFG file, no energy, forces, and stresses are written out. It cannot be used in\n" +
					 "     MTP training, but extrapolation score can be calculated.\n" +
					 "   3.For input as CFG file, if there are multiple images, an index is appended to the output name.\n" +
					 "   4.For command \"center\", OUTPUT_FORMAT is optional. If missing, use INPUT_FORMAT.\n" +
					 "\n" + 
					 "Usage:\n" + 
					 "     java -jar FormatTransfer.jar [-v] [-h] -i INPUT_FORMAT -o OUTPUT_FORMAT input_file output_file\n" +
					 "     java -jar FormatTransfer.jar center -i INPUT_FORMAT [ -o OUTPUT_FORMAT ] input_file output_file\n" +
					 "\n" + 
					 "Example:\n" + 
					 "     java -jar FormatTransfer.jar -h\n" +
					 "     java -jar FormatTransfer.jar -i POSCAR -o LAMMPS ./POSCAR ./struct.lmp\n" + 
					 "     java -jar FormatTransfer.jar -v -i POSCAR ./POSCAR -o POSCAR ./POSCAR.regulated\n" +
					 "     java -jar FormatTransfer.jar -i POSCAR ./POSCAR -o POSCAR ./POSCAR.regulated\n" + 
					 "     java -jar FormatTransfer.jar -i LAMMPS -e Au,S,C,H -o POSCAR ./end.lmp ./POSCAR\n" + 
					 "     java -jar FormatTransfer.jar -i LAMMPS -e Al -o CFG ./end.lmp ./relaxed.cfg\n" +
					 "     java -jar FormatTransfer.jar -i CFG -e Au,S,C,H -o POSCAR ./outcar.cfg ./POSCAR\n" +
					 "     java -jar FormatTransfer.jar center -i POSCAR ./CONTCAR ./CONTCAR_centered");
	}
}
