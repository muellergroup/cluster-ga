package ga.utils.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ga.io.Logger;
import ga.io.LAMMPS.LAMMPSDataFile_MTP;
import ga.io.mtp.CFG;
import ga.structure.ClusterFactory;
import ga.utils.ArgParser;
import ga.utils.GAUtils;
import matsci.Element;
import matsci.io.app.log.Status;
import matsci.io.vasp.POSCAR;
import matsci.structure.Structure;

/**
 * 
 * @author ywang393
 * @version 2020-08-17
 */
public class StructureAnalyzer {
	
	public static String BONDFORMAT = "%.5f";
	private static String INPUT_FORMAT = null;
	private static ArrayList<Element> ELEMENTS = new ArrayList<>(10);
	private static List<Structure> STRUCTURES = new ArrayList<>();
	
	public static void main(String[] args) {
		ArgParser parser = new ArgParser(args);
		Map<String, String> options = parser.getOptions();
		List<String> arguments = parser.getArguments();
		
		if (options.containsKey("-h")) {
			getHelp();
			System.exit(0);
		} else if (arguments.size() < 2) {
			Logger.error("Have to specify a command and an input file.");
			System.exit(1);
		} else if (!parseOptions(options)) {
			String msg = "Failed to parse options:";
			for (String option: options.keySet()) {
				msg += " " + option + " " + options.get(option);
			}
			msg += ".";
			Logger.error(msg);
			System.exit(1);
		}
		
		String command = arguments.get(0).toLowerCase();
		readStructure(arguments.get(1));
		// Check whether the input structure is realistic.
		if (command.equals("realistic")) {
			checkRealistic();
		} else if (command.equals("bonds")) {
			getBonds(options.containsKey("--sortZ"));
		}
	}
	
	private static void getBonds(boolean sortZ) {
		for (int k = 0; k < STRUCTURES.size(); k++) {
			Structure s = STRUCTURES.get(k);
			if (sortZ) {
				s = ClusterFactory.sortByZ(s);
			}
			Logger.basic("Structure " + k + ":");
			double[][] bonds = GAUtils.getBonds(s);
			for (int i = 0; i < s.numDefiningSites(); i++) {
				StringBuilder sb = new StringBuilder();
				for (int j = 0; j < s.numDefiningSites(); j++) {
					sb.append(String.format(BONDFORMAT + " ", bonds[i][j]));
				}
				Logger.basic(sb.toString());
			}
		}
	}
	
	private static void checkRealistic() {
		// For CFG, output is a list of true and false.
		for (Structure s: STRUCTURES) {
			if (ClusterFactory.checkCollapse(s) || ClusterFactory.checkExplosionByContiguous(s)) {
				Logger.basic("false");
			} else {
				Logger.basic("true");
			}
		}
	}
	
	/**
	 * Parse -i and -e options.
	 * @param options
	 * @return
	 */
	private static boolean parseOptions(Map<String, String> options) {
		if (options.containsKey("-i")) {
			if (options.get("-i").length() == 0) { // Empty string.
				Status.error("Option -i requires input format.");
				return false;
			}
			String inputFormat = options.get("-i");
			if (inputFormat.equalsIgnoreCase("POSCAR")) {
				INPUT_FORMAT = "POSCAR";
			} else if (inputFormat.equalsIgnoreCase("LAMMPS")) {
				INPUT_FORMAT = "LAMMPS";
			} else if (inputFormat.equalsIgnoreCase("CFG")) {
				INPUT_FORMAT = "CFG";
			} else {
				Status.error("Unrecongnised input format: " + inputFormat + ". " 
			                +"Use \"POSCAR\", \"LAMMPS\" or \"CFG\".");
				return false;
			}
		} 
		if (options.containsKey("-e")) {
			if (options.get("-e").length() == 0) {
				Status.error("Option -e requires element types.");
				return false;
			}
			String[] elements = options.get("-e").split(",");
			for (String e: elements) {
				ELEMENTS.add(Element.getElement(e));
			}
		}
		return true;
	}
	
	private static void readStructure(String input) {
		if (INPUT_FORMAT.equals("POSCAR")) {
			try {
				POSCAR poscar = new POSCAR(input); // POSCAR handles FileNotFoundException.
				STRUCTURES.add(new Structure(poscar));
			} catch (NumberFormatException e) {
				throw new RuntimeException("Cannot parse " + input + ". Wrong format!");
			}
		} else if (INPUT_FORMAT.equals("LAMMPS")) {
			try {
				LAMMPSDataFile_MTP lammps = new LAMMPSDataFile_MTP(input, 
						ELEMENTS.toArray(new Element[0]), new boolean[] {true, true, true});
				STRUCTURES.add(new Structure(lammps));;
			} catch (NumberFormatException e) {
				throw new RuntimeException("Cannot parse " + input + ". Wrong format!");
			}
		} else if (INPUT_FORMAT.equals("CFG")) {
			try {
				CFG cfgs = new CFG(input, ELEMENTS.toArray(new Element[0]));
				STRUCTURES = cfgs.getStructures();
			} catch (NumberFormatException e) {
				throw new RuntimeException("Cannot parse " + input + ". Wrong format!");
			} 
		}
	}
	
	public static void getHelp() {
		String EOL = System.getProperty("line.separator");
		StringBuilder sb = new StringBuilder("Usage: " + EOL);
		sb.append("Check whether a structure is realistic. \"true\" for realistic, and \"false\" otherwise." + EOL);
		sb.append("    java -jar StructureAnalyzer.jar realistic -i POSCAR ./POSCAR" + EOL);
		sb.append("    java -jar StructureAnalyzer.jar realistic -e Al -i LAMMPS ./struct.lmp" + EOL);
		sb.append(EOL);
		sb.append("Display a map of interatomic bonds. Use \"--sortZ\" to sort atoms in Z direction." + EOL);
		sb.append("    java -jar StructureAnalyzer.jar bonds [--sortZ] -i POSCAR ./POSCAR" + EOL);
		sb.append("    java -jar StructureAnalyzer.jar bonds [--sortZ] -i LAMMPS -e Al ./struct.lmp" + EOL);
		Logger.basic(sb.toString());
	}
}
