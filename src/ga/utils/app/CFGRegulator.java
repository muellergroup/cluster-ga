package ga.utils.app;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import ga.HPC;
import ga.io.Logger;
import ga.io.VASP.OSZICAR;
import ga.io.mtp.CFG;
import ga.io.mtp.CFGImage;
import ga.utils.ArgParser;
import matsci.Element;

/**
 * This is a class for picking out unrealistic ionic step from .cfg files by 
 * 		1. comparing its energy with the lowest one in the .cfg file. If the energy of one step 
 *         is higher than the threshold, remove it. Note, the images in the .cfg should belong 
 *         to same structure, in the sense that they share number of atoms and species. Otherwise, 
 *         the energy criteria wouldn't make sense.
 *      2. removing the images that have a different magnetization as the specified value.
 *      
 * Other functionalities:
 *      1. sort all images based on their energies / MV_grade, in the ascending order.
 *      2. remove similar images. It always include the last image and iterate backward to remove
 *         preceding similar CFGs.
 * 
 * User can specify the threshold, .cfg file and the output path through argument.
 * 
 * Usage:
 *     java -jar CFGRegulator.jar [-v] -t threshold /path_to_cfg /output
 *     java -jar CFGRegulator.jar [-v] cleanData [-m magnetization] -n NELM -c "RELAX"/"MD" 
 *                                     /path_to_OSZICAR /path_to_converted_cfg /output
 *     java -jar CFGRegulator.jar [-v] sort --energy /path_to_cfg /output
 *     java -jar CFGRegulator.jar [-v] sort --grade /path_to_cfg /output
 *     java -jar CFGRegulator.jar [-v] removeSimilar -e Au,S,C,H similarityThreshold /path_to_cfg /output
 *     java -jar CFGRegulator.jar [-v] groundMultiple multiplicity /path_to_cfg /output
 *     java -jar CFGRegulator.jar [-v] compileData total_number ./input.cfg ./output.cfg 
 *                                     [--cv 5] --low 0.25 --low-multiple 2 --high 0.1 --random 0.4 
 *                                     [--writeRemaining] [--exclude indices.txt]
 *     java -jar CFGRegulator.jar [-v] selectData ./indices.txt ./input.cfg ./output.cfg
 * 
 * Note:
 * 1. For command "compileData" and "selectData", indices in file indices.txt are list indices, i.e starting from 0.
 * 2. For "compileData", "--cv" represent cross-validation. The total_number of data will be divided
 *    according to --cv. Training and validation data will be written separately in files with names
 *    of "output_train_i.cfg" and "output_valid_i.cfg". If "--writeRemaining" is present, the CFGs
 *    that are not selected will be written in "output_remaining.cfg".
 * @author ywang393
 *
 */
public class CFGRegulator {

	public static void main(String[] args) {
		ArgParser parser = new ArgParser(args);
		Map<String, String> options = parser.getOptions();
		List<String> arguments = parser.getArguments();
		
		if (options.containsKey("-h")) {
			Logger.basic(getHelp());
			System.exit(0);
		}
		
		if (options.containsKey("-t")) {
			// Energy threshold
			try {
				double threshold = Double.parseDouble(options.get("-t"));
				CFG cfgs = new CFG(arguments.get(0));
				cfgs.removeHighEnergyImages(threshold);
				cfgs.writeFile(arguments.get(1));
			} catch (NumberFormatException e) {
				Logger.error("Threshold should be a float number. The input threshold is:" + args[1] + ".");
				Logger.basic(getHelp());
			} catch (ArrayIndexOutOfBoundsException e) {
				Logger.error("Wrong number of options or arguments: " + options.size() + " " + arguments.size());
				Logger.basic(getHelp());
			}
		} else if (arguments.get(0).equals("cleanData")) {
			// Remove cfg images that are unconverged, or have wrong magnetization.
			if (!options.containsKey("-n") || !options.containsKey("-c")) {
				Logger.error("Cleaning faulty images requires -n and -c!");
				System.exit(1);
			}
			try {
				int NELM = Integer.parseInt(options.get("-n"));
				String type = options.get("-c").toUpperCase();
				CFG cfgs = new CFG(arguments.get(2));
				cfgs.removeUnconvergedImages(new OSZICAR(arguments.get(1), type).getSCFSteps(), NELM);
				if (options.containsKey("-m")) {
					double magnetization = Double.parseDouble(options.get("-m"));
					ArrayList<Double> mags = extractMag(arguments.get(1));
					cfgs.removeWrongMagImages(magnetization, mags);
				}
				cfgs.writeFile(arguments.get(3));
			} catch (NumberFormatException e) {
				Logger.error("Cannot parse a number. Check -n or -m values.");
				Logger.basic(getHelp());
			} catch (ArrayIndexOutOfBoundsException e) {
				Logger.error("Wrong number of options or arguments: " + options.size() + " " + arguments.size());
				Logger.basic(getHelp());
			}
		} else if (arguments.get(0).equals("sort")) {
			if (options.containsKey("--energy")) {
				try {
					CFG cfgs = new CFG(arguments.get(1));
					cfgs.sortByEnergy();
					cfgs.writeFile(arguments.get(2));
				} catch (ArrayIndexOutOfBoundsException e) {
					Logger.error("There should be three arguments. Check yours: " + arguments.size());
					Logger.basic(getHelp());
				}
			} else if (options.containsKey("--grade")) {
				try {
					CFG cfgs = new CFG(arguments.get(1));
					cfgs.sortByMaxvolGrade();
					cfgs.writeFile(arguments.get(2));
				} catch (ArrayIndexOutOfBoundsException e) {
					Logger.error("There should be three arguments. Check yours: " + arguments.size());
					Logger.basic(getHelp());
				}
			}
		} else if (arguments.get(0).equals("removeSimilar")) {
			try {
				ArrayList<Element> elements = new ArrayList<>(5);
				if (options.get("-e").length() == 0) {
					Logger.error("Option -e requires specify element types.");
					Logger.basic(getHelp());
				}
				String[] elementSymbols = options.get("-e").split(",");
				for (String e: elementSymbols) {
					elements.add(Element.getElement(e));
				}
				
				CFG cfgs = new CFG(arguments.get(2), elements.toArray(new Element[0]));
				cfgs.removeSimilarImages(Double.parseDouble(arguments.get(1)));
				cfgs.writeFile(arguments.get(3));
			} catch (ArrayIndexOutOfBoundsException e) {
				Logger.error("There should be four arguments. Check yours: " + arguments.size());
				Logger.basic(getHelp());
			}
	    } else if (arguments.get(0).equals("groundMultiple")) {
			try {
				int multiple = Integer.parseInt(arguments.get(1));
				CFG cfgs = new CFG(arguments.get(2));
				cfgs.sortByEnergy();
				CFGImage lastImage = cfgs.getCFGImage(cfgs.getLastValidImageIndex());
				ArrayList<CFGImage> duplicates = new ArrayList<> (multiple);
				for (int i = 0; i < multiple - 1; i++) {
					duplicates.add(lastImage);
				}
				cfgs.add(duplicates);
				cfgs.writeFile(arguments.get(3));
			} catch (ArrayIndexOutOfBoundsException e) {
				Logger.error("There should be four arguments. Check yours: " + arguments.size());
				Logger.basic(getHelp());
			}
	    } else if (arguments.get(0).equalsIgnoreCase("compileData")) {
	    	if (arguments.size() < 4) {
	    		Logger.error("Command compileData needs 3 arguments: target # of data, input.cfg and output.cfg.");
	    		System.exit(1);
	    	}
	    	
	    	int total = Integer.parseInt(arguments.get(1));
	    	CFG cfgs = new CFG(arguments.get(2));
	    	
	    	//parse options
	    	double lowEnergyPercent = 0, randomPercent = 0, highEnergyPercent = 0;
	    	int lowEnergyMultiple = 1;
	    	int cvFold = 1; // Cross Validation 
	    	boolean writeRemaining = false; // Whether write the CFGs that are not selected
	    	List<Integer> excluded = new ArrayList<Integer>(100);
	    	
	    	if (options.containsKey("--cv")) {
	    	    cvFold = Integer.parseInt((options.get("--cv")));
	    	    if (cvFold < 1) {
	    	        Logger.error("Option --cv only accept integer values no less than 1.");
	    	        System.exit(1);
	    	    }
	    	}
	    	
	    	if (options.containsKey("--low")) {
	    		lowEnergyPercent = Double.parseDouble(options.get("--low"));
	    	} else {
	    		Logger.error("Missing option \"--low\"");
	    		Logger.basic(getHelp());
	    		System.exit(1);
	    	}
	    	
	    	if (options.containsKey("--low-multiple")) {
	    		lowEnergyMultiple = Integer.parseInt(options.get("--low-multiple"));
	    	}
	    	
	    	if (options.containsKey("--high")) {
	    		highEnergyPercent = Double.parseDouble(options.get("--high"));
	    	} else {
	    		Logger.error("Missing option \"--high\"");
	    		Logger.basic(getHelp());
	    		System.exit(1);
	    	}
	    	
	    	if (options.containsKey("--random")) {
	    		randomPercent = Double.parseDouble(options.get("--random"));
	    	} else {
	    		Logger.error("Missing option \"--random\"");
	    		Logger.basic(getHelp());
	    		System.exit(1);
	    	}
	    	
	    	// Note: To-be-excluded indices are in the list format (start from 0).
	    	if (options.containsKey("--exclude")) {
	    		List<String> indices = HPC.read(options.get("--exclude"));
	    		for (String index: indices) {
	    			excluded.add(Integer.parseInt(index));
	    		}
	    	}
	    	
	    	if (options.containsKey("--writeRemaining")) {
	    	    writeRemaining = true;
	    	}
	    	
	    	// Exclude these images before subsequent selection by setting include[i] to false.
	    	cfgs.removeImages(excluded);
	    	
	    	int lowEnergyCount = (int) Math.ceil(total * lowEnergyPercent * lowEnergyMultiple);
	    	int highEnergyCount = (int) Math.ceil(total * highEnergyPercent);
	    	int randomCount = total - lowEnergyCount - highEnergyCount;
	    	if (randomCount < 0) {
	    		highEnergyCount += randomCount;
	    		randomCount=0;
	    	}
	    	
	    	if (cfgs.validImageNum() < total) {
	    		Logger.error("Not enough valid images in file. # of valid images: " 
	    	                 + cfgs.validImageNum() + ". Required total: " + total);
	    		System.exit(1);
	    	}
	    	
	    	ArrayList<Integer> selectedIndices = new ArrayList<>(total);
	    	List<Integer> lowESelected = cfgs.selectLowEnergyImages(lowEnergyCount);
	    	for (int i = 0; i < lowEnergyMultiple; i++) {
	    		selectedIndices.addAll(lowESelected);
	    	}
	    	List<Integer> highESelected = cfgs.selectHighEnergyImages(highEnergyCount);
	    	selectedIndices.addAll(highESelected);
	    	List<Integer> randomSelected = cfgs.selectRandomImages(randomCount, selectedIndices);
	    	selectedIndices.addAll(randomSelected);
	    	
	    	
	    	File output = new File(arguments.get(3));
	    	if (cvFold == 1) {
	    	    Collections.sort(selectedIndices);
	    	    CFG selectedCFGs = new CFG();
	    	    selectedCFGs.add(cfgs.getCFGImages(selectedIndices));
	    	    selectedCFGs.writeFile(arguments.get(3));
	    	} else {
	    	    // cvFold > 1
	    	    if (lowEnergyCount % cvFold != 0) {
	    	        Logger.warning("Better use a --low that is divisible by --cv");
	    	    }
	    	    if (highEnergyCount % cvFold != 0) {
	    	        Logger.warning("Better use a --high that is divisible by --cv");
	    	    }
	    	    if (randomCount % cvFold != 0) {
	    	        Logger.warning("Better use a --random that is divisible by --cv");
	    	    }
	    	    Collections.shuffle(lowESelected);
	    	    Collections.shuffle(highESelected);
	    	    Collections.shuffle(randomSelected);
	    	    int lowEInterval = Math.floorDiv(lowEnergyCount, cvFold);
	    	    int highEInterval = Math.floorDiv(highEnergyCount, cvFold);
	    	    int randomInterval = Math.floorDiv(randomCount, cvFold);
	    	    String outputName = output.getName().replace(".cfg", ""); 
	    	    for (int i = 0; i < cvFold; i++) {
	    	        CFG trainCFGs = new CFG();
	    	        CFG validCFGs = new CFG();
	    	        for (int j = 0; j < lowEInterval; j++) {
	    	            validCFGs.add(cfgs.getCFGImage(lowESelected.get(j + i * lowEInterval)));
	    	        }
	    	        for (int j = lowEInterval; j < lowEnergyCount; j++) {
	    	            trainCFGs.add(cfgs.getCFGImage(lowESelected.get((j + i * lowEInterval) % lowEnergyCount)));
	    	        }
	    	        for (int j = 0; j < highEInterval; j++) {
                        validCFGs.add(cfgs.getCFGImage(highESelected.get(j + i * highEInterval)));
                    }
                    for (int j = highEInterval; j < highEnergyCount; j++) {
                        trainCFGs.add(cfgs.getCFGImage(highESelected.get((j + i * highEInterval) % highEnergyCount)));
                    }
                    for (int j = 0; j < randomInterval; j++) {
                        validCFGs.add(cfgs.getCFGImage(randomSelected.get(j + i * randomInterval)));
                    }
                    for (int j = randomInterval; j < randomCount; j++) {
                        trainCFGs.add(cfgs.getCFGImage(randomSelected.get((j + i * randomInterval) % randomCount)));
                    }
                    trainCFGs.writeFile(output.getParent() + HPC.FILESPTR + outputName 
                                      + String.format("_train_%d.cfg", i + 1));
                    validCFGs.writeFile(output.getParent() + HPC.FILESPTR + outputName 
                                      + String.format("_valid_%d.cfg", i + 1));
	    	    }
	    	}
	    	
	    	if (writeRemaining) {
	    	    CFG remainingCFGs = new CFG();
	    	    remainingCFGs.add(cfgs.getRemainingCFGImages(selectedIndices));
	    	    remainingCFGs.writeFile(output.getParent() + HPC.FILESPTR 
	    	                          + output.getName().replace(".cfg", "") + "_remaining.cfg");
	    	}
	    } else if (arguments.get(0).equalsIgnoreCase("selectData")) {
	    	if (arguments.size() < 4) {
	    		Logger.error("Command selectData needs 3 arguments: indices.txt, input.cfg and output.cfg.");
	    		System.exit(1);
	    	}
	    	
	    	List<String> indices = HPC.read(arguments.get(1));
	    	List<Integer> selectedIndices = new ArrayList<>(indices.size());
    		for (String index: indices) {
    			selectedIndices.add(Integer.parseInt(index));
    		}
	    	CFG cfgs = new CFG(arguments.get(2));
	    	CFG selectedCFGs = new CFG();
	    	selectedCFGs.add(cfgs.getCFGImages(selectedIndices));
	    	selectedCFGs.writeFile(arguments.get(3));
	    } else {
			// Also handles "-h".
			Logger.basic(getHelp());
		}
	}
	
	public static String getHelp() {
		String EOL = System.getProperty("line.separator");
		return "Usage:" + EOL + 
			   "    java -jar CFGRegulator.jar [-v] -t <energy_threshold> /path_to_cfg [output]" + EOL + 
			   "    java -jar CFGRegulator.jar [-v] cleanData [-m magnetization] -n <NELM> -c \"RELAX\"/\"MD\" \\" + EOL +
			   "                                    /path_to_OSZICAR /path_to_converted_cfg /output" + EOL +
			   "    java -jar CFGRegulator.jar [-v] sort --energy /path_to_cfg /output" + EOL +
			   "    java -jar CFGRegulator.jar [-v] sort --grade /path_to_cfg /output" + EOL +
			   "    java -jar CFGRegulator.jar [-v] removeSimilar -e Au,S,C,H similarityThreshold /path_to_cfg /output" + EOL +
			   "    java -jar CFGRegulator.jar [-v] groundMultiple multiplicity /path_to_cfg /output" + EOL +
			   "    java -jar CFGRegulator.jar [-v] compileData <total_number> ./input.cfg ./output.cfg \\" + EOL +
			   "                                    [--cv 5] --low 0.25 --low-multiple 2 --high 0.1 --random 0.4 \\" + EOL + 
			   "                                    [--writeRemaining] [--exclude indices.txt]" + EOL +
			   "    java -jar CFGRegulator.jar [-v] selectData ./indices.txt ./input.cfg ./output.cfg" + EOL + 
			   "    java -jar CFGRegulator.jar -h" + EOL + EOL +
			   "Note:" + EOL + 
			   "  1. For command compileData and selectData, indices in file indices.txt are list indices, i.e starting from 0." + EOL +
			   "  2. For \"compileData\", \"--cv\" represent cross-validation. The total_number of data will be divided" + EOL + 
			   "     according to --cv. Training and validation data will be written separately in files with names" + EOL +
			   "     of \"output_train_i.cfg\" and \"output_valid_i.cfg\". If \"--writeRemaining\" is present, the CFGs" + EOL +
			   "     that are not selected will be written in \"output_remaining.cfg\".";
	}
	
	/**
	 * Extract magnetization of each ionic step from OSZICAR.
	 * @param OSZICAR
	 * @return Array of magnetization values.
	 */
	public static ArrayList<Double> extractMag(String OSZICAR) {
		return extractMag(HPC.grep("mag=", OSZICAR));
	}
	
	public static ArrayList<Double> extractMag(String[] magLines) {
		ArrayList<Double> mags = new ArrayList<>(100);
		for (String line: magLines) {
			String[] tokens = line.split("\\s+");
			mags.add(Double.parseDouble(tokens[tokens.length - 1]));
		}
		return mags;
	}
}
