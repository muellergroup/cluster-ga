package ga.utils.app;

/**
 * An executable to compile a diverse set of training data from a collection of CFG images based
 * on energy. The aim is to diversify energies of training data to span as large as possible the
 * interpolating region. 
 * 
 * Usage: 
 * 		java -jar CFGCompiler.jar <total> <top_ratio> <multiplicity> <bottom_ratio> <random_ratio> <validation>./path_to_cfg ./output
 * 		java -jar CFGCompiler.jar --ignore-positive <total> <top_ratio> <multiplicity> <bottom_ratio> <random_ratio> <validation> ./path_to_cfg ./output
 * 		java -jar CFGCompiler.jar --remove-similar <total> <top_ratio> <multiplicity> <bottom_ratio> <random_ratio> <validation> ./path_to_cfg ./output
 * 		java -jar CFGCompiler.jar -h
 * 
 * Note:
 * 		1. The output contains three categories of data: top (low-energy), bottom (high-energy),
 * 		   and random (randomly picked from remaining data).
 * 		2. --ignore-positive will remove any inclusion of positive-energy CFG images, and fill
 * 		   the bottom category until target size.
 * 		3. --remove-similar will remove similar images in each of the three categories, and fill
 *         the set until target size.
 * @author ywang393
 *
 */
public class CFGCompiler {
	
	public static void main(String[] args) {
		
	}
	
	public static String getHelp() {
		String EOL = System.getProperty("line.separator");
		return "Usage:" + EOL + 
			   "    java -jar CFGCompiler.jar <total> <top_ratio> <multiplicity> <bottom_ratio> <random_ratio> <validation> ./path_to_cfg ./output" + EOL + 
			   "    java -jar CFGCompiler.jar --ignore-positive <total> <top_ratio> <multiplicity> <bottom_ratio> <random_ratio> <validation> ./path_to_cfg ./output" + EOL +
			   "    java -jar CFGCompiler.jar --remove-similar <total> <top_ratio> <multiplicity> <bottom_ratio> <random_ratio> <validation> ./path_to_cfg ./output" + EOL +
			   "    java -jar CFGCompiler.jar -h" + EOL + EOL +
			   "Deprecated. Use CFGRegulator.jar.";
	}
}
