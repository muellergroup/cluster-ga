package ga.utils;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import ga.io.Logger;


/**
 * Wrapper of ReentrantReadWriteLock to associate a file name with the locks.
 * Note: 
 * 		1. Because of reentrancy, write lock can require read lock, but not vice-versa.
 *      2. Each read(write) lock can acquire multiple read(write) locks.
 * @author ywang393
 *
 */
public class FileLock {
	private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private String fileName;
	
	public FileLock(String name) {
		this.fileName = name;
	}
	
	public synchronized void readLock() {
		Logger.debug("Read-lock: " + fileName);
		lock.readLock().lock();
	}
	
	public synchronized void readUnlock() {
		Logger.debug("Read-unlock: " + fileName);
		lock.readLock().unlock();
	}
	
	public synchronized void writeLock() {
		Logger.debug("Write-lock: " + fileName);
		lock.writeLock().lock();
	}
	
	public synchronized void writeUnLock() {
		Logger.debug("Write-unlock: " + fileName);
		lock.writeLock().unlock();
	}
	
	public String getFileName() {
		return fileName;
	}
}
