# General Introduction

Cluster-ga is a package aiming at predicting atomically precise structures for bare and ligated clusters with globally lowest energy. It achieves this goal by combining *ab initio* calculations with genetic algorithm. Structures are generated in three ways: random scattering, mutation from a single parent, and cross-over between a pair of parents.

There are three execution modes: "VASP", "LAMMPS" and "Active" (active learning). The first two represent different packages used in *ab initio* calculation. The third one is combining VASP and LAMMPS by active learning -- only invoke VASP calculation when necessary. Moment tensor potential (MTP) is used to parametrize the potential energy surface for LAMMPS and "Active Learning" modes.

# Installation

1. Download the source code from the repository. 

    Over HTTPS: 
    ```
    git clone https://gitlab.com/muellergroup/cluster-ga.git
    ```
	Or over SSH:
	```
	git clone git@gitlab.com:/muellergroup/cluster_ga.git
	```

2. Compile source code into runnable jar.
	```
	./install.sh
	```

# How to Run

## Command:

	java -jar ./cluster.jar &> stdout

Calculations must be run in separate directories, which is referred to as `root` from now on. `Root` must contains the configuration file `INGA`. All settings are specified in this file (`INGA`). 

## Input Files
### Common types of calculation

Depending on the type of calculation user wants to run, there are other necessary files:

* **VASP**: `INCAR`, `KPOINTS`,`POTCAR`
* **LAMMPS**: `mlip.mtp`, `input.lmp`, `mlip.ini`
* **Active Learning**: `INCAR`, `KPOINTS`,`POTCAR`, `mlip.mtp`, `input.lmp`, `mlip.ini`, `outcar.cfg`, `INCAR_MD` (Optional), `INCAR_static` (Optional)

where `mlip.mtp` is a MTP potential file; `input.lmp` is the configuration file for LAMMPS executable `lmp_serial` or `lmp_mpi`; `mlip.ini` is the potential set-up file for LAMMPS to recognize MTP potential (user normally dones't need to change the content of this file); `outcar.cfg` contains the training data set of `mlip.mtp`.

### Special types of calculations

* **Ligated Cluster**:
	* `LIGAND`: Structure of the ligand in the format of POSCAR.
	* `clusterFiles`: Folders of POSCARS named by numerical indeices. An example of the file structure would be:
		```
		clusterFiles
		|__ 1
		|   |__ POSCAR
		|__ 2
		|   |__ POSCAR
		|...
		```
	* `CLUSTER`: Structure of possible cluster in the format of POSCAR. It's used to read the number of metal atoms and the species of the metal core. (TODO: Same information can be read from INGA, we should remove this requirement.)


* **seed** operation: The third type of GA operations.
	* `seed.dat`: Structures of clusters used as seeds. If a seed has less number of atoms, atoms are randomly added to the seed until reached the target atom count. If a seed has more number of atoms, atoms are randomly removed from the seed until the target atom count. The subtraction operation could create voids in the generated cluster. Only bare clusters are allowed for seeds. For ligated cluster calculation, bare cluster seeds would be enough.
		
		More than one atoms can be added or subtracted from seeds. Seeds with different number of atoms can be used in seed.dat.

		The format of structure is the .xyz format, which can be obtained readily from the Quantum Cluster Database. The template would be:
		```
		<Num_Of_Atom>
		<Comment> #Must contain string "Energy" for partition purpose.
		<Species_type> <x_coord> <y_coord> <z_coord>
		<Species_type> <x_coord> <y_coord> <z_coord>
		...
		```

### Additional Notes
* When user runs **Active Learning** mode with an empty potential, he still needs to provide an empty `outcar.cfg`.
* When user sets the parameter `RUNMD=TRUE` in `INGA`, `INCAR_MD` must be also be provided in `root`.
* When user sets the parameter `USESTATICINCAR=TRUE` for ligated calculation, `INCAR_static` must be provided in `root`.

## Output

Output from `cluster-ga.jar` are written directly to standard output, except that it's redirected to another file.

Evaluation and analysis of clusters are written in a sub-folder of `root`. The path of folder is composed of element name and number of atoms. For example, for Al clusters with 21 atoms, the folder is `./Al/21`. This folder is referred to as `workDir` in all documents. The following path are relative to this `workDir`.

### .dat Files
Analysis results of GA run are written to separate `.dat` files. The most important ones are:

* `candidates.dat`: structural information of all candidate clusters, like size of cluster, atomic positions and relaxation energy.
* `pool.dat`: structural information of clusters in the pool, which are unique to each other and have lowest energies among all candidates.

Other useful but not essential `.dat` files:

* `score_table.dat`: a lower triangular matrix recording **similarity score** between a pair of clusters. Each line begins with the candidate index of the cluster this line represent. The (i, j) term of the matrix denoting the similarity score between i-th and (j - 1)-th clusters. For example:
	```
	1
	2 0.74
	3 0.75 0.07
	```
	The (3,2) term "0.75" represent the similarity score between cluster-3 and cluster-1.
* `similarity_table.dat`: a lower triangular matrix of "true" or "false", whose entries indicate whether a pair of clusters are similar or not. The boolean values are got by comparing values in `score_table.dat` with `SIMILARITYTHRESHOLD` of `INGA`. 

	For example, if `SIMILARITYTHRESHOLD=0.72`, then the example in `socre_table.dat` would have a corresponding file as
	```
	1
	2 false
	3 false true
	```

* `chosen.dat`: parent of clusters that are generated by cross-over operation.
* `select.dat`: "selectability" and "fitness" of the current pool clusters and dS of the pool. "dS" is used to decide whether GA has converged.
* `exitcode.dat`: exit codes of relaxation calculations of each candidates.
* `exploded.dat`: if a cluster is still exploaded (having atoms flying away from other atoms), the relaxed structure is written to this file. For "VASP" and "LAMMPS" mode, redoing the relaxation won't have better results, so a new structure is generated and relaxed again. For "Active Learsning" mode, this structrue is added to the training pool.
* `maxvolGrades.dat`: Only for **Active Learning** mode. It contains extrapolation grades (Maxvol Grades) and a boolean value indicating wehther it's extrapolating, for each relaxed structrue. If relaxation hasn't been finished, that of initial structures are recorded.

### Folders
* `all_candidates`: All relaxation calculations of clusters. Subfolder names are candidate indices. Users can perform more detailed / tailored analysis of the candidate clusters if they want. 
* `train`: Only for **Active Learning** mode. Folder name represent the sequence that the potential has been retrained on. For example, 
	* `train/0` stores the initial potential `0/mlip.mtp` and training data `0/outcar.cfg`. 
	* `train/2` contains VASP evaluation (and MD) of clusters in retraining pool for the second retraining of the potential. MTP training results are written in `2/stdout`.

# How to cite
The paper has been submitted and currently under review. A preprint version can be found on ChemRxiv at https://doi.org/10.33774/chemrxiv-2021-q3wng.

> *Wang Y, Liu S, Lile P, Norwood S, Hernandez A, Manna S et al. Accelerated Prediction of Atomically Precise Cluster Structures Using On-the-fly Machine Learning. ChemRxiv 2021.*

# Acknowledgement
* For the genetic algorithm, we refered to [this work](https://doi.org/10.1039/B305686D) by Roy L. Johnston from the University of Birmingham.
* For the similarity measurment between clusters, we used the algorithm formulated in [this paper](https://doi.org/10.1063/1.4981212) by Li, Yang and Zhao.
* For the Hungarian algorithm used in similarity calculation, we used the implementation by Kevin L. Stern at [here](https://github.com/KevinStern/software-and-algorithms/blob/master/src/main/java/blogspot/software_and_algorithms/stern_library/optimization/HungarianAlgorithm.java).
